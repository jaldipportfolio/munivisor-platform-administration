import React from "react"
import DefaultLayout from "./app/FunctionalComponents/containers/DefaultLayout"

const Dashboard = React.lazy(() => import("./app/FunctionalComponents/SideBarComponents/Dashboard"))
const Profile = React.lazy(() => import("./app/FunctionalComponents/SettingsComponents/Profile"))
const Configuration = React.lazy(() => import("./app/FunctionalComponents/SettingsComponents/Configuration"))
const Settings = React.lazy(() => import("./app/FunctionalComponents/SettingsComponents/Settings"))
const Tenant = React.lazy(() => import("./app/FunctionalComponents/SideBarComponents/Tenant"))
const ClientProspect = React.lazy(() => import("./app/FunctionalComponents/SideBarComponents/Entity/Client-Prospect"))
const ThirdParty = React.lazy(() => import("./app/FunctionalComponents/SideBarComponents/Entity/Third-Party"))
const User = React.lazy(() => import("./app/FunctionalComponents/SideBarComponents/User"))
const Payment = React.lazy(() => import("./app/FunctionalComponents/SideBarComponents/Payment"))
const Invoices = React.lazy(() => import("./app/FunctionalComponents/SideBarComponents/Invoices"))
const InvoicePreview = React.lazy(() => import("./app/FunctionalComponents/SideBarComponents/Invoices/Preview"))
const DefaultBillingSettings = React.lazy(() => import("./app/FunctionalComponents/SettingsComponents/DefaultBillingSettings"))
const TenantMainView = React.lazy(() => import("./app/FunctionalComponents/SideBarComponents/SelectedTenantDetails"))
const Events = React.lazy(() => import("./app/FunctionalComponents/SideBarComponents/Payment/Events"))
const Applications = React.lazy(() => import("./app/FunctionalComponents/SettingsComponents/Applications"))
const Pricing = React.lazy(() => import("./app/FunctionalComponents/SideBarComponents/Pricing"))
const CreatePicklist = React.lazy(() => import("./app/FunctionalComponents/SideBarComponents/CreatePicklist/CreatePicklist"))

export const routes = [
  { path: "/", name: "Home", component: DefaultLayout, exact: true },
  { path: "/dashboard", name: "Dashboard", component: Dashboard },
  { path: "/profile", name: "Profile", component: Profile },
  { path: "/configuration", name: "Configuration", component: Configuration },
  { path: "/settings", name: "Settings", component: Settings },
  // { path: "/new", name: "New", component: ClientProspect, exact: true },
  { path: "/new/tenant", name: "Tenant", component: Tenant },
  { path: "/new/entities", name: "Entities", component: ClientProspect, exact: true },
  { path: "/new/entities/client", name: "Client", component: ClientProspect },
  { path: "/new/entities/prospect", name: "Prospect", component: ClientProspect },
  { path: "/new/entities/thirdParty", name: "ThirdParty", component: ThirdParty },
  { path: "/new/user", name: "User", component: User },
  { path: "/payment", name: "Payment", component: Payment },
  { path: "/billingsettings", name: "Default Billing Settings", component: DefaultBillingSettings },
  { path: "/invoices", name: "Invoices", component: Invoices },
  { path: "/invoicepreview", name: "Invoice Preview", component: InvoicePreview },
  { path: "/tenant", name: "Tenant", component: TenantMainView },
  { path: "/events", name: "Events", component: Events },
  { path: "/pricing", name: "Events", component: Pricing },
  { path: "/applications", name: "Applications", component: Applications },
  { path: "/create-picklist", name: "Create Picklist", component: CreatePicklist },
]

export default routes
