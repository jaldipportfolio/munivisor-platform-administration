import React, { Component } from "react"
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom"
import Loadable from "react-loadable"
import "./App.scss"
import Provider from "react-redux/es/components/Provider"
import {muniVisorApplicationStore} from "./app/Services/store"
import {getToken} from "./app/globalutilities/consts"
import Loader from "./app/GlobalComponents/Loader"
import "ladda/dist/ladda-themeless.min.css"

// Pages
const SignIn = Loadable({
  loader: () => import("./app/LoginAuthentication/SignIn"),
  loading: () => <Loader/>
})

const SignUp = Loadable({
  loader: () => import("./app/LoginAuthentication/SignUp"),
  loading: () => <Loader/>
})

// Containers
const DefaultLayout = Loadable({
  loader: () => import("./app/FunctionalComponents/containers/DefaultLayout"),
  loading: () => <Loader/>
})

const PrivateRoute = ({ component: Component }) => (
  <Route render={(props) =>
    getToken() ? <Component {...props} /> : <Redirect to="/signin" />
  }/>
)

const RedirectRoute = ({ component: Component }) => (
  <Route render={(props) =>
    getToken() ? <Redirect to='/dashboard' /> : <Component {...props} />
  }/>
)

class App extends Component {
  render() {
    return (
      <Provider store={muniVisorApplicationStore}>
        <BrowserRouter>
          <Switch>
            <RedirectRoute exact path="/signin" name="SignIn Page" component={SignIn} />
            <RedirectRoute exact path="/signup" name="SignUp Page" component={SignUp} />
            <PrivateRoute path="/" name="Home" component={DefaultLayout} />
          </Switch>
        </BrowserRouter>
      </Provider>
    )
  }
}

export default App
