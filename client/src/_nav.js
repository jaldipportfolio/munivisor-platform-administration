export default {
  items: [
    {
      name: "Dashboard",
      url: "/dashboard",
      icon: "icon-speedometer",
    },
    {
      title: true,
      name: "Create New",
    },
    // {
    //     name: 'Entity',
    //     url: '/new/entities',
    //     icon: 'fa fa-handshake-o',
    //     children: [
    //         {
    //           name: 'Client',
    //           url: '/new/entities/client',
    //           icon: 'fa fa-plus',
    //         },
    //         {
    //           name: 'Prospect',
    //           url: '/new/entities/prospect',
    //           // icon: 'fa fa-plus-circle'
    //           icon: 'fa fa-plus',
    //         },
    //         {
    //           name: '3rd Party',
    //           url: '/new/entities/thirdParty',
    //           // icon: 'fa fa-plus-square'
    //           icon: 'fa fa-plus',
    //         }
    //     ]
    // },
    {
      name: "Tenant",
      url: "/new/tenant",
      icon: "fa fa-plus"
    },
    {
      name: "User",
      url: "/new/user",
      icon: "icon-user-follow"
    },
    {
      name: "Invoices",
      url: "/invoices",
      icon: "fa fa-list fa-lg"
    },
    /* {
      name: "Payment",
      url: "/payment",
      icon: "fa fa-cc-stripe"
    }, */
    {
      name: "Invoice Events",
      url: "/events",
      icon: "fa fa-list fa-lg"
    },
    {
      name: "Pricing",
      url: "/pricing",
      icon: "fa fa-dollar fa-lg"
    },
    {
        name: "Create Picklist",
        url: "/create-picklist",
        icon: "fa fa-list fa-lg"
    }
  ]
}
