import axios from "axios"
import {toast} from "react-toastify"
import _ from "lodash"
import { muniApiBaseURL, getToken } from "./consts"

export const errorToast = (msg) =>  toast.error(msg, { position: toast.POSITION.TOP_RIGHT})
export const successToast = (msg) =>  toast.success(msg, { position: toast.POSITION.TOP_RIGHT})

export const regexTestsForEmail = (matchString) => {
  matchString = matchString.toString().toLowerCase().trim()
  matchString.replace(/(\r\n|\n|\r|\s|\t)/gm, "")
  console.log(`${matchString}----`)
  const regexString = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ // eslint-disable-line
  const regex = new RegExp(regexString)
  return regex.test(matchString)
}

export const highlightSearch = (string, searchQuery) => {
  const reg = new RegExp(
    searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&"),
    "i"
  )
  return string.replace(reg, str => `<mark>${str}</mark>`)
}

export const getPicklistValues = async picklistName => {
  const res = await axios.get(`${muniApiBaseURL}common/picklists`, {
    headers: { Authorization: getToken() },
    params: { require: "picklists", names: picklistName.toString() }
  })

  let picklists
  if (res && res.data) {
    picklists = res.data
  } else {
    console.log("err in getting picklists")
  }

  const populateResult = picklistName => {
    let levels = 1
    let primaryValues = []
    const secondaryValues = {}
    const tertiaryValues = {}
    const picklist =
            picklists.filter(p => p.meta && p.meta.systemName === picklistName)[0] ||
            {}
    const isNum = (picklist.items || []).map(e => Number(e.label)).includes(NaN)

    primaryValues = isNum ? (picklist.items || []).map(e => e.label).sort() :
      (picklist.items || []).map(e => e.label).sort((a,b) =>{return a - b})

    if (picklist.meta) {
      if (picklist.meta.subListLevel2) {
        levels = 2
      } else if (picklist.meta.subListLevel3) {
        levels = 3
      }
      if (picklist.meta.subListLevel2) {
        picklist.items.forEach(e => {
          secondaryValues[e.label] = e.items
            .map(f => f.label)
            .filter(d => d !== "Add a label")
            .sort()
          if (picklist.meta.subListLevel3) {
            tertiaryValues[e.label] = {}
            e.items.forEach(f => {
              tertiaryValues[e.label][f.label] = f.items
                .map(g => g.label)
                .filter(d => d !== "Add a label")
                .sort()
            })
          }
        })
      }
    }
    return [levels, primaryValues, secondaryValues, tertiaryValues]
  }
  // console.log("picklists : ", picklists)
  if (picklists && picklists.length) {
    if (Array.isArray(picklistName) && picklistName.length) {
      return picklistName.map(d => populateResult(d))
    }
    console.log(
      "wrong type of or empty arugments or old format argument passed to getPicklistValues"
    )
    return populateResult(picklistName)
  }
  return picklists
}

export const checkEmptyElObject = obj => {
  let isEmpty = true
  const isEmptyFunc = obj => {
    Object.keys(obj).forEach(item => {
      if (_.isObject(obj[item])) {
        isEmptyFunc(obj[item])
      } else if (obj[item] !== "" && !_.isBoolean(obj[item])) isEmpty = false
    })
    return isEmpty
  }
  return isEmptyFunc(obj)
}

export const dateFormatted = (val) => {
  let date = null
  if(val){
    const mm = val.getMonth() + 1
    const dd = val.getDate()
    date =  [
      (mm>9 ? "" : "0") + mm,
      (dd>9 ? "" : "0") + dd,
      val.getFullYear()
    ].join("-")
    date = `${date}`
  }
  return date
}

