
console.log("PLATFORM_API_URL :-----------", process.env.PLATFORM_API_URL)
export const muniApiBaseURL = process.env.PLATFORM_NODE_ENV === "production" || process.env.NODE_ENV === "production"
  ? "/api/"
  : "http://localhost:8000/api/"
export const authBaseURL = process.env.PLATFORM_NODE_ENV === "production" || process.env.NODE_ENV === "production"
  ? "/auth/"
  : "http://localhost:8000/auth/"

export const getToken = () => localStorage.getItem("token") || ""

export const RFP_ASSIGNED_TO = ["Evaluation Team", "RFP Contacts", "RFP Assignees"]

export const PLATFORM_STRIPE_CLIENT_PUBLIC_KEY= "pk_test_7L7OC6QD9MKlgDpZhCy6dJM800sZ8BD4Ku"

export const toastTimeout = 3000

export const toastContainerStyle = {
  zIndex: 1999
}

export const regExp = {
  email: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
}

export const CONST = {
  confirmAlert: {
    title: "Are you sure?",
    text: "You want to delete this ?",
    buttons: true,
    dangerMode: true
  },
}

export const searchOptions = {
  shouldSort: true,
  threshold: 0.3,
  location: 0,
  distance: 100,
  maxPatternLength: 32,
  minMatchCharLength: 1,
  keys: [
    {
      name: "firmType",
      weight: 0.8
    },
    {
      name: "firmName",
      weight: 0.9,
    },
    {
      name: "state",
      weight: 0.89,
    },
    {
      name: "city",
      weight: 0.98
    },
    {
      name: "addressName",
      weight: 0.86
    },
    {
      name: "primaryContact",
      weight: 0.6,
    }
  ]
}