import React, { Component } from "react"
import Script from "react-load-script"
import {Col, FormGroup, Input, Label, Row} from "reactstrap"


class SearchAddress extends Component {
  // Define Constructor
  constructor(props) {
    super(props)

    // Declare State
    this.state = {
      city: "",
      query: "",
      addressName:""
    }
    // Bind Functions
    this.handleScriptLoad = this.handleScriptLoad.bind(this)
    this.handlePlaceSelect = this.handlePlaceSelect.bind(this)
    this.autocomplete = null
  }

  handleScriptLoad(event) {
    const options = {
      types: ["address"],
      // componentRestrictions: {country: "us"}
    } // To disable any eslint 'google not defined' errors

    // Initialize Google Autocomplete
    /* global google */ this.autocomplete = new google.maps.places.Autocomplete(
      document.getElementById(`${this.props.idx}-autocomplete`),
      options
    )

    this.autocomplete.addListener("place_changed", this.handlePlaceSelect)
  }

  handlePlaceSelect(e) {
    const addressObject = this.autocomplete.getPlace()
    const address = addressObject.address_components
    // Check if address is valid
    if (address) {
      this.setState({
        city: address[0].long_name,
        query: addressObject.formatted_address,
        address: JSON.stringify(address, null, 2),
        addressObject: JSON.stringify(addressObject, null, 2),
        addressName:addressObject.formatted_address
      })
      const obj = {
        addressLine1 : "",
        city:"",
        state:"",
        country:"",
        zipcode:"",
        formatted_address:"",
        url:"",
        location:{
          longitude:"",
          latitude:""
        }
      }
      address.forEach(item=>{
        switch (item.types[0]) {
        case "street_number":
          obj.addressLine1 = item.long_name
          break
        case "locality":
          obj.city = item.long_name
          break
        case "neighborhood":
          obj.city = item.long_name
          break
        case "administrative_area_level_1":
          obj.state = item.long_name
          break
        case "country":
          obj.country = item.long_name
          break
        case "route":
          obj.addressLine1 = `${obj.addressLine1} ${item.long_name}`
          break
        case "postal_code":
          obj.zipcode = item.long_name
          break
        default:
          break
        }
      })
      obj.formatted_address = addressObject.formatted_address
      obj.url = addressObject.url
      obj.location.latitude= addressObject.geometry.location.lat().toFixed(2)
      obj.location.longitude = addressObject.geometry.location.lng().toFixed(2)
      this.props.getAddressDetails(obj,this.props.idx)
      this.setState({
        addressName:""
      })
    }
  }
  changeState = (e)=>{
    e.preventDefault()
    this.setState({
      [e.target.name]:e.target.value
    })
    if(e.target.value===""){
      this.props.getAddressDetails()
    }
  }
  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" sm="12">
            <FormGroup>
              <Script
                url="https://maps.googleapis.com/maps/api/js?key=AIzaSyAGhakxmSkazXbWVFf5hPSKfJXg_Ds8ii0&libraries=places"
                onLoad={this.handleScriptLoad}
              />
              <Label><b>Search Address</b></Label>
              <Input
                className="form-control"
                id={`${this.props.idx}-autocomplete`}
                placeholder="Search Address"
                type="text"
                name = "addressName"
                value={this.state.addressName}
                onChange={this.changeState}
              />
            </FormGroup>
          </Col>
        </Row>
      </div>
    )
  }
}

export default SearchAddress
