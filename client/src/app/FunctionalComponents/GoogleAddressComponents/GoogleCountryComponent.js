import React, { Component } from "react"
import Script from "react-load-script"
import {Col, FormGroup, Input, Label, Row} from "reactstrap"

class SearchCountry extends Component {
  constructor(props) {
    super(props)
    this.state = {
      countryState: ""
    }
    this.autocomplete = null
  }

  componentWillMount() {
    this.setState({
      countryState: this.props.value || ""
    })
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.value !== nextProps.value){
      this.setState({
        countryState: nextProps.value || ""
      })
    }
  }

  handleScriptLoad = (event) => {
    const options = {
      types: ["geocode"],
      // componentRestrictions: {country: "us"}
    } // To disable any eslint 'google not defined' errors

    // Initialize Google Autocomplete
    /* global google */ this.autocomplete = new google.maps.places.Autocomplete(
      document.getElementById(`${this.props.idx}-autocomplete-country`),
      options
    )

    // Fire Event when a suggested name is selected
    this.autocomplete.addListener("place_changed", this.handlePlaceSelect)
  }

  handlePlaceSelect = () => {
    const addressObject = this.autocomplete.getPlace()
    const address = addressObject.address_components
    // Check if address is valid
    if (address) {
      const obj = {
        city:"",
        state:"",
        country:"",
      }
      address.forEach(item=>{
        switch (item.types[0]) {
        case "locality":
          obj.city = item.long_name
          break
        case "neighborhood":
          obj.city = item.long_name
          break
        case "administrative_area_level_1":
          obj.state = item.long_name
          break
        case "administrative_area_level_2":
          obj.county = item.long_name
          break
        case "country":
          obj.country = item.long_name
          break
        case "postal_code":
          obj.zipcode = item.long_name
          break
        default:
          break
        }
      })
      obj.formatted_address = addressObject.formatted_address
      obj.url = addressObject.url
      this.props.getCountryDetails(obj,this.props.idx)
    }
  }

  changeState = (e)=>{
    e.preventDefault()
    this.setState({
      [e.target.name]:e.target.value
    })
    if(e.target.value===""){
      this.props.getCountryDetails()
    }
  }

  render() {
    const {label, idx, disabled, error, required, dataMigrationview} = this.props
    if(dataMigrationview){
      return  (
        <div>
          <Script
            url="https://maps.googleapis.com/maps/api/js?key=AIzaSyAGhakxmSkazXbWVFf5hPSKfJXg_Ds8ii0&libraries=places"
            onLoad={this.handleScriptLoad}
          />
          <div className="control">
            <input
              // className="input is-small is-link"
              id={`${idx}-autocomplete-country`}
              placeholder={label || "Search City, State OR Country"}
              type="text"
              name = "countryState"
              disabled={disabled}
              value={this.state.countryState}
              onChange={this.changeState}
            />
            {error && (
              <small className="text-error">
                {error || ""}
              </small>
            )}
          </div>
        </div>
      )
    }

    return (
      <div>
        <Row>
          <Col xs="12" sm="12">
            <FormGroup>
              <Script
                url="https://maps.googleapis.com/maps/api/js?key=AIzaSyAGhakxmSkazXbWVFf5hPSKfJXg_Ds8ii0&libraries=places"
                onLoad={this.handleScriptLoad}
              />
              <Label><b>{label || "Search City, State OR Country"}{ required ? <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span> : null }</b></Label>
              <Input
                // className="input is-small is-link"
                id={`${idx}-autocomplete-country`}
                placeholder={label || "Search City, State OR Country"}
                type="text"
                name = "countryState"
                disabled={disabled}
                value={this.state.countryState}
                onChange={this.changeState}
              />
              {error && (
                <small className="text-danger">
                  {error || ""}
                </small>
              )}
            </FormGroup>
          </Col>
        </Row>
      </div>
    )
  }
}

export default SearchCountry
