import React, { Component, Suspense } from "react"
import {ToastContainer} from "react-toastify"
import {connect} from "react-redux"
import {Redirect, Route, Switch, withRouter} from "react-router-dom"
import { Container } from "reactstrap"
import {
  AppAside,
  AppBreadcrumb,
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppSidebarNav,
} from "@coreui/react"
import {routes} from "../../../../routes"
import {toastTimeout, toastContainerStyle, getToken} from "../../../globalutilities/consts"
import {checkAuth} from "../../../Services/actions"
import "react-toastify/dist/ReactToastify.css"
import Loader from "../../../GlobalComponents/Loader"
import navigation from "../../../../_nav"

const DefaultAside = React.lazy(() => import("./DefaultAside"))
const DefaultFooter = React.lazy(() => import("./DefaultFooter"))
const DefaultHeader = React.lazy(() => import("./DefaultHeader"))

class DefaultLayout extends Component {

  componentDidMount(){
    if(getToken()){
      this.props.checkAuth(getToken())
    }
  }

  loading = () => <Loader/>

  render() {
    return (
      <div className="app">
        <ToastContainer position="top-right" autoClose={toastTimeout} style={toastContainerStyle}/>
        <div className="app">
          <AppHeader fixed>
            <Suspense fallback={this.loading()}>
              <DefaultHeader />
            </Suspense>
          </AppHeader>
          <div className="app-body">
            <AppSidebar fixed display="lg">
              <AppSidebarHeader />
              <AppSidebarForm />
              <Suspense>
                <AppSidebarNav navConfig={navigation} {...this.props} />
              </Suspense>
              <AppSidebarFooter />
              <AppSidebarMinimizer />
            </AppSidebar>
            <main className="main">
              <AppBreadcrumb appRoutes={routes}/>
              <Container fluid>
                <Suspense fallback={this.loading()}>
                  <Switch>
                    {routes.map((route, idx) => {
                      return route.component ? (
                        <Route
                          key={idx.toString()}
                          path={route.path}
                          exact={route.exact}
                          name={route.name}
                          render={props => (
                            <route.component {...props} />
                          )} />
                      ) : (null)
                    })}
                    <Redirect from="/" to="/dashboard" />
                  </Switch>
                </Suspense>
              </Container>
            </main>
            <AppAside fixed>
              <Suspense fallback={this.loading()}>
                <DefaultAside />
              </Suspense>
            </AppAside>
          </div>
          <AppFooter>
            <Suspense fallback={this.loading()}>
              <DefaultFooter />
            </Suspense>
          </AppFooter>
        </div>
      </div>
    )
  }
}

export default connect(({ auth }) => ({ auth }),{ checkAuth })(withRouter(DefaultLayout))
