import React, { Component } from "react"
import { connect } from "react-redux"
import { DropdownItem, DropdownMenu, DropdownToggle, Nav } from "reactstrap"
import { AppHeaderDropdown, AppNavbarBrand, AppSidebarToggler } from "@coreui/react"
import { Link } from "react-router-dom"

import munilogo from "../../assets/img/brand/munilogo.jpg"
import logos from "../../assets/img/brand/logos.png"
import userLogo from "../../assets/img/brand/user-avatar.jpg"
import { signOut } from "../../../Services/actions"

class DefaultHeader extends Component {

    logOut = () => {
      // console.log("signing out")
      this.props.signOut()
    }

    render() {

      return (
        <React.Fragment>
          <AppSidebarToggler className="d-lg-none" display="md" mobile />
          <AppNavbarBrand
            full={{ src: munilogo, width: 89, height: 25, alt: "CoreUI Logo" }}
            minimized={{ src: logos, width: 30, height: 30, alt: "CoreUI Logo" }}
          />
          <AppSidebarToggler className="d-md-down-none" display="lg" />
          <Nav className="ml-auto" navbar>
            <AppHeaderDropdown>
              <DropdownToggle nav>
                <img src={userLogo} className="img-avatar" alt="admin@bootstrapmaster.com" />
              </DropdownToggle>
              <DropdownMenu right style={{ right: "auto" }}>
                {/* <DropdownItem header tag="div" className="text-center"><strong>Account</strong></DropdownItem> */}
                {/* <DropdownItem><i className="fa fa-bell-o" /> Updates<Badge color="info">42</Badge></DropdownItem> */}
                {/* <DropdownItem><i className="fa fa-envelope-o"/> Messages<Badge color="success">42</Badge></DropdownItem> */}
                {/* <DropdownItem><i className="fa fa-tasks"/> Tasks<Badge color="danger">42</Badge></DropdownItem> */}
                {/* <DropdownItem><i className="fa fa-comments"/> Comments<Badge color="warning">42</Badge></DropdownItem> */}
                <DropdownItem header tag="div" className="text-center"><strong>Settings</strong></DropdownItem>
                <Link to="/profile"><DropdownItem><i className="fa fa-user"/>Profile</DropdownItem></Link>
                <Link to="/applications"><DropdownItem><i className="fa fa-plus" />Applications</DropdownItem></Link>
                <Link to="/configuration"><DropdownItem><i className="fa fa-cogs" />Configuration</DropdownItem></Link>
                <Link to="/billingsettings"><DropdownItem><i className="fa fa-cogs" />Billing</DropdownItem></Link>
                {/* <Link to="/settings"><DropdownItem><i className="fa fa-wrench"/> Settings</DropdownItem></Link> */}
                {/* <DropdownItem><i className="fa fa-usd"/> Payments<Badge color="secondary">42</Badge></DropdownItem> */}
                {/* <DropdownItem><i className="fa fa-file"/> Projects<Badge color="primary">42</Badge></DropdownItem> */}
                <DropdownItem divider />
                {/* <DropdownItem><i className="fa fa-shield"/> Lock Account</DropdownItem> */}
                <DropdownItem onClick={this.logOut}><i className="fa fa-lock" /> Logout</DropdownItem>
              </DropdownMenu>
            </AppHeaderDropdown>
          </Nav>
          {/* <AppAsideToggler className="d-md-down-none" /> */}
          {/* <AppAsideToggler className="d-lg-none" mobile /> */}
        </React.Fragment>
      )
    }
}

const mapStateToProps = ({
  auth: { token, authenticated, loginDetails, exp },
}) => ({
  token,
  authenticated,
  loginDetails,
  exp
})

export default connect(mapStateToProps,{ signOut })(DefaultHeader)
