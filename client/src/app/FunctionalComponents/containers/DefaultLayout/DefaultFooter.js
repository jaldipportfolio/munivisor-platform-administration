import React, { Component } from "react"
import {withRouter } from "react-router-dom"
import {toast} from "react-toastify"
import {connect} from "react-redux"
// eslint-disable-next-line import/named
import { updateUserDetails } from "../../../Services/actions/User"

class DefaultFooter extends Component {

  constructor(props) {
    super(props)
    this.state = {
      byDefaultProject: ""
    }
  }

  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    }, async () => {
      await this.setDefaultProject()
    })
  }

  setDefaultProject = async () =>{
    const {byDefaultProject} = this.state
    const {loginDetails} = this.props.auth || {}
    const res = await updateUserDetails(loginDetails.userId || "", {byDefaultProject})
    if(res && res.status === 200){
      window.location.pathname  = "/dashboard"
    }else {
      toast.error("Something went wrong!", {
        position: toast.POSITION.TOP_RIGHT
      })
    }
  }

  render() {
    const {byDefaultProject} = this.state
    const {loginDetails} = this.props.auth || {}
    const userAuth = (loginDetails && loginDetails.applications) || []
    return (
      <React.Fragment>
        <span className="ml-auto">
            Otaras application &nbsp;
          {
            <select name="byDefaultProject" value={byDefaultProject || (loginDetails && loginDetails.byDefaultProject)} onChange={this.onChange}>
              {
                userAuth.map((item, i) => {
                  return (
                    <option key={i.toString()} value={item.name}>{item.name}</option>
                  )
                })
              }
            </select>
          }
        </span>
      </React.Fragment>
    )
  }
}

export default connect(({ auth }) => ({ auth }),null)(withRouter(DefaultFooter))