import React, { Component } from "react"
import {toast} from "react-toastify"
import {connect} from "react-redux"
import {withRouter} from "react-router-dom"
import { Col, Input, Label, Row, Container } from "reactstrap"
import LaddaButton, {ZOOM_IN} from "react-ladda"
import Accordion from "../../../GlobalComponents/Accordion"
import { getPlatFormSetting, updatePlatFormSettings } from "../../../Services/actions/Setting"
import {checkAuth} from "../../../Services/actions/LoginAuthentication"

class Configuration extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      self: false,
      client: false,
      thirdParty: false,
      auditFlag: "",
      setting: {}
    }
  }

  async componentWillMount(){
    const res = await getPlatFormSetting()
    if(res){
      const notifications = res.notifications
      this.setState({
        setting: res,
        self: (notifications && notifications.self) || false,
        client: (notifications && notifications.client) || false,
        thirdParty: (notifications && notifications.thirdParty) || false,
        auditFlag: (res && res.auditTrail && res.auditTrail.auditFlag) || ""
      })
    }
  }

  handleChange = (event) => {
    const {target} = event
    const value = target.type === "checkbox" ? target.checked : target.value
    this.setState({
      [target.name]: value,
    })
  }

    handleSubmit = async () => {
      const {self, client, thirdParty, auditFlag, setting} = this.state

      const postData = {
        notifications: {
          self,
          client,
          thirdParty,
        },
        auditTrail: {
          auditFlag
        }
      }
      this.setState({
        loading: true,
      }, async () => {
        const res = await updatePlatFormSettings(postData, (setting && setting._id))
        if(res && res.status === 200){
          toast.success("Configuration Details Save successfully.", {
            position: toast.POSITION.TOP_RIGHT
          })
        }else {
          toast.error("Something went wrong!", {
            position: toast.POSITION.TOP_RIGHT
          })
        }
        this.setState({
          loading: false,
        })
      })
    }

    render() {
      const {loading, self, client, thirdParty, auditFlag} = this.state
      return (
        <div className="animated fadeIn">
          <Row>
            <Col xs="12" md="12">
              <Accordion
                activeAccordionHeader="Audit Trail">
                <Container>
                  <Row>
                    <Col xs="6" sm="4">
                      <Label check>
                        <Input
                          type="radio"
                          name="auditFlag"
                          value="no"
                          checked={auditFlag === "no"}
                          onChange={this.handleChange}/>
                                  No Activity log will be maintained by MuniVisor.
                      </Label>
                    </Col>
                    <Col xs="6" sm="4">
                      <Label check>
                        <Input
                          type="radio"
                          name="auditFlag"
                          value="supervisor"
                          checked={auditFlag === "supervisor"}
                          onChange={this.handleChange}/>
                                  Activity log will be only visible to Compliance Officer / Supervisory Principals.
                      </Label>

                    </Col>
                    <Col sm="4">
                      <Label check>
                        <Input
                          type="radio"
                          name="auditFlag"
                          value="currentState"
                          checked={auditFlag === "currentState"}
                          onChange={this.handleChange}/>
                                  Activity log will be visible to all eligible users of the firm.
                      </Label>
                    </Col>
                  </Row>
                </Container>
              </Accordion>
              <Accordion
                activeAccordionHeader="Email Notification Configuration">
                <Container>
                  <Row>
                    <Col xs="6" sm="4">
                      <Label check>
                        <Input
                          type="checkbox"
                          name="self"
                          checked={self}
                          onChange={this.handleChange}/>
                                  Send emails to only firm users.
                      </Label>
                    </Col>
                    <Col xs="6" sm="4">
                      <Label check>
                        <Input
                          type="checkbox"
                          name="client"
                          checked={client}
                          onChange={this.handleChange}/>
                                  Send emails to users of clients and prospects.
                      </Label>

                    </Col>
                    <Col sm="4">
                      <Label check>
                        <Input
                          type="checkbox"
                          name="thirdParty"
                          checked={thirdParty}
                          onChange={this.handleChange}/>
                                  Send emails to users of Third Party market professionals.
                      </Label>
                    </Col>
                  </Row>
                </Container>
              </Accordion>
              <div className="row justify-content-center">
                <LaddaButton
                  className="btn btn-primary btn-ladda"
                  loading={loading}
                  onClick={this.handleSubmit}
                  data-color="green"
                  data-style={ZOOM_IN}
                >
                      Save
                </LaddaButton>
              </div>
            </Col>
          </Row>
        </div>
      )
    }
}

export default connect(({ auth }) => ({ auth }),{ checkAuth })(withRouter(Configuration))
