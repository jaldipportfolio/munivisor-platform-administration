import React, { Component } from "react"
import {connect} from "react-redux"
import LaddaButton, {ZOOM_IN} from "react-ladda"
import {Link} from "react-router-dom"
import {Col, FormGroup, Input, Label, Row} from "reactstrap"
import Accordion from "../../../GlobalComponents/Accordion"
import {TextInput} from "../../../GlobalComponents/TextInput"
import { errorToast, successToast } from "../../../globalutilities/helpers"
// eslint-disable-next-line import/named
import { updateUserDetails, getUserDetails } from "../../../Services/actions/User"
// eslint-disable-next-line import/named
import { getAllApps } from "../../../Services/actions/Setting"
import Loader from "../../../GlobalComponents/Loader"
import {getToken} from "../../../globalutilities/consts"
import {checkAuth} from "../../../Services/actions"

class Profile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      errorMessages: {},
      user: {},
      applications: [],
      loading: true
    }
  }

  async componentDidMount() {
    const {auth} = this.props
    if(auth.userId){
      const [userDetails, applications] = await Promise.all([
        getUserDetails(auth.userId),
        getAllApps()
      ])
      this.setState({
        user: userDetails || {},
        applications,
        loading: false
      })
    }
  }

  onChange = (e, selectApp) => {
    const {user} = this.state
    const {name, value} = e.target
    if(name === "applications"){
      const  findAppIndex = user.applications.findIndex(app => app._id === selectApp._id)
      if(findAppIndex === -1){
        user.applications.push(selectApp)
      }else {
        user.applications.splice(findAppIndex, 1)
      }
      this.setState({ user })
    }else {
      this.setState(prevState => ({
        user: {
          ...prevState.user,
          [name]: value
        }
      }))
    }
  }

  onSave = () => {
    const {userId, userFirstName, userMiddleName, userLastName, applications} = this.state.user

    if(!userFirstName || !userLastName){
      return errorToast("First name and last name required")
    }

    if(!(applications && applications.length)){
      return errorToast("Please select at least one application")
    }

    const user = {
      userFirstName,
      userMiddleName,
      userLastName,
      applications: applications.map(app => app._id)
    }

    this.setState({
      loading: true
    }, async () => {
      const res = await updateUserDetails(userId || "", user)
      if(res && res.status === 200){
        this.props.checkAuth(getToken())
        successToast("Profile updated.")
        this.setState({
          loading: false
        })
      }else {
        errorToast("Something went wrong!")
        this.setState({
          loading: false
        })
      }
    })
  }

  render() {
    const {user, loading, errorMessages, applications} = this.state
    return (
      <div className="animated fadeIn">
        { loading ? <Loader/> : null }
        <Row>
          <Col xs="12" md="12">
            <Accordion activeAccordionHeader="Profile">
              <FormGroup row>
                <TextInput
                  colXs={12}
                  colMd={4}
                  colXl={4}
                  title="First name"
                  label="First name"
                  name="userFirstName"
                  value={user.userFirstName || ""}
                  onChange={this.onChange}
                  error={(errorMessages && errorMessages.userFirstName) || ""}
                />
                <TextInput
                  colXs={12}
                  colMd={4}
                  colXl={4}
                  title="Middle name"
                  label="Middle name"
                  name="userMiddleName"
                  value={user.userMiddleName || ""}
                  onChange={this.onChange}
                  error={(errorMessages && errorMessages.userMiddleName) || ""}
                />
                <TextInput
                  colXs={12}
                  colMd={4}
                  colXl={4}
                  title="Last name"
                  label="Last name"
                  name="userLastName"
                  value={user.userLastName || ""}
                  onChange={this.onChange}
                  error={(errorMessages && errorMessages.userLastName) || ""}
                />
                <TextInput
                  colXs={12}
                  colMd={4}
                  colXl={4}
                  title="Email"
                  label="Email"
                  disabled
                  value={user.loginEmailId || ""}
                />
                <Col md={12} sm={12} xs={12}>
                  <Label><b>Application access  </b></Label>
                  <Link to="/applications" className="ml-2 d-inline">Create new application</Link>
                  <div>
                    {
                      applications.map(app => {
                        const checked = !!(user && user.applications && user.applications.find(ap => ap._id === app._id)) || false
                        return (
                          <FormGroup key={app._id} check inline>
                            <Input className="form-check-input" type="checkbox" id={app.name} name="applications" value={app.name} checked={checked} onChange={(e) => this.onChange(e, app)} />
                            <Label className="form-check-label" check htmlFor={app.name}>{app.name}</Label>
                          </FormGroup>
                        )
                      })
                    }
                  </div>
                </Col>
              </FormGroup>

              <Col xs="12" md="12">
                <div className="row justify-content-center">
                  <LaddaButton
                    className="btn btn-primary btn-ladda"
                    loading={loading}
                    onClick={this.onSave}
                    data-color="green"
                    data-style={ZOOM_IN}
                  >Save
                  </LaddaButton>
                </div>
              </Col>
            </Accordion>
          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = ({ auth }) => ({
  auth: (auth && auth.loginDetails) || {}
})

export default connect(mapStateToProps, { checkAuth })(Profile)