import React, { Component } from "react"
import { Progress, Col, Row } from "reactstrap"
import { AppSwitch } from "@coreui/react"
import Accordion from "../../../GlobalComponents/Accordion"


class Setting extends Component {

  constructor(props) {
    super(props)
    this.state = {
      activeTab: "1",
    }
  }

  toggle = (tab) => {
    const { activeTab } = this.state
    if (activeTab !== tab) {
      this.setState({
        activeTab: tab,
      })
    }
  }

  render() {

    return (
      <div className="animated">
        <Row>
          <Col xs="12" sm="12">
            <Accordion
              activeAccordionHeader="Settings">
              <div>
                <div className="aside-options">
                  <div className="clearfix mt-4">
                    <small><b>Option 1</b></small>
                    <AppSwitch className="float-right" variant="pill" label color="success" defaultChecked size="sm"/>
                  </div>
                  <div>
                    <p className="text-muted">Notification
                    </p>
                  </div>
                </div>

                <div className="aside-options">
                  <div className="clearfix mt-3">
                    <small><b>Option 2</b></small>
                    <AppSwitch className="float-right" variant="pill" label color="success" size="sm"/>
                  </div>
                  <div>
                    <p className="text-muted">View Password
                    </p>
                  </div>
                </div>

                <div className="aside-options">
                  <div className="clearfix mt-3">
                    <small><b>Option 3</b></small>
                    <AppSwitch className="float-right" variant="pill" label color="success" defaultChecked size="sm" disabled/>
                    <div>
                      <p className="text-muted">Chat Options</p>
                    </div>
                  </div>
                </div>

                <div className="aside-options">
                  <div className="clearfix mt-3">
                    <small><b>Option 4</b></small>
                    <AppSwitch className="float-right" variant="pill" label color="success" defaultChecked size="sm" />
                    <div>
                      <p className="text-muted">View Profile</p>
                    </div>
                  </div>
                </div>

                <hr />
                <h6>System Utilization</h6>

                <div className="text-uppercase mb-1 mt-4">
                  <small><b>CPU Usage</b></small>
                </div>
                <Progress className="progress-xs" color="info" value="25" />
                <small className="text-muted">348 Processes. 1/4 Cores.</small>

                <div className="text-uppercase mb-1 mt-2">
                  <small><b>Memory Usage</b></small>
                </div>
                <Progress className="progress-xs" color="warning" value="70" />
                <small className="text-muted">11444GB/16384MB</small>

                <div className="text-uppercase mb-1 mt-2">
                  <small><b>SSD 1 Usage</b></small>
                </div>
                <Progress className="progress-xs" color="danger" value="95" />
                <small className="text-muted">243GB/256GB</small>

                <div className="text-uppercase mb-1 mt-2">
                  <small><b>SSD 2 Usage</b></small>
                </div>
                <Progress className="progress-xs" color="success" value="10" />
                <small className="text-muted">25GB/256GB</small>
              </div>
            </Accordion>
          </Col>
        </Row>
      </div>
    )
  }
}


export default Setting
