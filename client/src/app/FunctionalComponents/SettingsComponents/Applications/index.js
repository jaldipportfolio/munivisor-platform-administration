import React, { Component } from "react"
import {Button, Col, FormGroup, Input, InputGroup, InputGroupAddon, Label, Row} from "reactstrap"
import LaddaButton, {ZOOM_IN} from "react-ladda"
import {toast} from "react-toastify"
import Fuse from "fuse.js"
import Accordion from "../../../GlobalComponents/Accordion"
import {searchOptions} from "../../../globalutilities/consts"
// eslint-disable-next-line import/named
import { getAllApps, postApplications } from "../../../Services/actions/Setting"
import AppList from "./AppList"

const urlRegex = /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i // eslint-disable-line
const localhotsURLRegex = /^http:\/\/\w+(\.\w+)*(:[0-9]+)?\/?(\/[.\w]*)*$/

class Applications extends Component {

  constructor(props) {
    super(props)
    this.state = {
      expZoomIn: false,
      name: "",
      url: "",
      token: "",
      searchString: "",
      errors: {},
      applications: [],
      searchAppList: []
    }
  }

  async componentDidMount() {
    this.getApplications()
  }

  getApplications = async () => {
    const applications = await getAllApps()
    this.setState({
      applications,
    })
  }

  onChange = (e) => {
    const { errors } = this.state
    const { name, value } = e.target
    if(name === "url" && !(urlRegex.test(value) || localhotsURLRegex.test(value))){
      errors[name] = "Please enter valid URL!"
    } else if(name === "name" && !value) {
      errors[name] = "Please enter Application Name!"
    } else if(name === "token" && !value) {
      errors[name] = "Please enter Access Token!"
    } else {
      delete errors[name]
    }
    this.setState({
      [name]: value,
      errors
    })
  }

  onSaveApplications = () => {
    const { errors, name, url, token } = this.state
    if(!name || !url || !token){
      if(!name){
        errors.name = "Please enter Application Name!"
      }
      if(!url){
        errors.url = "Please enter URL!"
      }
      if(!token){
        errors.token = "Please enter Access Token!"
      }
      this.setState({
        errors
      })
    }

    if(Object.keys(errors).length) return

    this.setState({
      expZoomIn: true
    }, async () => {
      if(name && url && token){
        const payload = {name: name.trim().replace(/\s+/g, " "), url, token}
        const application = await postApplications(payload)
        if(application && application.done){
          toast.success("Application Create successfully", {
            position: toast.POSITION.TOP_RIGHT
          })
          this.setState({
            errors: {},
            expZoomIn: false,
            name: "",
            url: "",
            token: "",
          },() => this.getApplications())
        } else {
          toast.error((application && application.message) || "Something went wrong!", {
            position: toast.POSITION.TOP_RIGHT
          })
          this.setState({
            errors: {
              ...errors,
              name: (application && application.message) || ""
            },
            expZoomIn: false,
          })
        }
      }
    })
  }

  onSearchChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    }, () => this.onSearch())
  }

  onSearch = () => {
    const {applications, searchString} = this.state
    searchOptions.keys = [
      "name",
      "token",
      "url"
    ]
    const fuse = new Fuse(applications, searchOptions)
    const searchAppList = fuse.search(searchString)
    this.setState({searchAppList})
  }

  render() {
    const { expZoomIn, name, url, token, errors, searchAppList, applications, searchString } = this.state
    const appList = searchString ? searchAppList : applications

    return (
      <div className="animated">
        <Row>
          <Col xs="12" sm="12">
            <Accordion
              activeAccordionHeader="Application List">
              <div>
                <FormGroup row className="my-0">
                  <Col xs="12" md="12" xl="12">
                    <div className="controls">
                      <InputGroup>
                        <Input type="text" name="searchString" onChange={this.onSearchChange} placeholder="Search..." />
                        <InputGroupAddon addonType="append">
                          <Button color="secondary" onClick={this.onSearch}><i className="fa fa-search"/></Button>
                        </InputGroupAddon>
                      </InputGroup>
                    </div>
                  </Col>
                </FormGroup>
                <AppList
                  className="mt-3"
                  applications={appList}
                  search={searchString}
                />
              </div>
            </Accordion>
            <Accordion
              activeAccordionHeader="Create New Application">
              <div>
                <FormGroup row className="my-0">
                  <Col xs="12" md="4" xl="4">
                    <FormGroup>
                      <Label><b>Application Name<span className="text-danger text-monospace"> *</span></b></Label>
                      <Input
                        type="text"
                        placeholder="Project Name"
                        name="name"
                        value={name || ""}
                        onChange={this.onChange}
                      />
                      <small className="text-danger">{(errors && errors.name) || ""}</small>
                    </FormGroup>
                  </Col>
                  <Col xs="12" md="4" xl="4">
                    <FormGroup>
                      <Label><b>URL<span className="text-danger text-monospace"> *</span></b></Label>
                      <Input
                        type="text"
                        placeholder="Project URL"
                        name="url"
                        value={url || ""}
                        onChange={this.onChange}
                      />
                      <small className="text-danger">{(errors && errors.url) || ""}</small>
                    </FormGroup>
                  </Col>
                  <Col xs="12" md="4" xl="4">
                    <FormGroup>
                      <Label><b>Access Token<span className="text-danger text-monospace"> *</span></b></Label>
                      <Input
                        type="text"
                        placeholder="Project Token"
                        name="token"
                        value={token || ""}
                        onChange={this.onChange}
                      />
                      <small className="text-danger">{(errors && errors.token) || ""}</small>
                    </FormGroup>
                  </Col>
                </FormGroup>
              </div>
              <div className="row justify-content-center">
                <LaddaButton
                  className="btn btn-primary btn-ladda m-2"
                  loading={expZoomIn}
                  onClick={this.onSaveApplications}
                  data-color="red"
                  data-style={ZOOM_IN}
                >
                    Save Application
                </LaddaButton>
              </div>
            </Accordion>
          </Col>
        </Row>
      </div>
    )
  }
}


export default Applications
