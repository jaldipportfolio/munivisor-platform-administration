import React from "react"
import ReactTable from "react-table"
import "react-table/react-table.css"

const highlightSearch = (string, searchQuery) => {
  const reg = new RegExp(
    searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&"),
    "i"
  )
  return string.replace(reg, str => `<mark>${str}</mark>`)
}

const AppList = ({
  applications,
  className,
  pageSizeOptions = [5, 10, 20, 50, 100],
  search
}) => {
  const columns = [
    {
      Header: "Name",
      id: "name",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: highlightSearch((item && item.name) || "-", search) }} />
        )
      },
      sortMethod: (a, b) => a.name.localeCompare(b.name)
    },
    {
      id: "url",
      Header: "URL",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: highlightSearch((item && item.url) || "-", search) }} />
        )
      },
      sortMethod: (a, b) => a.url.localeCompare(b.url)
    },
    {
      id: "token",
      Header: "Access Token",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: highlightSearch((item && item.token) || "-", search) }} />
        )
      },
      sortMethod: (a, b) => a.token.localeCompare(b.token)
    }
  ]
  return (
    <div className={className}>
      <ReactTable
        columns={columns}
        data={applications}
        showPaginationBottom
        defaultPageSize={10}
        pageSizeOptions={pageSizeOptions}
        className="-striped -highlight is-bordered"
        style={{ overflowX: "auto" }}
        showPageJump
        minRows={2}
      />
    </div>
  )
}

export default AppList
