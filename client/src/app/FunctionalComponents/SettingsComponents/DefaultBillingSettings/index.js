import React, { Component } from "react"
import {toast} from "react-toastify"
import { updatePlatFormSettings } from "../../../Services/actions/Setting"
import BillingConfiguration from "./BillingConfiguration"
import Loader from "../../../GlobalComponents/Loader"

class DefaultBillingSettings extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
    }
  }

  onSave = (billing, settingId) => {
    this.setState({
      loading: true,
    }, async () => {
      const res = await updatePlatFormSettings({billing}, settingId)
      if(res && res.status === 200){
        toast.success("Details updated successfully.", {
          position: toast.POSITION.TOP_RIGHT
        })
      }else {
        toast.error("Something went wrong!", {
          position: toast.POSITION.TOP_RIGHT
        })
      }
      this.setState({
        loading: false,
      })
    })
  }


  render() {
    const {billing, loading} = this.state
    return (
      <div className="animated fadeIn">
        {loading ? <Loader/> : null}
        <BillingConfiguration billing={billing || {}} onSave={this.onSave}/>
      </div>
    )
  }
}

export default DefaultBillingSettings