import React, { Component } from "react"
import {connect} from "react-redux"
import {AppSwitch} from "@coreui/react"
import { Col, FormGroup, Input, Label, Row } from "reactstrap"
import {withRouter} from "react-router-dom"
import LaddaButton, {ZOOM_IN} from "react-ladda"
import * as qs from "query-string"
import {toast} from "react-toastify"
import Accordion from "../../../GlobalComponents/Accordion"
import {DateInput, NumberInput, SelectLabelInputSecond} from "../../../GlobalComponents/TextInput"
import { getPlatFormSetting } from "../../../Services/actions/Setting"
// eslint-disable-next-line import/named
import {
  insertTenantConfig,
  getTenantConfigData,
  updateTenantConfigDetails
} from "../../../Services/actions/Tenant"
import Loader from "../../../GlobalComponents/Loader"

const billFreq = [
  {name: "billFrequency", value: "monthly", label: "Monthly"},
  {name: "billFrequency", value: "quarterly", label: "Quarterly"},
  {name: "billFrequency", value: "halfYearly", label: "Half Yearly"},
  {name: "billFrequency", value: "yearly", label: "Yearly"}
]

const discountTypes = [
  {name: "discountType", value: "percentageDiscount", label: "Percentage discount"},
  {name: "discountType", value: "firstDiscountPeriod", label: "First x periods ( monthly, quarterly, yearly ) free"},
  {name: "discountType", value: "monthlyDiscount", label: "x% discount for x months."}
]

const billingTypes = [
  { name: "Enterprise Billing", value: "enterprise"},
  { name: "User Specific Billing", value: "user"}
]

const prices = [
  {name: "6 to 10 users", value: "6 to 10 users", price: 2250 },
  {name: "11 to 15 users", value: "11 to 15 users", price: 3375 },
  {name: "16 to 20 users", value: "16 to 20 users", price: 4500 },
  {name: "21 to 25 users", value: "21 to 25 users", price: 5625 },
  {name: "More than 25 users", value: "More than 25 users", price: 0 }
]

class BillingConfiguration extends Component {
  constructor(props) {
    super(props)
    this.state = {
      billing: {
        percentageDiscount: "",
        enterpriseRate: "",
        nonSeries50: "",
        series50: "",
        freePeriod: "",
        discount: "",
        discountPeriod: "",
        paymentEnable: true,
        numberOfUsers: ""
      },
      loading: true,
      firmId: "",
      updateId: "",
      errorMessages: {},
      setting: {}
    }
  }

  async componentWillMount(){
    const {loginDetails} = this.props.auth || {}
    const { location } = this.props
    const { billing } = this.state
    const queryString = qs.parse(location.search)
    const firmId = (queryString && queryString.firmId) || ""
    if(firmId){
      const data = await getTenantConfigData(firmId)
      if(data && data.length){
        this.setState({
          billing: (data[0] && data[0].billing) || billing,
          updateId: (data[0] && data[0]._id),
          firmId,
          loading: false
        })
      }else if(loginDetails && loginDetails.userId){
        const setting = await getPlatFormSetting()
        console.log(setting)
        this.setState({
          billing: (setting && setting.billing) || billing,
          setting,
          firmId,
          updateId: "",
          loading: false
        })
      }
    } else if(loginDetails && loginDetails.userId){
      const setting = await getPlatFormSetting()
      console.log(setting)
      this.setState({
        billing: (setting && setting.billing) || {},
        setting,
        firmId,
        loading: false
      })
    }
  }

  async componentWillReceiveProps(nextProps){
    const {loginDetails} = nextProps.auth || {}
    const { billing } = this.state
    if(nextProps.auth && nextProps.auth.userEmail !== (this.props.auth && this.props.auth.userEmail) && !Object.keys(this.state.setting).length){
      const setting = await getPlatFormSetting(loginDetails.userId)
      console.log(setting)
      this.setState({
        billing: (setting && setting.billing) || billing,
        setting,
        loading: false
      })
    }
  }

  onChange = (event) => {
    const {name, value, type, checked} = event.target
    const stObj = {}
    if(name === "numberOfUsers"){
      const cost = prices.find(p => p.value === value)
      stObj.enterpriseRate = cost.price || 0
    }

    this.setState(prevState => ({
      billing: {
        ...prevState.billing,
        [name]: type === "checkbox" ? checked : value,
        ...stObj
      }
    }))
  }

  validate = (name, value) => {
    const {billing} = this.state
    switch (name) {
    case "billFrequency":
      if (!value) {
        return "Bill Frequency should not be empty."
      }
      return ""
    case "typeOfBilling":
      if (!value) {
        return "Type of Billing should not be empty."
      }
      return ""
    case "series50":
      if ((billing && billing.typeOfBilling) === "user") {
        if (!value) {
          return "Series 50 Rate should not be empty."
        } if (value <= 0) {
          return "Value must be greater than 0"
        }
      }
      return ""
    case "nonSeries50":
      if ((billing && billing.typeOfBilling) === "user") {
        if (!value) {
          return "Non Series 50 Rate should not be empty."
        } if (value <= 0) {
          return "Value must be greater than 0"
        }
        return ""
      }
      return ""
    case "numberOfUsers":
      if ((billing && billing.typeOfBilling) === "enterprise") {
        if (!value) {
          return "Number Of Users should not be empty."
        }
      }
      return ""
    case "enterpriseRate":
      if ((billing && billing.typeOfBilling) === "enterprise") {
        if (!value) {
          return "Enterprise Rate should not be empty."
        } if (value <= 0) {
          return "Value must be greater than 0"
        }
      }
      return ""
    case "percentageDiscount":
      if ((billing && billing.discountType) === "percentageDiscount") {
        if (!value) {
          return "Percentage Discount should not be empty."
        } if (value <= 0) {
          return "Value must be greater than 0"
        }
      }
      return ""
    case "freePeriod":
      if ((billing && billing.discountType) === "firstDiscountPeriod") {
        if (!value) {
          return "Free Period Discount should not be empty."
        }
      }
      return ""
    case "discount":
      if ((billing && billing.discountType) === "monthlyDiscount") {
        if (!value) {
          return "Discount should not be empty."
        }
      }
      return ""
    case "discountPeriod":
      if ((billing && billing.discountType) === "monthlyDiscount") {
        if (!value) {
          return "Discount Period should not be empty."
        }
      }
      return ""
    default: {
      return ""
    }
    }
  }

  onSave = async () => {
    const {billing, firmId, updateId, setting} = this.state
    const {auth} = this.props
    const validationErrors = {}

    const validatFields = (billing && billing.typeOfBilling === "enterprise") ? ["enterpriseRate", "discountPeriod", "discount", "freePeriod", "percentageDiscount", "numberOfUsers", "enterpriseRate", "typeOfBilling" ,"billFrequency" ] : (billing && billing.typeOfBilling === "user") ? ["series50", "nonSeries50", "discountPeriod", "discount", "freePeriod", "percentageDiscount", "typeOfBilling" ,"billFrequency"] : ["typeOfBilling", "billFrequency"]
    validatFields.forEach(name => {
      validationErrors[name] = this.validate(name, billing[name])
    })
    if (Object.values(validationErrors).join("").length > 0) {
      this.setState({errorMessages: validationErrors})
      return
    }

    if(firmId){
      const data = {
        tenantId: firmId,
        userId: (auth && auth.loginDetails && auth.loginDetails.userId) || "",
        billing
      }
      if(updateId){
        const res = await updateTenantConfigDetails(updateId || "", data)
        if(res && res.length){
          this.setState({
            billing: (res && res[0].billing) || {},
            errorMessages: {}
          })
          toast.success("Details updated successfully.", {
            position: toast.POSITION.TOP_RIGHT
          })
        }else {
          toast.error("Something went wrong!", {
            position: toast.POSITION.TOP_RIGHT
          })
        }
      }else {
        const res = await insertTenantConfig(data)
        if(res && res._id){
          this.setState({
            billing: (res && res.billing) || {},
            updateId: res._id,
            errorMessages: {}
          })
          toast.success("Details insert successfully.", {
            position: toast.POSITION.TOP_RIGHT
          })
        }else {
          toast.error("Something went wrong!", {
            position: toast.POSITION.TOP_RIGHT
          })
        }
      }
    }else {
      this.props.onSave(billing, setting._id)
      this.setState({
        errorMessages: {}
      })
    }
  }

  onPaymentToggle = () => {
    const {billing} = this.state
    this.setState({
      billing: {
        ...billing,
        paymentEnable: !billing.paymentEnable
      }
    })
  }

  render() {
    const {billing, loading, errorMessages} = this.state
    return (
      <Row>
        { loading ? <Loader/> : null }
        <Col xs="12" md="12">
          <Accordion
            activeAccordionHeader="Setting">
            <FormGroup row>
              <Col md="3">
                <Label>
                  <b>Billing Frequency</b>
                  <small> <i className="fa fa-asterisk text-danger" /></small>
                </Label>
                {
                  billFreq.map(freq => (
                    <FormGroup key={freq.value} check>
                      <Input className="form-check-input" type="radio" id={freq.value} name={freq.name} value={freq.value} checked={billing && billing[freq.name] === freq.value} onChange={this.onChange} onClick={this.onChange}/>
                      <Label className="form-check-label" check htmlFor={freq.value}>{freq.label}</Label>
                    </FormGroup>
                  ))
                }
                <small className="text-danger">{errorMessages && errorMessages.billFrequency}</small>
              </Col>
              <Col md="3">
                <SelectLabelInputSecond
                  title="Type of Billing"
                  label="Type of Billing"
                  list={billingTypes || []}
                  name="typeOfBilling"
                  value={billing.typeOfBilling || ""}
                  onChange={this.onChange}
                  required
                  error={errorMessages && errorMessages.typeOfBilling}
                />
              </Col>
              {
                billing && billing.typeOfBilling === "enterprise" ?
                <Col md="3">
                  <SelectLabelInputSecond
                    title="Number of users"
                    label="Number of users"
                    list={prices || []}
                    name="numberOfUsers"
                    value={billing.numberOfUsers || ""}
                    onChange={this.onChange}
                    required
                    error={errorMessages && errorMessages.numberOfUsers}
                  />
                </Col>
                : null
              }
              {
                billing && billing.typeOfBilling === "enterprise" ?
                  <Col md="3">
                    <NumberInput
                      className="class"
                      name="enterpriseRate"
                      title="Enterprise Rate"
                      label="Enterprise Rate"
                      placeholder="$"
                      value={(billing && billing.enterpriseRate) || ""}
                      prefix="$"
                      decimalScale={4}
                      onChange={this.onChange}
                      required
                      disabled={billing.numberOfUsers !== "More than 25 users"}
                      error={errorMessages && errorMessages.enterpriseRate}
                    />
                  </Col>
                  : null
              }
              {
                billing && billing.typeOfBilling === "user" ?
                  <Col md="3">
                    <NumberInput
                      className="class"
                      name="series50"
                      title="Series 50 Rate"
                      label="Series 50 Rate"
                      placeholder="$"
                      value={(billing && billing.series50) || ""}
                      prefix="$"
                      decimalScale={4}
                      onChange={this.onChange}
                      required
                      error={errorMessages && errorMessages.series50}
                    />
                  </Col>
                  : null
              }
              {
                billing && billing.typeOfBilling === "user" ?
                  <Col md="3">
                    <NumberInput
                      className="class"
                      name="nonSeries50"
                      title="Non Series 50 Rate"
                      label="Non Series 50 Rate"
                      placeholder="$"
                      value={(billing && billing.nonSeries50) || ""}
                      prefix="$"
                      decimalScale={4}
                      onChange={this.onChange}
                      required
                      error={errorMessages && errorMessages.nonSeries50}
                    />
                  </Col>
                  : null
              }
            </FormGroup>
            <FormGroup row>
              <Col md="3">
                <DateInput
                  className="class"
                  name="billingStartDate"
                  label="Billing start date"
                  type="date"
                  value={billing && billing.billingStartDate ? new Date(billing.billingStartDate) : null}
                  onChange={this.onChange}
                />
              </Col>
              <Col md="3">
                <Label><b>Send invoice automatically ?</b></Label>
                <FormGroup check >
                  <Input
                    className="form-check-input"
                    type="checkbox"
                    id="sendInvoiceAuto"
                    name="sendInvoiceAuto"
                    checked={billing.sendInvoiceAuto || false}
                    onChange={this.onChange}
                    onClick={this.onChange}
                  />
                </FormGroup>
              </Col>
              <Col md="3">
                <NumberInput
                  className="class"
                  name="recurringDay"
                  title="How many days available to the tenant to pay the invoice ?"
                  label="How many days available to the tenant to pay the invoice ?"
                  value={(billing && billing.recurringDay) || ""}
                  decimalScale={0}
                  onChange={this.onChange}
                  error={errorMessages && errorMessages.recurringDay}
                />
              </Col>
              <Col md="3">
                <Label><b>Discount Types</b></Label>
                {
                  discountTypes.map(discount => (
                    <FormGroup key={discount.value} check>
                      <Input className="form-check-input" type="radio" id={discount.value} name={discount.name} value={discount.value} checked={billing && billing[discount.name] === discount.value} onChange={this.onChange}/>
                      <Label className="form-check-label" check htmlFor={discount.value}>{discount.label}</Label>
                    </FormGroup>
                  ))
                }
              </Col>
              {
                (billing && billing.discountType === "percentageDiscount") ?
                  <Col md="3">
                    <NumberInput
                      className="class"
                      name="percentageDiscount"
                      title="Discount"
                      label="Percentage Discount"
                      value={(billing && billing.percentageDiscount) || ""}
                      suffix="%"
                      error={errorMessages && errorMessages.percentageDiscount}
                      decimalScale={0}
                      onChange={this.onChange}
                    />
                  </Col> : null
              }
              {
                (billing && billing.discountType === "firstDiscountPeriod") ?
                  <Col md="3">
                    <Label><b>Free period</b></Label>
                    <p className="multiExpTblVal">First
                      <input className="d-inline form-control"
                        style={{ maxWidth: 60, marginLeft: "5px", marginRight: "5px", border: "none", borderBottom: "1px solid darkturquoise" }}
                        name="freePeriod"
                        type="number"
                        min="1"
                        max="12"
                        value={billing.freePeriod || ""}
                        onChange={this.onChange}
                      /> periods {billing.billFrequency || "(monthly, quarterly, yearly)"} free
                    </p>
                    <small className="text-danger">{errorMessages && errorMessages.freePeriod}</small>
                  </Col> : null
              }
              {
                (billing && billing.discountType === "monthlyDiscount") ?
                  <Col md="3">
                    <Label><b>Discount</b></Label>
                    <p className="multiExpTblVal">
                      <input className="d-inline form-control"
                        style={{ maxWidth: 60, marginLeft: "5px", marginRight: "5px", border: "none", borderBottom: "1px solid darkturquoise" }}
                        name="discount"
                        type="number"
                        min="1"
                        max="12"
                        value={billing.discount || ""}
                        onChange={this.onChange}
                      />%  Discount for
                      <input className="d-inline form-control"
                        style={{ maxWidth: 60, marginLeft: "5px", marginRight: "5px", border: "none", borderBottom: "1px solid darkturquoise" }}
                        name="discountPeriod"
                        type="number"
                        min="1"
                        max="12"
                        value={billing.discountPeriod || ""}
                        onChange={this.onChange}
                      /> months
                    </p>
                    <small className="text-danger">{(errorMessages && errorMessages.discount && errorMessages.discountPeriod) ? errorMessages.discount : null}</small><br/>
                  </Col>: null
              }
              <Col xs="12" md="3">
                <FormGroup>
                  <div style={{paddingBottom: 5}}><b>Payment Enable</b></div>
                  <AppSwitch
                    label
                    className="mx-1"
                    variant="pill"
                    color="primary"
                    name="paymentEnable"
                    checked={billing.paymentEnable}
                    onChange={this.onPaymentToggle}/>
                </FormGroup>
              </Col>
            </FormGroup>
            <Col xs="12" md="12">
              <div className="row justify-content-center">
                <LaddaButton
                  className="btn btn-primary btn-ladda"
                  loading={loading}
                  onClick={this.onSave}
                  data-color="green"
                  data-style={ZOOM_IN}
                >Save
                </LaddaButton>
              </div>
            </Col>
          </Accordion>
        </Col>
      </Row>
    )
  }
}

export default connect(({ auth }) => ({ auth }),null)(withRouter(BillingConfiguration))