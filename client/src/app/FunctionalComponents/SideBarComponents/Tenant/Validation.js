import Joi from "joi-browser"

const officePhone = Joi.object().keys({
  _id: Joi.string()
    .allow("")
    .optional(),
  countryCode: Joi.string()
    .allow("")
    .label("Country Code")
    .optional(),
  phoneNumber: Joi.string()
    .allow("")
  // .min(10)
    .label("Phone Number")
    .optional(),
  extension: Joi.string()
    .allow("")
    .min(3)
    .max(10)
    .label("Extension")
    .optional()
})

const officeFax = Joi.object().keys({
  _id: Joi.string()
    .allow("")
    .optional(),
  faxNumber: Joi.string()
    .allow("")
  // .min(17)
    .label("Fax Number")
    .optional()
})

const officeEmails = Joi.object().keys({
  _id: Joi.string()
    .allow("")
    .optional(),
  emailId: Joi.string()
    .email()
    .regex(/^([a-zA-Z0-9_\-.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/i) // eslint-disable-line
    .allow("")
    .label("Valid Email Id")
    .optional()
})

const entityAddressSchema = Joi.object().keys({
  _id: Joi.string()
    .allow("")
    .optional(),
  addressName: Joi.string()
    .allow("")
    .label("Address Name")
    .optional(),
  addressType: Joi.string()
    .allow("")
    .label("Address Type")
    .optional(),
  isPrimary: Joi.boolean()
    .label("Is Primary")
    .optional(),
  isHeadQuarter: Joi.boolean()
    .required()
    .label("Is HeadQuarter")
    .optional(),
  /* isActive: Joi.boolean().required().label("Is Active").optional(), */
  website: Joi.string()
    .allow("")
    .label("WebSite")
    .optional(),
  officePhone: Joi.array().items(officePhone),
  officeFax: Joi.array().items(officeFax),
  officeEmails: Joi.array().items(officeEmails),
  addressLine1: Joi.string()
    .required()
    .label("Address Line 1"),
  addressLine2: Joi.string()
    .allow("")
    .label("Address Line 2")
    .optional(),
  country: Joi.string()
    .required()
    .label("Country")
    .optional(),
  county: Joi.string()
    .required()
    .label("County")
    .optional(),
  state: Joi.string()
    .required()
    .label("State")
    .optional(),
  city: Joi.string()
    .required()
    .label("City")
    .optional(),
  organizationType: Joi.string()
    .required()
    .label("Organization Type")
    .optional(),
  zipCode: {
    _id: Joi.string()
      .allow("")
      .optional(),
    zip1: Joi.string()
      .min(5)
      .max(5)
      .required()
      .label("Zip Code 1")
      .optional(),
    zip2: Joi.string()
      .min(4)
      .max(4)
      .allow("")
      .label("Zip Code 2")
      .optional()
  },
  formatted_address: Joi.string()
    .allow("")
    .label("Formatted Address")
    .optional(),
  url: Joi.string()
    .allow("")
    .label("URL")
    .optional(),
  location: {
    _id: Joi.string()
      .allow("")
      .optional(),
    longitude: Joi.string()
      .allow("")
      .label("Longitude")
      .optional(),
    latitude: Joi.string()
      .allow("")
      .label("Latitude")
      .optional()
  }
})

const firmAddOnsSchema = Joi.object().keys({
  _id: Joi.string()
    .allow("")
    .optional(),
  serviceName: Joi.string()
    .label("Service Name")
    .required()
    .optional(),
  serviceEnabled: Joi.boolean()
    .required()
    .optional()
})

const marketRole = Joi.object().keys({
  _id: Joi.string()
    .allow("")
    .optional(),
  label: Joi.string()
    .label("Market Role")
    .optional(),
  value: Joi.string()
    .label("Market Role")
    .optional()
})

const entityFlagSchema = Joi.object().keys({
  _id: Joi.string()
    .allow("")
    .optional(),
  marketRole: Joi.array()
    .items(marketRole)
    .required()
    .label("Market Role")
    .optional(),
  issuerFlags: Joi.array()
    .items(
      Joi.string()
        .required()
        .optional()
    )
    .min(1)
    .unique()
    .required()
    .label("Issuer Flag")
    .optional()
})

const entityLinkCusipSchema = Joi.object().keys({
  _id: Joi.string()
    .allow("")
    .optional(),
  debtType: Joi.string()
    .allow("")
    .label("Debt Type")
    .optional(),
  associatedCusip6: Joi.string()
    .uppercase()
    .min(6)
    .allow("")
    .label("CUSIP-6")
    .optional()
})

const entityLinkBorrowersObligorsSchema = Joi.object().keys({
  _id: Joi.string()
    .allow("")
    .optional(),
  borOblRel: Joi.string()
    .required()
    .label("Borrower Relationship")
    .optional(),
  borOblFirmName: Joi.string()
    .required()
    .label("Borrower Firm Name")
    .optional(),
  borOblDebtType: Joi.string()
    .required()
    .label("Borrower Debt Type")
    .optional(),
  borOblCusip6: Joi.string()
    .required()
    .label("Borrower Link Cusip")
    .optional(),
  borOblStartDate: Joi.date()
    .example(new Date("2005-01-01"))
    .required()
    .label("Start Date")
    .optional(),
  borOblEndDate: Joi.date()
    .example(new Date("2016-01-01"))
    .min(Joi.ref("borOblStartDate"))
    .required()
    .label("End Date")
    .optional() // switched order of min and example
})

// eslint-disable-next-line no-unused-vars
const firmDetailSchema = type =>
  Joi.object().keys({
    _id: Joi.string()
      .allow("")
      .optional(),
    entityFlags: /* type === "client" ? entityFlagSchema : */ entityFlagSchema,
    isMuniVisorClient: Joi.boolean()
      .required()
      .optional(),
    msrbFirmName: Joi.string()
      .allow("")
      .label("MSRB Firm Name")
      .optional(),
    msrbRegistrantType: Joi.string()
      .allow("")
      .label("MSRB Registrant Type")
      .optional(),
    msrbId: Joi.string()
      .allow("")
      .label("MSRB Id")
      .optional(),
    entityAliases: Joi.array()
      .items(
        Joi.string()
          .label("Aliases")
          .optional()
      )
      .optional(),
    entityType: Joi.string()
      .required()
      .label("Entity Type")
      .optional(),
    firmName: Joi.string()
      .required()
      .label("Firm Name")
      .optional(),
    firmType: Joi.string()
      .allow("")
      .label("Firm Type")
      .optional(),
    taxId: Joi.string()
      .allow("")
      .label("Tax Id")
      .optional(),
    primarySectors: Joi.string()
      .allow("")
      .label("Primary Sectors")
      .optional(),
    secondarySectors: Joi.string()
      .allow("")
      .label("Secondary Sectors")
      .optional(),
    firmLeadAdvisor: Joi.string()
      .allow("")
      .label("Firm Lead Advisor")
      .optional(),
    prevLeadAdvisor: Joi.string()
      .allow("")
      .label("Previous Lead Advisor")
      .optional(),
    prevAdvisorFirm: Joi.string()
      .allow("")
      .label("Previous Advisor Firm")
      .optional(),
    prevAdvisorContractExpire: Joi.string()
      .allow("")
      .label("Previous Advisor Contract Expire")
      .optional(),
    primaryContactNameInEmma: Joi.string()
      .allow("")
      .label("Primary Contact Name In Emma")
      .optional(),
    primaryContactPhone: Joi.string()
      .allow("")
      .label("Primary Contact Phone")
      .optional(),
    primaryContactEmail: Joi.string()
      .allow("")
      .required()
      .label("Primary Contact Email")
      .optional(),
    businessStructure: Joi.string()
      .required()
      .label("Business Structure")
      .optional(),
    numEmployees: Joi.string()
      .allow("")
      .label("Number Of Employees")
      .optional(),
    annualRevenue: Joi.string()
      .allow("")
      .label("Annual Revenue")
      .optional(),
    addresses:
            type === "address" || type === "all"
              ? Joi.array()
                .items(entityAddressSchema)
                .required()
              : Joi.array()
                .items(entityAddressSchema)
                .optional(),
    firmAddOns: Joi.array()
      .items(firmAddOnsSchema)
      .required()
      .optional(),
    entityLinkedCusips: Joi.array()
      .items(entityLinkCusipSchema)
      .optional(),
    entityBorObl: Joi.array()
      .items(entityLinkBorrowersObligorsSchema)
      .required()
      .optional(),
    __v: Joi.number()
      .integer()
      .optional()
  })

export const validatClientsDetail = (inputFirmDetail, type) =>
  Joi.validate(inputFirmDetail, firmDetailSchema(type), {
    convert: true,
    abortEarly: false,
    stripUnknown: false
  })
