import React, { Component } from "react"
import { toast } from "react-toastify"
import { Col, FormGroup, Input, Label, Row} from "reactstrap"
import "react-select/dist/react-select.min.css"
import LaddaButton, { ZOOM_IN } from "react-ladda"
import {withRouter} from "react-router-dom"
import {connect} from "react-redux"
import "react-toastify/dist/ReactToastify.css"
import "react-phone-number-input/style.css"
import "react-table/react-table.css"
import Accordion from "../../../GlobalComponents/Accordion"
import ContactInformation from "../../../GlobalComponents/ContactInformation"
import { getPicklistValues } from "../../../globalutilities/helpers"
import { createTenantEntity } from "../../../Services/actions/Tenant"
import Loader from "../../../GlobalComponents/Loader"
import {getTenantList} from "../../../Services/actions/Dashboard"
import {signOut} from "../../../Services/actions/LoginAuthentication"
import {NumberInput} from "../../../GlobalComponents/TextInput"


class Tenant extends Component {
  constructor(props) {
    super(props)
    this.state = {
      errors: {},
      tenantList: [],
      addressList: [],
      showAddressForm: false, // eslint-disable-line
      configFrom: "",
      dropDown: {},
      loading: true
    }
  }

  async componentDidMount() {
    const result = await getPicklistValues(["LKUPREGISTRANTTYPE", "LKUPBUSSTRUCT", "LKUPANNUALREVENUE", "LKUPNUMOFPEOPLE"])
    
    if(result && result.length){
      const registrantTypesList = result[0] && result[0][2] ?
        (result[0][2]["Tenants / Firms"] || []) : []
      const businessStructuresList = result[1] && result[1][1] ? result[1][1] : []
      const revenueList = result[2] && result[2][1] ? result[2][1] : []
      const numEmployeesList = result[3] && result[3][1] ? result[3][1] : []
      const payload = {
        filters: {
          userContactTypes: [],
          entityMarketRoleFlags:[],
          entityIssuerFlags:[],
          entityPrimaryAddressStates:[],
          freeTextSearchTerm:""
        },
        pagination: {
          serverPerformPagination:false,
          currentPage:0,
          size:25,
          sortFields:{
            entityName:1
          }
        }
      }
      const tenant = await getTenantList(payload)
      const tenantList = await (tenant && tenant.data && tenant.data.pipeLineQueryResults) || []
  
      this.setState(prevState => ({
        tenantList,
        dropDown: {
          ...prevState.dropDown,
          registrantTypesList,
          businessStructuresList,
          revenueList,
          numEmployeesList
        },
        loading: false
      }))
    } else {
      this.props.signOut()
    }
    
  }

    onValueChange = (e) => {
      const { errors } = this.state
      const { name, value, formattedValue } = e.target
      this.setState({
        [name]: name === "taxId" ? formattedValue : value
      }, () => {
        if((name === "firmName" || name === "configFrom") && value && (errors && (errors.firmName || errors.configFrom))){
          this.setState(prevState => ({
            errors: {
              ...prevState.errors,
              [name]: ""
            }
          }))
        }
        if (name === "taxId" && value.replace(/[^0-9]/g, "").length !== 9) {
          this.setState(prevState => ({
            errors: {
              ...prevState.errors,
              [name]: "Tax Id must be 9 digit"
            }
          }))
        } else if(name === "taxId" && value.replace(/[^0-9]/g, "").length === 9){
          this.setState(prevState => ({
            errors: {
              ...prevState.errors,
              [name]: ""
            }
          }))
        }
      })
    }

    saveTenantEntity = (name) => {
      const { firmName, msrbRegistrantType, msrbId, taxId, businessStructure, numEmployees, annualRevenue, errors, addressList, configFrom } = this.state
      const { history } = this.props
      if(!firmName || !configFrom || (errors && (errors.firmName || errors.configFrom))){
        if(!firmName){
          this.setState(prevState => ({
            errors: {
              ...prevState.errors,
              firmName: "Firm Name is Required!"
            }
          }))
        }
        if(!configFrom){
          this.setState(prevState => ({
            errors: {
              ...prevState.errors,
              configFrom: "Config From is Required!"
            }
          }))
        }
        toast.error((errors && (errors.firmName || errors.configFrom)) || "Field is Required!", {
          position: toast.POSITION.TOP_RIGHT
        })
        return
      }
      this.setState(prevState => ({
        errors: {
          ...prevState.errors,
          firmName: "",
          configFrom: ""
        }
      }))

      this.setState(prevState => ({
        [name]: !prevState[name],
        loading: true
      }), async () => {
        const payload = {}
        const firm = firmName.trim().replace(/\s+/g, " ")
        payload.firmDetails = {firmName: firm, msrbRegistrantType, addresses: addressList, msrbId, taxId, businessStructure, numEmployees, annualRevenue}
        payload.entityId = configFrom
        const res = await createTenantEntity(payload)
        if(res && res.done){
          toast.success("Create Tenant Entity successfully", {
            position: toast.POSITION.TOP_RIGHT
          })

          setTimeout(() => {
            history.push("/dashboard")
          }, 2000)
        } else {
          this.setState(prevState => ({
            errors: {
              ...prevState.errors,
              firmName: (res && res.message) || ""
            },
            loading: false,
            expZoomIn: ""
          }))
          toast.error((res && res.message) || "Something went wrong!", {
            position: toast.POSITION.TOP_RIGHT
          })
        }
      })
    }

    onSaveAddress = (addressList) => {
      this.setState({
          addressList
      }, () => toast.success("addressed saved successfully!", {
        position: toast.POSITION.TOP_RIGHT
      }))
    }

    render() {
      const { firmName, msrbRegistrantType, msrbId, taxId, businessStructure, numEmployees, annualRevenue, dropDown, loading, errors, expZoomIn, tenantList, configFrom } = this.state
      const { registrantTypesList, businessStructuresList, revenueList, numEmployeesList } = dropDown
      
      return (
        <div className="animated fadeIn">
          { loading ? <Loader/> : null }
          <Row>
            <Col xs="12" sm="12">
              <Accordion
                activeAccordionHeader="Firm Details">
                <div>
                  <FormGroup row className="my-0">
                    <Col xs="12" md="4" xl="4">
                      <FormGroup>
                        <Label><b>Firm Name<span className="text-danger text-monospace"> *</span></b></Label>
                        <Input
                          type="text"
                          placeholder="Firm Name"
                          name="firmName"
                          onChange={this.onValueChange}
                          value={firmName || ""}
                        />
                        <small className="text-danger">{(errors && errors.firmName) || ""}</small>
                      </FormGroup>
                    </Col>
                    <Col xs="12" md="4" xl="4">
                      <FormGroup>
                        <Label><b>Configuration From<span className="text-danger text-monospace"> *</span></b></Label>
                        <Input
                          type="select"
                          name="configFrom"
                          onChange={this.onValueChange}
                          value={configFrom || "Pick"}
                        >
                          <option value="Pick" disabled >Select Configuration From</option>
                          { tenantList && tenantList.length && tenantList.map((t, index) => (
                            <option key={index.toString()} value={t.entityId}>
                              {t.entityName}
                            </option>
                          ))}
                        </Input>
                        <small className="text-danger">{(errors && errors.configFrom) || ""}</small>
                      </FormGroup>
                    </Col>
                    <Col xs="12" md="4" xl="4">
                      <FormGroup>
                        <Label><b>MSRB Registrant Type</b></Label>
                        <Input
                          type="select"
                          name="msrbRegistrantType"
                          onChange={this.onValueChange}
                          value={msrbRegistrantType || "Pick"}
                        >
                          <option value="Pick" disabled>Select MSRB Registrant Type</option>
                          { registrantTypesList && registrantTypesList.length && registrantTypesList.map(t => (
                            <option key={t} value={t}>
                              {t}
                            </option>
                          ))}
                        </Input>
                      </FormGroup>
                    </Col>
                    <Col xs="12" md="4" xl="4">
                      <FormGroup>
                        <Label><b>MSRB ID</b></Label>
                        <Input
                          type="text"
                          name="msrbId"
                          placeholder="MSRB ID"
                          onChange={this.onValueChange}
                          value={msrbId || ""}
                        />
                      </FormGroup>
                    </Col>
                    <Col xs="12" md="12" xl="4" >
                      <FormGroup>
												<NumberInput
													className="class"
													name="taxId"
													title="Tax Id"
													label="Tax Id"
													format="###-##-####"
													mask="_"
													value={taxId || ""}
													placeholder="Enter Tax ID"
													onChange={this.onValueChange}
													error={(errors && errors.taxId) || ""}
												/>
                      </FormGroup>
                    </Col>
                    <Col xs="12" md="12" xl="4">
                      <FormGroup>
                        <Label><b>Business Structure</b></Label>
                        <Input
                          type="select"
                          name="businessStructure"
                          onChange={this.onValueChange}
                          value={businessStructure || "Pick"}
                        >
                          <option value="Pick" disabled>Select Business Structure</option>
                          { businessStructuresList && businessStructuresList.length && businessStructuresList.map(t => (
                            <option key={t} value={t}>
                              {t}
                            </option>
                          ))}
                        </Input>
                      </FormGroup>
                    </Col>
                    <Col xs="12" md="12" xl="4">
                      <FormGroup>
                        <Label><b>Number of Employees</b></Label>
                        <Input
                          type="select"
                          name="numEmployees"
                          onChange={this.onValueChange}
                          value={numEmployees || "Pick"}
                        >
                          <option value="Pick" disabled>Pick Number of Employees</option>
                          { numEmployeesList && numEmployeesList.length && numEmployeesList.map(t => (
                            <option key={t} value={t}>
                              {t}
                            </option>
                          ))}
                        </Input>
                      </FormGroup>
                    </Col>
                    <Col xs="12" md="12" xl="4">
                      <FormGroup>
                        <Label><b>Annual Revenue</b></Label>
                        <Input
                          type="select"
                          name="annualRevenue"
                          onChange={this.onValueChange}
                          value={annualRevenue || "Pick"}
                        >
                          <option value="Pick" disabled>Pick Annual Revenue</option>
                          { revenueList && revenueList.length && revenueList.map(t => (
                            <option key={t} value={t}>
                              {t}
                            </option>
                          ))}
                        </Input>
                      </FormGroup>
                    </Col>
                  </FormGroup>

                </div>
              </Accordion>
            </Col>
          </Row>
          <ContactInformation
            onSaveAddress={this.onSaveAddress}
          />
          <div className="row justify-content-center">
            <LaddaButton
              className="btn btn-primary btn-ladda m-2"
              loading={expZoomIn}
              onClick={() => this.saveTenantEntity("expZoomIn")}
              data-color="red"
              data-style={ZOOM_IN}
            >
              <span className="fa fa-save"/>  Save Firm Details
            </LaddaButton>
          </div>
        </div>
      )
    }
}

export default connect(({ auth }) => ({ auth }),{signOut})(withRouter(Tenant))
