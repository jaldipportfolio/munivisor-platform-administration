import React, { Component } from "react"
import { Col, FormGroup, Input, Label, Row } from "reactstrap"
import { AppSwitch } from "@coreui/react"
import Select from "react-select"
import "react-select/dist/react-select.min.css"
import {toast} from "react-toastify"
import LaddaButton, {ZOOM_IN} from "react-ladda"
import * as qs from "query-string"
import Accordion from "../../../GlobalComponents/Accordion"
// eslint-disable-next-line import/named
import { getTenantList } from "../../../Services/actions/Dashboard"
import { getPicklistValues } from "../../../globalutilities/helpers"
import { saveEntityUserDetails } from "../../../Services/actions/User"
import Loader from "../../../GlobalComponents/Loader"
import "react-toastify/dist/ReactToastify.css"
import {connect} from "react-redux"
import {signOut} from "../../../Services/actions/LoginAuthentication"
import {withRouter} from "react-router-dom"

const emailRegx = /^([a-zA-Z0-9_\-\.]+)@(?!gmail|hotmail|yahoo|rediffmail|msn|ymail|outlook|me|live|aol|facebook|free)((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/i // eslint-disable-line

class User extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userLastName: "",
      userEmailId: "",
      userMiddleName: "",
      userFirstName: "",
      userEntitlement: "tran-edit",
      userStatus: "active",
      errors: {},
      relatedEntities: [],
      entity: {},
      userFlags: [],
      userFlagsList: [],
      loading: true
    }
  }

  componentWillMount(){
    this.getTenantList()
    this.getContactList()
  }

    getTenantList = async () =>{
      const payload = {
        filters: {
          userContactTypes: [],
          entityMarketRoleFlags:[],
          entityIssuerFlags:[],
          entityPrimaryAddressStates:[],
          freeTextSearchTerm:""
        },
        pagination: {
          serverPerformPagination:true,
          currentPage:0,
          size:25,
          sortFields:{
            entityName:1
          }
        }
      }
      const result = await getTenantList(payload)
      const data = await result.data
      if (data && data.pipeLineQueryResults[0] && data.pipeLineQueryResults[0] && data.pipeLineQueryResults[0].data) {
        const relatedEntities = data.pipeLineQueryResults[0].data.map(e => ({
          entityId: e.entityId,
          entityName: e.entityName,
          entityType: e.entityRelationshipType
        }))
        this.setState({relatedEntities}, () => this.getQueryData())
      } else {
        toast.error("Something went wrong!", {
          position: toast.POSITION.TOP_RIGHT
        })
        this.setState({
          loading: false
        })
        this.props.signOut()
      }
    }

    getQueryData = () => {
      const { location } = this.props
      const queryString = qs.parse(location.search)
      const firmId = (queryString && queryString.firmId) || ""
      if(firmId){
        const { relatedEntities } = this.state
        const entityDetails = relatedEntities && relatedEntities.length && relatedEntities.find(r => r.entityId === firmId)
        if(entityDetails && entityDetails.entityId){
          const { entityName, entityId, entityType } = entityDetails
          this.setState({
            entity: {
              entityName,
              entityId,
              entityType
            }
          })
        }
      } else {
        this.setState({
          loading: false
        })
      }
    }

    getContactList = async () =>{
      const result = await getPicklistValues(["LKUPUSERFLAG"])
      if(result && result[0] && result[0][1] && result[0][1].length){
        const userFlags = result[0][1].map(e => ({
          value: e,
          label: e,
        }))
        this.setState({
          userFlagsList: userFlags || [],
          loading: false
        })
      } else {
        this.setState({
          loading: false
        })
        this.props.signOut()
      }
    }

    onUserFlagsChanges = (value) => {
      this.setState({
        userFlags: value
      })
    }

    savePrimaryDetails = async () => {
      const { userLastName, userEmailId, userMiddleName, userFirstName, userEntitlement, userStatus, entity, userFlags, errors } = this.state
      const { history } = this.props
      const userData  = userFlags.map(q => q.label)
      const payload = {
        entityId: (entity && entity.entityId) || "",
        userLastName,
        userFirmName: (entity && entity.entityName) || "",
        userMiddleName,
        userFirstName,
        userEntitlement,
        userStatus,
        userFlags: userData || [],
        userEmails: [{ emailId: userEmailId, emailPrimary: true}],
        isMuniVisorClient: false

      }

      if(!(entity && entity.entityId) || !userFirstName || !userLastName || (errors && Object.keys(errors).length)){
        if(!(entity && entity.entityId)){
          this.setState(prevState => ({
            errors: {
              ...prevState.errors,
              userFirmName: "Firm Name is Required!"
            }
          }))
        }
        if(!userFirstName){
          this.setState(prevState => ({
            errors: {
              ...prevState.errors,
              userFirstName: "First Name is Required!"
            }
          }))
        }
        if(!userLastName){
          this.setState(prevState => ({
            errors: {
              ...prevState.errors,
              userLastName: "Last Name is Required!"
            }
          }))
        }
        if(!userEmailId){
          this.setState(prevState => ({
            errors: {
              ...prevState.errors,
              userEmailId: (errors && errors.userEmailId) || "Primary Email is Required!"
            }
          }))
        }
        toast.error("Field is Required!", {
          position: toast.POSITION.TOP_RIGHT
        })
        return
      }

      if(!(errors && Object.keys(errors).length)){
        this.setState({
          expZoomIn: true,
          loading: true
        }, async () => {
          const res = await saveEntityUserDetails(payload)
          if(res && res.done){
            toast.success("Create User successfully", {
              position: toast.POSITION.TOP_RIGHT
            })
            if(res && res.sendEmailResponse && res.sendEmailResponse.done){
              toast.success("Email sent successfully", {
                position: toast.POSITION.TOP_RIGHT
              })
            }
            this.setState({
              expZoomIn: false,
              loading: false
            })
            setTimeout(() => {
              history.push("/dashboard")
            }, 2000)
          }else {
            toast.error((res && res.message) || "Something went wrong!", {
              position: toast.POSITION.TOP_RIGHT
            })
            this.setState({
              expZoomIn: false,
              loading: false
            })
          }
        })
      }
    }

    changeField = (e) => {
      const { errors } = this.state
      const { name, value } = e.target
      if(name === "entityName"){
        const { relatedEntities } = this.state
        const entityDetails = relatedEntities && relatedEntities.find(r => r.entityId === value)
        const { entityName, entityId, entityType } = entityDetails
        this.setState({
          entity: {
            entityName,
            entityId,
            entityType
          }
        }, () => {
          if(name === "entityName" && value){
            delete errors.userFirmName
            this.setState({
              errors
            })
          }
        })
      } else {
        this.setState({
          [name]: value
        }, () => {
          if(name === "userEmailId" && value){
            errors.userEmailId = !emailRegx.test(value) ? "Enter a valid email & not enter a email which contain public domain(ex gmail, yahoo)" : ""
            if(!(errors && errors.userEmailId)){
              delete errors.userEmailId
            }
          }
          if(name === "userFirstName" && value){
            delete errors.userFirstName
          }
          if(name === "userLastName" && value){
            delete errors.userLastName
          }
          this.setState({
            errors
          })
        })
      }
    }

    changeUserStatus = () => {
      this.setState(prevState => {
        let userStatus = "inactive"
        const prevStatus = prevState.userStatus
        if (prevStatus === "inactive") {
          userStatus = "active"
        }
        console.log("userStatus : ", userStatus)
        return { userStatus }
      })
    }

    render() {
      const {userLastName, userEmailId, userMiddleName, userFirstName, userEntitlement, userStatus, relatedEntities,
        userFlags, userFlagsList, expZoomIn, entity, loading, errors } = this.state
      
      return (
        <div className="animated fadeIn">
          { loading ? <Loader/> : null }
          <Row>
            <Col xs="12" sm="12">
              <Accordion
                activeAccordionHeader="Primary Details">
                <div>
                  <FormGroup row className="my-0">
                    <Col xs="12" md="4">
                      <FormGroup>
                        <Label>
                          <b>Firm Name<span className="text-danger text-monospace"> *</span></b>
                        </Label>
                        <Input type="select" name="entityName" value={ (entity && entity.entityId) || "" } onChange={(e) => this.changeField(e, entity)}>
                          <option value="" disabled>Select Firm Name</option>
                          { relatedEntities && relatedEntities.length && relatedEntities.map((entity, i) => {
                            return(
                              <option key={i.toString()} value={entity.entityId}>{entity.entityName}</option>
                            )
                          }) }
                        </Input>
                        { errors && errors.userFirmName ?
                          <small className="text-danger">
                            {errors.userFirmName}
                          </small>
                          : null
                        }
                      </FormGroup>
                    </Col>
                    <Col xs="12" md="6">
                      <FormGroup/>
                    </Col>
                    <Col xs="12" md="2">
                      <FormGroup>
                        <Label><b>Active / Inactive</b></Label>
                        <AppSwitch
                          label
                          className="mx-1"
                          variant="pill"
                          color="primary"
                          name="userstatus"
                          checked={userStatus !== "inactive"}
                          onChange={this.changeUserStatus}/>
                      </FormGroup>
                    </Col>
                  </FormGroup>
                  <FormGroup row className="my-0">
                    <Col xs="12" md="4">
                      <FormGroup>
                        <Label>
                          <b>First Name<span className="text-danger text-monospace"> *</span></b>
                        </Label>
                        <Input
                          type="text"
                          placeholder="First Name"
                          name="userFirstName"
                          value={userFirstName}
                          onChange={this.changeField}/>
                        { errors && errors.userFirstName ?
                          <small className="text-danger">
                            {errors.userFirstName}
                          </small>
                          : null
                        }
                      </FormGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <FormGroup>
                        <Label><b>Middle Name</b></Label>
                        <Input
                          id="postal-code"
                          type="text"
                          placeholder="Middle Name"
                          name="userMiddleName"
                          value={userMiddleName}
                          onChange={this.changeField}/>
                      </FormGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <FormGroup>
                        <Label>
                          <b>Last Name<span className="text-danger text-monospace"> *</span></b>
                        </Label>
                        <Input
                          id="postal-code"
                          type="text"
                          placeholder="Last Name"
                          name="userLastName"
                          value={userLastName}
                          onChange={this.changeField}
                        />
                        { errors && errors.userLastName ?
                          <small className="text-danger">
                            {errors.userLastName}
                          </small>
                          : null
                        }
                      </FormGroup>
                    </Col>
                  </FormGroup>
                  <FormGroup row className="my-0">
                    <Col xs="12" md="4">
                      <FormGroup>
                        <Label>
                          <b>Primary Email<span className="text-danger text-monospace"> *</span></b>
                        </Label>
                        <Input
                          id="hf-email"
                          type="email"
                          autoComplete="email"
                          placeholder="Primary Email"
                          name="userEmailId"
                          value={userEmailId}
                          onChange={this.changeField}
                        />
                        { errors && errors.userEmailId ?
                          <small className="text-danger">{errors.userEmailId}</small>
                          : null
                        }
                      </FormGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <FormGroup>
                        <Label>
                          <b>System Access Level<span className="text-danger text-monospace"> *</span></b>
                        </Label>
                        <Input
                          id="select"
                          type="select"
                          name="userEntitlement"
                          value={userEntitlement}
                          onChange={this.changeField}>
                          <option value="global-edit">global-edit</option>
                          <option value="global-readonly">global-readonly</option>
                          <option value="tran-edit">tran-edit</option>
                          <option value="tran-readonly">tran-readonly</option>
                          <option value="global-readonly-tran-edit">global-readonly-tran-edit</option>
                        </Input>
                        { errors && errors.userEntitlement ?
                          <small className="text-danger">
                            {errors.userEntitlement}
                          </small>
                          : null
                        }
                      </FormGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <FormGroup>
                        <Label><b>Contact Type</b></Label>
                        <Select
                          multi
                          closeOnSelect={false}
                          name="userFlags"
                          value={userFlags}
                          options={userFlagsList}
                          onChange={this.onUserFlagsChanges}
                          style={{border: "1px solid darkturquoise"}}
                        />
                      </FormGroup>
                    </Col>
                  </FormGroup>
                  <div className="row justify-content-center">
                    <LaddaButton
                      className="btn btn-primary btn-ladda"
                      loading={expZoomIn}
                      onClick={() => this.savePrimaryDetails()}
                      data-color="red"
                      data-style={ZOOM_IN}
                    >
                      Save Primary Details
                    </LaddaButton>
                  </div>
                </div>
              </Accordion>
            </Col>
          </Row>
        </div>
      )
    }
}

export default connect(({ auth }) => ({ auth }),{signOut})(withRouter(User))
