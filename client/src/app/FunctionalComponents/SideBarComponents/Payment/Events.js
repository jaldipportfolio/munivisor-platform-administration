import React from "react"
import { Col, FormGroup, Row, Button } from "reactstrap"
import Select from "react-select"
import "react-select/dist/react-select.min.css"
import ReactTable from "react-table"
import {toast} from "react-toastify"
import {DateRangePicker} from "react-dates"
import "react-table/react-table.css"
import "react-dates/lib/css/_datepicker.css"
import "react-dates/initialize"
import { getEvents } from "../../../Services/actions/Invoice"
import Accordion from "../../../GlobalComponents/Accordion"
import Loader from "../../../GlobalComponents/Loader"

const eventOption = [
  { value: "charge.succeeded", label: "Charge succeeded" },
  { value: "charge.refunded", label: "Charge refunded" },
  { value: "invoice.payment_failed", label: "Invoice payment failed" },
  { value: "customer.created", label: "Customer created" },
]

class Events extends React.Component{
  constructor(props) {
    super(props)
    this.state = {
      eventList: [],
      totalEvents: "",
      loading: true,
      eventListShow: [],
      types: [],
      startDate: null,
      endDate: null,
      startPage: 0,
      columnDefs : [
        {
          headerName: "Date",
          field: "date",
        },
        {
          headerName: "Name",
          field: "name",
        },
        {
          headerName: "Email",
          field: "email",
        },
        {
          headerName: "Description",
          field: "description",
        },
        {
          headerName: "Amount",
          field: "amount",
          valueFormatter: params => (
            (params && params.value) >= 0 ? `$${parseFloat(params.value/100)}` : "-"
          ),
        },
        {
          headerName: "Amount Refunded",
          field: "amountRefunded",
          valueFormatter: params => (
            (params && params.value) >= 0 ? `$${parseFloat(params.value/100)}` : "-"
          ),
        },
        {
          headerName: "Receipt",
          field: "receiptUrl",
          cellRendererFramework:  row => {
            const item = row.data
            return (
              <div className="hpTablesTd -striped">
                {
                  item && item.receiptUrl ?
                    <a target="_blank" title="Receipt" href={item.receiptUrl} rel="noopener noreferrer">
                      Get receipt
                    </a>
                    : ""
                }
              </div>
            )
          }
        }
      ]
    }
  }

  handleInputChange = async (value) =>{
    const { page } = this.state
    this.setState({
      types: value
    },()=>{
      this.getFilteredData(page.global, "selectType")
    })
  }

  getFilteredData = async (pageDetails, data) =>{
    this.setState({loading: true,  page: {global: pageDetails}  })
    const { page, pageSize, sorted } = pageDetails
    const { types, eventListShow, startDate, endDate, startPage} = this.state
    const ind = eventListShow.length - 1
    const starting_after = (eventListShow && eventListShow[ind] && eventListShow[ind].id)
    const ending_before = (eventListShow && eventListShow[0] && eventListShow[0].id)
    const newDate = Math.floor((new Date(startDate)).getTime() / 1000)
    const oldDate = Math.floor((new Date(endDate)).getTime() / 1000)

    const isPage = {
      types: types.map(i => i.value),
      page,
      starting_after,
      ending_before,
      pageSize,
      sorted,
      created: {gte: newDate, lte: oldDate}
    }

    if(!startDate){
      delete isPage.created.gte
    }

    if(!endDate){
      delete isPage.created.lte
    }

    // if((pageStatus < page) || !pageStatus || (page === 0) || ((isPage && isPage.starting_after) && (isPage && isPage.ending_before))){
    //   delete isPage.ending_before
    // }
    // if((pageStatus > page) || (page === 0)){
    //   delete isPage.starting_after
    // }

    const startRow = pageSize * page

    if(startPage > startRow){
      delete isPage.starting_after
    }
    if(startPage < startRow){
      delete isPage.ending_before
    }

    if(page === 0){
      delete isPage.ending_before
      delete isPage.starting_after
    }

    if(data){
      delete isPage.ending_before
      delete isPage.starting_after
    }

    if((isPage && isPage.ending_before) && (isPage && isPage.starting_after)){
      delete isPage.ending_before
    }

    const res = await getEvents(isPage)
    if((res && res.data)){
      const dataLst = this.isSordData(res.data, isPage.sorted)
      const totalPage = Math.ceil((res.total)/pageSize)
      this.setState({
        eventListShow: dataLst || [],
        loading: false,
        pageStatus: page,
        totalEvents: totalPage || "",
        startPage:  pageSize * page
      })
    }else {
      toast.error("Something went wrong!", {
        position: toast.POSITION.TOP_RIGHT
      })
      this.setState({
        loading: false,
      })
    }
  }

  isSordData = (eventListShow, sorted) =>{
    if(sorted && sorted.length){
      eventListShow.sort((a, b) => {
        const sortKey = sorted[0] || ""
        const keyA = ( typeof a[sortKey.id] === "string") ? (a[sortKey.id] && a[sortKey.id].toLowerCase()) : a[sortKey.id]
        const keyB = (typeof b[sortKey.id] === "string") ? (b[sortKey.id] && b[sortKey.id].toLowerCase()) : b[sortKey.id]
        if (sortKey.desc) {
          if (keyA > keyB)
            return -1
          if (keyA < keyB)
            return 1
          return 0
        }
        if (keyA < keyB)
          return -1
        if (keyA > keyB)
          return 1
        return 0

      })
      return eventListShow
    }
    return eventListShow
  }

  render(){
    const { eventListShow, loading, types, totalEvents, startDate, endDate, focusedInput, orientation, openDirection, page} = this.state
    const amountRefunded = []
    const refunded = types.filter(o => o.value === "charge.refunded")
    if(refunded.length || !types.length){
      amountRefunded.push({
        Header: "Amount Refunded",
        id: "amountRefunded",
        className: "multiExpTblVal",
        accessor: item => item,
        minWidth: 100,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd -striped">
              <div className="hpTablesTd -striped">
                {(item && item.amountRefunded) >= 0 ? `$${parseFloat(item.amountRefunded/100)}` : "-"}
              </div>
            </div>
          )
        },
        sortMethod: (a, b) => `${a.amountRefunded}` - `${b.amountRefunded}`
      },)
    }

    return(
      <div className="animated fadeIn payment-events">
        { loading ? <Loader/> : null }
        <Row>
          <Col xs="12" sm="12">
            <Accordion activeAccordionHeader="Invoices">
              <Row form>
                <Col md={6}>
                  <FormGroup>
                    <Select
                      multi
                      closeOnSelect={false}
                      name="types"
                      value={types}
                      options={eventOption}
                      onChange={this.handleInputChange}
                      style={{border: "1px solid darkturquoise"}}
                    />
                  </FormGroup>
                </Col>
                <Col md={6}>
                  <FormGroup>
                    <DateRangePicker
                      small
                      showClearDates
                      endDate={endDate}
                      endDateId="endDate"
                      showDefaultInputIcon
                      startDate={startDate}
                      startDateId="startDate"
                      orientation={orientation}
                      focusedInput={focusedInput}
                      isOutsideRange={() => false}
                      openDirection={openDirection}
                      onFocusChange={focusedInput => this.setState({focusedInput})}
                      onDatesChange={({startDate, endDate}) => this.setState({startDate, endDate},()=>{ this.getFilteredData(page.global) })}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <br/>
              <ReactTable
                columns={[
                  {
                    Header: "Date",
                    id: "date",
                    className: "multiExpTblVal",
                    accessor: item => item,
                    minWidth: 100,
                    Cell: row => {
                      const item = row.value
                      return (
                        <div className="hpTablesTd wrap-cell-text">
                          <div className="hpTablesTd -striped">
                            {(item && item.date) || "-"}
                          </div>
                        </div>
                      )
                    },
                    sortMethod: (a, b) => `${a.date}` - `${b.date}`
                  },
                  {
                    Header: "Name",
                    id: "name",
                    className: "multiExpTblVal",
                    accessor: item => item,
                    minWidth: 100,
                    Cell: row => {
                      const item = row.value
                      return (
                        <div className="hpTablesTd wrap-cell-text">
                          <div className="hpTablesTd -striped">
                            {(`${item && item.name && item.name.toString()}`) || "-"}
                          </div>
                        </div>
                      )
                    },
                    sortMethod: (a, b) => `${a.name}` - `${b.name}`
                  },
                  {
                    Header: "Email",
                    id: "email",
                    className: "multiExpTblVal",
                    accessor: item => item,
                    minWidth: 100,
                    Cell: row => {
                      const item = row.value
                      return (
                        <div className="hpTablesTd wrap-cell-text">
                          <div className="hpTablesTd -striped">
                            {(item && item.email && item.email.toString()) || "-"}
                          </div>
                        </div>
                      )
                    },
                    sortMethod: (a, b) => `${a.email}` - `${b.email}`
                  },
                  {
                    Header: "Description",
                    id: "description",
                    className: "multiExpTblVal",
                    accessor: item => item,
                    minWidth: 100,
                    Cell: row => {
                      const item = row.value
                      return (
                        <div className="hpTablesTd -striped">
                          <div className="hpTablesTd -striped">
                            {(item && item.description && item.description.toString()) || "-"}
                          </div>
                        </div>
                      )
                    },
                    sortMethod: (a, b) => `${a.description}` - `${b.description}`
                  },
                  {
                    Header: "Amount",
                    id: "amount",
                    className: "multiExpTblVal",
                    accessor: item => item,
                    minWidth: 100,
                    Cell: row => {
                      const item = row.value
                      return (
                        <div className="hpTablesTd -striped">
                          <div className="hpTablesTd -striped">
                            {(item && item.amount) >= 0 ? `$${parseFloat(item.amount/100)}` : "-"}
                          </div>
                        </div>
                      )
                    },
                    sortMethod: (a, b) => `${a.amount}` - `${b.amount}`
                  },
                  ...amountRefunded,
                  {
                    Header: "Receipt",
                    id: "receiptUrl",
                    className: "multiExpTblVal",
                    sortable: false,
                    accessor: item => item,
                    minWidth: 65,
                    Cell: row => {
                      const item = row.value
                      return (
                        <div className="hpTablesTd -striped">
                          {
                            item && item.receiptUrl ?
                              <a target="_blank" title="Preview" href={item.receiptUrl} rel="noopener noreferrer">
                                <Button color="secondary btn-brand">Get receipt </Button>
                              </a>
                              : ""
                          }
                        </div>
                      )
                    }
                  }
                ]}
                manual
                data={eventListShow || []}
                showPaginationBottom
                defaultPageSize={10}
                className="-striped -highlight is-bordered"
                style={{ overflowX: "auto" }}
                minRows={2}
                pages={totalEvents}
                showPageJump={false}
                onFetchData={state => {
                  this.getFilteredData(state)
                }}
              />
            </Accordion>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Events