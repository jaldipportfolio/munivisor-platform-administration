import React, { Component } from "react"
import {toast} from "react-toastify"
import {Button, Col, FormGroup, Input, InputGroup, InputGroupAddon, Row} from "reactstrap"
import ReactTable from "react-table"
import {Link, withRouter} from "react-router-dom"
import Accordion from "../../../GlobalComponents/Accordion"
// eslint-disable-next-line import/named
import { getTenantList } from "../../../Services/actions/Dashboard"
import "react-table/react-table.css"
import Loader from "../../../GlobalComponents/Loader"
import {connect} from "react-redux"
import { signOut } from "../../../Services/actions"


const highlightSearch = (string, searchQuery) => {
  const reg = new RegExp(
    searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&"),
    "i"
  )
  return string.replace(reg, str => `<mark>${str}</mark>`)
}

class Dashboard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      entitySearch: "",
      total: 0,
      loading: true
    }
  }

    decoupleResult = result => ({
      total: result && result.metadata.length > 0 ? result.metadata[0].total : 0,
      pages: result && result.metadata.length > 0 ? result.metadata[0].pages : 0,
      data: result ? result.data : []
    })

    getFilteredData = async (pageDetails) => {
      this.setState({loading: true,  page: {global: pageDetails}  })
      const { entitySearch } = this.state
      const { page, pageSize, sorted } = pageDetails
      let sortedFields = {}
      if (sorted && sorted[0]) {
        sortedFields[sorted[0].id] = sorted[0].desc ? -1 : 1
        this.setState({ sortedFields })
      } else {
        sortedFields = this.state.sortedFields
        if (sortedFields === undefined) {
          sortedFields = { entityName: 1 }
        }
      }

      const payload = {
        filters: {
          userContactTypes: [],
          entityMarketRoleFlags:[],
          entityIssuerFlags:[],
          entityPrimaryAddressStates:[],
          freeTextSearchTerm: entitySearch || ""
        },
        pagination: {
          serverPerformPagination:true,
          currentPage: page,
          size: pageSize,
          sortFields: sortedFields
        }
      }

      const result = await getTenantList(payload)
      const data = await result.data
      if (data) {
        const decoupledValue = this.decoupleResult(data && data.pipeLineQueryResults && data.pipeLineQueryResults[0])
        this.setState({
          loading: false,
          entityList: decoupledValue.data,
          total: decoupledValue.total || 0,
          pages: decoupledValue.pages,
        })
      } else {
        toast.error("Something went wrong!", {
          position: toast.POSITION.TOP_RIGHT
        })
        this.setState({
          loading: false
        })
        this.props.signOut()
      }
    }

    onEntitySearch = (e) => {
      const { name, value } = e.target
      const { page } = this.state
      this.setState({
        [name]: value
      }, () => this.getFilteredData(page.global))
    }

    render() {
      const { entityList, pages, entitySearch, loading, total } = this.state

      return (
        <div className="animated fadeIn">
          { loading ? <Loader/> : null }
          <Row>
            <Col xs="12" sm="12">
              <Accordion
                activeAccordionHeader="Tenants List"
                count={total || 0}
                showCount
              >

                <FormGroup row className="my-0">
                  <Col xs="12" md="6">
                    <FormGroup/>
                  </Col>
                  <Col xs="12" md="6">
                    <div className="controls">
                      <InputGroup>
                        <Input type="text" name="entitySearch" onChange={this.onEntitySearch} placeholder="Search..." />
                        <InputGroupAddon addonType="append">
                          <Button color="secondary"><i className="fa fa-search"/></Button>
                        </InputGroupAddon>
                      </InputGroup>
                    </div>
                  </Col>
                </FormGroup>

                <br/>
                <div>
                  <ReactTable
                    columns={[
                      {
                        Header: "Full Name",
                        id: "userFirstName",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        minWidth: 100,
                        Cell: row => {
                          const item = row.value
                          const fullName = `${item.userFirstName || ""} ${item.userLastName || ""}`
                          return (
                            <div className="hpTablesTd -striped">
                              <div
                                dangerouslySetInnerHTML={{
                                  __html: highlightSearch(fullName || "-", entitySearch)
                                }}
                              />
                            </div>
                          )
                        },
                        sortMethod: (a, b) => `${a.userFirstName}` - `${b.userFirstName}`
                      },
                      {
                        Header: "Associated Entity",
                        id: "entityName",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        minWidth: 150,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="hpTablesTd wrap-cell-text">
                              <Link to={`/tenant/firm?firmId=${item._id}&name=${item && item.entityName}`}
                                dangerouslySetInnerHTML={{
                                  __html: highlightSearch(`${item && item.entityName && item.entityName.toString()}` || "-", entitySearch)
                                }}>
                              </Link>
                            </div>
                          )
                        },
                        sortMethod: (a, b) => a.entityName !== undefined && b.entityName !== undefined ? a.entityName.localeCompare(b.entityName) : ""
                      },
                      {
                        Header: "Primary Email",
                        id: "userPrimaryEmail",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        minWidth: 150,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="hpTablesTd wrap-cell-text">
                              <div
                                dangerouslySetInnerHTML={{
                                  __html: highlightSearch((item && item.userPrimaryEmail && item.userPrimaryEmail.toString()) || "-", entitySearch)
                                }}
                              />
                            </div>
                          )
                        },
                        sortMethod: (a, b) => `${a.userPrimaryEmail}` - `${b.userPrimaryEmail}`
                      },
                      {
                        Header: "Primary Phone",
                        id: "userPrimaryPhoneNumber",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        minWidth: 100,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="hpTablesTd wrap-cell-text">
                              <div
                                dangerouslySetInnerHTML={{
                                  __html: highlightSearch((item && item.userPrimaryPhoneNumber && item.userPrimaryPhoneNumber.toString()) || "-", entitySearch)
                                }}
                              />
                            </div>
                          )
                        },

                        sortMethod: (a, b) => `${a.userPrimaryPhoneNumber}` - `${b.userPrimaryPhoneNumber}`
                      },
                      {
                        Header: "Primary Address",
                        id: "entityPrimaryAddressGoogle",
                        className: "entityPrimaryAddressGoogle",
                        accessor: item => item,
                        minWidth: 100,
                        Cell: row => {
                          const item = row.value
                          const url = (item && item.entityPrimaryAddressGoogle) ? `https://maps.google.com/?q=${item && item.entityPrimaryAddressGoogle}` : ""
                          return (
                            <div className="hpTablesTd wrap-cell-text">
                              {
                                url ?
                                  <a
                                    href={url || ""} target="_blank" rel="noopener noreferrer"
                                    dangerouslySetInnerHTML={{
                                      __html: highlightSearch(`${item && item.entityPrimaryAddressGoogle}` || "-", entitySearch)
                                    }}
                                  /> :
                                  <span>-</span>
                              }
                            </div>
                          )
                        },
                        sortMethod: (a, b) => `${a.entityPrimaryAddressGoogle}` - `${b.entityPrimaryAddressGoogle}`
                      }
                    ]}
                    data={entityList || []}
                    manual
                    showPaginationBottom
                    defaultPageSize={10}
                    pageSizeOptions={[5, 10, 20, 50, 100]}
                    className="-striped -highlight is-bordered"
                    style={{ overflowX: "auto" }}
                    pages={pages}
                    showPageJump
                    minRows={2}
                    onFetchData={state => {
                      this.getFilteredData(state)
                    }}
                  />
                </div>
              </Accordion>
            </Col>
          </Row>
        </div>
      )
    }
}

export default connect(({ auth }) => ({ auth }),{signOut})(withRouter(Dashboard))
