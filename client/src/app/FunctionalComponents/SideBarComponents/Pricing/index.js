import React, { Component } from "react"
// eslint-disable-next-line import/named
import "react-table/react-table.css"
import Loader from "../../../GlobalComponents/Loader"
import SectionAccordion from "../../../GlobalComponents/SectionAccordion"
import Section from "../../../GlobalComponents/Section"


class Pricing extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      prices: [
        {label: "6 to 10 users", price: "$2,250/month"},
        {label: "11 to 15 users", price: "$3,375/month"},
        {label: "16 to 20 users", price: "$4,500/month"},
        {label: "21 to 25 users", price: "$5,625/month"},
        {label: "More than 25 users", price: "Please call"}
      ]
    }
  }

  componentDidMount() {
    this.setState({
      loading: false
    })
  }

  render() {
    const { loading, prices } = this.state

    return (
      <div>
        { loading ? <Loader/> : null }
        <SectionAccordion
          multiple
          activeItem={[0]}
          isRequired
          boxHidden
          render={({activeAccordions, onAccordion}) =>
            <div>
              <Section
                onAccordion={() => onAccordion(0)}
                title="Pricing"
              >
                {activeAccordions.includes(0) &&
                <div>
                  <p>Subscription Pricing Options</p>
                  <p>Registered Municipal Advisory (MA) firms can choose to pay a monthly subscription fee under two different plan types:</p>
                  <br/>
                  <p>Per user at $250 per user per month, OR  flat fee for an enterprise license, which varies depending on enterprise size, as follows: </p>
                  <table className="table table-striped">
                    <thead>
                      <tr>
                        <th className="center">Number of users</th>
                        <th className="center">Price</th>
                      </tr>
                    </thead>
                    <tbody>
                    {
                      prices.map(pricing => (
                        <tr key={pricing.label}>
                          <td className="center">{pricing.label}</td>
                          <td className="center">{pricing.price}</td>
                        </tr>
                      ))
                    }
                    </tbody>
                  </table>

                  <p>Billing is quarterly, however a 10% discount is available for firms that choose to pay the entire annual fee upfront each year.</p>

                  <p>Based on our strong relationship with the National Association of Municipal Advisors (NAMA), NAMA members are also eligible for a 20% discount off of the list price.</p>
                </div>
                }
              </Section>
            </div>
          }
        />

      </div>
    )
  }
}

export default Pricing
