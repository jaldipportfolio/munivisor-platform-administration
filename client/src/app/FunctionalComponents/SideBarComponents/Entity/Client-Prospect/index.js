import React, { Component } from "react"
import { Button, Col, FormGroup, Input, Label, Row, InputGroup, InputGroupAddon } from "reactstrap"
import "react-select/dist/react-select.min.css"
import Select from "react-select"
import Accordion from "../../../../GlobalComponents/Accordion"
import EntityLookUp from "../../../../GlobalComponents/EntityLookUp"
import { getPicklistValues } from "../../../../globalutilities/helpers"
import Loader from "../../../../GlobalComponents/Loader"


class ClientProspect extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      dropDown: {}
    }
  }

  async componentDidMount(){
    const result = await getPicklistValues([
      "LKUPENTITYTYPE",
      "LKUPPRIMARYSECTOR",
      "LKUPISSUERFLAG",
      "LKUPDEBTTYPE",
      "LKUPCRMBORROWER",
      "LKUPLEGALTYPE",
      "LKUPPARTICIPANTTYPE"
    ])
    const entityTypes = result[0] && result[0][1] ? result[0][1] : []
    const primarySectorsList = result[1] && result[1][1] ? result[1][1] : []
    const secondarySectorsList = result[1] && result[1][2] ? result[1][2] : {}
    const issuerFlagsList = result[2] && result[2][1] ? result[2][1] : []
    const flagsList = issuerFlagsList && issuerFlagsList.length && issuerFlagsList.map(issuer => ({label: issuer, value: issuer}))
    this.setState(prevState => ({
      dropDown: {
        ...prevState.dropDown,
        entityTypes,
        primarySectorsList,
        secondarySectorsList,
        issuerFlagsList: flagsList
      },
      loading: false
    }))
  }

    changeField = (e) => {
      const { name, type } = e.target
      const value = type === "checkbox"
        ? e.target.checked
        : e.target.value
      this.setState(prevState => {
        const error = {
          ...prevState.error
        }
        error[name] = ""
        if (name === "taxId" && value.replace(/[^0-9]/g, "").length !== 9) {
          error.taxId = "Tax Id must be 9 digits"
        }
        let issuerFlags = [...prevState.issuerFlags]
        const { currentEntity } = prevState
        if (name === "entityType") {
          if (!["Governmental Entity / Issuer", "501c3 - Obligor"].includes(value)) {
            issuerFlags = []
          } else if (!issuerFlags.length && currentEntity.entityFlags && currentEntity.entityFlags.issuerFlags) {
            issuerFlags = [...currentEntity.entityFlags.issuerFlags]
          }
        }
        return { [name]: value, issuerFlags, error }
      })
    }

    changeIssuerFlags = (issuerFlags) => {
      this.setState({ issuerFlags })
    }

    addAlias = () => {
      console.log("in addAlias")
      this.setState(prevState => {
        const error = {
          ...prevState.error
        }
        const entityAliases = [...prevState.entityAliases || []]
        if (entityAliases.length) {
          const emptyAliasIdx = this.checkEmptyArray(entityAliases)
          if(emptyAliasIdx > -1) {
            error.entityAliases = { ...error.entityAliases }
            error.entityAliases[emptyAliasIdx] = "Please provide a value here or reset before adding another Alias"
            return { error }
          }
        }
        error.entityAliases = ""
        entityAliases.unshift("")
        console.log("entityAliases : ", entityAliases)
        return { entityAliases, error }
      })
    }

    checkEmptyArray = (arr) => {
      let result = -1
      arr.some((e, i) => {
        if(!e) {
          result = i
          return true
        }
        return null
      })
      return result
    }

    onChange = (e) => {
      this.setState({
        [e.target.name]: e.target.value
      })
    }

    onChangeEntityName = (entityName) =>  {
      this.setState(prevState => {
        const error = {
          ...prevState.error
        }
        error.entityName = ""
        return { entityName, error }
      })
    }

    render() {
      const { dropDown, primarySectors, issuerFlags, entityAliases, currentEntity, entityType, error, entityName, loading } = this.state
      const { entityTypes, primarySectorsList, secondarySectorsList, issuerFlagsList } = dropDown

      if(loading){
        return <Loader/>
      }
      return (
        <div className="animated fadeIn">
          <Row>
            <Col xs="12" sm="12">
              <Accordion
                activeAccordionHeader="Entity Type">
                <div>
                  <FormGroup row className="my-0">
                    <Col xs="12" md="4">
                      <FormGroup>
                        <Label>Entity Type</Label>
                        <Input type="select" name="entityType" value={entityType} onChange={this.onChange}>
                          <option value>Select Entity Type</option>
                          { entityTypes && entityTypes.length && entityTypes.map(entity => {
                            return(
                              <option key={entity} value={entity}>{entity}</option>
                            )
                          }) }
                        </Input>
                      </FormGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <FormGroup>
                        <Label>Entity Name</Label>
                        <EntityLookUp
                          entityName={entityName}
                          onChange={this.onChangeEntityName}
                          participantType={entityType}
                          entityType="Prospects/Clients"
                          error={(error && error.entityName) || ""}
                          isRequired={false}
                        />
                      </FormGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <FormGroup>
                        <Label>Issuer Type</Label>
                        <Select
                          multi
                          placeholder="Select issuer types"
                          closeOnSelect={false}
                          value={issuerFlags || []}
                          filter="contains"
                          options={issuerFlagsList}
                          onChange={this.changeIssuerFlags}
                        />
                      </FormGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <FormGroup>
                        <Label>Last Name</Label>
                        <Input type="text" id="postal-code" placeholder="Last Name" />
                      </FormGroup>
                    </Col>
                  </FormGroup>
                  {entityAliases && entityAliases.length && entityAliases.map((a, i) => {
                    if (!(currentEntity && currentEntity.entityAliases && currentEntity.entityAliases.length) || (i < (entityAliases.length - currentEntity.entityAliases.length))) {
                      return (
                        <div key={i.toString()}>
                          <FormGroup row className="my-0">
                            <Col xs="12" md="4">
                              <FormGroup/>
                            </Col>
                            <Col xs="12" md="8">
                              <div className="controls">
                                <InputGroup>
                                  <Input type="text" placeholder="alias" />
                                  <InputGroupAddon addonType="append">
                                    <Button color="secondary"><i className="cui-delete icons"/></Button>
                                  </InputGroupAddon>
                                </InputGroup>
                              </div>
                              {error && error.entityAliases && error.entityAliases[i]
                                ? <p className="text-error has-text-danger">
                                  {error.entityAliases[i]}
                                </p>
                                : undefined
                              }
                            </Col>
                          </FormGroup>


                        </div>
                      )
                    }
                    return null
                  })}
                  <FormGroup row className="my-0">
                    <Col xs="12" md="5">
                      <FormGroup/>
                    </Col>
                    <Col xs="12" md="5">
                      <FormGroup/>
                    </Col>
                    <Col xs="12" md="2">
                      <FormGroup>
                        <Button color="primary" className="btn-sm" onClick={this.addAlias}>Add Alias</Button>&nbsp;
                        <Button color="secondary" className="btn-sm">Cancel</Button>
                      </FormGroup>
                    </Col>
                  </FormGroup>
                  <FormGroup row className="my-0">
                    <Col xs="12" md="4">
                      <FormGroup>
                        <Label for="exampleSelect" >Primary Sector</Label>
                        <Input type="select" name="primarySectors" id="exampleSelect" onChange={this.changeField}>
                          <option value>Select Entity Type</option>
                          { primarySectorsList && primarySectorsList.length && primarySectorsList.map(entity => {
                            return(
                              <option key={entity} value={entity}>{entity}</option>
                            )
                          }) }
                        </Input>
                      </FormGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <FormGroup>
                        <Label>Secondary Sector</Label>
                        <Input type="select" name="select" id="select">
                          <option value="">Select Secondary Sector</option>
                          { secondarySectorsList && secondarySectorsList[primarySectors] && secondarySectorsList[primarySectors].map(t => (
                            <option key={t} value={t}>
                              {t}
                            </option>
                          ))
                          }
                        </Input>
                      </FormGroup>
                    </Col>
                    <Col xs="12" md="4">
                      <FormGroup>
                        <Label>Tax Id</Label>
                        <Input type="text" placeholder="Enter Tax ID" />
                      </FormGroup>
                    </Col>
                  </FormGroup>
                  <div className="row justify-content-center">
                    <Button outline color="success">Save Primary Details</Button>
                  </div>
                </div>
              </Accordion>
            </Col>
          </Row>
        </div>
      )
    }
}

export default ClientProspect
