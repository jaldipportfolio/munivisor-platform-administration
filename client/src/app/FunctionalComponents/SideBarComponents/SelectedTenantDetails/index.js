import React, { Component } from "react"
import {connect} from "react-redux"
import {
  Col,
  Nav,
  NavItem,
  NavLink,
  Row,
  TabContent,
  TabPane
} from "reactstrap"
import * as qs from "query-string"
import Loader from "../../../GlobalComponents/Loader"
import FirmDetails from "./components/FirmDetails"
import BillingConfiguration from "../../SettingsComponents/DefaultBillingSettings/BillingConfiguration"
import SnapShots from "./components/SnapShots"
import UserList from "../../../GlobalComponents/UserList"
import Invoice from "../Invoices"
import Picklists from "./components/Picklists"
import ChecklistsMain from "./components/Checklists/ChecklistsMain"
import Configuration from "../../SettingsComponents/Configuration";
import ConfigInfo from "./components/ConfigInfo";

const TABS = [
  { name: "Firm", key: "firm" },
  { name: "Billing Setting", key: "billingSetting" },
  { name: "Users", key: "userslist" },
  { name: "Snap Shots", key: "snapshots" },
  { name: "Invoices", key: "invoices" },
  { name: "Picklists", key: "picklists" },
  { name: "Checklists", key: "checklists" },
  { name: "Configuration", key: "configuration" },
]

class TenantMainView extends Component {
  constructor(props) {
    super(props)
    this.state = {
      activeTab: "",
      firmId: "",
      loading: true,
      queryString: {}
    }
  }

  componentWillMount() {
    const { location } = this.props
    const tab = window.location.pathname.substring(1).split("/")
    const queryString = qs.parse(location.search)
    const firmId = (queryString && queryString.firmId) || ""
    this.setState({
      firmId,
      activeTab: tab && tab.length ? tab && tab[1] : "firm",
      loading: false,
      queryString
    })
  }

  toggle = (tab) => {
    const {queryString} = this.state
    this.setState({
      activeTab: tab
    }, () => {
      this.props.history.push("/")
      if(tab){
        this.props.history.push(`tenant/${tab}?firmId=${this.state.firmId}&name=${(queryString && queryString.name) || ""}`)
      }
    })
  }

  tabPane = (option) => {
    const { user } = this.props
    const { firmId, snapShotsList, queryString } = this.state
    switch (option) {
    case "firm":
      return (
        <TabPane>
          <FirmDetails firmId={firmId} {...queryString}/>
        </TabPane>
      )
    case "billingSetting":
      return (
        <TabPane>
          <BillingConfiguration {...queryString} {...user}/>
        </TabPane>
      )
    case "userslist":
      return (
        <TabPane>
          <UserList firmId={firmId} {...queryString} {...user}/>
        </TabPane>
      )
    case "snapshots":
      return (
        <TabPane>
          <SnapShots firmId={firmId} snapShotsList={snapShotsList} {...queryString} {...user}/>
        </TabPane>
      )
    case "invoices":
      return (
        <TabPane>
          <Invoice tenantId={firmId} {...queryString} {...user}/>
        </TabPane>
      )
    case "picklists":
      return (
        <TabPane>
          <Picklists tenantId={firmId} {...queryString} {...user}/>
        </TabPane>
      )
    case "checklists":
      return (
        <TabPane>
          <ChecklistsMain tenantId={firmId} {...queryString} {...user} />
        </TabPane>
      )
    case "configuration":
        return (
            <TabPane>
                <ConfigInfo tenantId={firmId}/>
            </TabPane>
        )
    default:
      return <p>{option}</p>
    }

  }


  render() {
    const { activeTab, loading, queryString } = this.state

    return (
      <div className="animated fadeIn">
        { loading ? <Loader/> : null }
        <div className="card">
          <div className="card-header">
            <b>{(queryString && queryString.name) || ""}</b>
          </div>
        </div>
        <Row>
          <Col xs="12" md="12" className="mb-4">
            <Nav tabs>
              {
                TABS.map((tab, index) => (
                  <NavItem key={index.toString()}>
                    <NavLink
                      active={activeTab === tab.key}
                      onClick={() => this.toggle(tab.key)}
                    >
                      {tab.name}
                    </NavLink>
                  </NavItem>
                ))
              }
            </Nav>
            <TabContent >
              {this.tabPane(activeTab)}
            </TabContent>
          </Col>
        </Row>
      </div>
    )
  }
}

export default connect(({ auth }) => {
  const user = (auth && auth.loginDetails) || {}
  if(Object.keys(user).length){
    user.userName = `${user.userFirstName || ""} ${user.userLastName || ""}`
  }
  return {
    user
  }
},null)(TenantMainView)