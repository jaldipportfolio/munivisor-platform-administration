import React, { Component } from "react"
import {toast} from "react-toastify"
import { Col, FormGroup, Label, Row } from "reactstrap"
import ReactTable from "react-table"
import { DateRangePicker } from "react-dates"
import moment from "moment"
import Loader from "../../../../GlobalComponents/Loader"
import Accordion from "../../../../GlobalComponents/Accordion"
// eslint-disable-next-line import/named
import { getSnapShotsList } from "../../../../Services/actions/Dashboard"
import "react-table/react-table.css"
import { SnapShotsTransactionModal } from "./SnapShotsTransactionModal"
import "react-dates/initialize"
import "react-dates/lib/css/_datepicker.css"


class SnapShots extends Component {
  constructor(props) {
    super(props)
    this.state = {
      snapShotsList: [],
      transactionWise: {},
      total: 0,
      isModal: false,
      isTran: false,
      isElastic: false,
      loading: true
    }
  }

  async componentDidMount() {
    this.updateDimensions()
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions = () => {
    const windowWidth = window.innerWidth
    this.setState({
      orientation: windowWidth < 620 ? "vertical" : "horizontal",
      openDirection: windowWidth < 620 ? "down" : "down"
    })
  }

  onModalChange = (state, total, type, level) => {
    if(type === "main"){
      if(level === "tran"){
        this.setState({
          isModal: !this.state.isModal,
          transactionWise: state || {},
          isTran: true,
          total,
          modalTitle: "Transactions"
        })
      } else {
        this.setState({
          isModal: !this.state.isModal,
          isElastic: true,
          elasticData: state || {},
          modalTitle: "Elasticsearch Storage"
        })
      }
    } else {
      this.setState({
        ...state,
        isTran: false,
        isElastic: false,
        transactionWise: {},
        elasticData: {},
        modalTitle: "",
        total: 0
      })
    }
  }

  onChange = (startDate, endDate) => {
    startDate = startDate !== undefined ? startDate : this.state.startDate
    endDate = endDate !== undefined ? endDate : this.state.endDate
    this.setState({
      startDate,
      endDate
    }, async () => {
      this.setState({loading: true})
      const { page, pageSize, sortedFields } = this.state
      const { firmId } = this.props
      const start = this.state.startDate
      const end = this.state.endDate
      if(start && end){
        const startingDate = start && start._d && moment(start._d).format("MM/DD/YYYY")
        const endingDate = end && end._d && moment(end._d).format("YYYY-MM-DD")
        const snapList = await getSnapShotsList(firmId, {startDate: startingDate, endDate: endingDate, currentPage: page, size: pageSize, sortFields: sortedFields})
        this.setState({
          snapShotsList: (snapList && snapList.snapShots && snapList.snapShots.data) || [],
          pages: (snapList && snapList.snapShots && snapList.snapShots.metadata && snapList.snapShots.metadata.pages) || 0,
          total: (snapList && snapList.snapShots && snapList.snapShots.metadata && snapList.snapShots.metadata.total) || 0,
          loading: false
        })
      } else {
        const snapList = await getSnapShotsList(firmId, {currentPage: page, size: pageSize, sortFields: sortedFields})
        this.setState({
          snapShotsList: (snapList && snapList.snapShots && snapList.snapShots.data) || [],
          pages: (snapList && snapList.snapShots && snapList.snapShots.metadata && snapList.snapShots.metadata.pages) || 0,
          total: (snapList && snapList.snapShots && snapList.snapShots.metadata && snapList.snapShots.metadata.total) || 0,
          loading: false
        })
      }
    })
  }

  getFilteredData = async (pageDetails) => {
    this.setState({loading: true,  page: {global: pageDetails}  })
    const { page, pageSize, sorted } = pageDetails
    let sortedFields = {}
    if (sorted && sorted[0]) {
      sortedFields[sorted[0].id] = sorted[0].desc ? -1 : 1
    } else {
      sortedFields = this.state.sortedFields
      if (sortedFields === undefined) {
        sortedFields = { date: -1 }
      }
    }
    this.setState({ sortedFields })

    let { startDate, endDate } = this.state
    const { firmId } = this.props
    startDate = (startDate && startDate._d && moment(startDate._d).format("MM/DD/YYYY")) || ""
    endDate = (endDate && endDate._d && moment(endDate._d).format("YYYY-MM-DD")) || ""
    const snapList = await getSnapShotsList(firmId, {currentPage: page, size: pageSize, sortFields: sortedFields, startDate, endDate})
    if (snapList && snapList.done) {
      this.setState({
        snapShotsList: (snapList && snapList.snapShots && snapList.snapShots.data) || [],
        pages: (snapList && snapList.snapShots && snapList.snapShots.metadata && snapList.snapShots.metadata.pages) || 0,
        total: (snapList && snapList.snapShots && snapList.snapShots.metadata && snapList.snapShots.metadata.total) || 0,
        page,
        pageSize,
        loading: false
      })
    } else {
      toast.error("Something went wrong!", {
        position: toast.POSITION.TOP_RIGHT
      })
      this.setState({
        loading: false
      })
    }
  }

  render() {
    const {
      snapShotsList,
      transactionWise,
      isModal,
      startDate,
      endDate,
      focusedInput,
      orientation,
      openDirection,
      total,
      loading,
      pages,
      modalTitle,
      elasticData,
      isElastic,
      isTran
    } = this.state

    return (
      <div className="animated fadeIn">
        { loading ? <Loader/> : null }
        <SnapShotsTransactionModal
          onModalChange={this.onModalChange}
          elasticData={elasticData || {}}
          transactionWise={transactionWise}
          total={total}
          isModal={isModal}
          modalTitle={modalTitle || ""}
          isElastic={isElastic || false}
          isTran={isTran || false}
        />
        <Row>
          <Col xs="12" sm="12">
            <Accordion
              activeAccordionHeader="Snap Shot List">

              <FormGroup row className="my-0  ">
                <Col xs="12" md="12" xl="12">
                  <FormGroup>
                    <Label>&nbsp;</Label>
                    <DateRangePicker
                      startDate={startDate}
                      startDateId="startDate"
                      endDate={endDate}
                      endDateId="endDate"
                      isOutsideRange={() => false}
                      small
                      showDefaultInputIcon
                      showClearDates
                      hideKeyboardShortcutsPanel
                      onDatesChange={({startDate, endDate}) => this.onChange(startDate, endDate)}
                      focusedInput={focusedInput}
                      onFocusChange={focusedInput => this.setState({focusedInput})}
                      orientation={orientation}
                      openDirection={openDirection}
                    />
                  </FormGroup>
                </Col>
              </FormGroup>

              <ReactTable
                columns={[
                  {
                    Header: "Date",
                    id: "date",
                    className: "multiExpTblVal",
                    accessor: item => item,
                    Cell: row => {
                      const item = row.value
                      return (
                        <div className="hpTablesTd -striped">
                          {item && item.date && moment(item.date).format("MM/DD/YYYY hh:mm A")}
                        </div>
                      )
                    },
                    sortMethod: (a, b) => a.date - b.date
                  },
                  {
                    Header: "Series50 Users",
                    id: "users.series50",
                    className: "multiExpTblVal",
                    accessor: item => item,
                    Cell: row => {
                      const item = row.value
                      return (
                        <div className="hpTablesTd wrap-cell-text">
                          {(item && item.users && item.users.series50) || 0 }
                        </div>
                      )
                    },
                    sortMethod: (a, b) => a.users.series50 - b.users.series50
                  },
                  {
                    Header: "Non Series50 Users",
                    id: "users.nonSeries50",
                    className: "multiExpTblVal",
                    accessor: item => item,
                    Cell: row => {
                      const item = row.value
                      return (
                        <div className="hpTablesTd wrap-cell-text">
                          {(item && item.users && item.users.nonSeries50) || 0 }
                        </div>
                      )
                    },
                    sortMethod: (a, b) => a.users.nonSeries50 - b.users.nonSeries50
                  },
                  {
                    Header: "Transactions",
                    id: "transactions",
                    className: "multiExpTblVal",
                    accessor: item => item,
                    Cell: row => {
                      const item = row.value
                      return (
                        <div className="hpTablesTd wrap-cell-text" >
                          <a
                            style={{color: "#20a8d8", cursor: "pointer"}}
                            onClick={() => this.onModalChange(item && item.transactionWise, item && item.transactions, "main", "tran")}
                          >{(item && item.transactions) || 0}</a>
                        </div>
                      )
                    },
                    sortMethod: (a, b) => a.transactions - b.transactions
                  },
                  {
                    Header: "Client",
                    id: "client",
                    className: "multiExpTblVal",
                    accessor: item => item,
                    Cell: row => {
                      const item = row.value
                      return (
                        <div className="hpTablesTd wrap-cell-text">
                          {(item && item.client) || 0}
                        </div>
                      )
                    },
                    sortMethod: (a, b) => a.client - b.client
                  },
                  {
                    Header: "Prospect",
                    id: "prospect",
                    className: "multiExpTblVal",
                    accessor: item => item,
                    Cell: row => {
                      const item = row.value
                      return (
                        <div className="hpTablesTd wrap-cell-text">
                          {(item && item.prospect) || 0}
                        </div>
                      )
                    },
                    sortMethod: (a, b) => a.prospect - b.prospect
                  },
                  {
                    Header: "Third Party",
                    id: "thirdParty",
                    className: "multiExpTblVal",
                    accessor: item => item,
                    Cell: row => {
                      const item = row.value
                      return (
                        <div className="hpTablesTd wrap-cell-text">
                          {(item && item.thirdParty) || 0}
                        </div>
                      )
                    },
                    sortMethod: (a, b) => a.thirdParty - b.thirdParty
                  },
                  {
                    Header: "Document Storage",
                    id: "storageSize",
                    className: "multiExpTblVal",
                    accessor: item => item,
                    Cell: row => {
                      const item = row.value
                      return (
                          <div className="hpTablesTd wrap-cell-text">
                            {(item && item.storageSize) || "0.00 Bytes"}
                          </div>
                      )
                    },
                    sortMethod: (a, b) => a.storageSize - b.storageSize
                  },
                  {
                    Header: "DB Storage",
                    id: "dbStorage",
                    className: "multiExpTblVal",
                    accessor: item => item,
                    Cell: row => {
                    const item = row.value
                    return (
                    <div className="hpTablesTd wrap-cell-text">
                    {(item && item.dbStorage) || "0.00 Bytes"}
                    </div>
                    )
                  },
                    sortMethod: (a, b) => a.storageSize - b.storageSize
                  },
                  {
                    Header: "Elastic Storage",
                    id: "dbStorage",
                    className: "multiExpTblVal",
                    accessor: item => item,
                    Cell: row => {
                      const item = row.value
                      return (
                        <div className="hpTablesTd wrap-cell-text">
                          <i
                            className="fa fa-eye"
                            style={{color: "#20a8d8", cursor: "pointer"}}
                            onClick={() => this.onModalChange(item && item.elasticStorage, "", "main")}
                          />
                        </div>
                      )
                    },
                    sortMethod: (a, b) => a.storageSize - b.storageSize
                  }
                ]}
                data={snapShotsList || []}
                showPaginationBottom
                defaultPageSize={10}
                pageSizeOptions={[5, 10, 20, 50, 100]}
                className="-striped -highlight is-bordered"
                style={{ overflowX: "auto" }}
                pages={pages}
                manual
                onFetchData={state => {
                  this.getFilteredData(state)
                }}
              />
            </Accordion>
          </Col>
        </Row>
      </div>
    )
  }
}

export default SnapShots
