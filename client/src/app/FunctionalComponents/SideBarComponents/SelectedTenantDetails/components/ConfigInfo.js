import React from "react"
import { toast } from "react-toastify"
import AuditAndLogo from "./AuditAndLogo";
import connect from "react-redux/es/connect/connect";
import {checkAuth} from "../../../../Services/actions/LoginAuthentication";
import Accordion from "../../../../GlobalComponents/Accordion";
import LaddaButton from "react-ladda";
import {updateFirmDetails} from "../../../../Services/actions/Setting";
import {CONST} from "../../../../globalutilities/consts";
import {getFirmDetailsInfo} from "../../../../Services/actions/Dashboard";

class ConfigInfo extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            logoLocation: [],
            logoAlignment: [],
            settings: {
                auditFlag: "no",
                logo: {
                    awsDocId: "",
                    fileName: ""
                }
            },
            changeLog: [],
        }
    }

    async componentWillMount(){
        const {tenantId} = this.props
        const res = await getFirmDetailsInfo(tenantId)
        if (res && res.length) {
            let settings = {
                auditFlag: (res[0] && res[0].settings && res[0].settings.auditFlag) || "no",
                logo: {
                    awsDocId: "",
                    fileName: ""
                }
            }
            this.setState({
                settings
            })
        }
    }

    onAuditAndLogoChange = async (settings) => {
        this.setState({
            settings
        })
    }

    getAuditChangeLog = (log) => {
        const { changeLog } = this.state
        if(log) {
            changeLog.push(log)
            this.setState({changeLog})
        }
    }

    onSave = async () => {
        const {settings, changeLog} = this.state
        const {tenantId} = this.props
        let logIndex = changeLog.length - 1
        const data = {
            settings,
            auditLogs: (changeLog && changeLog[logIndex])
        }
        const res = await updateFirmDetails(tenantId, data || "")
        if(res.data){
            toast("Changes Saved", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS })
        }else {
            toast("Error in saving changes", { autoClose: CONST.ToastTimeout, type: toast.TYPE.WARNING })
        }
    }

    render() {
        const { logoLocation, logoAlignment, settings } = this.state
        const { auth, tenantId } = this.props
        return (
            <div>
                <Accordion
                    activeAccordionHeader="Details">
                    <AuditAndLogo
                        user={auth || {}}
                        tenantId={tenantId}
                        logoLocation={logoLocation}
                        logoAlignment={logoAlignment}
                        settings={settings || {}}
                        onChange={this.onAuditAndLogoChange}
                        getAuditChangeLog={this.getAuditChangeLog}
                    />
                    <hr/>
                    <div className="row justify-content-center">
                        <LaddaButton
                            className="btn btn-primary btn-ladda"
                            onClick={this.onSave}
                            data-color="red"
                        >
                            Save
                        </LaddaButton>
                    </div>
                </Accordion>
            </div>
        )
    }
}

const mapStateToProps = ({ auth }) => ({
    auth: (auth && auth.loginDetails) || {}
})

export default connect(mapStateToProps, { checkAuth })(ConfigInfo)