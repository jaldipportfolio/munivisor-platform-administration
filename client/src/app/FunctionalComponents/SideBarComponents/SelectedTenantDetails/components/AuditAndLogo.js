import React  from "react"
import {Container, Row, Col, FormGroup, Label} from 'reactstrap';

const AuditAndLogo = (props) => {

  const onChangeAuditFlag = e => {
    const {settings} = props
    const {name, value} = e.target
    settings[name] = value

    props.onChange(settings)
  }

  const onBlurInput = (e) => {
    const {user} = props
    const { name, type } = e.target
    const value = type === "checkbox" ? e.target.checked : e.target.value
    let change = ""
    if (name && value) {
      change = value === "no" ? "No activity log will be maintained by MuniVisor" :
        value === "supervisor" ? "Activity log will be only visible to Compliance Officer / Supervisory Principals" :
          value === "currentState" ? "Activity log will be visible to all eligible users of the firm" : value
      const audit = {
        userId: user && user.userId,
        userName: `${user.userFirstName} ${user.userLastName}`,
        log: `${name || "empty"} change to ${change || "empty"} in Firms`,
        date: new Date()
      }

      props.getAuditChangeLog(audit)
    }
  }

  return (
    <Container>
        <Row>
            <Col xs="12" md="12">
                <FormGroup row>
                    <Col md="4">
                        <Label check>
                            <input
                                type="radio"
                                name="auditFlag"
                                value="no"
                                checked={props.settings.auditFlag === "no"}
                                onChange={onChangeAuditFlag}
                                onBlur={(event) => onBlurInput(event)}
                            />
                            No Activity log will be maintained by MuniVisor.
                        </Label>
                    </Col>
                    <Col md="4">
                        <Label check>
                            <input
                                type="radio"
                                name="auditFlag"
                                value="supervisor"
                                checked={props.settings.auditFlag === "supervisor"}
                                onChange={onChangeAuditFlag}
                                onBlur={(event) => onBlurInput(event)}
                            />
                            Activity log will be only visible to Compliance Officer / Supervisory Principals.
                        </Label>
                    </Col>
                    <Col md="4">
                        <Label check>
                            <input
                                type="radio"
                                name="auditFlag"
                                value="currentState"
                                checked={props.settings.auditFlag === "currentState"}
                                onChange={onChangeAuditFlag}
                                onBlur={(event) => onBlurInput(event)}
                            />
                            Activity log will be visible to all eligible users of the firm.
                        </Label>
                    </Col>
                </FormGroup>
            </Col>
        </Row>
    </Container>
  )
}


export default AuditAndLogo
