import React from "react"
import {TextInput} from "../../../../../GlobalComponents/TextInput"

const ChecklistTemplate4 = () => (
  <section className="accordions">

    <article className="accordion is-active">
      <div className="accordion-header toggle">
        <div className="field is-grouped" style={{marginBottom: 0}}>
          <p>Category</p>
          <span className="has-text-link">
            <i className="fa fa-pencil-square-o fa-lg text-info"/>
          </span>
        </div>
        <div className="field is-grouped">
          <div className="control">
            <button className="button is-link is-small">Add List Item</button> {/* eslint-disable-line */}
          </div>
          <div className="control">
            <button className="button is-light is-small">Reset</button> {/* eslint-disable-line */}
          </div>
        </div>
      </div>

      <div className="accordion-body">
        <div className="accordion-content">

          <div>

            <table className="table is-bordered is-striped is-hoverable is-fullwidth">
              <thead>
                <tr>
                  <th>
                    <div className="field is-grouped">
                      <div className="control">
                        <p className="emmaTablesTh">List Item</p>
                      </div>
                      <div className="control">
                        <a> {/* eslint-disable-line */}
                          <span className="has-text-link">
                            <i className="fa fa-pencil-square-o fa-lg text-info" />
                          </span>
                        </a>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="field is-grouped">
                      <div className="control">
                        <p className="emmaTablesTh">Responsible Party</p>
                      </div>
                      <div className="control">
                        <a> {/* eslint-disable-line */}
                          <span className="has-text-link">
                            <i className="fa fa-pencil-square-o fa-lg text-info" />
                          </span>
                        </a>
                      </div>
                    </div>
                  </th>
                  <th>
                    <p className="emmaTablesTh">Start Date</p>
                  </th>
                  <th>
                    <p className="emmaTablesTh">End Date</p>
                  </th>
                  <th>
                    <p className="emmaTablesTh">Status</p>
                  </th>
                  <th>
                    <p className="emmaTablesTh">Drop</p>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td className="soeExpLblVal">
                    <input className="input is-small is-link" type="text" placeholder="Checklist Item"/>
                  </td>
                  <td>
                    <input className="input is-small is-link" type="text" placeholder="Picklist from CRM"/>
                  </td>
                  <td>
                    <TextInput disabled />
                    {/* <input id="datepickerDemo" className="input is-small is-link" type="date"/> */}
                  </td>
                  <td>
                    {/* <input id="datepickerDemo" className="input is-small is-link" type="date"/> */}
                    <TextInput disabled />
                  </td>
                  <td>
                    <div className="select is-small is-link">
                      <select>
                        <option value="" disabled selected>Pick</option>
                        <option>LKUPSOEEVENTSTATUS</option>
                        <option>Planned</option>
                      </select>
                    </div>
                  </td>
                  <td>
                    <a> {/* eslint-disable-line */}
                      <span className="has-text-link">
                        <i className="far fa-trash-alt" />
                      </span>
                    </a>
                  </td>
                </tr>
              </tbody>
            </table>

          </div>

        </div>
      </div>

    </article>
  </section>
)

export default ChecklistTemplate4
