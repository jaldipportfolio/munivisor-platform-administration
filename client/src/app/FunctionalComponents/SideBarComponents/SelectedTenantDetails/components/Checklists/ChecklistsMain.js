import React, { Component } from "react"
import { Link, withRouter } from "react-router-dom"
import {Button, Col, FormGroup, Input, InputGroup, InputGroupAddon} from "reactstrap"
import qs from "qs"
import {toast} from "react-toastify"
import {checkListsList, saveConfigChecklist} from "../../../../../Services/actions/Dashboard"
import Loader from "../../../../../GlobalComponents/Loader"
import ChecklistTemplateSelection from "./ChecklistTemplateSelection"
import ChecklistView from "./ChecklistView"
import {errorToast} from "../../../../../globalutilities/helpers"

class ChecklistsMain extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedListId: "", selectedAction: "", newListId: "",
      newChecklistName: "", newChecklistAttributedTo: "", searchString: "",
      newChecklistBestPractise: "", newChecklistNotes: "", stringToMatch: "",
      queryString: {}
    }
  }

  async componentDidMount() {
    const { location } = this.props
    const queryString = qs.parse(location.search)
    const configChecklists = await checkListsList(this.props.tenantId)
    this.setState({configChecklists, queryString})
  }

  componentDidUpdate(prevProps) {
    const { location } = this.props
    const queryString = qs.parse(location.search)
    if(prevProps.location !== location){
      this.setState({queryString})
    }
  }

  changeNewChecklistFields = (e) => {
    const { name, value } = e.target
    this.setState({ [name]: value })
  }

  changeSearchString = (e) => {
    const searchString = e.target.value
    if (!searchString) {
      this.setState({ searchString: "", stringToMatch: "" })
    } else {
      this.setState({ searchString, stringToMatch: searchString })
    }
  }

  handleKeyPressinSearch = (e) => {
    if (e.key === "Enter") {
      const { searchString } = this.state
      this.setState({ stringToMatch: searchString })
    }
  }

  performAction = () => {
    const { selectedListId, selectedAction } = this.state
    const { tenantId, name } = this.props
    switch (selectedAction) {
    case "edit":
      this.props.history.push(`/tenant/checklists?firmId=${tenantId}&name=${name}&list=${selectedListId}`)
      break
    case "copy":
      console.log("copying : ", selectedListId)
      this.copyChecklist(selectedListId)
      break
    default:
      console.log("unknown action")
      break
    }
  }

  onSaveCallback(err) {
    if (err) {
      toast.error("Error in saving changes", { position: toast.POSITION.TOP_RIGHT })
    } else {
      toast.success("Changes Saved Successfully", { position: toast.POSITION.TOP_RIGHT })
    }
  }

  addNewChecklist = () => {
    if (!this.state.newListId) {
      const configChecklists = [...this.state.configChecklists]
      const ids = configChecklists.map(c => +c.id.split("CL")[1])
      ids.sort((a, b) => b - a)
      const newListId = `CL${(ids[0] || 0) + 1}`
      const newChecklist = {
        id: newListId,
        name: "",
        attributedTo: this.props.name || "",
        bestPractice: "",
        notes: "",
        lastUpdated: {
          date: new Date().toISOString(),
          by: "user"
        },
        data: {
          "Add a title": [{}]
        }
      }
      configChecklists.push(newChecklist)
      this.setState({ newListId, newChecklist, configChecklists, newChecklistAttributedTo: this.props.name || "" }) // eslint-disable-line
    }
  }

  copyChecklist = (id) => {
    if (!this.state.newListId) {
      const configChecklists = [...this.state.configChecklists]
      const checklistToCopy = configChecklists.filter(c => c.id === id)[0] || {}
      const ids = configChecklists.map(c => +c.id.split("CL")[1])
      ids.sort((a, b) => b - a)
      const newListId = `CL${ids[0] + 1}`
      const newChecklist = {
        ...checklistToCopy,
        id: newListId,
        lastUpdated: {
          date: new Date().toISOString(),
          by: "user"
        }
      }
      configChecklists.push(newChecklist)
      this.props.history.push(`/tenant/checklists?firmId=${this.props.tenantId}&name=${this.props.name}&list=${newListId}`)
    }
  }

  changeSelectedAction = (selectedListId, e, index) => {
    console.log("selectedListId : ", selectedListId)
    console.log("e : ", e)
    const { tenantId, name, userId, userName } = this.props
    const { configChecklists } = this.state

    const selectedAction = e.target.value
    if (selectedAction === "edit") {
      this.props.history.push(`/tenant/checklists?firmId=${tenantId}&name=${name}&list=${selectedListId}`)
    }
    if (selectedAction === "active" || selectedAction === "inactive") {
      configChecklists.forEach(e => {
        if (e.id === selectedListId) {
          e.action = selectedAction
        }
      })
      // eslint-disable-next-line
      const data = configChecklists[index].action = selectedAction
      const changeLog = [{
        userId,
        userName,
        log: `Checklist ${selectedListId} is ${selectedAction}`,
        date: new Date()
      }]
      this.setState({ selectedListId, selectedAction, ...data }, async () =>
        await saveConfigChecklist(tenantId, configChecklists, changeLog, (err) => this.onSaveCallback(err))
      )
    }
    // this.setState({ selectedListId, selectedAction }, this.performAction)
  }

  renderTableHeader() {
    return (
      <tr>
        <th>
          <p className="emmaTablesTh">ID</p>
        </th>
        <th>
          <p className="emmaTablesTh">Chk List Name</p>
        </th>
        <th>
          <p className="emmaTablesTh">Attributed to</p>
        </th>
        <th>
          <p className="emmaTablesTh">Best Practice</p>
        </th>
        <th>
          <p className="emmaTablesTh">Notes</p>
        </th>
        <th>
          <p className="emmaTablesTh">Last Updated</p>
        </th>
        <th>
          <p className="emmaTablesTh">Action</p>
        </th>
      </tr>
    )
  }

  renderChecklistsRows(checklists) {
    const { newListId, newChecklistName, newChecklistBestPractise, newChecklistNotes } = this.state
    const { tenantId, name } = this.props
    let {newChecklistAttributedTo } = this.state
    newChecklistAttributedTo = newChecklistAttributedTo || ""
    const currentChecklists = newListId ? checklists.slice(0, -1) : checklists
    const newChecklist = newListId ? checklists.slice(-1)[0] : {}
    const rows = currentChecklists.map((d, i) =>
      <tr key={d._id || d.id}>
        <td className="emmaTablesTd">
          { d.action !== "inactive" ? <Link to={`/tenant/checklists?firmId=${tenantId}&name=${name}&list=${d.id}`}>{d.id}</Link> : d.id }
        </td>
        <td className="emmaTablesTd">
          <small>{d.name}</small>
        </td>
        <td className="emmaTablesTd">
          <small>{d.attributedTo}</small>
        </td>
        <td className="emmaTablesTd">
          <small>{d.bestPractice}</small>
        </td>
        <td className="emmaTablesTd">
          <small>{d.notes}</small>
        </td>
        <td className="emmaTablesTd">
          <small>{new Date(d.lastUpdated.date).toLocaleString()} {d.lastUpdated.by}</small>
        </td>
        <td>
          <div className="select is-small is-link">
            <select value={d.action || ""} onChange={(e) => this.changeSelectedAction(d.id, e, i)}>
              <option value="" disabled>Action</option>
              <option value="edit" disabled={d.action === "inactive"}>Edit</option>
              <option value="active" disabled={d.action === "active" || d.action === undefined}>Active</option>
              <option value="inactive" disabled={d.action === "inactive"}>InActive</option>
              {/* <option value="copy">Make a copy</option> */}
            </select>
          </div>
        </td>
      </tr>
    )
    if (newListId) {
      const query = qs.stringify({
        newListId,
        newChecklistName,
        newChecklistAttributedTo,
        newChecklistBestPractise,
        newChecklistNotes
      })
      rows.unshift(
        <tr key={newListId}>
          <td className="emmaTablesTd">
            <small>{newListId}</small>
          </td>
          <td className="emmaTablesTd">
            <input className="input is-small is-link"
              type="text"
              placeholder="Checklist name"
              name="newChecklistName"
              value={newChecklistName}
              onChange={this.changeNewChecklistFields}
            />
          </td>
          <td className="emmaTablesTd">
            <input className="input is-small is-link"
              type="text"
              placeholder="Defaults to firmname"
              name="newChecklistAttributedTo"
              value={newChecklistAttributedTo}
              onChange={this.changeNewChecklistFields}
            />
          </td>
          <td className="emmaTablesTd">
            <input className="input is-small is-link"
              type="text"
              placeholder=""
              name="newChecklistBestPractise"
              value={newChecklistBestPractise}
              onChange={this.changeNewChecklistFields}
            />
          </td>
          <td className="emmaTablesTd">
            <input className="input is-small is-link"
              type="text"
              placeholder=""
              name="newChecklistNotes"
              value={newChecklistNotes}
              onChange={this.changeNewChecklistFields}
            />
          </td>
          <td className="emmaTablesTd">
            <small>{new Date(newChecklist.lastUpdated.date).toLocaleString()} {newChecklist.lastUpdated.by}</small>
          </td>
          <td className="emmaTablesTd" style={{paddingTop: "10px"}} >
            <div className="button is-small is-link" onClick={() => this.onCheckListSave(newListId, query)}>
              Save & Update
            </div>
          </td>
        </tr>
      )
    }
    return rows
  }

  renderChecklists(checklists) {
    return (
      <section>
        <FormGroup row className="my-0">
          <Col xs="12" md="2">
            <div className="row justify-content-sm-start" style={{marginLeft: 0}}>
              <Button color="primary" onClick={this.addNewChecklist}> Add new checklist</Button>
            </div>
          </Col>
          <Col xs="12" md="10">
            <div className="controls">
              <InputGroup>
                <Input type="text"
                  placeholder=" Search by checklist name"
                  value={this.state.searchString}
                  onChange={this.changeSearchString}
                />
                <InputGroupAddon addonType="append">
                  <Button color="secondary" onClick={this.handleKeyPressinSearch}><i className="fa fa-search"/></Button>
                </InputGroupAddon>
              </InputGroup>
            </div>
          </Col>
        </FormGroup>
        <br/>
        <div className="box overflow-auto" >
          <table className="table is-bordered is-striped is-hoverable is-fullwidth">
            <thead>
              {this.renderTableHeader()}
            </thead>
            <tbody>
              {this.renderChecklistsRows(checklists)}
            </tbody>
          </table>
        </div>
      </section>
    )
  }

  onCheckListSave = (newListId, query) => {
    const {newChecklistName} = this.state
    if(!newChecklistName){
      return errorToast("Checklist name should not be empty")
    }
    this.props.history.push(`/tenant/checklists?firmId=${this.props.tenantId}&name=${this.props.name}&list=${newListId}&${query}`)
  }

  render() {
    const { stringToMatch, configChecklists, queryString } = this.state
    const { tenantId, name, userId, userName, token } = this.props
    const matchingLists = stringToMatch ?
      configChecklists.filter(c => c.name.toLowerCase().includes(stringToMatch.toLowerCase())) : configChecklists
    const checklist = configChecklists && configChecklists.length && configChecklists.filter(c => c.id === queryString.list)[0]

    if (queryString.list) {
      if(queryString.newListId){
        return (
          <div className="overflow-auto">
            <ChecklistTemplateSelection
              token={token}
              userId={userId}
              userName={userName}
              tenantId={tenantId}
              name={name}
              configChecklists={configChecklists}
              query={this.props.location.search}
            />
          </div>
        )
      }

      if (checklist) {
        const { data } = checklist
        let oldList = true
        Object.keys(data).some(k => {  // eslint-disable-line
          if (data[k].length === 1 && !data[k][0]) {
            oldList = false
            return true
          }
        })
        return (
          <div className="overflow-auto">
            <ChecklistView start={!oldList}
              cid={queryString.list}
              userId={userId}
              userName={userName}
              tenantId={tenantId}
              name={name}
              token={token}
              checklist={checklist}
              configChecklists={configChecklists} />
          </div>
        )
      }
    }

    if (configChecklists) {
      return (
        <div id="main">
          {this.renderChecklists(matchingLists || [])}
        </div>
      )
    }

    return <Loader />
  }
}

export default withRouter(ChecklistsMain)
