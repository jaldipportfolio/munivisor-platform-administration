import React, { Component } from "react"
import { withRouter } from "react-router-dom"
import { toast } from "react-toastify"
import { Multiselect } from "react-widgets"
import { saveConfigChecklist } from "../../../../../Services/actions/Dashboard"
import { CONST, RFP_ASSIGNED_TO } from "../../../../../globalutilities/consts"
import Loader from "../../../../../GlobalComponents/Loader"
import SectionAccordion from "../../../../../GlobalComponents/SectionAccordion"
import Section from "../../../../../GlobalComponents/Section"
import {errorToast} from "../../../../../globalutilities/helpers"

const ObjectID = require("bson-objectid")

class ChecklistDetails1 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      accordions: {}, expanded: ["Add a title"], validationError: {}, activeItem: [],
      accordionTitleToChange: "", keyChanges: {}, removedKeys: [], waiting: false,
      changeLog: [], removedRows: [], generalError: "", itemHeaders: {}, itemHeaderToChange: "",
      newChecklistName: "", newChecklistAttributedTo: "", newListId: "",
      newChecklistBestPractise: "", newChecklistNotes: "", newListType: "" /* , rfpAssignee:[] */
    }
    this.validateSubmition = this.validateSubmition.bind(this)
    this.saveAccordions = this.saveAccordions.bind(this)
    this.addAccordion = this.addAccordion.bind(this)
    this.resetAllAcccordions = this.resetAllAcccordions.bind(this)
    this.saveData = this.saveData.bind(this)
    this.changeAccordionTitle = this.changeAccordionTitle.bind(this)
    this.changeItemHeader = this.changeItemHeader.bind(this)
    this.changeNewChecklistFields = this.changeNewChecklistFields.bind(this)
    this.onBlur = this.onBlur.bind(this)
    this.onSaveCallback = this.onSaveCallback.bind(this)
  }

  async componentDidMount() {
    console.log("setting data in componentDidMount")
    this.setInitialStateFromProps()
  }

  componentWillReceiveProps(nextProps) {
    const { config, configChecklists, cid, itemHeaders, newListParams } = nextProps
    const idx = configChecklists.findIndex(c => c.id === cid)
    const checklist = idx >= 0 ? { ...configChecklists[idx] } : {}
    console.log("setting data in componentWillReceiveProps")
    this.setDataInState(config, checklist, itemHeaders, newListParams)
  }

  onSaveCallback(err) {
    const {newListId} = this.state
    const {tenantId, name} = this.props
    this.setState({ waiting: false })
    if (err) {
      toast("Error in saving changes", { autoClose: CONST.ToastTimeout, type: toast.TYPE.WARNING })
    } else {
      if (newListId) {
        window.location.href = `/tenant/checklists?firmId=${tenantId}&name=${name}`
      }
      toast("Changes Saved", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS })
    }
  }

  setInitialStateFromProps() {
    console.log("in setInitialStateFromProps")
    const { config, configChecklists, cid, itemHeaders, newListParams } = this.props
    const idx = configChecklists.findIndex(c => c.id === cid)
    const checklist = idx >= 0 ? { ...configChecklists[idx] } : {}
    // this.setDataInState(config)
    if (!config || !Object.keys(config).length) {
      console.log("getting db data")
      // this.props.getConfigChecklist()
      this.setDataInState(config, checklist, itemHeaders, newListParams)
    } else {
      this.setDataInState(config, checklist, itemHeaders, newListParams)
    }
  }

  setaccordionTitleToChange = (accordionTitleToChange, e) => {
    this.setState({ accordionTitleToChange })
    e.stopPropagation()
  }

  setDataInState(data, checklist = {}, itemHeaders = {}, newListParams = {}) {
    if (!data || !Object.keys(data).length) {
      data = this.initialAccordionsData()
    }
    if (!itemHeaders || !Object.keys(itemHeaders).length) {
      itemHeaders = this.initialItemHeader()
    }
    itemHeaders = { ...itemHeaders }
    Object.keys(itemHeaders).forEach(k => {
      if (!itemHeaders[k] || !itemHeaders[k][0]) {
        itemHeaders[k] = ["List Item"]
      }
    })
    const newListId = newListParams.newListId || ""
    const newListType = newListParams.type || ""
    const newChecklistName = checklist.name || newListParams.newChecklistName || ""
    const newChecklistAttributedTo = checklist.attributedTo || newListParams.newChecklistAttributedTo || ""
    const newChecklistBestPractise = checklist.bestPractice || newListParams.newChecklistBestPractise || ""
    const newChecklistNotes = checklist.notes || newListParams.newChecklistNotes || ""
    this.setState({
      accordions: data, newChecklistName, newChecklistAttributedTo, newListType,
      newChecklistBestPractise, newChecklistNotes, itemHeaders, newListId, validationError: {}, removedRows: []
    })
  }

  removeAccordion(key) {
    this.setState(prevState => {
      const accordions = {}
      const keys = Object.keys(prevState.accordions).filter(k => k !== key)
      keys.forEach(k => {
        accordions[k] = prevState.accordions[k]
      })
      const removedKeys = [...prevState.removedKeys]
      removedKeys.push(key)
      return { accordions, removedKeys }
    })
  }

  changeNewChecklistFields(e) {
    const { name, value } = e.target
    this.setState({ [name]: value })
  }

  onBlur(e, id, name) {
    const { removedRows } = this.state
    const { value } = e.target
    const row = `${id || ""} in ${name || ""} change to ${value || ""}`
    removedRows.push(row)
    this.setState({
      removedRows
    })
  }

  initialAccordionsData() {
    return {
      "Add a title": [
        {
          _id: ObjectID(),
          label: "",
          assignedToTypes: []
        }
      ]
    }
  }

  initialItemHeader() {
    return {
      "Add a title": ["List Item"]
    }
  }

  addAccordion() {
    this.setState(prevState => {
      const accordions = { ...prevState.accordions }
      const itemHeaders = { ...prevState.itemHeaders }
      const keys = Object.keys(accordions)
      const numKeys = (keys.map(k => +k.replace("Add Title ", "")).sort((a, b) => b - a)[0]) || 0 + 1
      accordions[`Add Title ${numKeys}`] = this.initialAccordionsData()["Add a title"]
      itemHeaders[`Add Title ${numKeys}`] = this.initialItemHeader()["Add a title"]
      return { accordions, itemHeaders }
    })
  }

  updateKeyChanges(prevKeyChanges, oldKey, newKey) {
    let newKeyChanges = { ...prevKeyChanges }
    const oldKeys = Object.keys(prevKeyChanges)

    if (!oldKeys.length) {
      newKeyChanges[oldKey] = newKey
      return newKeyChanges
    }

    if (newKeyChanges.hasOwnProperty(newKey)) {
      newKeyChanges = {}
      console.log("changed to original value !!")
      oldKeys.forEach(k => {
        if (k !== newKey) {
          newKeyChanges[k] = prevKeyChanges[k]
        }
      })
      return newKeyChanges
    }
    let foundOldKey = false
    oldKeys.some(k => {
      if (prevKeyChanges[k] === oldKey) {
        console.log("found match for key : ", k)
        console.log("newKeyChanges : ", JSON.stringify(newKeyChanges))
        foundOldKey = true
        newKeyChanges[k] = newKey
        console.log("newKeyChanges : ", JSON.stringify(newKeyChanges))
        return true
      }
      return false
    })
    if (!foundOldKey) {
      console.log("match not found : ", oldKey)
      newKeyChanges[oldKey] = newKey
    }

    return newKeyChanges
  }

  clickAccordionTitle = (event) => {
    const type = (event && event.target && event.target.type) || ""
    if (type === "text") {
      event.stopPropagation()
    }
  }

  changeAccordionTitle(e) {
    const newKey = e.target.value
    console.log("newKey : ", newKey)
    this.setState(prevState => {
      const { accordionTitleToChange } = prevState

      if (accordionTitleToChange === newKey) {
        return {}
      }

      const accordionsKeys = Object.keys(prevState.accordions)
      if (accordionsKeys.includes(newKey)) {
        return { generalError: "Duplicate Title" }
      }

      const prevKeyChanges = { ...prevState.keyChanges }
      let keyChanges = {}
      const accordions = {}
      const itemHeaders = {}
      accordionsKeys.forEach(k => {
        console.log("k : ", k)
        console.log("accordionTitleToChange : ", accordionTitleToChange)
        if (k === accordionTitleToChange) {
          console.log("doing new key")
          accordions[newKey] = prevState.accordions[accordionTitleToChange]
          itemHeaders[newKey] = prevState.itemHeaders[accordionTitleToChange]
          keyChanges = this.updateKeyChanges(prevKeyChanges, accordionTitleToChange, newKey)
        } else {
          console.log("doing old key")
          accordions[k] = prevState.accordions[k]
          itemHeaders[k] = prevState.itemHeaders[k]
        }
      })
      return { accordions, itemHeaders, accordionTitleToChange: newKey, keyChanges, generalError: "" }
    })
  }

  setItemHeaderToChange(itemHeaderToChange) {
    this.setState({ itemHeaderToChange })
  }

  changeItemHeader(e) {
    const { value } = e.target
    this.setState(prevState => {
      const itemHeaders = { ...prevState.itemHeaders }
      const { itemHeaderToChange } = prevState
      itemHeaders[itemHeaderToChange][0] = value
      return { itemHeaders }
    })
  }

  saveConfigInDB = async () => {
    this.setState({ waiting: true })
    const { accordions, itemHeaders, changeLog, newChecklistName,
      newChecklistAttributedTo, newChecklistBestPractise,
      newChecklistNotes, newListId, newListType } = this.state
    const { tenantId, cid, configChecklists } = this.props
    const checklists = [...configChecklists]
    const idx = checklists.findIndex(c => c.id === cid)
    const checklist = idx >= 0 ? { ...checklists[idx] } : {}
    checklist.name = newChecklistName
    checklist.attributedTo = newChecklistAttributedTo
    checklist.bestPractice = newChecklistBestPractise
    checklist.notes = newChecklistNotes
    checklist.data = accordions
    checklist.lastUpdated = {
      date: new Date(),
      by: "user"
    }
    checklist.itemHeaders = itemHeaders
    if (idx >= 0) {
      checklists[idx] = checklist
    } else {
      checklist.id = newListId
      checklist.type = newListType
      checklists.push(checklist)
    }
    console.log("changeLog : ", changeLog)
    await saveConfigChecklist(tenantId, checklists, changeLog, this.onSaveCallback)
  }

  newItemLog(e) {
    let logMessage = ""
    if (!e) {
      return logMessage
    }
    Object.keys(e).forEach(k => {
      if (k !== "_id") {
        logMessage += ` ${k} - ${e[k]}`
      }
    })
    return logMessage
  }

  checkElemChange(newItem, oldItem) {
    let elemChange = ""
    Object.keys(newItem).forEach(k => {
      if (newItem[k] !== oldItem[k]) {
        elemChange += ` ${k} - ${oldItem[k]} to ${newItem[k]}`
      }
    })
    return elemChange
  }

  logAndSave = () => {
    const { userId, userName } = this.props
    const date = new Date()
    // let dateString = date.toLocaleString();
    const oldData = this.props.config || {}
    const { accordions, keyChanges, removedKeys, removedRows } = this.state
    const changeLog = []
    const newData = {}

    removedKeys.forEach(k => {
      changeLog.push({ userId, userName, log: `removed checklist with title ${k}`, date })
    })

    removedRows.forEach(row => {
      changeLog.push({ userId, userName, log: row, date })
    })

    Object.keys(accordions).forEach(k => {
      console.log("k : ", k)
      let oldKey
      Object.keys(keyChanges).some(c => {
        console.log("c : ", c)
        if ((keyChanges[c] === k) && oldData.hasOwnProperty(c)) {
          oldKey = c
          changeLog.push({ userId, userName, log: `changed checklist Title ${c} to ${k}`, date })
          return true
        }
        return false
      })
      console.log("oldKey : ", oldKey)
      if (oldKey) {
        newData[oldKey] = accordions[k]
      } else {
        newData[k] = accordions[k]
      }
    })
    console.log("oldData : ", oldData)
    console.log("newData : ", newData)

    Object.keys(newData).forEach(n => {
      const newItems = newData[n]
      const oldItems = oldData[n]
      if (oldItems) {
        const len = oldItems.length
        newItems.forEach((e, i) => {
          if (i < len) {
            const elemChange = this.checkElemChange(e, oldItems[i])
            if (elemChange) {
              changeLog.push({ userId, userName, log: `changed item${i + 1} in checklist with title ${n} - ${elemChange}`, date })
            }
          } else {
            changeLog.push({ userId, userName, log: `added following item in checklist with title ${n} - ${this.newItemLog(e)}`, date })
          }
        })
      } else {
        let newListMessage = `added new checklist with title ${n} with below items : `
        newItems.forEach((e, i) => { newListMessage += ` |${i + 1}: ${this.newItemLog(e)}|` })
        changeLog.push({ userId, userName, log: newListMessage, date })
      }
    })

    this.setState({ changeLog, accordionTitleToChange: "" }, this.saveConfigInDB)
  }

  saveData() {
    const { validationError, newChecklistName } = this.state
    const errKeys = Object.keys(validationError)
    console.log("errKeys : ", errKeys)
    let save = true
    if(!newChecklistName){
      return errorToast("Checklist name should not be empty")
    }

    if (errKeys.length) {
      let error = false
      errKeys.some(i => {
        const itemKeys = Object.keys(validationError[i])
        console.log("itemKeys : ", itemKeys)
        if (!itemKeys.length) {
          return true
        }
        itemKeys.some(j => {
          const subItemKeys = Object.keys(validationError[i][j])
          console.log("subItemKeys : ", subItemKeys)
          if (!subItemKeys.length) {
            return true
          }
          subItemKeys.some(k => {
            console.log("subItem : ", validationError[i][j][k])
            if (validationError[i][j][k]) {
              error = true
              return true
            }
            return false
          })
          if (error) {
            return true
          }
          return false
        })
        if (error) {
          return true
        }
        return false
      })
      if (error) {
        save = false
      }
      return false
    }

    if (save) {
      this.logAndSave()
    }
  }

  validateSubmition() {
    this.setState(prevState => {
      const { accordions, validationError } = prevState
      const newErr = { ...validationError }
      Object.keys(accordions).forEach(a => {
        Object.keys(accordions[a]).forEach(k => {
          if (!accordions[a][k].label) {
            newErr[a] = { ...newErr[a] }
            newErr[a][k] = { ...newErr[a][k] }
            newErr[a][k].label = "No input provided"
          }
          // if(!accordions[a][k].assignedToTypes || !accordions[a][k].assignedToTypes.length) {
          //   newErr[a] = { ...newErr[a] }
          //   newErr[a][k] = { ...newErr[a][k]}
          //   newErr[a][k].assignedToTypes = "No input provided"
          // }
        })
      })
      return { validationError: newErr }
    }, this.saveData)
  }

  saveAccordions() {
    this.validateSubmition()
  }

  /* handleClick(key, e) {
    this.setState(prevState => {
      let expanded = [...prevState.expanded]
      if (!expanded.includes(key)) {
        expanded.push(key)
      } else {
        expanded = expanded.filter(k => k !== key)
      }
      return { expanded }
    })
  } */

  changeLabel(key, i, e) {
    const { value } = e.target
    // console.log(" key: ", key)
    // console.log(" i: ", i)
    // console.log(" val: ", value)
    this.setState(prevState => {
      const accordions = { ...prevState.accordions }
      accordions[key] = [...accordions[key]]
      accordions[key][i] = { ...accordions[key][i], label: value }
      const validationError = { ...prevState.validationError }
      validationError[key] = { ...validationError[key] }
      validationError[key][i] = { ...validationError[key][i], label: "" }
      return { accordions, validationError }
    })
  }

  changeAssignedTo(key, i, values) {
    // console.log(" key: ", key)
    // console.log(" i: ", i)
    // console.log(" val: ", values)
    this.setState(prevState => {
      const accordions = { ...prevState.accordions }
      accordions[key] = [...accordions[key]]
      accordions[key][i] = { ...accordions[key][i], assignedToTypes: values }
      const validationError = { ...prevState.validationError }
      validationError[key] = { ...validationError[key] }
      validationError[key][i] = { ...validationError[key][i], assignedToTypes: "" }
      return { accordions, validationError }
    })
  }

  removeAccordionItem(key, i) {
    const { removedRows } = this.state
    this.setState(prevState => {
      const accordions = { ...prevState.accordions }
      accordions[key] = [...accordions[key]]
      if(accordions && accordions[key] && accordions[key][i] && accordions[key][i].label){
        const row = `checklist in removed ${accordions[key][i].label} row from ${key}`
        removedRows.push(row)
      }
      accordions[key].splice(i, 1)
      return { accordions, removedRows }
    })
  }

  addAcccordionItem(key, index) {
    this.setState(prevState => {
      const expanded = [...prevState.expanded]
      if (!expanded.includes(key)) {
        expanded.push(key)
      }
      const activeItem = [...prevState.activeItem, index]
      const accordions = { ...prevState.accordions }
      accordions[key] = [...accordions[key]]
      accordions[key] = accordions[key].filter(e => e && e.label)
      accordions[key].push({
        _id: ObjectID(),
        label: "",
        assignedToTypes: []
      })
      return { accordions, expanded, activeItem }
    })
  }

  resetAcccordion(key) {
    this.setState((prevState, props) => {
      const expanded = [...prevState.expanded]
      if (!expanded.includes(key)) {
        expanded.push(key)
      }
      const accordions = { ...prevState.accordions }
      if (props.config[key]) {
        accordions[key] = [...props.config[key]]
      } else {
        accordions[key] = this.initialAccordionsData()["Add a title"]
      }
      return { accordions, expanded, validationError: {} }
    })
    // this.setState({ accordions: data });
  }

  resetAllAcccordions() {
    // const { token } = this.props
    // this.props.getConfigChecklist(token)
    // this.setState({ changeLog: [], removedKeys: [], keyChanges: {} })
    this.setInitialStateFromProps()
  }

  renderTitleInputFieldWithIcon(key) {
    if (this.state.accordionTitleToChange === key) {
      return (
        <div>
          <div className="field is-grouped is-pulled-left">
            <div className="control">
              <input autoFocus
                className="input is-small is-link"
                type="text"
                value={key}
                onClick={(e) => this.clickAccordionTitle(e)}
                onChange={this.changeAccordionTitle} />
            </div>
            <div className="control" style={{paddingTop: "4px"}}>
              <a onClick={(e) => this.setaccordionTitleToChange("", e)}> {/* eslint-disable-line */}
                <span className="has-text-link">
                  <i className="fa fa-save" title="Save"/>
                </span>
              </a>
            </div>
          </div>
        </div>
      )
    }
    return (
      <div className="field is-grouped is-pulled-left" style={{padding: 0}}>
        <div>
          {key}
        </div>
        <span className="has-text-link" onClick={(e) => this.setaccordionTitleToChange(key, e)} style={{padding: 0, marginLeft: 10}}>
          <i className="fa fa-pencil-square-o fa-lg text-info" title="Edit"/>
        </span>
      </div>
    )
  }

  renderHeader(key , i) {
    return (
      <div className="columns">
        <div className="column">
          <div className="field is-grouped">
            <div className="control">
              <button className="button is-link is-small" // eslint-disable-line
                onClick={() => this.addAcccordionItem(key, i)}>Add List Item</button>
            </div>
            <div className="control">
              <button className="button is-light is-small" // eslint-disable-line
                onClick={() => this.resetAcccordion(key, i)}>Reset</button>
            </div>
            <div className="control">
              <i className="fa fa-trash-o is-size-4"
                onClick={() => this.removeAccordion(key)} />
            </div>
          </div>
        </div>
      </div>
    )
  }

  renderAccordionBodyItem(e, key, i) {
    return (
      <tr key={key + i}>
        <td>
          <input
            className="input is-small is-link"
            type="text"
            value={e.label}
            onChange={this.changeLabel.bind(this, key, i)}
          />
          {this.state.validationError && this.state.validationError[key] && this.state.validationError[key][i] &&
            this.state.validationError[key][i].label ?
            <strong className="has-text-danger">{this.state.validationError[key][i].label}</strong>
            : undefined
          }
        </td>
        <td style={{width: "40%"}}>
          <Multiselect
            data={RFP_ASSIGNED_TO}
            caseSensitive={false}
            minLength={1}
            value={e.assignedToTypes}
            filter="contains"
            onChange={this.changeAssignedTo.bind(this, key, i)}
          />
          {this.state.validationError && this.state.validationError[key] && this.state.validationError[key][i] &&
            this.state.validationError[key][i].assignedToTypes ?
            <strong className="has-text-danger">{this.state.validationError[key][i].assignedToTypes}</strong>
            : undefined
          }
        </td>
        <td>
          <i className="fa fa-trash-o is-size-4" style={{cursor: "pointer"}} title="Delete"
            onClick={this.removeAccordionItem.bind(this, key, i)} />
        </td>
      </tr>
    )
  }

  renderAccordionBody(key, accordion) {
    const { itemHeaders, itemHeaderToChange } = this.state
    return (
      <div className="tbl-scroll">
        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
          <thead>
            <tr>
              <th>
                {itemHeaderToChange === key ?
                  <div className="field is-grouped">
                    <div className="control">
                      <input
                        className="input is-small is-link"
                        type="text"
                        value={itemHeaders[key][0]}
                        // onKeyPress={this.handleKeyPressItemHeader}
                        onChange={this.changeItemHeader} />
                    </div>
                    <div className="control">
                      <span className="icon has-text-link is-small is-right checklist-edit-icon">
                        <i className="fa fa-save" onClick={this.setItemHeaderToChange.bind(this, "")} />
                      </span>
                    </div>
                  </div> :
                  <div className="field is-grouped">
                    <div className="control">
                      <p className="emmaTablesTh">
                        {itemHeaders[key][0]}
                      </p>
                    </div>
                    <div className="control">
                      <a> {/* eslint-disable-line */}
                        <span className="has-text-link">
                          <i className="fa fa-pencil-square-o fa-lg text-info checklist-edit-icon"
                            onClick={this.setItemHeaderToChange.bind(this, key)} />
                        </span>
                      </a>
                    </div>
                  </div>
                }
              </th>
              <th>
                <p className="multiExpLbl "
                  title="Checklist item related activity assigned to">
                  Assigned to
                </p>
              </th>
              <th>
                <p className="multiExpLbl "
                  title="Checklist item related activity assigned to">
                  Delete
                </p>
              </th>
            </tr>
          </thead>
          <tbody>
            {accordion.map((e, i) => this.renderAccordionBodyItem(e, key, i))}
          </tbody>
        </table>
      </div>
    )
  }

  renderAccordions(accordions) {
    let {activeItem} = this.state
    const style = {padding: 0}
    // const accIndex = Object.keys(accordions).findIndex(a => a === accordionTitleToChange)
    return (
      <SectionAccordion
        multiple
        activeItem={activeItem}
        isRequired={!!activeItem.length}
        boxHidden
        render={({ activeAccordions, onAccordion }) => (
          <div>
            {
              Object.keys(accordions).map((a, i) => (
                <Section
                  onAccordion={() => {
                    if (!activeItem.includes(i)) {
                      activeItem.push(i)
                    } else {
                      activeItem = activeItem.filter(k => k !== i)
                    }
                    this.setState({
                      activeItem
                    }, onAccordion(i))
                  }}
                  key={i.toString()}
                  actionButtons={this.renderHeader(a, i)}
                  title={this.renderTitleInputFieldWithIcon(a)}
                  headerStyle={style}
                >
                  {activeAccordions.includes(i) && (
                    <div>
                      {
                        this.renderAccordionBody(a, accordions[a])
                      }
                    </div>
                  )}
                </Section>
              ))
            }
          </div>
        )}
      />
    )
  }

  renderTableHeader() {
    return (
      <tr>
        <th>
          <p className="emmaTablesTh">ID</p>
        </th>
        <th>
          <p className="emmaTablesTh">Chk List Name</p>
        </th>
        <th>
          <p className="emmaTablesTh">Attributed to</p>
        </th>
        <th>
          <p className="emmaTablesTh">Best Practice</p>
        </th>
        <th>
          <p className="emmaTablesTh">Notes</p>
        </th>
      </tr>
    )
  }

  renderChecklistBasicDetails() {
    const { newChecklistName, newChecklistAttributedTo,
      newChecklistBestPractise, newChecklistNotes, newListId } = this.state
    return (
      <tr>
        <td className="emmaTablesTd">
          <small>{this.props.cid || newListId}</small>
        </td>
        <td className="emmaTablesTd">
          <input className="input is-small is-link"
            type="text"
            placeholder="Checklist name"
            name="newChecklistName"
            value={newChecklistName}
            onBlur={(e) => this.onBlur(e, this.props.cid || newListId, "Chk List Name")}
            onChange={this.changeNewChecklistFields}
          />
        </td>
        <td className="emmaTablesTd">
          <input className="input is-small is-link"
            type="text"
            placeholder="Defaults to firmname"
            name="newChecklistAttributedTo"
            value={newChecklistAttributedTo}
            onBlur={(e) => this.onBlur(e, this.props.cid || newListId, "Attributed to")}
            onChange={this.changeNewChecklistFields}
          />
        </td>
        <td className="emmaTablesTd">
          <input className="input is-small is-link"
            type="text"
            placeholder=""
            name="newChecklistBestPractise"
            value={newChecklistBestPractise}
            onBlur={(e) => this.onBlur(e, this.props.cid || newListId, "Best Practice")}
            onChange={this.changeNewChecklistFields}
          />
        </td>
        <td className="emmaTablesTd">
          <input className="input is-small is-link"
            type="text"
            placeholder=""
            name="newChecklistNotes"
            value={newChecklistNotes}
            onBlur={(e) => this.onBlur(e, this.props.cid || newListId, "Notes")}
            onChange={this.changeNewChecklistFields}
          />
        </td>
      </tr>
    )
  }

  render() {
    const { waiting } = this.state
    const {configChecklists, cid} = this.props
    const currentChecklist = (configChecklists && configChecklists.length && cid) ? configChecklists.find(e => e.id === cid) : {}
    const isDisabled = !!(currentChecklist && currentChecklist.action && currentChecklist.action === "inactive")
    if (waiting) {
      return <Loader />
    }
    return (
      <div>
        <div>
          <div className="tbl-scroll">
            <table className="table is-bordered is-striped is-hoverable is-fullwidth">
              <thead>
                {this.renderTableHeader()}
              </thead>
              <tbody>
                {this.renderChecklistBasicDetails()}
              </tbody>
            </table>
          </div>
        </div>
        {this.renderAccordions(this.state.accordions)}
        <strong>{this.state.generalError}</strong>
        <div className="text-center mt-2">
          <button className="button is-link is-small mr-2" disabled={isDisabled} onClick={this.addAccordion}>Add a new checklist</button>
          <button className="button is-link is-small mr-2" disabled={isDisabled} onClick={this.resetAllAcccordions}>Undo All Changes</button>
          <button className="button is-link is-small mr-2" disabled={isDisabled} onClick={this.saveAccordions}>Save</button>
        </div>
      </div>
    )
  }
}

// const mapStateToProps = ({ configChecklist }) => {
//   console.log("configChecklist in state : ", configChecklist)
//   return { config: configChecklist }
// }

export default withRouter(ChecklistDetails1)
