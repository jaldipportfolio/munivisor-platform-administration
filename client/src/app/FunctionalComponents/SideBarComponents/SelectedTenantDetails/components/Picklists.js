import React from "react"
import {Row, Col, FormGroup, Button, InputGroup, Input, InputGroupAddon} from "reactstrap"
import ReactTable from "react-table"
import { Combobox } from "react-widgets"
import cloneDeep from "lodash.clonedeep"
import {toast} from "react-toastify"
import swal from "sweetalert"
import SectionAccordion from "../../../../GlobalComponents/SectionAccordion"
import Section from "../../../../GlobalComponents/Section"
// eslint-disable-next-line import/named
import { getFirmPicklists, putPicklist, deletePicklist } from "../../../../Services/actions/Dashboard"
import Loader from "../../../../GlobalComponents/Loader"
import {highlightSearch} from "../../../../globalutilities/helpers"
import {TextInput} from "../../../../GlobalComponents/TextInput"
import { CONST } from "../../../../globalutilities/consts"

const ObjectID = require("bson-objectid")

const getColumns = (searchTerm, onSelectPicklist) => [
  {
    Header: "Screen Name",
    id: "title",
    className: "multiExpTblVal",
    accessor: item => item,
    minWidth: 100,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd -striped">
          <small
            dangerouslySetInnerHTML={{
              __html: highlightSearch((item && item.title && item.title.toString()) || "-", searchTerm)
            }}
          />
        </div>
      )
    },
    sortMethod: (a, b) => `${a.title}` - `${b.title}`
  },
  {
    Header: "System Name",
    id: "systemName",
    className: "multiExpTblVal",
    accessor: item => item,
    minWidth: 150,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text">
          <small
            dangerouslySetInnerHTML={{
              __html: highlightSearch((item && item.meta && item.meta.systemName && item.meta.systemName.toString()) || "-", searchTerm)
            }}
          />
        </div>
      )
    },
    sortMethod: (a, b) => a.meta.systemName !== undefined && b.meta.systemName !== undefined ? a.meta.systemName.localeCompare(b.meta.systemName) : ""
  },
  {
    Header: "2nd Sublist",
    id: "subListLevel2",
    className: "multiExpTblVal",
    accessor: item => item,
    minWidth: 150,
    Cell: row => {
      const item = row.value
      return (
        <label className="checkbox"> {/* eslint-disable-line */}
          <input type="checkbox" checked={item.meta.subListLevel2} disabled/>
        </label>
      )
    }
  },
  {
    Header: "3rd Sublist",
    id: "subListLevel3",
    className: "multiExpTblVal",
    accessor: item => item,
    minWidth: 100,
    Cell: row => {
      const item = row.value
      return (
        <label className="checkbox"> {/* eslint-disable-line */}
          <input type="checkbox" checked={item.meta.subListLevel3} disabled/>
        </label>
      )
    }
  },
  {
    Header: "Client Edit",
    id: "editable",
    className: "multiExpTblVal",
    accessor: item => item,
    minWidth: 100,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text">
          <small>{item.meta.editable ? "Yes" : "No"}</small>
        </div>
      )
    },
    sortMethod: (a, b) => a.meta.editable.toString().localeCompare(b.meta.editable.toString()) //  `${a.meta.editable.toString()}` - `${b.meta.editable.toString()}`
  },
  {
    Header: "Action",
    id: "action",
    className: "multiExpTblVal",
    accessor: item => item,
    minWidth: 100,
    Cell: row => {
      const item = row.value
      return (
        <div className="text-center">
          <span title="Edit" style={{cursor: "pointer"}} onClick={() => onSelectPicklist(item)}>
            <i className="fa fa-pencil-square-o fa-lg text-info"/>
          </span>
        </div>
      )
    }
  }
]

const tableHead = [
  {name: "Name", title: "Picklist item name as it appears on the screen"},
  {name: "Included", title: "Item is part of picklist"},
  {name: "Visible", title: "Item is visible in picklist"},
  {name: "Action", title: "Item is visible in picklist"},
]

const initialAccordionsData = {
  "Add a title": [
    {
      label: "",
      included: true,
      visible: true,
      items: []
    }
  ]
}

const initialPicklistsMeta = {
  "Add a title": {
    key: ObjectID(),
    systemName: "",
    subListLevel2: false,
    subListLevel3: false,
    systemConfig: true,
    restrictedList: false,
    externalList: false,
    bestPractice: true
  }
}

class Picklists extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      // picklists: [],
      // ediTablePicklist: [],
      nonEdiTablePicklist: [],
      newPicklists: [],
      validationError: {},
      activeItem: [],
      selectedPicklist: {},
      copyOfSelectedPicklist: {},
      loading: true,
      edit: true,
      searchTerm: "",
      generalError: "",
      confirmAlert: CONST.confirmAlert
    }
  }

  async componentDidMount() {
    this.fetchPicklists()
  }

  fetchPicklists = (names) => {
    const {tenantId} = this.props
    const {searchTerm} = this.state
    this.setState({
      loading: true
    }, async () => {
      const details = await getFirmPicklists(tenantId, searchTerm, names || "")
      if (details &&  Array.isArray(details)) {
        if(names){
          const selectedPicklist = cloneDeep((details && details.length && details[0]) || {})
          this.setState({
            selectedPicklist,
            copyOfSelectedPicklist: selectedPicklist,
            loading: false,
            activeItem: [1,2,3]
          })
        }else {
          // const ediTablePicklist = (details || []).filter(pick => pick.meta.editable)
          // const nonEdiTablePicklist = (details || []).filter(pick => !pick.meta.editable)
          this.setState(prevState => ({
            // ediTablePicklist,
            nonEdiTablePicklist: details || [],
            picklists: !searchTerm ? details : prevState.picklists,
            loading: false
          }))
        }
      }
    })
  }

  onChange = (event) => {
    const {name, value, type, checked} = event.target
    this.setState({
      [name]: type === "checkbox" ? checked : value
    }, () => {
      if(name === "searchTerm"){
        this.fetchPicklists()
      }
    })
  }

  onSelectPicklist = (item) => {
    const  { meta } = item
    this.fetchPicklists(meta.systemName)
    console.log(item)
  }

  renderHeader = (key, levelIndex) => {
    const {secondLevelIdx, thirdLevelIdx, selectedPicklist} = this.state
    return (
      <div className="field is-grouped">
        <div className="control">
          <button className="button is-link is-small" type="button" onClick={() => this.addAcccordionItem(key, levelIndex, secondLevelIdx, thirdLevelIdx)} disabled={!this.state.edit}>Add
          </button>
        </div>
        <div className="control">
          <button className="button is-light is-small" type="button"
            onClick={() => this.resetAcccordion(key, levelIndex, secondLevelIdx, thirdLevelIdx)} disabled={!this.state.edit}>Reset
          </button>
        </div>
        <div className="control">
          {this.state.edit && levelIndex === 1?
            <div className="control">
              <i className="fa fa-trash-o is-size-4 " onClick={() => this.removeAccordion(selectedPicklist && selectedPicklist._id)}/>
            </div> : null}
        </div>
      </div>
    )
  }

  renderPicklist = (key, levelIndex) => (
    <div>
      {this.renderBody(key, levelIndex)}
    </div>
  )

  renderBody = (key, levelIndex) => {
    const {picklists, secondLevelIdx, secondLevelKey, selectedPicklist, thirdLevelKey, thirdLevelIdx} = this.state
    let pickListArray = []
    let secondLevelItems = []

    if(levelIndex === 1){
      pickListArray = selectedPicklist.items
    }else if(levelIndex === 2){
      pickListArray = (secondLevelIdx >= 0) && selectedPicklist && selectedPicklist.items && selectedPicklist.items[secondLevelIdx] ? selectedPicklist.items[secondLevelIdx].items: []
    }else if(levelIndex === 3){
      secondLevelItems = (secondLevelIdx >= 0) && selectedPicklist && selectedPicklist.items && selectedPicklist.items[secondLevelIdx] ?  selectedPicklist.items[secondLevelIdx].items: []
      pickListArray = secondLevelItems && secondLevelItems.length && (secondLevelIdx >= 0) && (thirdLevelIdx >= 0) &&
      secondLevelItems[thirdLevelIdx] ? secondLevelItems[thirdLevelIdx].items : []
      console.log("secondLevelItems : ", secondLevelItems)
    }

    return (
      <div className="tbl-scroll">
        {
          levelIndex === 1 ?
            <div className="columns">
              <div className="column is one-third is-pulled-left">
                <label className="label">System Name</label>
                <Combobox
                  style={{"maxWidth": "350px"}}
                  data={picklists.map(item => item.meta.systemName)}
                  caseSensitive={false}
                  minLength={1}
                  disabled={!!selectedPicklist._id}
                  value={selectedPicklist.meta.systemName || ""}
                  onChange={this.changeSystemName}
                  filter="contains"
                />
              </div>
              <div className="column is one-third">
                <label className="label">2nd level</label>
                <input
                  className="checkbox"
                  type="checkbox"
                  name="subListLevel2"
                  checked={selectedPicklist.meta.subListLevel2 || false}
                  onChange={this.changePicklistMeta}
                />
              </div>
              <div className="column is one-third">
                <label className="label">3rd level</label>
                <input
                  className="checkbox"
                  type="checkbox"
                  name="subListLevel3"
                  checked={selectedPicklist.meta.subListLevel3 || false}
                  onChange={this.changePicklistMeta}
                />
              </div>
              <div className="column is one-third">
                <label className="label">Client Edit</label>
                <input
                  className="checkbox"
                  type="checkbox"
                  name="editable"
                  checked={selectedPicklist.meta.editable || false}
                  onChange={this.changePicklistMeta}
                />
              </div>
            </div> : null
        }
        {
          levelIndex === 2 ?
            <div className="column is one-third is-pulled-left">
              <label className="label">Pick related level 1 picklist item</label>
              <div className="select is-link is-small form-group" style={{ maxWidth: 350}}>
                <select className="form-control" value={JSON.stringify({ idx: secondLevelIdx, val: secondLevelKey })}
                  onChange={this.changeSecondLevelKey} >
                  <option value={JSON.stringify({ idx: -1, val: "" })}>Pick Level 2 List Item</option>
                  {
                    selectedPicklist && selectedPicklist.items ? selectedPicklist.items.map((e, i) => (
                      <option key={i.toString()} value={JSON.stringify({ idx: i, val: e.label })}>{e.label}</option>
                    )) : null
                  }
                </select>
              </div>
            </div> : null
        }
        {
          levelIndex === 3 ?
            <div className="column is one-third is-pulled-left">
              <label className="label">Pick related level 2 picklist item</label> {/* eslint-disable-line */}
              <div className="select is-link is-small">
                <select value={JSON.stringify({ idx: thirdLevelIdx, val: thirdLevelKey })}
                  onChange={this.changeThirdLevelKey}>
                  <option value={JSON.stringify({ idx: -1, val: "" })}>Pick Level 3 List Item</option>
                  {secondLevelItems &&
                  secondLevelItems.map((e, i) => (
                    <option key={i.toString()}
                      value={JSON.stringify({ idx: i, val: e.label })}>{e.label}</option>
                  ))
                  }
                </select>
              </div>
            </div> : null
        }
        <br/>
        <table className="table">
          <thead>
            <tr>
              {tableHead.map(head => <th key={head.name}> <span title={head.title}>{head.name}</span></th>)}
            </tr>
          </thead>
          <tbody>
            {(pickListArray || []).map((e, i) => this.renderBodyItem(e, key, i, levelIndex, secondLevelIdx, secondLevelKey, thirdLevelIdx, thirdLevelKey))}
          </tbody>
        </table>
      </div>
    )
  }

  renderBodyItem = (e, key,  i, levelIndex, secondLevelIdx, secondLevelKey, thirdLevelIdx, thirdLevelKey) => (
    <tr key={key + i}>
      <td className="emmaTablesTd">
        <TextInput
          colXs={18}
          colMd={18}
          colXl={18}
          className="m-0"
          inputClassName="form-control form-control-sm"
          value={e.label}
          disabled={!this.state.edit}
          onChange={(event) => this.changeLabel( key, i, event, secondLevelIdx, secondLevelKey, levelIndex, thirdLevelIdx, thirdLevelKey )}
          error={(this.state.validationError && this.state.validationError[levelIndex.toString()] && this.state.validationError[levelIndex.toString()][i]) || ""}
        />
      </td>
      <td style={{ "textAlign": "left" }} className="emmaTablesTd">
        <input
          className="checkbox"
          type="checkbox"
          checked={e.included}
          onChange={(event) => this.changeIncluded( key, i, event, secondLevelIdx, secondLevelKey, levelIndex, thirdLevelIdx, thirdLevelKey )}
          disabled={!this.state.edit}
        />
      </td>
      <td style={{ "textAlign": "left" }} className="emmaTablesTd">
        <input
          className="checkbox"
          type="checkbox"
          checked={e.visible}
          onChange={(event) => this.changeVisible(key, i, event, secondLevelIdx, secondLevelKey, levelIndex, thirdLevelIdx, thirdLevelKey )}
          disabled={!this.state.edit}
        />
      </td>
      <td style={{ "textAlign": "left" }} className="emmaTablesTd">
        <a className="has-text-link is-small" onClick={(event) => this.removeItem(key, i, event, secondLevelIdx, secondLevelKey, levelIndex, thirdLevelIdx, thirdLevelKey )}>
          <i className="fa fa-trash-o " />
        </a>
      </td>
    </tr>
  )

  renderTitleInputFieldWithIcon = () => {
    const {selectedPicklist} = this.state
    const iconStyle = {paddingLeft: "10px"}
    return (
      <div>
        <span>
          <strong>{selectedPicklist.title}</strong>
        </span>
        <span className="control" style={iconStyle}>
          <i className="fa fa-edit is-link" onClick={this.openScreenNameModal} />
        </span>
      </div>
    )
  }

  openScreenNameModal = () => {
    const {selectedPicklist} = this.state
    swal("System Name:", {
      content:  {
        element:  "input",
        attributes: {
          value: selectedPicklist.title
        }
      }
    }).then((value) => {
      selectedPicklist.title = value || selectedPicklist.title
      this.setState({
        selectedPicklist
      })
    })
  }

  changePicklistMeta = async (event) => {
    const {name, checked} = event.target
    const {selectedPicklist} = this.state
    if (name === "subListLevel3" && !(selectedPicklist && selectedPicklist.meta && selectedPicklist.meta.subListLevel2)) {
      return toast.warn("Please select 2nd level first", {
        position: toast.POSITION.TOP_RIGHT
      })
    }
    if (name === "subListLevel2" && !checked && (selectedPicklist && selectedPicklist.meta && selectedPicklist.meta.subListLevel3)) {
      return toast.warn("Please remove 3rd level first", {
        position: toast.POSITION.TOP_RIGHT
      })
    }
    selectedPicklist.meta[name] = checked
    this.setState({
      selectedPicklist
    })
  }

  removeItem = ( key, i, event, secondLevelIdx, level, levelIndex, thirdLevelIdx ) => {
    const {selectedPicklist} = this.state
    if(levelIndex === 2) {
      selectedPicklist.items[secondLevelIdx].items.splice(i, 1)
    } else if(levelIndex === 3) {
      selectedPicklist.items[secondLevelIdx].items[thirdLevelIdx].items.splice(i, 1)
    } else {
      selectedPicklist.items.splice(i, 1)
    }
    this.setState({
      selectedPicklist,
      validationError: {}
    })
  }

  changeSystemName = (systemName) => {
    const {selectedPicklist} = this.state
    selectedPicklist.meta.systemName = systemName
    this.setState({selectedPicklist})
  }

  changeSecondLevelKey = (e) => {
    let {validationError} = this.state
    const { idx, val } = JSON.parse(e.target.value)
    validationError = {
      ...validationError,
      second: {}
    }
    this.setState({ secondLevelIdx: idx, secondLevelKey: val, thirdLevelKey:null, thirdLevelIdx: null, validationError })
  }

  changeThirdLevelKey = (e) => {
    let {validationError} = this.state
    const { idx, val } = JSON.parse(e.target.value)
    validationError = {
      ...validationError,
      third: {}
    }
    this.setState({ thirdLevelIdx: idx, thirdLevelKey: val, validationError })
  }

  changeLabel = ( key, i, e, secondLevelIdx, secondLevelKey, levelIndex, thirdLevelIdx, thirdLevelKey ) => {
    const {selectedPicklist} = this.state
    const { value } = e.target
    if(levelIndex === 1){
      selectedPicklist.items[i] = { ...selectedPicklist.items[i], label: value }
    }else if(levelIndex === 2){
      selectedPicklist.items[secondLevelIdx].items[i].label = value
    }else if(levelIndex === 3){
      selectedPicklist.items[secondLevelIdx].items[thirdLevelIdx].items[i].label = value
    }
    this.setState({
      selectedPicklist
    })
  }

  changeIncluded = ( key, i, event, j, level, levelIndex ) => {
    const {selectedPicklist} = this.state
    const val = event.target.checked

    if(levelIndex === 2) {
      selectedPicklist.items.forEach((e => {
        if(e.label === level) {
          e.items[i] = { ...e.items[i], included: val }
        }
      }))
    } else if(levelIndex === 3) {
      selectedPicklist.items.forEach((e => {
        if(e.label === level) {
          e.items.forEach((item => {
            // item.items.map((data) => {
            item.items[i] = { ...item.items[i], included: val }
            // })
          }))
        }
      }))
    }
    else {
      selectedPicklist.items[i] = { ...selectedPicklist.items[i], included: val }
    }
    this.setState({
      selectedPicklist
    })
  }

  changeVisible = ( key, i, event, j, level, levelIndex ) => {
    const {selectedPicklist} = this.state
    const val = event.target.checked

    if(levelIndex === 2) {
      selectedPicklist.items.forEach((e => {
        if(e.label === level) {
          e.items[i] = { ...e.items[i], visible: val }
        }
      }))
    } else if(levelIndex === 3) {
      selectedPicklist.items.forEach((e => {
        if(e.label === level) {
          e.items.forEach((item => {
            item.items[i] = { ...item.items[i], visible: val }
          }))
        }
      }))
    } else {
      selectedPicklist.items[i] = { ...selectedPicklist.items[i], visible: val }
    }
    this.setState({
      selectedPicklist
    })
  }

  addAcccordionItem = (key, level, secondLevelIdx, thirdLevelIdx) => {
    this.setState(prevState => {
      const selectedPicklist = cloneDeep(prevState.selectedPicklist)
      if (level === 3) {
        if (secondLevelIdx >= 0 && thirdLevelIdx >= 0) {
          selectedPicklist.items[secondLevelIdx] = cloneDeep(selectedPicklist.items[secondLevelIdx])
          selectedPicklist.items[secondLevelIdx].items = cloneDeep(selectedPicklist.items[secondLevelIdx].items)
          selectedPicklist.items[secondLevelIdx].items[thirdLevelIdx] = cloneDeep(selectedPicklist.items[secondLevelIdx].items[thirdLevelIdx])
          selectedPicklist.items[secondLevelIdx].items[thirdLevelIdx].items.unshift(cloneDeep(initialAccordionsData["Add a title"][0]))
        }
      } else if (level === 2) {
        if (secondLevelIdx >= 0) {
          selectedPicklist.items[secondLevelIdx] = { ...selectedPicklist.items[secondLevelIdx] }
          selectedPicklist.items[secondLevelIdx].items = [...selectedPicklist.items[secondLevelIdx].items]
          selectedPicklist.items[secondLevelIdx].items.unshift(cloneDeep(initialAccordionsData["Add a title"][0]))
          thirdLevelIdx = null
        }
      } else {
        selectedPicklist.items.unshift(cloneDeep(initialAccordionsData["Add a title"][0]))
        thirdLevelIdx = null
        secondLevelIdx = null
      }
      return { selectedPicklist, secondLevelIdx, thirdLevelIdx }
    })
  }

  resetAcccordion = (key, level, secondLevelIdx, thirdLevelIdx) => {
    console.log("params : ", key, level, secondLevelIdx, thirdLevelIdx)
    const {secondLevelKey, thirdLevelKey} = this.state
    this.setState((prevState) => {
      const copyOfSelectedPicklist = cloneDeep(prevState.copyOfSelectedPicklist)
      const pickListItems = cloneDeep(prevState.selectedPicklist)
      let validationError = { ...prevState.validationError }
      if (level === 3) {
        if (secondLevelIdx >= 0 && thirdLevelIdx >= 0) {
          const originalSecondLevelPick = copyOfSelectedPicklist.items.find(item => item.label === secondLevelKey)

          pickListItems.items[secondLevelIdx] = pickListItems.items.find(item => item.label === secondLevelKey)
          pickListItems.items[secondLevelIdx].items = cloneDeep(pickListItems.items[secondLevelIdx].items)
          pickListItems.items[secondLevelIdx].items[thirdLevelIdx] = cloneDeep(originalSecondLevelPick.items.find(item => item.label === thirdLevelKey) || initialAccordionsData["Add a title"][0])

          if (validationError && validationError.third) {
            validationError.third = {}
          }
        }
      } else if (level === 2) {
        if (secondLevelIdx >= 0) {
          pickListItems.items[secondLevelIdx] = copyOfSelectedPicklist.items.find(item => item.label === secondLevelKey) || [cloneDeep(initialAccordionsData["Add a title"][0])]
          if (validationError && validationError.second) {
            validationError.second = {}
          }
        }
        thirdLevelIdx = null
      } else  {
        pickListItems.items = copyOfSelectedPicklist.items || [cloneDeep(initialAccordionsData["Add a title"][0])]
        validationError = {}
        thirdLevelIdx = null
        secondLevelIdx = null
      }
      return { selectedPicklist: pickListItems, validationError, secondLevelIdx, thirdLevelIdx }
    })
  }

  removeAccordion = (picklistId) => {
    const {tenantId} = this.props
    const { confirmAlert} = this.state
    if(!picklistId) return

    confirmAlert.text = "You want to delete this picklist?"
    swal(confirmAlert).then(willDelete => {
      if (willDelete) {
        this.setState({
          loading: true
        }, async () => {
          const result = await deletePicklist( {tenantId, picklistId})
          if(result.done){
            toast.success("Picklist removed successfully", {
              position: toast.POSITION.TOP_RIGHT
            })
            this.setState({
              selectedPicklist: {},
              loading: false
            })
            this.fetchPicklists()
          }else {
            toast.error("Something went wrong!", {
              position: toast.POSITION.TOP_RIGHT
            })
            this.setState({
              loading: false
            })
          }
        })
      }
    })

  }

  resetAllAcccordions = () => {
    this.setState({
      selectedPicklist: {},
      copyOfSelectedPicklist: {},
      secondLevelKey: "",
      searchString: "",
      generalError: "",
      validationError: {},
      systemName: "",
      secondLevelIdx: -1,
      thirdLevelKey: "",
      thirdLevelIdx: -1,
      secondLevelClearConfirmation: "",
      thirdLevelClearConfirmation: "",
      changedPicklists: [],
      newPicklists: []
    })
  }

  addAccordion = () => {
    this.setState(prevState => {
      const pickListItems = {}
      const newPicklists = [...prevState.newPicklists]
      const accordions = { ...prevState.picklists }
      const numKeys = Object.keys(accordions).length
      pickListItems.title = `Add Title ${numKeys}`
      pickListItems.items = cloneDeep(initialAccordionsData["Add a title"])
      pickListItems.meta = cloneDeep(initialPicklistsMeta["Add a title"])
      newPicklists.push(pickListItems.title)
      const activeItem = [1]
      return {
        config: {},
        selectedPicklist: pickListItems,
        newPicklists,
        secondLevelKey: "",
        accordionTitleToChange: "", searchString: "", metaView: false,
        generalError: "", systemName: "", newPicklist: true,
        secondLevelIdx: -1, thirdLevelKey: "", thirdLevelIdx: -1, activeItem,
        secondLevelClearConfirmation: "", thirdLevelClearConfirmation: "", edit: true
      }
    })
  }

  saveAccordions = () => {
    console.log("in saveAccordions")
    const {selectedPicklist, picklists} = this.state
    const picklistNames = picklists.filter(picklist => picklist.meta.systemName.toString().toLocaleLowerCase() === ((selectedPicklist && selectedPicklist.meta && selectedPicklist.meta.systemName) || "").toString().toLocaleLowerCase())
    if(!(selectedPicklist && selectedPicklist.meta && selectedPicklist.meta.systemName)) {
      return toast.error("System name should not be empty", {
        position: toast.POSITION.TOP_RIGHT
      })
    }
    if((picklistNames.length && !selectedPicklist._id) || (picklistNames.length > 1 && selectedPicklist._id)) {
      return toast.error("System name should not be duplicate", {
        position: toast.POSITION.TOP_RIGHT
      })
    }
    this.validateSubmition()
  }

  validateSubmition = () => {
    console.log("in validateSubmition")
    const {selectedPicklist} = this.state
    const data = cloneDeep(selectedPicklist.items)
    this.setState(() => {
      let newErr = {}
      Object.keys(data).forEach((a, i) => {
        if (!(data && data[a] && data[a].label)) {
          // selectedPicklist.items.splice(i, 1)
          newErr = {
            ...newErr,
            "1": {
              ...newErr["1"],
              [i] : "Required"
            }
          }
        }
        if (selectedPicklist.meta && selectedPicklist.meta.subListLevel2) {
          Object.keys(data[a].items).forEach((e, j) => {
            if (!data[a].items[e].label) {
              // selectedPicklist.items[a].items.splice(j, 1)
              newErr = {
                ...newErr,
                "2": {
                  ...newErr["2"],
                  [j] : "Required"
                }
              }
            }
            if (selectedPicklist.meta && selectedPicklist.meta.subListLevel3) {
              Object.keys(data[a].items[e].items).forEach((f, k) => {
                if (!data[a].items[e].items[f].label) {
                  // selectedPicklist.items[a].items[e].items.splice(k, 1)
                  newErr = {
                    ...newErr,
                    "3": {
                      ...newErr["3"],
                      [k] : "Required"
                    }
                  }
                }
              })
            }
          })
        }
      })
      return { validationError: newErr, selectedPicklist }
    }, this.saveData)
  }

  saveData = () => {
    const {tenantId} = this.props
    const { validationError, selectedPicklist } = this.state
    let error = false
    const errKeys = Object.keys(validationError)

    const checkErrorRecursively = (err) => {
      console.log("err1 : ", err)
      if (Object.getPrototypeOf(err).constructor.name === "Object") {
        const errKeys = Object.keys(err)
        console.log("errKeys : ", errKeys)
        if (!errKeys.length) {
          error = false
          return true
        }
        errKeys.some(k => checkErrorRecursively(err[k]))
      } else {
        console.log("err2 : ", err)
        if (err) {
          error = true
          return true
        }
        error = false
        return true
      }
    }

    if (!errKeys.length) {
      error = false
      // return true
    }
    errKeys.some(i => {
      const itemKeys = Object.keys(validationError[i])
      if (!itemKeys.length) {
        error = false
        return true
      }
      itemKeys.forEach(j => {
        const subItemKeys = Object.keys(validationError[i][j])
        if (!subItemKeys.length) {
          error = false
        }else {
          subItemKeys.some(k => checkErrorRecursively(validationError[i][j][k]))
        }
      })
      return false
    })

    console.log("error : ", error)

    if (!error) {
      console.log("Save Picklist: ",  {tenantId, pickList: selectedPicklist})
      this.setState({
        loading: true
      }, async () => {
        const result = await putPicklist( {tenantId, pickList: selectedPicklist})
        if(result.done){
          toast.success("Updated successfully", {
            position: toast.POSITION.TOP_RIGHT
          })
          this.setState({
            selectedPicklist: result.pickLists || {},
            loading: false
          },() => this.fetchPicklists())
        }else {
          toast.error("Something went wrong!", {
            position: toast.POSITION.TOP_RIGHT
          })
          this.setState({
            loading: false
          })
        }
      })
    }
  }

  render() {
    const {selectedPicklist, searchTerm, loading, nonEdiTablePicklist, activeItem, generalError} = this.state
    return (
      <div className="animated fadeIn">
        { loading ? <Loader/> : null }
        <Row>
          <Col xs="12" sm="12">
            <SectionAccordion
              multiple
              activeItem={activeItem.length ?  activeItem : [0] }
              isRequired
              render={({activeAccordions, onAccordion}) =>
                <div>
                  <Section
                    onAccordion={() => onAccordion(0)}
                    title="List"
                  >
                    {activeAccordions.includes(0) &&
                      <div>
                        <FormGroup row className="my-0">
                          <Col>
                            <div className="controls">
                              <InputGroup>
                                <Input type="text" name="searchTerm" value={searchTerm} onChange={this.onChange} placeholder="Search..." />
                                <InputGroupAddon addonType="append">
                                  <Button color="secondary" onClick={this.fetchPicklists}><i className="fa fa-search"/></Button>
                                </InputGroupAddon>
                              </InputGroup>
                            </div>
                          </Col>
                        </FormGroup>

                        <br/>

                        <ReactTable
                          columns={getColumns(searchTerm, this.onSelectPicklist)}
                          data={nonEdiTablePicklist || []}
                          showPaginationBottom
                          defaultPageSize={20}
                          pageSizeOptions={[5, 10, 20, 50, 100]}
                          className="-striped -highlight is-bordered"
                          style={{ overflowX: "auto" }}
                          showPageJump
                          minRows={2}
                        />
                      </div>
                    }
                  </Section>
                  <strong>{generalError}</strong>
                  <div className="text-right mt-2">
                    <button className="button is-link is-small mr-2" onClick={this.addAccordion}>Add new picklist</button>
                    <button className="button is-link is-small mr-2" onClick={this.resetAllAcccordions}>Undo All Changes</button>
                    <button className="button is-link is-small mr-2" onClick={this.saveAccordions} disabled={!(selectedPicklist && selectedPicklist.title)}>Save</button>
                  </div>
                  {
                    selectedPicklist && selectedPicklist.title ?
                      <Section
                        onAccordion={() => onAccordion(1)}
                        actionButtons={this.renderHeader(selectedPicklist.title, 1)}
                        title={this.renderTitleInputFieldWithIcon()}
                      >
                        {activeAccordions.includes(1) &&
                          <div>{selectedPicklist.title && this.renderPicklist(selectedPicklist.title, 1)}</div>
                        }
                      </Section>
                      : null
                  }
                  {
                    selectedPicklist && selectedPicklist.meta && selectedPicklist.meta.subListLevel2 ?
                      <Section
                        onAccordion={() => onAccordion(2)}
                        actionButtons={this.renderHeader(selectedPicklist.title, 2)}
                        title={`Configure 2nd Linked Sublist for ${selectedPicklist.title}`}
                      >
                        {activeAccordions.includes(2) &&
                          <div>{this.renderPicklist(selectedPicklist.title, 2)}</div>
                        }
                      </Section>
                      : null
                  }
                  {
                    selectedPicklist && selectedPicklist.meta && selectedPicklist.meta.subListLevel3 ?
                      <Section
                        onAccordion={() => onAccordion(3)}
                        actionButtons={this.renderHeader(selectedPicklist.title, 3)}
                        title={`Configure 3rd Linked Sublist for ${selectedPicklist.title}`}
                      >
                        {activeAccordions.includes(3) &&
                          <div>{this.renderPicklist(selectedPicklist.title, 3)}</div>
                        }
                      </Section>
                      : null
                  }
                </div>
              } />
          </Col>
        </Row>
      </div>
    )
  }
}

export default Picklists