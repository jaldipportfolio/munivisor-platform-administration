import React from "react"
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap"

export const SnapShotsTransactionModal  = ({onModalChange, isModal, transactionWise, total, modalTitle, elasticData, isTran, isElastic}) => {

  const { Deal, BankLoan, Derivative, Others, RFP, MaRfp, BusinessDevelopment } = transactionWise || {}


  const toggleModal = () => {
    onModalChange({ isModal: !isModal })
  }

  return(
    <Modal isOpen={isModal} toggle={toggleModal}
      className="modal-md">
      <ModalHeader toggle={toggleModal}>{modalTitle || ""}</ModalHeader>
      <ModalBody>

        { transactionWise && Object.keys(transactionWise).length && isTran ?

          <table className="table table-striped" style={{width: "100%"}}>
            <thead>
            <tr>
              <th>Type Of Transaction</th>
              <th className="center">Number Of Transaction</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td className="center">Deal</td>
              <td className="center">{Deal || 0}</td>
            </tr>
            <tr>
              <td className="center">Bank Loan</td>
              <td className="center">{BankLoan || 0}</td>
            </tr>
            <tr>
              <td className="center">Derivative</td>
              <td className="center">{Derivative || 0}</td>
            </tr>
            <tr>
              <td className="center">Others</td>
              <td className="center">{Others || 0}</td>
            </tr>
            <tr>
              <td className="center">RFP</td>
              <td className="center">{RFP || 0}</td>
            </tr>
            <tr>
              <td className="center">MaRfp</td>
              <td className="center">{MaRfp || 0}</td>
            </tr>
            <tr>
              <td className="center">Business Development</td>
              <td className="center">{BusinessDevelopment || 0}</td>
            </tr>
            <tr>
              <td className="center"><b>Total</b></td>
              <td className="center"><b>{total || 0}</b></td>
            </tr>
            </tbody>
          </table> : null
        }

        { elasticData && isElastic ?
          <table className="table table-striped" style={{width: "100%"}}>
            <thead>
            <tr>
              <th>Type</th>
              <th>Data</th>
              <th className="center">Docs</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td className="center"><b>Docs Count</b></td>
              <td className="center">{(elasticData && elasticData.data && elasticData.data.docsCount) || 0}</td>
              <td className="center">{(elasticData && elasticData.docs && elasticData.docs.docsCount) || 0}</td>
            </tr>
            <tr>
              <td className="center"><b>Docs Deleted</b></td>
              <td className="center">{(elasticData && elasticData.data && elasticData.data.docsDeleted) || 0}</td>
              <td className="center">{(elasticData && elasticData.docs && elasticData.docs.docsDeleted) || 0}</td>
            </tr>
            <tr>
              <td className="center"><b>Indexing Index Total</b></td>
              <td
                className="center">{(elasticData && elasticData.data && elasticData.data.indexingIndexTotal) || 0}</td>
              <td
                className="center">{(elasticData && elasticData.docs && elasticData.docs.indexingIndexTotal) || 0}</td>
            </tr>
            <tr>
              <td className="center"><b>Search Query Total</b></td>
              <td className="center">{(elasticData && elasticData.data && elasticData.data.searchQueryTotal) || 0}</td>
              <td className="center">{(elasticData && elasticData.docs && elasticData.docs.searchQueryTotal) || 0}</td>
            </tr>
            <tr>
              <td className="center"><b>Search Fetch Total</b></td>
              <td className="center">{(elasticData && elasticData.data && elasticData.data.searchFetchTotal) || 0}</td>
              <td className="center">{(elasticData && elasticData.docs && elasticData.docs.searchFetchTotal) || 0}</td>
            </tr>
            <tr>
              <td className="center"><b>Size</b></td>
              <td className="center">{(elasticData && elasticData.data && elasticData.data.size) || "0.00 Bytes"}</td>
              <td className="center">{(elasticData && elasticData.docs && elasticData.docs.size) || "0.00 Bytes"}</td>
            </tr>
            </tbody>
          </table> : null
        }

      </ModalBody>
      <ModalFooter>
        <Button color="secondary" onClick={toggleModal}>Cancel</Button>
      </ModalFooter>
    </Modal>
  )
}