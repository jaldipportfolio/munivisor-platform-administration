import React from "react"
import { Row, Col, Label, FormGroup } from "reactstrap"
import Accordion from "../../../../GlobalComponents/Accordion"
// eslint-disable-next-line import/named
import { getFirmDetailsInfo } from "../../../../Services/actions/Dashboard"
import Loader from "../../../../GlobalComponents/Loader"

class FirmDetails extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      firmDetails: [],
      loading: true
    }
  }

  async componentDidMount() {
    const {firmId} = this.props
    const details = await getFirmDetailsInfo(firmId)
    if (details && details.length) {
      this.setState({
        firmDetails: details || [],
        loading: false
      })
    }
  }

  render() {
    const { firmDetails, loading } = this.state
    const firm = (firmDetails && firmDetails[0]) || {}
    return (
      <div>
        { loading ? <Loader/> : null }
        <Accordion
          activeAccordionHeader="Details">
          <Row>
            <Col xs="6" sm="4">
              <FormGroup>
                <Label><b>Firm Name:</b></Label><br/>
                {(firm && firm.msrbFirmName) || "--"}
              </FormGroup>
            </Col>
            <Col xs="6" sm="4">
              <FormGroup>
                <Label><b>Registrant Type:</b></Label><br/>
                {(firm && firm.msrbRegistrantType) || "--"}
              </FormGroup>
            </Col>
            <Col sm="4">
              <FormGroup>
                <Label><b>ID:</b></Label><br/>
                {(firm && firm.msrbId) || "--"}
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col xs="6" sm="4">
              <FormGroup>
                <Label><b>Tax Id:</b></Label><br/>
                {(firm && firm.taxId) || "--"}
              </FormGroup>
            </Col>
            <Col xs="6" sm="4">
              <FormGroup>
                <Label><b>Business Structure:</b></Label><br/>
                {(firm && firm.businessStructure) || "--"}
              </FormGroup>
            </Col>
            <Col sm="4">
              <FormGroup>
                <Label><b>Number of Employees:</b></Label><br/>
                {(firm && firm.numEmployees) || "--"}
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col xs="6" sm="4">
              <FormGroup>
                <Label><b>Annual Revenue:</b></Label><br/>
                {(firm && firm.annualRevenue) || "--"}
              </FormGroup>
            </Col>
          </Row>
        </Accordion>
      </div>
    )
  }
}

export default FirmDetails