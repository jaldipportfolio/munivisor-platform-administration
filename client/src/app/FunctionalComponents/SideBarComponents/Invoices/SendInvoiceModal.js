import React from "react"
import {Col, FormGroup, Input, Label} from "reactstrap"
import CKEditor from "react-ckeditor-component"
import { StrapModal } from "../../../GlobalComponents/Modal"
import Loader from "../../../GlobalComponents/Loader"
import {SelectLabelInput, TextInput} from "../../../GlobalComponents/TextInput"

const emailsFrom = [
  {name: "addEmail", value: "existingEmail", label: "Select existing email"},
  {name: "addEmail", value: "enterManually", label: "Add manually"}
]

const SendInvoiceModal = ({state, onSend, onModalChange}) => {
  const disabled = false
  // const {state, onSend} = props
  const {invoice, addEmail, invoiceNumber, sendEmail, loading, modalLoading, emailTitle, errorMessages} = state || {}
  const toggleModal = () => {
    onModalChange({
      modalVisible: !state.modalVisible,
    })
  }

  const onChange = (e, name) => {
    let stateObject = {}
    if(name === "emailMessage"){
      stateObject = {
        [name]: e.editor.getData() || ""
      }
    }else if(e && e.target && e.target.name === "addEmail"){
      stateObject = {
        [e.target.name]: e.target.value || "",
        sendEmail: ""
      }
    }else {
      stateObject = {
        [e.target.name]: e.target.value || ""
      }
    }
    onModalChange(stateObject)
  }

  return (
    <StrapModal
      closeModal={toggleModal}
      modalState={state.modalVisible}
      title="Send Invoice"
      size="modal-lg"
      buttonName="Send"
      saveModal={disabled ? false : onSend}
      modalWidth={{ width: "70%" }}
      disabled={loading || false}
    >
      {modalLoading ? (
        <Loader/>
      ) : (
        <div>
          <section className="container">
            <FormGroup row>
              <Col sm="4">
                <h6>Name:</h6>
                <div>{invoice.name}</div>
              </Col>
              <Col sm="4">
                <h6>Invoice Number:</h6>
                <div>{invoiceNumber}</div>
              </Col>
              <Col sm="4" className="mb-4">
                <h6>Amount:</h6>
                <div>${invoice.payableAmount}</div>
              </Col>
              <Col md={12} xs={12} xl={12} className="mb-4">
                {
                  emailsFrom.map(email => (
                    <FormGroup key={email.value} check inline>
                      <Input className="form-check-input" type="radio" id={email.value} name={email.name} value={email.value} checked={state && state[email.name] === email.value} onChange={onChange}/>
                      <Label className="form-check-label" check htmlFor={email.value}>{email.label}</Label>
                    </FormGroup>
                  ))
                }
              </Col>
              {
                addEmail && addEmail === "enterManually" ?
                  <TextInput
                    colXs={12}
                    colMd={12}
                    colXl={12}
                    className="mb-4"
                    title="Email"
                    label="Email"
                    name="sendEmail"
                    value={sendEmail || ""}
                    onChange={onChange}
                    required
                    error={(errorMessages && errorMessages.sendEmail) || ""}
                  /> :
                  <SelectLabelInput
                    colXs={12}
                    colMd={12}
                    colXl={12}
                    className="mb-4"
                    title="Email"
                    label="Email"
                    list={(invoice && invoice.allFirmUserEmails && invoice.allFirmUserEmails.map(uEmail => ({name: uEmail, value: uEmail}))) || []}
                    name="sendEmail"
                    value={sendEmail || ""}
                    onChange={onChange}
                    required
                    error={(errorMessages && errorMessages.sendEmail) || ""}
                  />
              }

              <TextInput
                colXs={12}
                colMd={12}
                colXl={12}
                title="Email Title"
                label="Email Title"
                name="emailTitle"
                value={emailTitle || ""}
                onChange={onChange}
                error={(errorMessages && errorMessages.emailTitle) || ""}
              />
              <Col md="12">
                <Label><b>Message</b></Label>
                <div className="control">
                  <CKEditor
                    activeClass="p10"
                    content={state.emailMessage}
                    events={{
                      change: e => onChange(e, "emailMessage")
                    }}
                  />
                </div>
              </Col>
            </FormGroup>
          </section>
        </div>
      )}
    </StrapModal>
  )
}

export default SendInvoiceModal
