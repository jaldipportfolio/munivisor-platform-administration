import React from "react"
import moment from "moment"
import {Button, Col, FormGroup, Input, InputGroup, InputGroupAddon, Row} from "reactstrap"
import ReactTable from "react-table"
import {toast} from "react-toastify"
import {Link} from "react-router-dom"
import { DateRangePicker } from "react-dates"
import "react-table/react-table.css"
import "react-dates/lib/css/_datepicker.css"
import "react-dates/initialize"
import { getInvoicesList } from "../../../Services/actions/Invoice"
import Accordion from "../../../GlobalComponents/Accordion"
import Loader from "../../../GlobalComponents/Loader"
import DocLink from "../../../GlobalComponents/DocLink"

const highlightSearch = (string, searchQuery) => {
  const reg = new RegExp(searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&"), "i")
  return string.replace(reg, str => `<mark>${str}</mark>`)
}

class Invoice extends React.Component{
  constructor(props) {
    super(props)
    this.state = {
      invoiceList: [],
      invoiceSearch: "",
      totalPages: "",
      loading: true,
      startDate: null,
      endDate: null
    }
  }

  getFilteredData = async (pageDetails) => {
    const {tenantId} = this.props
    this.setState({loading: true,  page: {global: pageDetails}  })
    const { invoiceSearch, startDate, endDate } = this.state
    const { page, pageSize, sorted } = pageDetails
    let sortedFields = {}
    if (sorted && sorted[0]) {
      sortedFields[sorted[0].id] = sorted[0].desc ? -1 : 1
      this.setState({ sortedFields })
    } else {
      sortedFields = this.state.sortedFields
      if (sortedFields === undefined) {
        sortedFields = { "_id": -1 }
      }
    }

    const isPage = {
      page,
      tenantId,
      pageSize,
      sorted: sortedFields,
      searchField: invoiceSearch || "",
      startDate: startDate ? moment(startDate) : null,
      endDate: endDate ? moment(endDate) : null
    }
    const invoice = await getInvoicesList(isPage)
    if(invoice && invoice.data && invoice.metadata){
      // const total = (invoice.metadata[0] && invoice.metadata[0].total) || 0
      const totalPage = (invoice.metadata[0] && invoice.metadata[0].pages) || 0
      this.setState({
        invoiceList: invoice && invoice.data,
        totalPages: totalPage,
        loading: false
      })
    }else {
      toast.error("Something went wrong!", {
        position: toast.POSITION.TOP_RIGHT
      })
      this.setState({
        loading: false
      })
    }
  }

    onInvoicesSearch = (e) => {
      const { name, value } = e.target
      const { page } = this.state
      this.setState({
        [name]: value
      }, () => this.getFilteredData(page.global))
    }

    render(){
      const { invoiceList , invoiceSearch, loading, totalPages, page, startDate, endDate, focusedInput, orientation, openDirection} = this.state
      return(
        <div className="animated fadeIn">
          { loading ? <Loader/> : null }
          <Row>
            <Col xs="12" sm="12">
              <Accordion activeAccordionHeader="Invoices">
                <FormGroup row className="my-0">
                  <Col xs="12" md="12">
                    <div className="controls">
                      <InputGroup>
                        <InputGroupAddon addonType="append">
                          <Button color="secondary"><i className="fa fa-search"/></Button>
                        </InputGroupAddon>
                        <Input type="text" name="invoiceSearch" onChange={this.onInvoicesSearch} placeholder="Search..."/>
                        <DateRangePicker
                          small
                          showClearDates
                          endDate={endDate}
                          endDateId="endDate"
                          showDefaultInputIcon
                          startDate={startDate}
                          startDateId="startDate"
                          orientation={orientation}
                          focusedInput={focusedInput}
                          isOutsideRange={() => false}
                          openDirection={openDirection}
                          onFocusChange={focusedInput => this.setState({focusedInput})}
                          onDatesChange={({startDate, endDate}) => this.setState({startDate, endDate},()=>{ this.getFilteredData(page.global) })}
                        />
                      </InputGroup>
                    </div>
                  </Col>
                </FormGroup><br/>
                {/* <br/>
                <InvoceGrid/><br/><br/> */}
                <ReactTable
                  columns={[
                    {
                      Header: "Name",
                      id: "name",
                      className: "multiExpTblVal",
                      accessor: item => item,
                      minWidth: 100,
                      Cell: row => {
                        const item = row.value
                        return (
                          <div className="hpTablesTd wrap-cell-text">
                            <Link to={`/tenant/firm?firmId=${item.tenantId}&name=${item && item.name}`}
                              dangerouslySetInnerHTML={{
                                __html: highlightSearch(`${item && item.name && item.name.toString()}` || "-", invoiceSearch)
                              }} />
                          </div>
                        )
                      },
                      sortMethod: (a, b) => `${a.name}` - `${b.name}`
                    },
                    {
                      Header: "Series 50 users",
                      id: "series50",
                      className: "multiExpTblVal",
                      accessor: item => item,
                      minWidth: 100,
                      Cell: row => {
                        const item = row.value
                        return (
                          <div className="hpTablesTd -striped">
                            <div
                              dangerouslySetInnerHTML={{
                                __html: highlightSearch((item && item.series50 && `${Number(item.series50).toLocaleString()}`) || "0", invoiceSearch)
                              }}
                            />
                          </div>
                        )
                      },
                      sortMethod: (a, b) => `${a.series50}` - `${b.series50}`
                    },
                    {
                      Header: "Non series 50 users",
                      id: "nonSeries50",
                      className: "multiExpTblVal",
                      accessor: item => item,
                      minWidth: 100,
                      Cell: row => {
                        const item = row.value
                        return (
                          <div className="hpTablesTd -striped">
                            <div
                              dangerouslySetInnerHTML={{
                                __html: highlightSearch((item && item.nonSeries50 && `${Number(item.nonSeries50).toLocaleString()}`) || "0", invoiceSearch)
                              }}
                            />
                          </div>
                        )
                      },
                      sortMethod: (a, b) => `${a.series50}` - `${b.series50}`
                    },
                    {
                      Header: "Payable Amount",
                      id: "payableAmount",
                      className: "multiExpTblVal",
                      accessor: item => item,
                      minWidth: 100,
                      Cell: row => {
                        const item = row.value
                        return (
                          <div className="hpTablesTd -striped">
                            <div
                              dangerouslySetInnerHTML={{
                                __html: highlightSearch((item && item.payableAmount && `$${Number(item.payableAmount).toLocaleString()}`) || "$0", invoiceSearch)
                              }}
                            />
                          </div>
                        )
                      },
                      sortMethod: (a, b) => `${a.payableAmount}` - `${b.payableAmount}`
                    },
                    {
                      Header: "Type Of Billing",
                      id: "typeOfBilling",
                      className: "multiExpTblVal",
                      accessor: item => item,
                      minWidth: 100,
                      Cell: row => {
                        const item = row.value
                        return (
                          <div className="hpTablesTd -striped">
                            <div
                              dangerouslySetInnerHTML={{
                                __html: highlightSearch((item && item.typeOfBilling && item.typeOfBilling.toString()) || "-", invoiceSearch)
                              }}
                            />
                          </div>
                        )
                      },
                      sortMethod: (a, b) => `${a.typeOfBilling}` - `${b.typeOfBilling}`
                    },
                    {
                      Header: "Bill Frequency",
                      id: "billFrequency",
                      className: "multiExpTblVal",
                      accessor: item => item,
                      minWidth: 100,
                      Cell: row => {
                        const item = row.value
                        return (
                          <div className="hpTablesTd -striped">
                            <div
                              dangerouslySetInnerHTML={{
                                __html: highlightSearch((item && item.billFrequency && item.billFrequency.toString()) || "-", invoiceSearch)
                              }}
                            />
                          </div>
                        )
                      },
                      sortMethod: (a, b) => `${a.billFrequency}` - `${b.billFrequency}`
                    },
                    {
                      Header: "Status",
                      id: "status",
                      className: "multiExpTblVal",
                      accessor: item => item,
                      minWidth: 100,
                      Cell: row => {
                        const item = row.value
                        return (
                          <div className="hpTablesTd -striped">
                            <div
                              dangerouslySetInnerHTML={{
                                __html: highlightSearch((item && item.status && item.status.toString()) || "-", invoiceSearch)
                              }}
                            />
                          </div>
                        )
                      },
                      sortMethod: (a, b) => `${a.status}` - `${b.status}`
                    },
                    {
                      Header: "Created At",
                      id: "createdAt",
                      className: "multiExpTblVal",
                      accessor: item => item,
                      Cell: row => {
                        const item = row.value
                        return (
                          <div className="hpTablesTd -striped">
                            {item && item.createdAt && moment(item.createdAt).format("MM/DD/YYYY hh:mm A")}
                          </div>
                        )
                      },
                      sortMethod: (a, b) => a.date - b.date
                    },
                    {
                      Header: "Action",
                      id: "action",
                      className: "multiExpTblVal",
                      sortable: false,
                      accessor: item => item,
                      minWidth: 80,
                      Cell: row => {
                        const item = row.value
                        return (
                          <div>
                            <Link className="btn-link btn-block d-inline-block" title="Preview" style={{width: "25%"}} to={`/invoicepreview/invoice?invoiceId=${item._id}`}>
                              <i className="fa fa-eye" />
                            </Link>
                            {item.invoiceDocId ? <DocLink className="text-center d-inline-block" style={{width: "25%"}} downloadIcon docId={item.invoiceDocId}/> : null}
                          </div>
                        )
                      }
                    },
                  ]}
                  data={invoiceList || []}
                  manual
                  showPaginationBottom
                  defaultPageSize={10}
                  pageSizeOptions={[5, 10, 20, 50, 100]}
                  className="-striped -highlight is-bordered"
                  style={{ overflowX: "auto" }}
                  pages={totalPages}
                  minRows={2}
                  onFetchData={state => {
                    this.getFilteredData(state)
                  }}
                />
              </Accordion>
            </Col>
          </Row>
        </div>
      )
    }
}

export default Invoice