import React from "react"
import {toast} from "react-toastify"
import swal from "sweetalert"
import moment from "moment"
import {Card, Button, CardHeader, CardBody, Row, Col, Table, Input, InputGroup, InputGroupAddon } from "reactstrap"
import {
  getInvoicePreview,
  updateInvoicePreview,
  generateInvoice,
  invoicePaymentRefund,
  invoiceIdGenerator,
  updateInvoiceLog,
  sendInvoiceOnEmail,
  // eslint-disable-next-line import/named
  getPartialRefunds
} from "../../../Services/actions/Invoice"
import Loader from "../../../GlobalComponents/Loader"
import {
  errorToast,
  regexTestsForEmail
} from "../../../globalutilities/helpers"
// eslint-disable-next-line import/named
import { addDocInDB, getSignedUrl } from "../../../Services/actions/Docs"
import SendInvoiceModal from "./SendInvoiceModal"
import RefundModal from "./RefundModal"
import ViewPartialRefundsModal from "./ViewPartialRefundsModal"
import {connect} from "react-redux"
import { Link, withRouter} from "react-router-dom"

class InvoicePreview extends React.Component{
  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      invoiceId: "",
      invoiceNumber: "",
      emailTitle: "Invoice From Munivisor",
      emailMessage: "",
      sendEmail: "",
      addEmail: "existingEmail",
      errorMessages: {},
      modalLoading: false,
      modalVisible: false,
      refundModal: false,
      reason: "",
      reasonDescription: "",
      invoice: {},
      isInvoiceEditable: false,
      partialRefundedModal: false,
      partialRefunds: [],
      payableAmount: "",
      discountPercentage: "",
      refundAmount: 0,
      refundedAmount: 0,
      queryString: {},
      confirmAlert: {
        title: "Are you sure?",
        text: "",
        buttons: true,
        dangerMode: true
      }
    }
  }

  async componentWillMount() {
    const { invoiceId } = this.props
    console.log("invoiceId", invoiceId)
    const result = await getInvoicePreview(invoiceId)
    console.log(result)
    this.setState({
      invoiceNumber: (result && result.invoice && result.invoice.invoiceId) || invoiceIdGenerator(),
      invoice: (result && result.invoice) || {},
      loading: false
    },() => this.showPartialRefunds())
  }

  renderTableHeadOrData =(tag, value) => {
    if(tag === "head") return <th className="right">{value}</th>
    if(tag === "data") return <td className="right">{value}</td>
    return null
  }

  onPrint = async () => {
    const {emailTitle, sendEmail, emailMessage, invoice, invoiceNumber} = this.state
    const  payload = {
      invoiceId: invoice._id,
      invoiceNumber,
      sendEmail,
      emailTitle,
      emailMessage,
    }
    console.log(payload)
    await generateInvoice(payload, "preview")
  }

  onInvoiceModal = () => {
    this.setState({
      modalVisible: true,
    })
  }

  onSend = async () => {
    const {emailTitle, sendEmail, emailMessage, invoice, invoiceNumber} = this.state
    const  payload = {
      invoiceId: invoice._id,
      invoiceNumber,
      sendEmail,
      emailTitle,
      emailMessage,
      status: "Approved"
    }
    console.log(payload)
    const email = await regexTestsForEmail(sendEmail)
    if(!sendEmail || !email){
      return errorToast("Please enter valid email")
    }

    this.setState({
      loading: true
    }, () => {
      this.onInoiceGenerateUpload(payload)
      this.setState({
        modalVisible: false,
        sendEmail: "",
      })
    })
  }

  onInoiceGenerateUpload = async (payload) => {
    let fileName = `INVOICE_${invoiceIdGenerator()}.pdf`
    const file = await generateInvoice(payload)
    console.log("============>", file)
    file.name = fileName
    const tags = {}
    const extnIdx = fileName.lastIndexOf(".")
    if (extnIdx > -1) {
      fileName = `${fileName.substr(
        0,
        extnIdx
      )}_${new Date().getTime()}${fileName.substr(extnIdx)}`
    }
    const contentType = file.type
    if (!fileName) {
      return console.log("No file name provided")
    }
    const opType = "upload"
    const options = { contentType, tags }

    const res = await getSignedUrl({  // eslint-disable-line
      opType,
      fileName: `INVOICES/${fileName}`,
      options
    })
    if (res) {
      console.log("res : ", res)
      this.uploadWithSignedUrl(res.data.url, file, fileName, tags, payload)
    }else {
      console.log("err : ", res)
    }
  }

  uploadWithSignedUrl = (signedS3Url, file, fileName, tags, payload) => {
    console.log(
      "in uploadWithSignedUrl : ",
      signedS3Url,
      fileName,
      file.type
    )
    const xhr = new XMLHttpRequest()
    xhr.open("PUT", signedS3Url, true)
    xhr.setRequestHeader("Content-Type", file.type)
    if (tags) {
      console.log("no tagging")
      // xhr.setRequestHeader("x-amz-tagging", qs.stringify(tags))
    }
    xhr.onload = () => {
      console.log("readyState : ", xhr.readyState)
      if (xhr.status === 200) {
        console.log("file uploaded ok")
        console.log(`Last upload successful for "${fileName}" at ${new Date()}`)
        this.updateDocsDB(file, fileName, payload)
      }
    }
    xhr.onerror = err => {
      console.log("error in file uploaded", xhr.status, xhr.responseText, err)
    }
    xhr.send(file)
  }

  updateDocsDB = async (file, fileName, payload) => {
    const {tenantId} = this.state.invoice
    const doc = {
      name: fileName,
      originalName: file.name,
      contextType: "INVOICES",
      tenantId
    }
    const invoiceDoc = await addDocInDB(doc)
    if(invoiceDoc){
      payload.invoiceDocId = invoiceDoc
      await this.sendInvoice(payload)
      console.log(invoiceDoc)
    }else {
      console.log("err in inserting in docs db : ", invoiceDoc)
    }
  }

  sendInvoice = async (payload) => {
    const result = await sendInvoiceOnEmail(payload)
    if(result && result.data && result.data.done){
      toast.success("Invoice send successfully", {
        position: toast.POSITION.TOP_RIGHT
      })
      this.setState({
        invoice: (result && result.data && result.data.invoice) || {},
        loading: false
      })
    }else {
      errorToast("Something went wrong, please try a latter.")
      this.setState({
        loading: false
      })
    }
  }

  onModalChange = (state, flag) => {
    this.setState({
      ...state
    })
  }

  handleOnEdit = () => {
    const {invoice} = this.state
    this.setState({
      isInvoiceEditable: true,
      payableAmount: (invoice && invoice.payableAmount) || 0,
      discountPercentage: (invoice && invoice.discountPercentage) || 0
    })
  }

  handleOnPercentageChange = (event) => {
    const {invoice} = this.state
    const { value} = event.target
    const final = Number(value)
    const result = invoice.totalCost - (final * invoice.totalCost / 100)
    this.setState({
      discountPercentage: final || "",
      payableAmount: result ? result.toFixed(2) : "",
    })
  }

  handlePayableAmount = (event) => {
    const {invoice} = this.state
    const {value} = event.target
    const final = parseFloat(value)
    const result = (final * 100) / invoice.totalCost
    this.setState({
      discountPercentage: result ? (100 - result).toFixed(2) : "",
      payableAmount: final || "",
    })
  }

  handleOnSave = async () => {
    const {payableAmount, invoice, discountPercentage} = this.state
    const payAmount = parseFloat(payableAmount || 0)
    const percentage = parseFloat(discountPercentage || 0)
    if(invoice.totalCost < payAmount || payAmount < 0){
      this.setState({
        totalErrorMsg: `Value must be greater than 0 or less than ${invoice.totalCost}`
      })
    } else {
      const payload = {
        discountPercentage: `${percentage && percentage.toFixed(2)}%`,
        payableAmount: payAmount,
      }
      if(invoice.payableAmount !== payAmount){
        const result = await updateInvoicePreview(invoice._id, payload)
        if(result && result.done){
          this.setState({
            isInvoiceEditable: false,
            totalErrorMsg: "",
            payableAmount: 0,
            invoice: (result && result.invoice) || {}
          },async () => {
            updateInvoiceLog([{
              invoiceId: invoice._id || "",
              tenantId: invoice.tenantId || "",
              logMeta: {log: `Invoice discount percentage updated from ${invoice.discountPercentage} to ${payload.discountPercentage} and payable amount updated from ${invoice.payableAmount} to ${payAmount}`},
              appId: invoice.appId || "",
            }])
            toast.success("Invoice update successfully", {
              position: toast.POSITION.TOP_RIGHT
            })
          })
        }
      }else {
        this.setState({
          isInvoiceEditable: false,
        },()=>{
          toast.warn("You do not to change the information", {
            position: toast.POSITION.TOP_RIGHT
          })
        })
      }
    }
  }

  handleOnRejection = () => {
    const { confirmAlert, invoiceNumber, invoice } = this.state
    confirmAlert.text = `You want to reject this invoice ${invoiceNumber}?`
    swal(confirmAlert).then(willDelete => {
      if (willDelete) {
        this.setState({
          loading: true
        }, async () => {
          const result = await updateInvoicePreview(invoice._id, {status: "Rejected", invoiceId: invoiceNumber})
          if(result && result.done){
            this.setState({
              loading: false,
              invoice: (result && result.invoice) || {}
            },async () => {
              updateInvoiceLog([{
                invoiceId: invoice._id,
                tenantId: invoice.tenantId,
                logMeta: {
                  log: `Invoice rejected ${invoiceNumber}`
                },
                appId: invoice.appId,
              }])
              toast.success("Invoice update successfully", {
                position: toast.POSITION.TOP_RIGHT
              })
            })
          }else {
            errorToast("Something went wrong please try after some time.")
            this.setState({
              loading: false
            })
          }
        })
      }
    })
  }

  onPaymentRefund = () => {
    const { invoice, reason, reasonDescription, refundAmount, refundedAmount } = this.state
    const logs = []

    if(Number(refundAmount) > Number(invoice.payableAmount - refundedAmount/100) || !Number(refundAmount)){
      return errorToast(`Refund amount must be less than $${invoice.payableAmount - refundedAmount/100} and greater than 0`)
    }

    this.setState({
      loading: true,
      refundModal: false,
    }, async () => {
      const refund = await invoicePaymentRefund({...invoice, refundAmount, reason, reasonDescription})
      if(refund && refund.data && refund.data.done){
        logs.push({
          invoiceId: invoice._id || "",
          tenantId: invoice.tenantId || "",
          logMeta: {log: `${Number(refundAmount) < Number(invoice.payableAmount) ? "Partial" : "Full"} payment refunded $${refundAmount}, Reason: ${reason || "No"} ${reasonDescription ? `, ${reasonDescription}`: ""}`},
          appId: invoice.appId || "",
        })
        toast.success("Payment refund successfully", {
          position: toast.POSITION.TOP_RIGHT
        })
        this.setState({
          loading: false,
          invoice: {
            ...invoice,
            status: "Refunded"
          }
        })
      }else {
        logs.push({
          invoiceId: invoice._id || "",
          tenantId: invoice.tenantId || "",
          logMeta: { log: `$${Number(refundAmount)} ${Number(refundAmount) < Number(invoice.payableAmount) ? "Partial" : "Full"} payment refund process failed. because of ${(refund && refund.data && refund.data.message) || ""}` },
          appId: invoice.appId || "",
        })
        this.setState({
          loading: false
        })
        errorToast((refund && refund.data && refund.data.message) || "Payment refund process failed")
      }
      if(logs.length){
        await updateInvoiceLog(logs)
      }
    })
  }

  handleRefundModal = () => {
    this.setState({
      refundModal: true,
      reason: "",
      reasonDescription: ""
    })
  }

  showPartialRefunds = async (modal) => {
    const { invoice } = this.state
    let charge = ""
    let refundedAmount = 0
    if(invoice && invoice.receipt_url) {
      charge = invoice.receipt_url.split("/").find(url => url.includes("ch_"))
    }

    if(!charge) return /* errorToast("Payment charge not found") */

    this.setState({
      loading: true
    })
    const res = await getPartialRefunds(charge)
    if(res && !res.done){
      errorToast((res && res.message) || "Something went wrong")
    }else {
      res.data.forEach(refund => { refundedAmount += refund.amount })
    }
    console.log("Refunded Amount: ", refundedAmount)
    this.setState({
      loading: false,
      refundAmount: (invoice && Number(invoice.payableAmount - refundedAmount/100)) || 0,
      partialRefundedModal: modal || false,
      partialRefunds: (res && res.data) || [],
      refundedAmount
    })
  }

  render() {
    const { loading, invoice, invoiceNumber, isInvoiceEditable, payableAmount, refundedAmount, totalErrorMsg, discountPercentage } = this.state
    const { addresses, tenantId } = invoice
    let primaryAddress = {}
    let email = ""
    let phone = ""
    if (addresses && Array.isArray(addresses) && addresses.length) {
      primaryAddress = addresses.find(e => e.isPrimary)
      primaryAddress = Object.keys(primaryAddress || {}).length ? primaryAddress : addresses[0]
      email = primaryAddress.officeEmails && primaryAddress.officeEmails.length ? primaryAddress.officeEmails[0].emailId : ""
      phone = primaryAddress.officePhone && primaryAddress.officePhone.length ? primaryAddress.officePhone[0].phoneNumber : ""
    }

    return (
      <div className="animated fadeIn">
        <SendInvoiceModal
          state={this.state}
          onSend={this.onSend}
          onModalChange={this.onModalChange}
        />
        <RefundModal
          state={this.state}
          onSend={this.onPaymentRefund}
          onModalChange={this.onModalChange}
        />
        <ViewPartialRefundsModal
          state={this.state}
          onModalChange={this.onModalChange}
        />
        {loading ? <Loader/> : null}
        <Card>
          <CardHeader>
            Invoice <strong>#{invoiceNumber}</strong>

            {isInvoiceEditable && invoice.status === "Open" ? <Button className="btn btn-sm ml-1 btn-success" onClick={this.handleOnSave}><i className="fa fa-save"/></Button> : null}
            {!isInvoiceEditable && invoice.status === "Open" ? <Button className="btn btn-sm ml-1 btn-secondary" onClick={this.handleOnEdit}><i className="fa fa-edit"/></Button> : null}

            <Button className="btn btn-sm btn-secondary mr-1 float-right" onClick={this.onPrint}><i className="fa fa-print" /> Print</Button>
            {invoice.status === "Open" ? <Button className="btn btn-sm mr-1 float-right btn-danger" onClick={this.handleOnRejection}><i className="fa fa-ban"/> Reject / Cancel </Button> : null}
            {invoice.status === "Refunded" &&  Number(refundedAmount/100) < Number(invoice.payableAmount) ? <Button className="btn btn-sm mr-1 float-right btn-info" onClick={() => this.showPartialRefunds(true)}>View Partial Refunds</Button> : null}
            {invoice.status === "Open" ? <Button className="btn btn-sm btn-info float-right mr-1 d-print-none" onClick={this.onInvoiceModal}><i className="fa fa-send fa-lg mr-1" />Send</Button> : null }
            {invoice.status === "Paid" || (invoice.status === "Refunded" && Number(refundedAmount/100) < Number(invoice.payableAmount)) ? <Button className="btn btn-sm btn-warning float-right mr-1 d-print-none" onClick={this.handleRefundModal}><i className="fa fa-undo fa-lg mr-1" />Refund</Button> : null }

          </CardHeader>
          <CardBody id="invoicepreview">
            <Row className="mb-4">
              <Col sm="4">
                <h6 className="mb-1">From:</h6>
                <div><strong>Munivisor By Otaras</strong></div>
                <div>Email: munivisor@otaras.com</div>
                <div>Phone: +48 123 456 789</div>
              </Col>
              <Col sm="4">
                <h6 className="mb-1">To:</h6>
                <div><strong><Link to={`/tenant/invoices?firmId=${tenantId || ""}&name=${invoice && invoice.name}`}>{invoice.name || ""}</Link></strong></div>
                <div>{primaryAddress.addressLine1 || ""}</div>
                <div>{primaryAddress.addressLine2 || ""}</div>
                <div>{primaryAddress.city || ""} {primaryAddress.state || ""} {primaryAddress.country || ""}</div>
                <div>{ email ? `Email: ${email}` : null}</div>
                <div>{ email ? `Phone: ${phone}` : null}</div>
              </Col>
              <Col sm="4">
                <h6 className="mb-1">Details:</h6>
                <div>Invoice <strong>#{invoiceNumber}</strong></div>
                <div>{moment(new Date()).format("MMM DD, YYYY")}</div>
                <div>Account Name: {invoice.name || ""}</div>
                <div>Type Of Billing: {invoice.typeOfBilling === "user" ? "User Specific" : "Enterprise"}</div>
                <div>Bill Frequency: <strong> {(invoice.billFrequency && invoice.billFrequency.toUpperCase()) || ""}</strong></div>
                {invoice.status !== "Open" ? <div>Status: <strong>{invoice.status || ""}</strong></div> : null}
              </Col>
            </Row>
            <Table striped responsive>
              <thead>
                <tr>
                  <th>Type of user</th>
                  <th className="center">Number of user</th>
                  {invoice && invoice.typeOfBilling === "user" ? this.renderTableHeadOrData("head", "Unit Cost") : null}
                  {invoice && invoice.typeOfBilling === "user" ? this.renderTableHeadOrData("head", "Total") : null}
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td className="center">Series 50</td>
                  <td className="center">{invoice.series50 || 0}</td>
                  {invoice && invoice.typeOfBilling === "user" ? this.renderTableHeadOrData("data", invoice.chargPerseries50 ? `$${invoice.chargPerseries50}` : "-" ) : null}
                  {invoice && invoice.typeOfBilling === "user" ? this.renderTableHeadOrData("data", invoice.chargPerseries50 ? `$${invoice.chargPerseries50*invoice.series50}` : "-") : null}
                </tr>
                <tr>
                  <td className="center">Non Series 50</td>
                  <td className="center">{invoice.nonSeries50 || 0}</td>
                  {invoice && invoice.typeOfBilling === "user" ? this.renderTableHeadOrData("data", invoice.chargPerNonSeries50 ? `$${invoice.chargPerNonSeries50}` : "-" ) : null}
                  {invoice && invoice.typeOfBilling === "user" ? this.renderTableHeadOrData("data", invoice.chargPerNonSeries50 ? `$${invoice.chargPerNonSeries50*invoice.nonSeries50}` : "-") : null}
                </tr>
              </tbody>
            </Table>
            <Row>
              <Col lg="4" sm="5" />
              <Col lg="4" sm="5" className="ml-auto">
                <Table className="table-clear">
                  <tbody>
                    {
                      invoice.billNumberFrequency && invoice.billNumberFrequency !== 1 ?
                        <tr>
                          <td className="left"><strong>Monthly Cost</strong></td>
                          <td className="right">{`$${Number(invoice.monthlyCharge || 0).toLocaleString()}` || "-"}</td>
                        </tr>
                        : null
                    }
                    <tr>
                      <td className="left"><strong>Subtotal</strong></td>
                      <td className="right">{`$${Number(invoice.totalCost || 0).toLocaleString()}` || "-"}</td>
                    </tr>
                    <tr>
                      <td className="left"><strong>Discount {invoice.discountPercentage ? `(${invoice.discountPercentage || 0})` : ""}</strong></td>
                      <td className="right">
                        {isInvoiceEditable ?
                          <InputGroup>
                            <Input
                              name="discountPercentage"
                              title="Discount"
                              value={discountPercentage || 0}
                              placeholder="%"
                              decimalScale={0}
                              required
                              onChange={this.handleOnPercentageChange}
                            />
                            <InputGroupAddon addonType="append">{`$${parseFloat( invoice.totalCost - payableAmount).toLocaleString()}`}</InputGroupAddon>
                          </InputGroup> : `$${Number(invoice.totalCost-invoice.payableAmount || 0).toLocaleString()}` || "-"}
                      </td>
                    </tr>
                    <tr>
                      <td className="left"><strong>Total</strong></td>
                      <td className="right">
                        {isInvoiceEditable ?
                          <Input
                            name="payableAmount"
                            title="Payable Amount"
                            placeholder="$"
                            value={payableAmount || 0}
                            decimalScale={4}
                            onChange={this.handlePayableAmount}
                            required
                          /> : `$${Number(invoice.payableAmount || 0).toLocaleString()}` || "-"
                        }
                        <small className="text-danger">{totalErrorMsg}</small>
                      </td>
                    </tr>
                  </tbody>
                </Table>
              </Col>
            </Row>
          </CardBody>
        </Card>
      </div>
    )
  }
}

export default connect(({ auth }) => ({ auth }),null)(withRouter(InvoicePreview))
