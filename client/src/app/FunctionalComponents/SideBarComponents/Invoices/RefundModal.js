import React from "react"
import NumberFormat from "react-number-format"
import {Col, FormGroup, Input, InputGroup, InputGroupAddon, Label} from "reactstrap"
import { StrapModal } from "../../../GlobalComponents/Modal"

const reasons = [
  {name: "reason", value: "duplicate", label: "Duplicate"},
  {name: "reason", value: "fraudulent", label: "Fraudulent"},
  {name: "reason", value: "requested_by_customer", label: "Requested by customer"},
  {name: "reason", value: "other", label: "Other"}
]

const placeholders = {
  duplicate: "Add more details about this refund...",
  fraudulent: "Payment seems fraudulent because...",
  requested_by_customer: "Add more details about this refund...",
  other: "Add a reason for this refund",
}

const RefundModal = ({state, onSend, onModalChange}) => {
  const disabled = false
  // const {state, onSend} = props
  const {invoice, loading, reasonDescription, refundedAmount, reason, refundAmount} = state || {}
  const toggleModal = () => {
    onModalChange({
      refundModal: !state.refundModal,
    })
  }

  const onChange = (e) => {
    onModalChange({
      [e.target.name]: e.target.value || ""
    })
  }

  return (
    <StrapModal
      closeModal={toggleModal}
      modalState={state.refundModal}
      title="Refund payment"
      buttonName="Refund"
      saveModal={disabled ? false : onSend}
      modalWidth={{ width: "70%" }}
      disabled={loading || false}
    >
      <div>
        <section className="container has-text-centered">
          <FormGroup row>
            <Col md="3">
              <Label>Refund($):</Label>
            </Col>
            <Col xs="12" md="9">
              <InputGroup>
                <NumberFormat
                  className="form-control"
                  name="refundAmount"
                  title="Refund Amount"
                  value={refundAmount || 0}
                  placeholder="$"
                  decimalScale={2}
                  required
                  onChange={onChange}
                />
                <InputGroupAddon addonType="append">{`$${invoice.payableAmount - refundedAmount/100}`}</InputGroupAddon>
              </InputGroup>
            </Col>
          </FormGroup>
          <FormGroup row>
            <Col md="3">
              <Label htmlFor="selectSm">Reason:</Label>
            </Col>
            <Col xs="12" md="9">
              <Input type="select" name="reason" value={reason} bsSize="sm" onChange={onChange}>
                <option value="">Select a reason</option>
                {reasons.map(reason => <option key={reason.value} value={reason.value} >{reason.label}</option>)}
              </Input>
            </Col>
          </FormGroup>
          {
            reason ?
              <FormGroup row>
                <Col md="3">
                  <Label>Description:</Label>
                </Col>
                <Col xs="12" md="9">
                  <Input type="textarea" bsSize="sm" name="reasonDescription" rows="4" placeholder={placeholders[reason] || ""} value={reasonDescription} onChange={onChange} />
                  {reason === "other" ? <small>A note is required when a provided reason isn't selected.</small> : null}
                </Col>
              </FormGroup>
              : null
          }
          <FormGroup row>
            <Col xs="12" md="12" xl="12">
              <i className="fa fa-info-circle fa-lg mt-4 mr-1 text-info"/>
              Refunds take 5-10 days to appear on a customer's statement.
            </Col>
          </FormGroup>
        </section>
      </div>
    </StrapModal>
  )
}

export default RefundModal
