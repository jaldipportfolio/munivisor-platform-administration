import React, { Component } from "react"
import * as qs from "query-string"
import {
  Col,
  Nav,
  NavItem,
  NavLink,
  Row,
  TabContent,
  TabPane
} from "reactstrap"
import Loader from "../../../../GlobalComponents/Loader"
import InvoicePreview from "../InvoicePreview"
import InvoiceHistory from "../InvoiceHistory"

const TABS = [
  { name: "Invoice", key: "invoice" },
  { name: "History", key: "history" }
]

class TenantMainView extends Component {
  constructor(props) {
    super(props)
    this.state = {
      activeTab: "",
      invoiceId: "",
      loading: true
    }
  }

  componentWillMount() {
    const { location } = this.props
    const tab = window.location.pathname.substring(1).split("/")
    const queryString = qs.parse(location.search)
    const invoiceId = (queryString && queryString.invoiceId) || ""
    this.setState({
      invoiceId,
      activeTab: tab && tab.length ? tab && tab[1] : "firm",
      loading: false
    })
  }

  toggle = (tab) => {
    this.setState({
      activeTab: tab
    }, () => {
      this.props.history.push("/")
      if(tab){
        this.props.history.push(`invoicepreview/${tab}?invoiceId=${this.state.invoiceId}`)
      }
    })
  }

  tabPane = (option) => {
    const { invoiceId } = this.state
    switch (option) {
    case "invoice":
      return (
        <TabPane>
          <InvoicePreview invoiceId={invoiceId}/>
        </TabPane>
      )
    case "history":
      return (
        <TabPane>
          <InvoiceHistory invoiceId={invoiceId}/>
        </TabPane>
      )
    default:
      return <p>{option}</p>
    }
  }

  render() {
    const { activeTab, loading } = this.state

    return (
      <div className="animated fadeIn">
        { loading ? <Loader/> : null }
        <Row>
          <Col xs="12" md="12" className="mb-4">
            <Nav tabs>
              {
                TABS.map((tab, index) => (
                  <NavItem key={index.toString()}>
                    <NavLink
                      active={activeTab === tab.key}
                      onClick={() => this.toggle(tab.key)}
                    >
                      {tab.name}
                    </NavLink>
                  </NavItem>
                ))
              }
            </Nav>
            <TabContent >
              {this.tabPane(activeTab)}
            </TabContent>
          </Col>
        </Row>
      </div>
    )
  }
}

export default TenantMainView
