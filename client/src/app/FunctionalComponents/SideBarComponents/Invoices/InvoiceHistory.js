import React from "react"
import moment from "moment"
import {toast} from "react-toastify"
import ReactTable from "react-table"
import {InputGroupText, Col, FormGroup, Input, InputGroup, InputGroupAddon} from "reactstrap"
import "react-dates/initialize"
import { DateRangePicker } from "react-dates"
import "react-dates/lib/css/_datepicker.css"
import Loader from "../../../GlobalComponents/Loader"
import Accordion from "../../../GlobalComponents/Accordion"
import { getInvoiceLogById } from "../../../Services/actions/Invoice"
import "react-table/react-table.css"

const highlightSearch = (string, searchQuery) => {
  const reg = new RegExp(searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&"), "i")
  return string.replace(reg, str => `<mark>${str}</mark>`)
}

class InvoiceHistory extends React.Component{
  constructor(props) {
    super(props)
    this.state = {
      logs: [],
      historySearch: "",
      totalPages: "",
      loading: true,
      orientation: "vertical",
      openDirection: "down",
      startDate: null,
      endDate: null
    }
  }

  componentDidMount() {
    const { invoiceId } = this.props
    console.log("invoiceId", invoiceId)
    this.updateDimensions()
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions = () => {
    const windowWidth = window.innerWidth
    this.setState({
      orientation: windowWidth < 620 ? "vertical" : "horizontal",
      openDirection: windowWidth < 620 ? "down" : "down"
    })
  }

  getFilteredData = async (pageDetails) => {
    const { invoiceId } = this.props
    const { historySearch, startDate, endDate } = this.state
    const { page, pageSize, sorted } = pageDetails
    let sortedFields = {}

    this.setState({
      loading: true,
      page: { global: pageDetails }
    }, async () => {
      if (sorted && sorted[0]) {
        sortedFields[sorted[0].id] = sorted[0].desc ? -1 : 1
        this.setState({ sortedFields })
      } else if (this.state.sortedFields === undefined) {
        sortedFields = { "_id": -1 }
      }

      const isPage = {
        page,
        invoiceId,
        pageSize,
        sorted: sortedFields,
        searchField: historySearch || "",
        startDate: startDate ? moment(startDate) : null,
        endDate: endDate ? moment(endDate) : null
      }
      const invoice = await getInvoiceLogById(isPage)
      if(invoice && invoice.data){
        const totalPages = (invoice.metadata.length && invoice.metadata[0] && invoice.metadata[0].pages) || 0
        this.setState({
          logs: (invoice && invoice.data) || [],
          totalPages,
          loading: false
        })
      }else {
        toast.error("Something went wrong!", {
          position: toast.POSITION.TOP_RIGHT
        })
        this.setState({
          loading: false
        })
      }
    })
  }

  onSearch = (e) => {
    const { name, value } = e.target
    const { page } = this.state
    this.setState({
      [name]: value
    }, () => this.getFilteredData(page.global))
  }

  render() {
    const { logs , page, historySearch, loading, totalPages, focusedInput, orientation, openDirection, startDate, endDate} = this.state
    return (
      <div className="animated fadeIn">
        { loading ? <Loader/> : null }
        <Accordion activeAccordionHeader="History">
          <FormGroup row className="my-0">
            <Col xs="12" md="12">
              <div className="controls">
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="fa fa-search"/>
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input type="text" name="historySearch" onChange={this.onSearch} placeholder="Search..." />
                  <DateRangePicker
                    startDate={startDate}
                    startDateId="startDate"
                    endDate={endDate}
                    endDateId="endDate"
                    showClearDates
                    showDefaultInputIcon
                    small
                    isOutsideRange={() => false}
                    onDatesChange={({startDate, endDate}) => this.setState({startDate, endDate}, () => this.getFilteredData(page.global))}
                    focusedInput={focusedInput}
                    onFocusChange={focusedInput => this.setState({focusedInput})}
                    orientation={orientation}
                    openDirection={openDirection}
                  />
                </InputGroup>
              </div>
            </Col>
          </FormGroup><br/>
          <ReactTable
            columns={[
              {
                Header: "User Name",
                id: "userName",
                className: "multiExpTblVal",
                accessor: item => item,
                minWidth: 100,
                Cell: row => {
                  const item = row.value
                  return (
                    <div className="hpTablesTd -striped">
                      <div
                        dangerouslySetInnerHTML={{
                          __html: highlightSearch((item && item.logMeta && item.logMeta.userName) || "", historySearch)
                        }}
                      />
                    </div>
                  )
                },
                sortMethod: (a, b) => `${a.logMeta.userName}` - `${b.logMeta.userName}`
              },
              {
                Header: "Change log",
                id: "log",
                className: "multiExpTblVal",
                accessor: item => item,
                minWidth: 100,
                Cell: row => {
                  const item = row.value
                  return (
                    <div className="hpTablesTd -striped wrap-cell-text">
                      <div
                        dangerouslySetInnerHTML={{
                          __html: highlightSearch((item && item.logMeta && item.logMeta.log) || "", historySearch)
                        }}
                      />
                    </div>
                  )
                },
                sortMethod: (a, b) => `${a.logMeta.log}` - `${b.logMeta.log}`
              },

              {
                Header: "Ip",
                id: "ip",
                className: "multiExpTblVal",
                accessor: item => item,
                minWidth: 100,
                Cell: row => {
                  const item = row.value
                  return (
                    <div className="hpTablesTd -striped wrap-cell-text" >
                      <div
                        dangerouslySetInnerHTML={{
                          __html: highlightSearch((item && item.logMeta && item.logMeta.ip) || "", historySearch)
                        }}
                      />
                    </div>
                  )
                },
                sortMethod: (a, b) => `${a.logMeta.ip}` - `${b.logMeta.ip}`
              },
              {
                Header: "Date",
                id: "date",
                className: "multiExpTblVal",
                accessor: item => item,
                minWidth: 100,
                Cell: row => {
                  const item = row.value
                  return (
                    <div className="hpTablesTd -striped wrap-cell-text">
                      <div
                        dangerouslySetInnerHTML={{
                          __html: highlightSearch((item && item.createdAt && moment(item.createdAt).format("MM/DD/YYYY hh:mm A")) || "", historySearch)
                        }}
                      />
                    </div>
                  )
                },
                sortMethod: (a, b) => `${a.createdAt}` - `${b.createdAt}`
              }
            ]}
            data={logs || []}
            manual
            showPaginationBottom
            defaultPageSize={10}
            pageSizeOptions={[5, 10, 20, 50, 100]}
            className="-striped -highlight is-bordered"
            style={{ overflowX: "auto" }}
            pages={totalPages}
            minRows={2}
            onFetchData={state => {
              this.getFilteredData(state)
            }}
          />
        </Accordion>
      </div>
    )
  }
}

export default InvoiceHistory
