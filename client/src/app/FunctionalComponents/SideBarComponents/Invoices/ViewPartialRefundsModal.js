import React from "react"
import moment from "moment"
import { StrapModal } from "../../../GlobalComponents/Modal"

const ViewPartialRefundsModal = ({state, onModalChange}) => {
  // const {state} = props
  const { loading, partialRefunds, refundedAmount, partialRefundedModal } = state || {}
  const toggleModal = () => {
    onModalChange({
      partialRefundedModal: !state.partialRefundedModal,
    })
  }

  return (
    <StrapModal
      closeModal={toggleModal}
      modalState={partialRefundedModal}
      title="Partial Refunds"
      modalWidth={{ width: "70%" }}
      disabled={loading || false}
    >
      <table className="table table-striped" style={{width: "100%"}}>
        <thead>
          <tr>
            <th className="center">Event</th>
            <th className="center">Status</th>
            <th className="center">Date</th>
          </tr>
        </thead>
        <tbody>
          {
            partialRefunds && partialRefunds.length && partialRefunds.map(refund => (
              <tr key={refund.id}>
                <td className="center">{`$${refund.amount/100} refunded ${refund.reason ? `due to ${refund.reason} charge` : ""}`}</td>
                <td className="center">{refund.status}</td>
                <td className="center">{moment.unix(refund.created).format("MM/DD/YYYY hh:mm A")}</td>
              </tr>
            ))
          }
          <tr>
            <td className="center"><strong>Total</strong></td>
            <td className="center" colSpan={2}>${refundedAmount/100 || 0} Refunded</td>
          </tr>
        </tbody>
      </table>
    </StrapModal>
  )
}

export default ViewPartialRefundsModal
