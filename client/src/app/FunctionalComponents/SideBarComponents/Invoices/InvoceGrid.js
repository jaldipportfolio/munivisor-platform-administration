import React, { Component } from "react"
import { AgGridReact } from "ag-grid-react"
import "ag-grid-community/dist/styles/ag-grid.css"
import "ag-grid-community/dist/styles/ag-theme-balham.css"
import "ag-grid-enterprise"
import moment from "moment"
import {toast} from "react-toastify"
import {Link} from "react-router-dom"
import {getInvoicesList} from "../../../Services/actions/Invoice"
import DocLink from "../../../GlobalComponents/DocLink"

const columnData = [
  {
    headerName: "Name",
    field: "name",
    cellRendererFramework: row => {
      const item = row.data
      return (
        <div className="hpTablesTd -striped">
          <Link to={`/tenant/firm?firmId=${item.tenantId}&name=${item && item.name}`}>
            {item && item.name}
          </Link>
        </div>
      )
    }
  },
  {
    headerName: "Series 50 users",
    field: "series50",
    cellRendererFramework: row => {
      const item = row.data
      return (
        <div className="hpTablesTd -striped">
          {item && item.series50 ? `$${Number(item && item.series50).toLocaleString()}` : "$0"}
        </div>
      )
    }
  },
  {
    headerName: "Non series 50 users",
    field: "nonSeries50",
    cellRendererFramework: row => {
      const item = row.data
      return (
        <div className="hpTablesTd -striped" style={{heigth: 50}}>
          {item && item.nonSeries50 ? `$${Number(item && item.nonSeries50).toLocaleString()}` : "$0"}
        </div>
      )
    }
  },
  {
    headerName: "Payable Amount",
    field: "payableAmount",
    cellRendererFramework: row => {
      const item = row.data
      return (
        <div className="hpTablesTd -striped">
          {item && item.payableAmount ? `$${Number(item && item.payableAmount).toLocaleString()}` : "$0"}
        </div>
      )
    }
  },
  {
    headerName: "Type Of Billing",
    field: "typeOfBilling"
  },
  {
    headerName: "Bill Frequency",
    field: "billFrequency"
  },
  {
    headerName: "Status",
    field: "status",
  },
  {
    headerName: "Created At",
    field: "createdAt",
    cellRendererFramework: row => {
      const item = row.data
      return (
        <div className="hpTablesTd -striped">
          {item && item.createdAt && moment(item.createdAt).format("MM/DD/YYYY hh:mm A")}
        </div>
      )
    }
  },
  {
    headerName: "Action",
    cellRendererFramework: row => {
      const item = row.data
      return (
        <div className="hpTablesTd -striped">
          <Link className="btn-link btn-block d-inline-block" title="Preview" style={{width: "25%"}}
            to={`/invoicepreview/invoice?invoiceId=${item._id}`}>
            <i className="fa fa-eye"/>
          </Link>
          {item.invoiceDocId ?
            <DocLink className="text-center d-inline-block" style={{width: "25%"}} downloadIcon
              docId={item.invoiceDocId}/> : null}
        </div>
      )
    }
  },
]

class InvoceGrid extends Component {
  constructor(props) {
    super(props)
    this.state = {
      defaultColDef: {
        sortable: true,
        resizable: true
      },
      loading: true,
      invoiceList: [],
      totalPages: 1,
      page: 0,
      pageSize: 10,
      columnDefs: columnData,
    }
  }

  onGridReady = params => {
    this.gridApi = params.api
    this.gridColumnApi = params.columnApi
    this.gridApi.sizeColumnsToFit()
  }

  onPaginationChanged = async () => {
    const {pageSize, page} = this.state

    const isPage = {
      page,
      pageSize,
      sorted: {_id: -1},
      searchField: "",
      startDate: null,
      endDate:  null
    }
    const invoice = await getInvoicesList(isPage)
    if(invoice && invoice.data && invoice.metadata){
      // const total = (invoice.metadata[0] && invoice.metadata[0].total) || 0
      const totalPage = (invoice.metadata[0] && invoice.metadata[0].pages) || 0
      this.setState({
        invoiceList: invoice && invoice.data,
        totalPages: totalPage,
        loading: false,
        page: isPage.page
      })
    }else {
      toast.error("Something went wrong!", {
        position: toast.POSITION.TOP_RIGHT
      })
      this.setState({
        loading: false
      })
    }
  }

  onBtFirst = () => {
    this.setState({
      page: 0
    },()=>{
      this.onPaginationChanged()
    })
  }

  onBtLast = () => {
    const { pageSize } = this.state
    this.setState({
      page: pageSize - 1
    },()=>{
      this.onPaginationChanged()
    })
  }

  onBtNext = () => {
    const {page} = this.state
    if(page >= 0){
      this.setState({
        page: page + 1
      },()=>{
        this.onPaginationChanged()
      })
    }
  }

  onBtPrevious = () => {
    const {page} = this.state
    if(page >= 0){
      this.setState({
        page: page - 1
      },()=>{
        this.onPaginationChanged()
      })
    }
  }

  /* onBtPageFive = () => {
    this.gridApi.paginationGoToPage(4)
  }

  onBtPageFifty = () => {
    this.gridApi.paginationGoToPage(49)
  } */

  // onPageSizeChanged = () =>{
  //     let value = document.getElementById("page-size").value;
  //     this.setState({
  //         pageSize: Number(value)
  //     },()=>{
  //         this.onPaginationChanged()
  //     })
  // }

  render() {
    const { totalPages, page, pageSize, invoiceList } = this.state
    return (
      <div style={{height: "100%", width: "100%" }}>
        <div>
          <button title="First" className="btn btn-brand btn-sm btn-default mb-2" disabled={page <= 0} onClick={this.onBtFirst} >
            <i className="fa fa-angle-double-left fa-lg" />
          </button>
          <button title="Previous" className="btn btn-brand btn-sm btn-default mb-2 ml-2 mr-4" disabled={page <= 0} onClick={this.onBtPrevious} >
            <i className="fa fa-angle-left fa-lg" />
          </button>
           Page <span>{page + 1}</span> of <span>{totalPages}</span>
          <button title="Next" className="btn btn-brand btn-sm btn-default mb-2 ml-4" disabled={(page + 1) === pageSize} onClick={this.onBtNext} >
            <i className="fa fa-angle-right fa-lg" />
          </button>
          <button title="Last" className="btn btn-brand btn-sm btn-default mb-2 ml-2" disabled={(page + 1) === pageSize} onClick={this.onBtLast} >
            <i className="fa fa-angle-double-right fa-lg" />
          </button>
          {/* Page Size: */}
          {/* <select onChange={this.onPageSizeChanged} value={pageSize} id="page-size"> */}
          {/* <option value="10" selected="">10</option> */}
          {/* <option value="20">20</option> */}
          {/* <option value="50">50</option> */}
          {/* <option value="100">100</option> */}
          {/* </select> */}
        </div>
        <div
          className="ag-theme-balham"
          style={{height: "335px", width: "100%" }}
        >
          <AgGridReact
            columnDefs={this.state.columnDefs}
            defaultColDef={this.state.defaultColDef}
            pagination
            suppressPaginationPanel
            paginationPageSize ={10}
            onPaginationChanged={this.onPaginationChanged}
            rowData={invoiceList}
            onGridReady={this.onGridReady}
          />
        </div>
      </div>
    )
  }
}

export default InvoceGrid