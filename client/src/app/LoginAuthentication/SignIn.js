import React, { Component } from "react"
import { connect } from "react-redux"
import {Link, withRouter} from "react-router-dom"
import LaddaButton, {ZOOM_IN} from "react-ladda"
import { Card, CardBody, CardGroup, Col, Container, FormGroup, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from "reactstrap"
import logo from "../FunctionalComponents/assets/img/brand/munivisorlogin.png"
import { regExp } from "../globalutilities/consts"
import { startSignInProcess } from "../Services/actions"

class SignIn extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: "",
      password: "",
      loginError: "",
      expZoomIn: false,
      errors: {}
    }
  }

    onLogin = () => {
      const { email, password } = this.state
      const errors = {}
      if(!email || email){
        let emailError = true
        if(email){
          emailError = regExp.email.test(email)
          errors.email = !emailError ? "Enter a valid Email" : ""
        } else if(!email){
          errors.email = "Enter Your Email"
        } else {
          delete errors.email
        }
      }
      if(!password){
        errors.password = !password ? "Enter Your password" : ""
      }
      this.setState({
        errors
      })
      if((errors && errors.email !== "") || (errors && errors.password)){
        return
      }
      this.setState({
        expZoomIn: true
      }, async () => {
        const message = await startSignInProcess({email, password})
        if (message === "") {
          this.props.history.push("/dashboard")
        } else {
          this.setState({
            expZoomIn: false,
            loginError: message
          })
        }
      })
    }

    onChange = (e) => {
      const { name, value } = e.target
      let error = true
      if(name === "email"){
        error = regExp.email.test(value)
      }
      this.setState(prevState => ({
        errors: {
          ...prevState.errors,
          [name]: !error ? "Enter a valid Email" : ""
        },
        [name]: value
      }))
    }

    render() {
      const { expZoomIn, email, password, errors, loginError } = this.state
      return (
        <div className="app flex-row align-items-center">
          <Container>
            <Row className="justify-content-center">
              <Col md="8">
                <CardGroup>
                  <Card className="p-4">
                    <CardBody>

                      <img style={{padding: "0 80px 0 80px"}} className="card-img-top" src={logo} alt="logo"/>
                      <hr/>
                      <h4>Sign in to your account here</h4>

                      <InputGroup className={errors && errors.email ? "" : "mb-3"}>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="fa fa-envelope-o"/>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="email"
                          placeholder="Email"
                          name="email"
                          value={email}
                          onChange={this.onChange}
                        />
                      </InputGroup>
                      { errors && errors.email ?
                        <small className="text-danger">{errors.email}</small> : null
                      }

                      <InputGroup className={errors && errors.password ? "" : "mb-4"}>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"/>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="password"
                          placeholder="Password"
                          name="password"
                          value={password}
                          onChange={this.onChange}
                        />
                      </InputGroup>
                      { errors && errors.password ?
                        <small className="text-danger">{errors.password}</small> : null
                      }
                      { loginError ? <p className="text-danger">{loginError}</p> : null }
                      <div className="row justify-content-center">

                        <LaddaButton
                          className="btn btn-primary btn-ladda col-xs-6 col-xs-offset-0 col-sm-offset-3 col-sm-6"
                          loading={expZoomIn}
                          onClick={this.onLogin}
                          data-color="red"
                          data-style={ZOOM_IN}
                        >
                          Sign In
                        </LaddaButton>

                      </div>
                      <br/>
                      <FormGroup row className="my-0">
                        <Col xs="12" md="12">
                          <div className="row justify-content-sm-start" >
                            <Link to="/signup">Create new account</Link>
                          </div>
                        </Col>
                      </FormGroup>
                    </CardBody>
                  </Card>
                </CardGroup>
              </Col>
            </Row>
          </Container>
        </div>
      )
    }
}


export default connect(({ auth }) => ({ auth }),null)(withRouter(SignIn))
