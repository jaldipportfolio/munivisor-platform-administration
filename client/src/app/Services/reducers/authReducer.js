import {
  SIGNUP_SUCCESS,
  SIGNUP_ERROR,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  CHECKAUTH_SUCCESS,
  CHECKAUTH_ERROR,
  SIGN_OUT
} from "../actions/types"

const initialState = {
  userEmail: "",
  relatedFaEntities: {},
  userEntities: {},
}


export const authReducer = (state = initialState , action) => {
  const { payload } = action
  switch (action.type) {
  case SIGNUP_SUCCESS:
  case LOGIN_SUCCESS:
  case CHECKAUTH_SUCCESS:
    console.log("auth action : ", action)
    return {
      ...state,
      ...payload,
      userEmail: (payload && payload.loginDetails && payload.loginDetails._id) || ""
    }
  case SIGNUP_ERROR:
  case LOGIN_ERROR:
  case CHECKAUTH_ERROR:
    return {...state,...payload}
  case SIGN_OUT:
    console.log("reducer SIGN_OUT")
    return { ...initialState }
  default:
    return state
  }
}
