import axios from "axios"
import {getToken, muniApiBaseURL} from "../../../globalutilities/consts"


/** Create Tenant Entity * */

export const createTenantEntity = async payload => {
  let result = []
  try {
    const res = await axios.post(`${muniApiBaseURL}platformadmin/create/tenant`, payload, {headers: { Authorization: getToken() }})
    result = res.data || []
  } catch (err) {
    console.log("error in getting entities : ", err)
  }
  return result
}

/** Tenant Users List * */

export const getTenantUserList = async filteredVal => {
  try {
    const filteredValue = filteredVal === undefined ? "" : filteredVal
    const   Authorization = getToken()
    const response = await axios.post(`${muniApiBaseURL}dashboard/tenantusers`, filteredValue, { headers: { Authorization } } )
    return response
  } catch (error) {
    return error
  }
}

/** Tenant Config * */

export const insertTenantConfig = async (payload) => {
  try {
    const res = await axios.post(`${muniApiBaseURL}platformadmin/create/createtenantconfig`, payload, {headers: { Authorization: getToken() }})
    return res.data || {}
  } catch (err) {
    console.log("error in getting entities : ", err)
  }
}

export const getTenantConfigData = async (userId) => {
  try {
    const res = await axios.get(`${muniApiBaseURL}tenantconfig/${userId || ""}`, { headers: { Authorization: getToken() } })
    return res.data || []
  } catch (error) {
    return error
  }
}

export const updateTenantConfigDetails = async (tenantId, payload) => {
  try {
    const res = await axios.put(`${muniApiBaseURL}tenantconfig/${tenantId || ""}`, payload,{ headers: { Authorization: getToken() } })
    return res.data || []
  } catch (error) {
    return error
  }
}