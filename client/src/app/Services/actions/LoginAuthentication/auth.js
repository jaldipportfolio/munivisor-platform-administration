import axios from "axios"
import { authBaseURL } from "../../../globalutilities/consts"
import {muniVisorApplicationStore} from "../../../../app/Services/store"
import {
  SIGNUP_SUCCESS,
  SIGNUP_ERROR,
  LOGIN_SUCCESS,
  CHECKAUTH_SUCCESS,
  SIGN_OUT,
} from "../types"

export const startSignUpProcess = ({ email, password }) => async dispatch => {
  try {
    localStorage.removeItem("token")
    localStorage.removeItem("email")

    localStorage.setItem("email", "")
    const loginResponse = await axios.post(`${authBaseURL}signup`, {
      email,
      password
    })
    const {
      token,
      authenticated,
      loginDetails,
      errorMessage
    } = loginResponse.data
    dispatch({
      type: SIGNUP_SUCCESS,
      payload: { token, authenticated, loginDetails, errorMessage }
    })
    return loginResponse.data && loginResponse.data.done ? "true" : loginResponse.error
  } catch (error) {
    const { data } = error.response
    const errorMessage = data.error
      ? data.error
      : "Unable to Sign Up as a user on the application"
    dispatch({
      type: SIGNUP_ERROR,
      payload: {
        token: "",
        authenticated: false,
        loginDetails: {},
        errorMessage
      }
    })
    return errorMessage || "Need strong password"
  }
}

export const getUserByResetId = async (resetId) => {
  try {
    localStorage.removeItem("token")
    localStorage.removeItem("email")

    localStorage.setItem("email", "")
    const loginResponse = await axios.get(`${authBaseURL}resetpassconfirm?id=${resetId}`)
    return loginResponse.done ? loginResponse : loginResponse
  } catch (error) {
    const { data } = error.response
    const errorMessage = data.error
      ? data.error
      : "Invalid reset id"
    return errorMessage || "Invalid reset id"
  }
}

export const startSignInProcess = async ({ email, password }) => {
  let errorMessage = ""
  let loginResponse = ""
  try {
    loginResponse = await axios.post(`${authBaseURL}signin`, {
      email,
      password
    })
    const {
      token,
      authenticated,
      loginDetails,
      errorMessage,
      exp
    } = loginResponse.data
    localStorage.setItem("token", token)
    localStorage.setItem("email", email)

    muniVisorApplicationStore.dispatch({
      type: LOGIN_SUCCESS,
      payload: { token, authenticated, loginDetails, errorMessage, exp }
    })
    return ""
  } catch ({response}) {
    // const { data } = error.response
    errorMessage = response ? response.data.message : "Either password or user ID doesn't match"
    return errorMessage
  }
}

export const checkAuth = tokenforheader => async dispatch => {
  console.log("tokenforheader : ", tokenforheader)
  try {
    const authResponse = await axios.get(`${authBaseURL}checkauth`, {
      headers: { Authorization: tokenforheader }
    })
    const {
      token,
      authenticated,
      loginDetails,
      errorMessage,
      exp
    } = authResponse.data

    dispatch({
      type: CHECKAUTH_SUCCESS,
      payload: { token, authenticated, loginDetails, errorMessage, exp }
    })
  } catch (error) {
    const { data = {} } = (error && error.response) || {}
    console.log(data)
    // const errorMessage = data.error ? data.error : "Unable to Check Authentication"
    localStorage.setItem("token", "")
    localStorage.setItem("email", "")
    localStorage.setItem("tableCache", "")
    dispatch({ type: SIGN_OUT })
  }
}

export const signOut = () => {
  console.log("in action signOut")
  localStorage.setItem("token", "")
  localStorage.setItem("email", "")
  localStorage.setItem("tableCache", "")

  if(window.mvAuthWarningTimeout1) {
    window.mvAuthWarningTimeout1 = null
  }

  if(window.mvAuthWarningTimeout5) {
    window.mvAuthWarningTimeout5 = null
  }

  if(window.mvAuthTimeout) {
    window.mvAuthTimeout = null
  }
  window.location.href="/signin"
  return {
    type: SIGN_OUT
  }
}
