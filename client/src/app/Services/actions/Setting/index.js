import axios from "axios"
import {getToken, muniApiBaseURL} from "../../../globalutilities/consts"

/** Plat Form Settings * */

export const getPlatFormSetting = async () => {
  try {
    const res = await axios.get(`${muniApiBaseURL}common/platformsetting`, { headers: { Authorization: getToken() } })
    return res.data
  } catch (error) {
    return error
  }
}

export const updatePlatFormSettings = async (payload, settingId) => {
  try {
    return await axios.put(`${muniApiBaseURL}common/platformsetting?settingId=${settingId || ""}`, payload,{ headers: { Authorization: getToken() } })
  } catch (error) {
    return error
  }
}

/** Plat Form Applications * */

export const getAllApps = async () => {
  try {
    const res = await axios.get(`${muniApiBaseURL}common/applications`, { headers: { Authorization: getToken() } })
    return (res && res.data && res.data.applications) || []
  } catch (error) {
    return error
  }
}

export const postApplications = async (payload) => {
  try {
    const res = await axios.post(`${muniApiBaseURL}common/applications`, payload, { headers: { Authorization: getToken() } })
    return (res.data && res.data) || {}
  } catch (error) {
    return error
  }
}

export const updateFirmDetails = async (entityId, payload) => {
    try {
        return await axios.post(`${muniApiBaseURL}common/updateFirm/${entityId || ""}`, payload,{ headers: { Authorization: getToken() } })
    } catch (error) {
        return error
    }
}
