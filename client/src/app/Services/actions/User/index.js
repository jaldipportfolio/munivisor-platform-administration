import axios from "axios"
import {getToken, muniApiBaseURL} from "../../../globalutilities/consts"

/** Users Details * */

export const saveEntityUserDetails = async (userDetails) => {
  try {
    const response = await axios.post(`${muniApiBaseURL}platformadmin/create/user`, {userDetails}, { headers: { Authorization: getToken()  } } )
    return response.data || {}
  } catch (error) {
    return error
  }
}

export const updateUserDetails = async (userId, payload) => {
  try {
    return await axios.put(`${muniApiBaseURL}platformuser/${userId || ""}`, payload,{ headers: { Authorization: getToken() } })
  } catch (error) {
    return error
  }
}

export const getUserDetails = async (userId) => {
  try {
    const res = await axios.get(`${muniApiBaseURL}platformuser/${userId || ""}`, { headers: { Authorization: getToken() } })
    return res.data
  } catch (error) {
    return error
  }
}