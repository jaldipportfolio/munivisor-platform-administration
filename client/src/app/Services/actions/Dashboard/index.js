import axios from "axios"
import { muniApiBaseURL, getToken } from "../../../globalutilities/consts"

/** Tenant List * */

export const getTenantList = async filteredVal => {
  try {
    const filteredValue = filteredVal === undefined ? "" : filteredVal
    const response = await axios.post(`${muniApiBaseURL}dashboard/tenants`, filteredValue, { headers: { Authorization: getToken()  } })
    return response
  } catch (error) {
    return error
  }
}

/** Entity Look Up Data * */

export const getEntityLookUpData = async payload => {
  let result = []
  try {
    const res = await axios.post(`${muniApiBaseURL}entity/entity-look-up`, payload, {headers: { Authorization: getToken() }})
    result = res.data ? res.data.globIssuer || [] : []
  } catch (err) {
    console.log("error in getting entities : ", err)
  }
  return result
}

/** Snap Shots List * */

export const getSnapShotsList = async (firmId, payload) => {
  try {
    const res = await axios.post(`${muniApiBaseURL}common/snapshot/${firmId || ""}`, payload, { headers: { Authorization: getToken() } })
    return res.data || []
  } catch (error) {
    return error
  }
}

/** Firm Details * */

export const getFirmDetailsInfo = async (firmId) => {
  try {
    const res = await axios.get(`${muniApiBaseURL}dashboard/${firmId || ""}`, { headers: { Authorization: getToken() } })
    return res.data
  } catch (error) {
    return error
  }
}

/** Pick lists Details * */

export const getFirmPicklists = async (entityId, searchTerm, names) => {
  try {
    const res = await axios.get(`${muniApiBaseURL}common/disablepicklists?entityId=${entityId || ""}&searchTerm=${searchTerm || ""}&names=${names || ""}`, { headers: { Authorization: getToken() } })
    return res.data
  } catch (error) {
    return error
  }
}

export const putPicklist = async (payload) => {
  try {
    const res = await axios.put(`${muniApiBaseURL}common/disablepicklists`, payload, { headers: { Authorization: getToken() } })
    return res.data
  } catch (error) {
    return error
  }
}

export const saveSpecificPicklist = async (payload) => {
  try {
    const res = await axios.post(`${muniApiBaseURL}common/savepicklists`, payload , { headers: { Authorization: getToken() } })
    return res.data
  } catch (error) {
    return error
  }
}

export const deletePicklist = async (payload) => {
  try {
    const query = `?tenantId=${payload.tenantId}&picklistId=${payload.picklistId}`
    const res = await axios.delete(`${muniApiBaseURL}common/disablepicklists${query}`, { headers: { Authorization: getToken() } })
    return res.data
  } catch (error) {
    return error
  }
}

/** Check lists Details * */

export const mapAccordionsArrayToObject = (data, otherDataRequired) => {
  const result = {}
  const otherData = {}
  data.forEach(e => {
    const key = e.title
    if (!result.hasOwnProperty(key)) {
      result[key] = e.items
      if (otherDataRequired) {
        otherData[key] = {}
        const otherKeys = Object.keys(e).filter(
          k => k !== "title" || k !== "items"
        )
        otherKeys.forEach(k => {
          otherData[key][k] = e[k]
        })
      }
    }
  })
  if (otherDataRequired) {
    return [result, otherData]
  }
  return result
}


export const mapAccordionsObjectToArray = (data, otherData = {}) => {
  return Object.keys(data).map(k => ({
    title: k,
    ...otherData[k],
    items: data[k]
  }))
}

export const checkListsList = async (tenantId) => {
  try {
    const query = `?tenantId=${tenantId}`
    const res = await axios.get(`${muniApiBaseURL}common/checklists${query}`, { headers:{ "Authorization": getToken() } })
    console.log("res : ", res)
    if(res && res.status === 200 && res.data && res.data && res.data.length) {
      const checklists = res.data
      const payload = checklists.map(c => {
        const { id, name, type, attributedTo, bestPractice, notes, lastUpdated, data } = c
        const item = { id, name, type, attributedTo, bestPractice, notes, lastUpdated }
        const [itemData, otherData] = mapAccordionsArrayToObject(data, true)
        const itemHeaders = {}
        Object.keys(otherData).forEach(k => {
          itemHeaders[k] = otherData[k].headers
        })
        item.data = itemData
        item.itemHeaders = itemHeaders
        item.action = c && c.action
        return item
      })
      console.log("===============>", payload)
      return payload
    } else {
      console.log("no data in getConfigChecklist: ")

    }
  } catch (err) {
    console.log("err in getConfigChecklist: ", err)
  }
}

export const saveConfigChecklist = async (tenantId, checklists, changeLog, callback, target) => {
  console.log("in action saveConfigChecklist : ", checklists)
  checklists = checklists.filter(item => item.name)
  const configChecklists = [...checklists]
  checklists.forEach((checklist, i) => {
    configChecklists[i] = {...checklist}
    const otherData = {}
    Object.keys(checklist.itemHeaders || {}).forEach(k => {
      otherData[k] = {}
      otherData[k].headers = checklist.itemHeaders[k]
    })
    configChecklists[i].data = mapAccordionsObjectToArray(checklist.data, otherData)
  })
  try {
    const res = await axios.post(`${muniApiBaseURL}common/checklists`, {checklists: configChecklists, tenantId}, { headers:{ "Authorization": getToken() } })
    console.log("res : ", res)
    if (res && res.status >= 200 && res.status < 300) {
      callback()
      // await updateAuditLog("config", changeLog)
    } else {
      callback("err")
      console.log("err in saveConfigChecklist put: ", res)
    }
  } catch (err) {
    callback("err")
    console.log("err in saveConfigChecklist: ", err)
  }
}
