import axios from "axios"
import { getToken, muniApiBaseURL } from "../../../globalutilities/consts"

/** Docs * */

export const getFile = (query, fileName) => {
  try {
    /* const {objectName, fileName} = this.state
        query = `?objectName=${objectName}` */

    axios
      .get(`${muniApiBaseURL}s3/get-s3-object-as-stream${query}`, {
        responseType: "arraybuffer",
        headers: { Authorization: getToken() }
      })
      .then(res => {
        console.log("ok", res)
        if (res.status === 200) {
          return res.data
        }
        return null
      })
      .then(data => {
        const blob = new Blob([data])
        const url = window.URL.createObjectURL(blob)
        // attach blob url to anchor element with download attribute
        const anchor = document.createElement("a")
        anchor.setAttribute("href", url)
        anchor.setAttribute("download", fileName)
        anchor.click()
        window.URL.revokeObjectURL(url)
      })
      .catch(err => {
        console.log("err : ", err)
        return null
      })
  } catch (err) {
    console.log("Error in streaming object to download : ", err)
  }
}

export const getDocDetails = async docId => {
  let error
  let result
  if (!docId) {
    error = "No docId provided"
  }
  try {
    const res = await axios.get(`${muniApiBaseURL}/docs/${docId}`, {
      headers: { Authorization: getToken() }
    })
    console.log("res : ", res)
    if (res && res.data) {
      result = res.data
    } else {
      error = "No document found with the docId"
    }
  } catch (err) {
    console.log("error in getting doc : ", err)
    error = "Error in getting document"
  }
  return [error, result]
}

export const addDocInDB = async (payload) => {
  try {
    const res = await axios.post(`${muniApiBaseURL}/docs`, payload,{headers:{"Authorization":getToken()}})
    if(res && (res.status === 201 || res.status === 200)){
      return res.data || {}
    }
    return null
  } catch (err) {
    console.log("err in addDocInDB ", err.message)
    return null
  }
}

export const getSignedUrl = async (payload) => {
  try {
    const res = await axios.post(`${muniApiBaseURL}s3/get-signed-url`, payload,{headers:{"Authorization":getToken()}})
    if(res && res.status === 200){
      return res
    }
    return null
  } catch (err) {
    console.log("err in getSignedUrl ", err.message)
    return null
  }
}
