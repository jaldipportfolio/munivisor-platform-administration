import axios from "axios"
import moment from "moment"
import {getToken, muniApiBaseURL} from "../../../globalutilities/consts"
import {errorToast} from "../../../globalutilities/helpers"


/** Invoice ID Generator * */

export const invoiceIdGenerator = () => {
  const date = new Date()
  const mm = moment(new Date()).format("MMM")
  const dd = date.getDate()
  const yyyy = date.getFullYear()
  const hrs = date.getHours()
  const min = date.getMinutes()
  const sec = date.getSeconds()
  return `${dd}${mm}-${yyyy}${hrs}${min}${sec}`.toUpperCase()
}

/** Invoice Preview * */

export const getInvoicePreview = async (invoiceId) => {
  try {
    if(!invoiceId) return errorToast("Please pass invoice id.")
    const res = await axios.get(`${muniApiBaseURL}common/invoicespreview/${invoiceId || ""}`, { headers: { Authorization: getToken() } })
    return res.data
  } catch (error) {
    errorToast("Not getting invoice please try after some time.")
    return error
  }
}

export const updateInvoicePreview = async (invoiceId, payload) => {
  try {
    if(!invoiceId) return errorToast("Please pass invoice id.")
    const res = await axios.put(`${muniApiBaseURL}common/invoicespreview/${invoiceId || ""}`, payload, { headers: { Authorization: getToken() } })
    return res.data
  } catch (error) {
    errorToast("Not getting invoice please try after some time.")
    return error
  }
}

/** Invoice Activity Log * */

export const updateInvoiceLog = async (logs) => {
  try {
    const res = await axios.post(`${muniApiBaseURL}common/invoicelogs`, logs, { headers: { Authorization: getToken() } })
    return res
  } catch (error) {
    return error
  }
}

export const getInvoiceLogById = async (payload) => {
  try {
    const res = await axios.put(`${muniApiBaseURL}common/invoicelogs`, payload, { headers: { Authorization: getToken() } })
    return (res && res.data && res.data.length && res.data[0]) || {}
  } catch (error) {
    return error
  }
}

/** Send Invoice On Email * */

export const sendInvoiceOnEmail = async (payload) => {
  try {
    const res = await axios.post(`${muniApiBaseURL}billing/sendinvoice`, payload, { headers: { Authorization: getToken() } })
    return res
  } catch (error) {
    return error
  }
}

/** Invoices List * */

export const getInvoicesList = async (payload) => {
  try {
    const res = await axios.post(`${muniApiBaseURL}common/invoice`, payload, { headers: { Authorization: getToken() } })
    return (res.data && res.data[0]) || {}
  } catch (error) {
    return error
  }
}

/** Generate Invoice * */

export const generateInvoice = async (payload, type) => {
  try{

    const res = await axios.post(`${muniApiBaseURL}pdf/generateinvoicepdf`,{...payload},{  responseType: "arraybuffer", headers: { Authorization: getToken() } })
    const file = new Blob([res.data], {type: "application/pdf"})
    const fileURL = URL.createObjectURL(file)
    if(type === "preview"){
      window.open(fileURL)
    }
    return file
  }catch (e) {
    console.log("err in generate pdf : ", e)
  }
}

/** Invoice Payment Refund * */

export const invoicePaymentRefund = async (payload) => {
  try {
    const res = await axios.post(`${muniApiBaseURL}payment/refunds`, payload, { headers: { Authorization: getToken() } })
    return res
  } catch (error) {
    return (error && error.response && error.response) || {}
  }
}

export const getPartialRefunds = async (charge) => {
  try {
    const res = await axios.get(`${muniApiBaseURL}payment/refunds?charge=${charge}`, { headers: { Authorization: getToken() } })
    return (res && res.data) || {}
  } catch (error) {
    return (error && error.response && error.response) || {}
  }
}

/** Payment Events * */

export const getEvents = async (payload) => {
  try {
    const res = await axios.post(`${muniApiBaseURL}payment/events`, payload, { headers: { Authorization: getToken() } })
    return (res && res.data) || {}
  } catch (error) {
    return (error && error.response && error.response) || {}
  }
}