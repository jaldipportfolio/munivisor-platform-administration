// eslint-disable-next-line no-unused-vars
export const errorHandler = store => next => action => {
  try {
    return next(action)
  } catch (e) {
    console.log("ERROR!", e)
    return next(e)
  }
}
