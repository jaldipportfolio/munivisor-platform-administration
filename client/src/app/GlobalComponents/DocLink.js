import React, { Component } from "react"
// eslint-disable-next-line import/named
import { getDocDetails, getFile } from "../Services/actions/Docs"

class DocLink extends Component {
  constructor(props) {
    super(props)
    this.state = { fileName: "", waiting: true, error: "", objectName: "" }
  }

  async componentDidMount() {
    await this.getDocInfo(this.props.docId)
  }

  async componentWillReceiveProps(nextProps) {
    if((nextProps.docId !== this.props.docId) ||
      (nextProps.name !== this.props.name)) {
      await this.getDocInfo(nextProps.docId)
    }
  }

  async getDocInfo(_id) {
    const res = await getDocDetails(_id)
    console.log("res : ", res)
    const [error, doc] = res
    if(error) {
      this.setState({ error, waiting: false })
    } else {
      let err = ""
      let objectName = ""
      const { contextId, contextType, tenantId, name } = doc
      if(tenantId && contextId && contextType && name) {
        objectName = `${tenantId}/${contextType}/${contextType}_${contextId}/${name}`
      } else if(tenantId && contextType && name && contextType === "INVOICES"){
        objectName = `${contextType}/${name}`
      }else {
        err = "invalid doc details"
      }
      this.setState({ fileName: doc.originalName, waiting: false, error: err, objectName })
    }
  }

  render() {
    const { downloadIcon, className, style } = this.props
    const { fileName, waiting, error, objectName } = this.state

    return (
      <div className={className || ""} style={style || {}}>
        {fileName &&
          <button onClick={() => getFile(`?objectName=${encodeURIComponent(objectName)}`, fileName)} title="Download" className="btn btn-sm btn-link btn-block">{
            downloadIcon ?
              <i className="fa fa-download fa-lg" />
              : fileName
          }</button>
        }
        {/* {fileName &&
          <a onClick={() => getFile(`?objectName=${encodeURIComponent(objectName)}`, fileName)}
            download={fileName}>{fileName}</a>
        } */}
        {(waiting || !fileName) && null}
        {error && <strong>{error}</strong>}
      </div>
    )
  }
}

export default DocLink
