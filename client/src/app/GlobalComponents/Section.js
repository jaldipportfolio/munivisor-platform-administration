import React from "react"

const props = {
  paddingTop: 0,
  paddingBottom: 0
}

const Section = ({
  children,
  actionButtons,
  title,
  headerStyle,
  onAccordion,
  style
}) => (
  <article  className="accordion is-active">
    <div className="accordion-header" style={{outline: "none", boxShadow: "none", border: "none", borderBottom: "2px solid #f29718", background: "white"}}>
      <p onClick={onAccordion} style={{textTransform: "uppercase"}}>{title}</p>
      <div>
        <div style={{ right: 20, top: 6, position: "absolute"}}>
          <div>
            {actionButtons || null}
          </div>&nbsp;
          <i className={`${children ? "fa fa-chevron-down":"fa fa-chevron-up"}`} style={actionButtons ? { float: "right", marginTop: -22, marginRight: -8} : {}} onClick={onAccordion || null}/>
        </div>
      </div>
    </div>
    <div className="accordion-body" style={style}>
      <div
        className="accordion-content"
        style={!children ? props : { overflow: "initial" }}
      >
        {children}
      </div>
    </div>
  </article>
)

export default Section
