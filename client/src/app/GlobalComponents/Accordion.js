import React, { Component } from "react"
import { Button, Card, CardBody, CardFooter, Collapse } from "reactstrap"

class Accordion extends Component {

  constructor(props) {
    super(props)
    this.state = {
      accordion: props.defaultAccordion || [true]
    }
  }

    toggleAccordion = () => {
      const { accordion } = this.state
      const state = !accordion[0]
      this.setState({
        accordion: [state]
      })
    }

    render() {
      const { accordion } = this.state
      const { activeAccordionHeader, children, footerChildren } = this.props
      return (
        <Card>

          <div className="accordion-header" style={{outline: "none", boxShadow: "none", border: "none", borderBottom: "2px solid #f29718", background: "white"}}>
            <p onClick={this.toggleAccordion} style={{textTransform: "uppercase", marginLeft: 10}}><b>{activeAccordionHeader}</b></p>
            <div>
              <div className="float-right" style={{marginTop: -35, marginRight: 20}}>
                { this.props.add ? ( <Button color="primary" className="btn-sm" onClick={this.props.add}>Add</Button> ) : null }&nbsp;
                { this.props.reset ? ( <Button color="secondary" className="btn-sm" onClick={this.props.reset}>Reset</Button> ) : null }&nbsp;
                { this.props.showCount ? (<span onClick={this.toggleAccordion || null}><b>({`${this.props.count || 0} FOUND`})</b></span>) : null }&nbsp;
                <i onClick={this.toggleAccordion || null} className={`${accordion && accordion[0] ? "fa fa-chevron-down" : "fa fa-chevron-up"}`}/>
              </div>
            </div>
          </div>

          <Collapse isOpen={accordion[0]} data-parent="#accordion" id="collapseOne" aria-labelledby="headingOne">

            <CardBody>
              {children}
            </CardBody>

            { footerChildren ? (
              <CardFooter>
                { footerChildren }
              </CardFooter>
            ) : null
            }

          </Collapse>
        </Card>
      )
    }
}

export default Accordion
