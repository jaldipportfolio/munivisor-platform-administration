import React, { Component } from "react"
import cloneDeep from "lodash.clonedeep"
import {Button, Col, FormGroup, Input, InputGroup, Label, Row} from "reactstrap"
import PhoneInput, {formatPhoneNumber, isValidPhoneNumber} from "react-phone-number-input"
import LaddaButton, {ZOOM_IN} from "react-ladda"
import ReactTable from "react-table"
import swal from "sweetalert"
import { checkEmptyElObject } from "../globalutilities/helpers"
import { CONST } from "../globalutilities/consts"
import { validatClientsDetail } from "../FunctionalComponents/SideBarComponents/Tenant/Validation"
import Loader from "./Loader"
import SearchAddress from "../FunctionalComponents/GoogleAddressComponents/GoogleAddressFormComponent"
import SearchCountry from "../FunctionalComponents/GoogleAddressComponents/GoogleCountryComponent"
import Accordion from "./Accordion"
import { NumberInput } from "../GlobalComponents/TextInput"

class ContactInformation extends Component {
  constructor(props) {
    super(props)
    this.state = {
      errorFirmDetail: {},
      addressList: [],
      showAddress: true,
      confirmAlert: CONST.confirmAlert,
      businessAddress: this.initialAddresses()[0],
      loading: true
    }
  }

  initialAddresses() {
    return [
      {
        addressName: "",
        isPrimary: false,
        isHeadQuarter: false,
        website: "",
        officePhone: [
          {
            countryCode: "",
            phoneNumber: "",
            extension: ""
          }
        ],
        officeFax: [
          {
            faxNumber: ""
          }
        ],
        officeEmails: [{ emailId: "" }],
        addressLine1: "",
        addressLine2: "",
        country: "",
        state: "",
        city: "",
        zipCode: { zip1: "", zip2: "" }
      }
    ]
  }

  componentDidMount() {
    const { addressList } = this.state
    let addressInitial = this.initialAddresses()[0]
    if(addressList && !addressList.length){
      addressInitial.isPrimary = true
    }
    this.setState({
      loading: false,
      businessAddress: addressInitial
    })
  }

    addNew = (e = null) => {
      const { addressList } = this.state
      this.setState({
        businessAddress: this.initialAddresses()[0],
        listAddressToggle: true,
        isEdit: false
      })
      if (
        addressList &&
            addressList.length === 1 &&
            addressList[0].addressName === ""
      ) {
        addressList.splice(0, 1)
      }
      this.resetBussinessAddress(e, true)
    }

    resetBussinessAddress = (e, showAddress) => {
      const addressInitial = this.initialAddresses()[0]
      const { addressList } = this.state
      if (addressList && addressList.filter(add => add.isPrimary).length === 0) {
        addressInitial.isPrimary = true
      } else if (addressList === undefined) {
        addressInitial.isPrimary = true
      }
      this.setState(prevState => ({
        ...prevState,
        businessAddress: addressInitial ,
        showAddress,
        errorFirmDetail: {},
        isEdit: showAddress === true ? false : null
      }))
    }

    getAddressDetails = (address = "", idx) => {
      if (address !== "") {
        const { businessAddress, errorFirmDetail } = this.state
        let id = ""
        if (businessAddress._id !== "") {
          id = businessAddress._id
        }
        businessAddress._id = id
        if (address.addressLine1 !== "") {
          businessAddress.addressName = `${address.addressLine1}, ${address.city}` || ""
          businessAddress.addressLine1 = address.addressLine1.trim()
        }
        if (address.country !== "") {
          businessAddress.country = address.country.trim()
        }
        if (address.state !== "") {
          businessAddress.state = address.state.trim()
        }
        if (address.city !== "") {
          businessAddress.city = address.city.trim()
        }
        if (address.zipcode !== "") {
          if(businessAddress.zipCode){
            businessAddress.zipCode.zip1 = address.zipcode.trim().trim() || ""
            businessAddress.zipCode.zip2 = ""
          }else {
            businessAddress.zipCode = {
              zip1: address.zipcode.trim().trim() || "",
              zip2: "",
            }
          }
        }
        businessAddress.formatted_address = address.formatted_address
        businessAddress.url = address.url.trim()
        businessAddress.location = { ...address.location }

        this.setState(prevState => ({
          ...prevState,
          businessAddress,
          errorFirmDetail: {
            ...prevState.errorFirmDetail,
            ...errorFirmDetail
          }
        }))
      }
    }

    getCountryDetails = (cityStateCountry = "") => {
      const { businessAddress } = this.state
      if (cityStateCountry && cityStateCountry.country) {
        businessAddress.country = cityStateCountry.country.trim()
      }else {
        businessAddress.country = ""
      }
      if (cityStateCountry && cityStateCountry.state) {
        businessAddress.state = cityStateCountry.state.trim()
      }else {
        businessAddress.state = ""
      }
      if (cityStateCountry && cityStateCountry.city) {
        businessAddress.city =  cityStateCountry.city.trim()
      }else {
        businessAddress.city =  ""
      }
      if (cityStateCountry && cityStateCountry.zipcode) {
        businessAddress.zipCode.zip1 = cityStateCountry.zipcode.trim()
        businessAddress.zipCode.zip2 = cityStateCountry.zipcode.trim()
      }else {
        businessAddress.zipCode.zip1 = ""
        businessAddress.zipCode.zip2 = ""
      }
      this.setState({
        businessAddress
      })
    }

    onPhoneNumberChange = (e, nameProp, idx, key) => {
      const { businessAddress } = this.state
      const Internationals = e && formatPhoneNumber(e, "International")
      const error = Internationals ? (isValidPhoneNumber(Internationals) ? undefined : "Invalid phone number") : ""
      businessAddress[nameProp][idx][key] = e || ""
      this.setState(prevState => ({
        ...prevState,
        businessAddress,
        errorFirmDetail: {
          ...prevState.errorFirmDetail,
          [nameProp]: {
            ...prevState.errorFirmDetail[nameProp],
            [idx]: {[key]: error || ""}
          }
        }
      }))
    }

    addMore = (e, type) => {
      e.preventDefault()
      const {businessAddress} = this.state
      switch (type) {
      case "email":
        businessAddress.officeEmails.push({ emailId: "" })
        break
      case "phone":
        businessAddress.officePhone.push({
          countryCode: "",
          phoneNumber: "",
          extension: ""
        })
        break
      case "fax":
        businessAddress.officeFax.push({ faxNumber: "" })
        break
      default:
        return ""
      }
      this.setState(prevState => ({
        ...prevState,
        businessAddress
      }))
    }

    cancelAdd = (e, type) => {
      e.preventDefault()
      const {businessAddress, errorFirmDetail} = this.state
      const name = type === "email" ? "officeEmails" : type === "phone" ? "officePhone" : type === "fax" ? "officeFax" : ""
      const localData = businessAddress[name].filter(f => f._id)
      switch (type) {
      case "email":
        businessAddress.officeEmails = (localData) || [{
          countryCode: "",
          phoneNumber: "",
          extension: ""
        }]
        errorFirmDetail.officeEmails = {}
        break
      case "phone":
        businessAddress.officePhone = (localData) || [{ emailId: "" }]
        errorFirmDetail.officePhone = {}
        break
      case "fax":
        businessAddress.officeFax = (localData) || [{ faxNumber: "" }]
        errorFirmDetail.officeFax = {}
        break
      default:
        return ""
      }
      this.setState(prevState => ({
        ...prevState,
        errorFirmDetail,
        businessAddress
      }))
    }

    onAddressPartChange(e, nameProp, idx, nextToClean) {
      const { value, name, type, checked } = e.target
      const { businessAddress } = this.state
      if (idx === undefined || idx === null) {
        if (type === "checkbox") {
          businessAddress[name] = checked
        } else if (name === "zip1" || name === "zip2") {
          businessAddress.zipCode[name] = value.trim() || ""
        } else {
          businessAddress[name] = value
        }
      } else {
        businessAddress[nameProp][idx][name] = value.trim()
      }
      if (nextToClean !== undefined) {
        nextToClean.forEach(item => (businessAddress[item] = ""))
      }
      this.setState(prevState => ({
        ...prevState,
        businessAddress
      }))
    }

    onReset = (key, index) => {
      const { businessAddress, errorFirmDetail } = this.state
      businessAddress[key].splice(index, 1)
      this.setState({
        businessAddress,
        errorFirmDetail: {
          ...errorFirmDetail,
          [key]: {
            ...errorFirmDetail[key],
            [index]: {[key]: ""}
          }
        }
      })
    }

    onSaveAddress = () => {
      this.setState({ modalState: false })
      const {
        addressList,
        addressId,
        errorFirmDetail
      } = this.state
      let {  businessAddress } = this.state
      const emptyErrorFirmDetail = this.initialAddresses()[0]
      // To Be Removed
      const { officeFax, officePhone  } = errorFirmDetail
      const phoneError = []
      if(officeFax !== undefined){
        Object.values(officeFax).forEach(m => {
          if(m.faxNumber){
            phoneError.push(m.faxNumber)
          }
        })
        if(phoneError && phoneError.length && phoneError.length < 2){
          if(phoneError[0] === "Duplicate values are not allowed"){
            phoneError.splice(0, 1)
          }
        }
      }
      if(officePhone !== undefined){
        Object.values(officePhone).forEach(m => {
          if(m.phoneNumber){
            phoneError.push(m.phoneNumber)
          }
        })
        if(phoneError && phoneError.length && phoneError.length < 2){
          if(phoneError[0] === "Duplicate values are not allowed"){
            phoneError.splice(0, 1)
          }
        }
      }
      if(phoneError && phoneError.length){
        return
      }
      addressList.forEach(add => {
        add.zipCode.zip2 = add.zipCode.zip2
          ? add.zipCode.zip2.length > 4
            ? add.zipCode.zip2.substring(0, 4)
            : add.zipCode.zip2
          : add.zipCode.zip2
        add.zipCode.zip1 = add.zipCode.zip1
          ? add.zipCode.zip1.length > 5
            ? add.zipCode.zip1.substring(0, 5)
            : add.zipCode.zip1
          : add.zipCode.zip1
      })
      if (businessAddress) {
        businessAddress.zipCode.zip2 = businessAddress.zipCode.zip2
          ? businessAddress.zipCode.zip2.length > 4
            ? businessAddress.zipCode.zip2.substring(0, 4)
            : businessAddress.zipCode.zip2
          : businessAddress.zipCode.zip2 || ""
        businessAddress.zipCode.zip1 = businessAddress.zipCode.zip1
          ? businessAddress.zipCode.zip1.length > 5
            ? businessAddress.zipCode.zip1.substring(0, 5)
            : businessAddress.zipCode.zip1
          : businessAddress.zipCode.zip1 || ""
        delete businessAddress.isActive
      }
      addressList.map(add => delete add.isActive)
      // To Be Removed

      let errorData
      if (
        businessAddress &&
            !checkEmptyElObject(businessAddress) &&
            this.state.isEdit !== null
      ) {
        const addressData = {
          _id: "",
          addresses: [businessAddress]
        }
        errorData = validatClientsDetail(addressData, "address")
      }
      if (errorData && errorData.error) {
        const errorMessages = {}
        const setArrIndex = ["officeEmails", "officePhone", "officeFax"]
        console.log("=======>", errorData.error.details)

            errorData.error.details.map(err => { //eslint-disable-line
                //eslint-disable-line
                if (errorMessages.hasOwnProperty(err.path[0])) { //eslint-disable-line
                    //eslint-disable-line
            if (
                        errorMessages[err.path[0]].hasOwnProperty(err.path[1]) || //eslint-disable-line
                        setArrIndex.indexOf(err.path[2]) !== -1
            ) {
                        //eslint-disable-line
              if (setArrIndex.indexOf(err.path[2]) !== -1) {
                if (
                                errorMessages[err.path[0]][err.path[1]].hasOwnProperty( //eslint-disable-line
                    err.path[2]
                  )
                ) {
                  if (
                    errorMessages[err.path[0]][err.path[1]][
                      err.path[2]
                                        ].hasOwnProperty(err.path[3]) //eslint-disable-line
                  ) {
                    errorMessages[err.path[0]][err.path[1]][err.path[2]][
                      err.path[3]
                    ] = {
                      ...errorMessages[err.path[0]][err.path[1]][err.path[2]][
                        err.path[3]
                      ],
                      [err.path[4]]: err.message
                    }
                  } else {
                    errorMessages[err.path[0]][err.path[1]][err.path[2]] = {
                      ...errorMessages[err.path[0]][err.path[1]][err.path[2]],
                      [err.path[3]]: {
                        [err.path[4]]: err.message
                      }
                    }
                  }
                } else {
                  errorMessages[err.path[0]][err.path[1]] = {
                    ...errorMessages[err.path[0]][err.path[1]],
                    [err.path[2]]: {
                      [err.path[3]]: {
                        [err.path[4]]: err.message
                      }
                    }
                  }
                }
              } else if(err.path[2] === "zipCode" && (err.path[3] === "zip1" || err.path[3] === "zip2")){
                            if(errorMessages[err.path[0]][err.path[1]].hasOwnProperty(err.path[2])){ //eslint-disable-line
                  errorMessages[err.path[0]][err.path[1]][err.path[2]] ={
                    ...errorMessages[err.path[0]][err.path[1]][err.path[2]],
                    [err.path[3]]: err.message
                  }
                }else {
                  errorMessages[err.path[0]][err.path[1]] = {
                    ...errorMessages[err.path[0]][err.path[1]],
                    [err.path[2]]: {
                      [err.path[3]]: err.message
                    }
                  }
                }
              }else {
                errorMessages[err.path[0]][err.path[1]][err.path[2]] = err.message
              }
            } else if (err.path[2]) {
              if(err.path[2] === "zipCode"){
                            if(errorMessages[err.path[0]].hasOwnProperty(err.path[2])){ //eslint-disable-line
                  errorMessages[err.path[0]][err.path[2]][err.path[3]] = err.message
                }else {
                  errorMessages[err.path[0]] = {
                    [err.path[2]]: {
                      [err.path[3]]: err.message
                    }
                  }
                }
              }else {
                errorMessages[err.path[0]][err.path[1]] = {
                  [err.path[2]]: setArrIndex.indexOf(err.path[2]) !== -1 ? {} : err.message
                }
              }
            } else {
              errorMessages[err.path[0]][err.path[1]] = err.message
            }
          } else if (err.path[1] !== undefined && err.path[2] !== undefined) {
            if (setArrIndex.indexOf(err.path[2]) !== -1) {
              errorMessages[err.path[0]] = {
                [err.path[1]]: {
                  [err.path[2]]: {
                    [err.path[3]]: {
                      [err.path[4]]: err.message
                    }
                  }
                }
              }
            } else {
                        if (err.path[2]) { //eslint-disable-line
                errorMessages[err.path[0]] = {
                  [err.path[1]]: {
                    [err.path[2]]: setArrIndex.indexOf(err.path[2]) !== -1 ? {} : err.message
                  }
                }
              } else {
                errorMessages[err.path[0]][err.path[1]] = err.message
              }
            }
          } else {
                    if (err.path[1]) { //eslint-disable-line
                        if (errorMessages.hasOwnProperty(err.path[0])) { //eslint-disable-line
                errorMessages[err.path[0]][err.path[1]] = err.message
              } else {
                errorMessages[err.path[0]] = {
                  [err.path[1]]: err.message
                }
              }
            } else {
              errorMessages[err.path[0]] = err.message
            }
          }
        })

        console.log("*******errorMessages.addresses********", errorMessages.addresses["0"])
        return this.setState({ errorFirmDetail: errorMessages.addresses["0"] })
      }
      const error = {}
      const types = ["officePhone", "officeEmails", "officeFax"]

      types.forEach(o => {
        const key = o === "officePhone" ? "phoneNumber" : o === "officeEmails" ? "emailId" : o === "officeFax" ? "faxNumber" : ""
        const dupResult = this.checkDupInArray((businessAddress && businessAddress[o] && businessAddress[o].map(d => d[key])) || [])
        if(dupResult && dupResult.length > 1 && dupResult[0] > -1 && dupResult[1] > -1) {
          error[o] = { ...error[o] }
          error[o][dupResult[0]] = { ...error[o][dupResult[0]] }
          error[o][dupResult[0]][key] = "Duplicate values are not allowed"
          error[o][dupResult[1]] = { ...error[o][dupResult[1]] }
          error[o][dupResult[1]][key] = "Duplicate values are not allowed"
        }
      })
      if((error && error.officePhone) || (error && error.officeEmails) || (error && error.officeFax)){
        return this.setState({errorFirmDetail: error, waiting: false})
      }

      this.setState({ errorFirmDetail: emptyErrorFirmDetail })
      const emptyAddressIndex = addressList.findIndex(add =>
        checkEmptyElObject(add)
      )
      if (emptyAddressIndex !== undefined && emptyAddressIndex > -1)
        addressList.splice(emptyAddressIndex, 1)

      businessAddress && businessAddress.officeFax && businessAddress.officeFax.length && businessAddress.officeFax.forEach((item, idx) => {
        const value = (item && item.faxNumber && item.faxNumber.includes("+")) || false
        const Internationals = (item.faxNumber && formatPhoneNumber(value ? item.faxNumber : `+1${item.faxNumber}`, "International")) || "International"
        if(!value && item.faxNumber){
          businessAddress.officeFax[idx].faxNumber = Internationals || ""
          this.setState({businessAddress})
        }
      })

      businessAddress && businessAddress.officePhone && businessAddress.officePhone.length && businessAddress.officePhone.forEach((item, idx) => {
        const value = (item && item.phoneNumber && item.phoneNumber.includes("+")) || false
        const Internationals = (item.phoneNumber && formatPhoneNumber(value ? item.phoneNumber : `+1${item.phoneNumber}`, "International")) || "International"
        if(!value && item.phoneNumber){
          businessAddress.officePhone[idx].phoneNumber = Internationals || ""
          this.setState({businessAddress})
        }
      })

      businessAddress.officePhone = businessAddress.officePhone && businessAddress.officePhone.filter(phone => phone.phoneNumber || phone.extension)
      businessAddress.officeEmails = businessAddress.officeEmails && businessAddress.officeEmails.filter(email => email.emailId)
      businessAddress.officeFax = businessAddress.officeFax && businessAddress.officeFax.filter(fax => fax.faxNumber)

      if (this.state.isUserAddress) {
        businessAddress = this.removePropsForUserAddress(businessAddress)
      }

      if (!this.state.isEdit) {
        if (businessAddress && !checkEmptyElObject(businessAddress)) {
          addressList.push(businessAddress)
          if (this.checkForUniqueness(addressList)) {
            this.setState(prevState => ({ ...prevState, isEdit: false }))
            this.setState({
              errorFirmDetail: {},
              businessAddress: this.initialAddresses()[0],
            }, () => this.props.onSaveAddress(addressList))
          } else {
            addressList.splice(addressList.length - 1, 1)
          }
        } else if (this.checkForUniqueness(addressList))
          this.setState({
            errorFirmDetail: {},
            businessAddress: this.initialAddresses()[0],
          }, () => this.props.onSaveAddress(addressList))
      } else if (!checkEmptyElObject(businessAddress)) {
        addressList[addressId] = businessAddress
        if (this.checkForUniqueness(addressList))
          this.setState({
            errorFirmDetail: {},
            businessAddress: this.initialAddresses()[0],
          }, () => this.props.onSaveAddress(addressList))
      }
    }

    checkForUniqueness = addressList => {
      let primaryError = ""
      let uniqueAddressName = ""
      const noOfPrimaryAddresses = addressList.filter(add => add.isPrimary).length
      if (addressList.length !== 0) {
        if (noOfPrimaryAddresses !== 1) {
          primaryError = "We need exact one primary address"
        }
        addressList.forEach(add => {
          if (addressList.filter(checkAdd => add.addressName === checkAdd.addressName).length > 1) {
            uniqueAddressName = "Please have a unique address name"
          }
        })
        this.setState(prevState => ({
          ...prevState,
          addressList,
          uniqueAddressName,
          primaryError
        }))
      }
      return primaryError === "" && uniqueAddressName === ""
    }

    checkDupInArray(arr) {
      console.log("arr : ", arr)
      let result = [-1, -1]
      arr.some((e, i) => {
        if(e) {
          const dupIdx = arr.slice(i + 1).findIndex(d => d === e)
          if(dupIdx > -1) {
            result = [i, dupIdx+i+1]
            return true
          }
        }
        return false
      })
      console.log("dupResult : ", result)
      return result
    }

    selectAddress = (id) => {
      this.state.isEdit = true
      const address = cloneDeep(this.state.addressList[id])

      this.setState(prevState => {
        let { errorFirmDetail } = prevState
        errorFirmDetail = this.initialAddresses()[0]
        const businessAddress = cloneDeep(address)
        const arr = ["officeEmails", "officePhone", "officeFax"]
        if (!this.state.isUserAddress) {
          arr.forEach(item => {
            if (address[item].length > 1) {
              for (let i = 1; i < address[item].length; i++) {
                errorFirmDetail[item].push(this.initialAddresses()[0][item][0])
              }
            }
          })
        }

        arr.forEach(item => {
          businessAddress[item] = (address[item] && address[item].length && address[item]) || this.initialAddresses()[0][item]
        })

        return {
          businessAddress,
          errorFirmDetail,
          showAddress: true,
          addressId: id
        }
      })
    }

    showModal = (type, id) => {
      const { confirmAlert, addressList } = this.state
      confirmAlert.text = type === "primary" ? "Are You Sure You Want To Make This Address As Primary?" : "Are You Sure You Want Delete This Address?"
      swal(confirmAlert).then(willDelete => {
        if (willDelete) {
          if(type === "primary"){
            const primaryIndex = addressList.findIndex(add => add.isPrimary)
            if (primaryIndex >= 0) {
              addressList[primaryIndex].isPrimary = false
            }
            addressList[id].isPrimary = true
            this.setState(
              prevState => ({
                ...prevState,
                addressList,
                listAddressToggle: true
              }),
              () => this.onSaveAddress()
            )
          }
          if(type === "delete"){
            addressList.splice(id, 1)
            this.setState(
              prevState => ({
                ...prevState,
                addressList,
                listAddressToggle: true
              }),
              () => this.onSaveAddress()
            )
          }

        }
      })
    }


    render() {
      const { businessAddress, errorFirmDetail, showAddress, expZoomIn, addressList, uniqueAddressName } = this.state

      const loading = () => <Loader />

      if (this.state.loading) {
        return loading()
      }
      return (
        <div className="animated fadeIn">
          <Row>
            <Col xs="12" sm="12">
              <Accordion
                activeAccordionHeader="Business Contact Information"
                add={this.addNew}
                reset={(e) => this.resetBussinessAddress(e, false)}
              >
                {addressList && addressList.length ?
                  <ReactTable
                    columns={[
                      {
                        Header: "Address Name",
                        id: "addressName",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        minWidth: 100,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="hpTablesTd -striped">
                              { (item && item.addressName) || "" }
                            </div>
                          )
                        },
                        sortMethod: (a, b) => `${a.addressName}` - `${b.addressName}`
                      },
                      {
                        Header: "Is Primary?",
                        id: "isPrimary",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        minWidth: 100,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="hpTablesTd -striped">
                              <input
                                name={`tablePrimary${item.index}`}
                                type="checkbox"
                                value={item.isPrimary}
                                checked={item.isPrimary}
                                onChange={event => {
                                  if (!(event.target.checked === false && item.isPrimary === true)) {
                                    this.showModal("primary", row.index)
                                  }
                                }
                                }
                              />
                            </div>
                          )
                        },
                        sortMethod: (a, b) => `${a.isPrimary}` - `${b.isPrimary}`
                      },
                      {
                        Header: "Update",
                        id: "update",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        minWidth: 100,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="hpTablesTd -striped">
                            <span className="has-text-link">
                              <i
                                className="nav-icon fa fa-edit"
                                onClick={event => {
                                  event.preventDefault()
                                  this.selectAddress(row.index)
                                }} />
                            </span>
                              {"\u00A0\u00A0\u00A0"}
                              { !item.isPrimary ? (
                                <span className="has-text-link">
                                <i
                                  className="nav-icon fa fa-trash"
                                  onClick={event => {
                                    event.preventDefault()
                                    this.showModal("delete", row.index)
                                  }}
                                />
                              </span>
                              ) : null}
                            </div>
                          )
                        },
                      }
                    ]}
                    data={addressList || []}
                    manual
                    showPaginationBottom
                    defaultPageSize={10}
                    pageSizeOptions={[5, 10, 20, 50, 100]}
                    className="-striped -highlight is-bordered"
                    style={{ overflowX: "auto" }}
                    showPageJump
                    minRows={2}
                  />
                  : null}

                { showAddress ?
                  <div>
                    <FormGroup row className="my-0">
                      <Col xs="12" md="12" xl="12">
                        <FormGroup>
                          <SearchAddress
                            idx={0}
                            getAddressDetails={this.getAddressDetails}
                          />
                        </FormGroup>
                      </Col>
                    </FormGroup>
                    <FormGroup row className="my-0">
                      <Col xs="12" md="6" xl="6">
                        <FormGroup>
                          <Label><b>Address Name</b></Label>
                          <Input
                            error={
                              (errorFirmDetail && errorFirmDetail.addressName) || ""
                            }
                            name="addressName"
                            type="text"
                            label="Address Name"
                            value={(businessAddress && businessAddress.addressName) || ""}
                            onChange={event => {
                              this.onAddressPartChange(event, "addressName")
                            }}
                          />
                          <p className="text-danger">{uniqueAddressName || ""}</p>
                        </FormGroup>
                      </Col>
                      <Col xs="12" md="6" xl="6">
                        <FormGroup>
                          { (businessAddress && businessAddress.isPrimary) ?
                            <FormGroup>
                              <Label/>
                              <h5>This is a primary address</h5>
                            </FormGroup>
                            : null
                          }
                        </FormGroup>
                      </Col>
                    </FormGroup>
                    <FormGroup row className="my-0">
                      <Col xs="12" md="6" xl="6">
                        <FormGroup>
                          <Label><b>Address Line 1<span className="text-danger text-monospace"> *</span></b></Label>
                          <Input
                            error={
                              (errorFirmDetail && errorFirmDetail.addressLine1) || ""
                            }
                            type="text"
                            label="Address Line 1"
                            name="addressLine1"
                            placeholder="address line 1"
                            value={(businessAddress && businessAddress.addressLine1) || ""}
                            onChange={event => {
                              this.onAddressPartChange(event, "addressLine1")
                            }}
                          />
                        </FormGroup>
                      </Col>
                      <Col xs="12" md="6" xl="6">
                        <FormGroup>
                          <Label><b>Address Line 2</b></Label>
                          <Input
                            error={
                              (errorFirmDetail && errorFirmDetail.addressLine2) || ""
                            }
                            type="text"
                            label="Address Line 2"
                            name="addressLine2"
                            placeholder="address line 2"
                            value={(businessAddress && businessAddress.addressLine2) || ""}
                            onChange={event => {
                              this.onAddressPartChange(event, "addressLine2")
                            }}
                          />
                        </FormGroup>
                      </Col>
                    </FormGroup>
                    <FormGroup row className="my-0">
                      <Col xs="12" md="6" xl="6">
                        <FormGroup>
                          <SearchCountry
                            idx={0}
                            value={(businessAddress && businessAddress.country) ? `${businessAddress && businessAddress.city}, ${(businessAddress && businessAddress.state)}, ${(businessAddress && businessAddress.country)}` : ""}
                            getCountryDetails={this.getCountryDetails}
                            error={
                              (errorFirmDetail && errorFirmDetail.city) || (errorFirmDetail && errorFirmDetail.state) || (errorFirmDetail && errorFirmDetail.country) || ""
                            }
                          />
                        </FormGroup>
                      </Col>
                      <Col xs="12" md="6" xl="2">
                        <FormGroup>
                          <Label><b>Zip Code<span className="text-danger text-monospace"> *</span></b></Label>
                          <NumberInput
                            className="class"
                            name="zip1"
                            title="Tax Id"
                            format="#####"
                            value={(businessAddress && businessAddress.zipCode && businessAddress.zipCode.zip1) || ""}
                            placeholder="Zip Code 1"
                            onChange={event => {
                              this.onAddressPartChange(event, "zip1")
                            }}
                            error={
                              (errorFirmDetail && errorFirmDetail.zipCode && errorFirmDetail.zipCode.zip1) ||
                              (businessAddress && businessAddress.zipCode && businessAddress.zipCode.zip1 && businessAddress.zipCode.zip1.length && businessAddress.zipCode.zip1.length !== 5 ?
                                "\"Zip Code 1\" length must be at least 5 characters long" : "" ) ||
                              ""}
                          />
                        </FormGroup>
                      </Col>
                      <Col xs="12" md="6" xl="2">
                        <FormGroup>
                          <Label>&nbsp;</Label>
                          <NumberInput
                            className="class"
                            name="zip2"
                            format="####"
                            value={(businessAddress && businessAddress.zipCode && businessAddress.zipCode.zip2) || ""}
                            placeholder="Zip Code 2"
                            onChange={event => {
                              this.onAddressPartChange(event, "zip2")
                            }}
                            error={
                              (errorFirmDetail && errorFirmDetail.zipCode && errorFirmDetail.zipCode.zip2) ||
                              (businessAddress && businessAddress.zipCode && businessAddress.zipCode.zip2 && businessAddress.zipCode.zip2.length && businessAddress.zipCode.zip2.length !== 4 ?
                                "\"Zip Code 2\" length must be at least 4 characters long" : ""  ) ||
                              ""}
                          />
                        </FormGroup>
                      </Col>
                    </FormGroup>
                    <hr/>
                    <FormGroup row className="my-0">
                      <Col xs="12" md="12" xl="12">
                        <FormGroup>
                          <Label><b>Website(s)</b></Label>
                          <Input
                            error={
                              (errorFirmDetail && errorFirmDetail.website) || ""
                            }
                            name="website"
                            type="text"
                            label="Website(s)"
                            value={(businessAddress && businessAddress.website) || ""}
                            onChange={event => {
                              this.onAddressPartChange(event, "website")
                            }}
                          />
                        </FormGroup>
                      </Col>
                    </FormGroup>
                    <FormGroup row className="my-0">
                      <Col xs="12" md="6" xl="5">
                        <FormGroup>
                          <Label><b>Office Phone</b></Label>
                          {businessAddress && businessAddress.officePhone.map(
                            (item, idx) => {
                              const phoneError = (errorFirmDetail && errorFirmDetail.officePhone && errorFirmDetail.officePhone[idx]) || {}
                              const value = (item && item.phoneNumber && item.phoneNumber.includes("+")) || false
                              const Internationals = (item.phoneNumber && formatPhoneNumber(value ? item.phoneNumber : `+1${item.phoneNumber}`, "International")) || "International"
                              return (
                                <div className="controls" style={{marginTop: 5}} key={idx.toString()}>
                                  <InputGroup>
                                    <PhoneInput
                                      className="form-control"
                                      style={{marginRight: 5}}
                                      value={Internationals || ""}
                                      onChange={event => {
                                        this.onPhoneNumberChange(event, "officePhone", idx, "phoneNumber")
                                      }}
                                    />
                                    <Input
                                      format="##########"
                                      className="react-phone-number-input form-control"
                                      name="extension"
                                      placeholder="Ext"
                                      size="10"
                                      limit="10"
                                      error={phoneError.extension ? "Enter valid phone extension" : ""}
                                      value={item.extension || ""}
                                      onChange={event => {
                                        this.onAddressPartChange(
                                          event,
                                          "officePhone",
                                          idx
                                        )
                                      }}
                                    />
                                    <span className="has-text-link"
                                      onClick={() => this.onReset("officePhone", idx)}
                                      style={{fontSize: 22, marginLeft: 25, cursor: "pointer"}}>
                                      <a className="fa fa-trash"/>
                                    </span>
                                  </InputGroup>
                                  <small
                                    className="text-danger">{phoneError.phoneNumber === "Duplicate values are not allowed" ? "Duplicate phone number are not allowed" : phoneError.phoneNumber ? "Enter valid phone number" : ""}</small>
                                </div>
                              )
                            })}
                        </FormGroup>
                        <FormGroup row className="my-0">
                          <Col xs="12" md="12">
                            <FormGroup>
                              <Button color="primary" className="btn-sm" onClick={event => {
                                this.addMore(event, "phone")
                              }}>Add More</Button>&nbsp;
                              <Button color="secondary" className="btn-sm" onClick={event => {
                                this.cancelAdd(event, "phone")
                              }}>Cancel</Button>
                            </FormGroup>
                          </Col>
                        </FormGroup>
                      </Col>
                      <Col xs="12" md="6" xl="3">
                        <FormGroup>
                          <Label><b>Office Fax</b></Label>
                          {businessAddress && businessAddress.officeFax.map((item, idx) => {
                            const faxError = (errorFirmDetail && errorFirmDetail.officeFax && errorFirmDetail.officeFax[idx]) || {}
                            const value = (item && item.faxNumber && item.faxNumber.includes("+")) || false
                            const Internationals = (item.faxNumber && formatPhoneNumber(value ? item.faxNumber : `+1${item.faxNumber}`, "International")) || "International"
                            return (
                              <div className="controls" style={{marginTop: 5}} key={idx.toString()}>
                                <InputGroup>
                                  <PhoneInput
                                    className="form-control"
                                    value={Internationals || ""}
                                    onChange={event => {
                                      this.onPhoneNumberChange(event, "officeFax", idx, "faxNumber")
                                    }}
                                  />
                                  <span className="has-text-link"
                                    onClick={() => this.onReset("officeFax", idx)}
                                    style={{fontSize: 22, marginLeft: 25, cursor: "pointer"}}>
                                    <a className="fa fa-trash"/>
                                  </span>
                                </InputGroup>
                                <small
                                  className="text-danger">{faxError.faxNumber === "Duplicate values are not allowed" ? "Duplicate Fax number are not allowed" : faxError.faxNumber ? "Enter valid fax number" : ""}</small>
                              </div>
                            )
                          })}
                        </FormGroup>
                        <FormGroup row className="my-0">
                          <Col xs="12" md="12">
                            <FormGroup>
                              <Button color="primary" className="btn-sm" onClick={event => {
                                this.addMore(event, "fax")
                              }}>Add More</Button>&nbsp;
                              <Button color="secondary" className="btn-sm" onClick={event => {
                                this.cancelAdd(event, "fax")
                              }}>Cancel</Button>
                            </FormGroup>
                          </Col>
                        </FormGroup>
                      </Col>
                      <Col xs="12" md="6" xl="4">
                        <FormGroup>
                          <Label><b>Office Email</b></Label>
                          {businessAddress && businessAddress.officeEmails.map(
                            (item, idx) => {
                              const emailError = (errorFirmDetail && errorFirmDetail.officeEmails && errorFirmDetail.officeEmails[idx]) || {}
                              return (
                                <div className="field" key={parseInt(idx * 5, 10)} style={{marginTop: 5}}>
                                  <div className="control d-flex">
                                    <Input
                                      name="emailId"
                                      type="email"
                                      placeholder="e.g. alexsmith@gmail.com"
                                      value={item.emailId ? item.emailId : ""}
                                      onChange={event => {
                                        this.onAddressPartChange(
                                          event,
                                          "officeEmails",
                                          idx
                                        )
                                      }}
                                    />
                                  </div>
                                  <small className="text-danger">{emailError && emailError.emailId === "Duplicate values are not allowed" ? "Duplicate email are not allowed" : emailError.emailId ? "Enter valid email" : ""}</small>
                                </div>

                              )
                            }
                          )}
                        </FormGroup>
                        <FormGroup row className="my-0">
                          <Col xs="12" md="12">
                            <FormGroup>
                              <Button color="primary" className="btn-sm" onClick={event => {
                                this.addMore(event, "email")
                              }}>Add More</Button>&nbsp;
                              <Button color="secondary" className="btn-sm" onClick={event => {
                                this.cancelAdd(event, "email")
                              }}>Cancel</Button>
                            </FormGroup>
                          </Col>
                        </FormGroup>
                      </Col>
                    </FormGroup>
                    <hr/>
                    <div className="row justify-content-center">

                      <LaddaButton
                        className="btn btn-primary btn-ladda"
                        loading={expZoomIn}
                        onClick={this.onSaveAddress}
                        data-color="red"
                        data-style={ZOOM_IN}
                        disabled={!(businessAddress && businessAddress.addressLine1) || false}
                      >
                        Save Contact Info
                      </LaddaButton>

                    </div>
                  </div> : null
                }
              </Accordion>
            </Col>
          </Row>
        </div>
      )
    }
}

export default ContactInformation
