import React from "react"
import {Col, FormGroup, Input } from "reactstrap"
import NumberFormat from "react-number-format"
import DatePicker from "react-datepicker"
import {dateFormatted} from "../globalutilities/helpers"

import "react-datepicker/dist/react-datepicker.css"

export const TextInput = ({
  type,
  name,
  style,
  label,
  value,
  className,
  inputClassName,
  title,
  onChange,
  placeholder,
  error,
  onBlur,
  disabled,
  required,
  colXs,
  colMd,
  colXl
}) => {
  const props = {
    name,
    value,
    style,
    title: title || label || "",
    placeholder: placeholder || "",
    onChange,
    type: type || "text",
    disabled: disabled || false,
    className: inputClassName || "",
    onBlur: onBlur || (() => { })
  }

  return (
    <Col xs={colXs || "12"} md={colMd || "6"} xl={colXl || "6"}>
      <FormGroup className={className}>
        {label ?
          <p className="mb-1 multiExpLblBlk">
            <b>{label}</b>
            {required ? <small> <i className="fa fa-asterisk text-danger" /></small> : null}
          </p> : null}
        <Input
          {...props}
        />
        {error && (
          <p className="is-small text-danger " style={{ fontSize: 12 }}>
            {error}
          </p>
        )}
      </FormGroup>
    </Col>
  )
}


export const NumberInput = ({
  type,
  name,
  label,
  prefix,
  suffix,
  value,
  title,
  format,
  mask,
  className,
  thousandSeparator,
  onChange,
  placeholder,
  error,
  onBlur,
  disabled,
  decimalScale,
  required,
  style
}) => {
  const onChaneInput = e => {
    const event = {}
    if(e.value === "-"){
      e.value = 0
    }
    event.target = {
      name,
      title: title || label || "",
      placeholder: placeholder || "",
      type: type || "text",
      ...e,
    }
    onChange(event)
  }
  const props = {
    name,
    title: title || label || "",
    value: value || "",
    placeholder: placeholder || "",
    onValueChange: onChaneInput,
    type: type || "text",
    disabled: disabled || false,
    prefix: prefix || "",
    suffix: suffix || "",
    style,
    onBlur: onBlur || (() => { }),
    thousandSeparator: thousandSeparator === undefined ? true : thousandSeparator,
    decimalScale: parseInt(decimalScale, 10) || 2,
    mask: mask || ""
  }

  if (format) {
    delete props.thousandSeparator
    props.format = format
  }
  return (
    <div className={className || `${label ? "column" : ""}`}>
      {label && (
        <p className="mb-1 multiExpLblBlk">
          <b>{label}</b>
          {required ? <small> <i className="fa fa-asterisk text-danger" /></small> : null}
        </p>
      )}
      <NumberFormat
        className="form-control"
        {...props}
      />
      {error && (
        <small className="text-danger">
          {error}
        </small>
      )}
    </div>
  )
}

export const SelectLabelInput = ({ list = [], name, placeholder, className, label, title, value, onChange, error, onBlur, disabled, inputStyle, required, disableValue, colXs, colMd, colXl }) => {
  const props = {
    name,
    value,
    title: title || label || "",
    onChange: e => onChange(e),
    disabled: disabled || false,
    onBlur: onBlur || (() => { })
  }
  disableValue =
    (disableValue && Array.isArray(disableValue) && disableValue) || []

  return (
    <Col className={className || ""} xs={colXs || "12"} md={colMd || "6"} xl={colXl || "6"}>
      {label && (
        <p className="mb-1 multiExpLblBlk">
          <b>{label}</b>
          {required ? <small> <i className="fa fa-asterisk text-danger" /></small> : null}
        </p>
      )}
      <div>
        <div
          className=""
          style={inputStyle || { width: "100%" }}
        >
          <select className="form-control" style={inputStyle || { width: "100%" }} {...props}>
            <option value="" disabled>{placeholder || "Pick"}</option>
            {
              list.map((op, i) => (
                <option key={i} value={op.value} disabled={disableValue.indexOf(op.value) !== -1}>
                  {op.name}
                </option>
              ))
            }
          </select>
        </div>
        {error && (
          <small className="text-danger">
            {error}
          </small>
        )}
      </div>
    </Col>
  )
}

export const SelectLabelInputSecond = ({ list = [], name, placeholder, className, label, title, value, onChange, error, onBlur, disabled, inputStyle, required, disableValue, colXs, colMd, colXl }) => {
  const props = {
    name,
    value,
    title: title || label || "",
    onChange: e => onChange(e),
    disabled: disabled || false,
    onBlur: onBlur || (() => { })
  }
  disableValue =
    (disableValue && Array.isArray(disableValue) && disableValue) || []
  
  return (
    <div className={className || ""}>
      {label && (
        <p className="mb-1 multiExpLblBlk">
          <b>{label}</b>
          {required ? <small> <i className="fa fa-asterisk text-danger" /></small> : null}
        </p>
      )}
      <div>
        <div
          className=""
          style={inputStyle || { width: "100%" }}
        >
          <select className="form-control" style={inputStyle || { width: "100%" }} {...props}>
            <option value="" disabled>{placeholder || "Pick"}</option>
            {
              list.map((op, i) => (
                <option key={i} value={op.value} disabled={disableValue.indexOf(op.value) !== -1}>
                  {op.name}
                </option>
              ))
            }
          </select>
        </div>
        {error && (
          <small className="text-danger">
            {error}
          </small>
        )}
      </div>
    </div>
  )
}

export const DateInput = ({
  type,
  name,
  style,
  label,
  value,
  className,
  title,
  onChange,
  placeholder,
  error,
  min,
  max,
  onBlur,
  disabled,
  required
}) => {
  const props = {
    name,
    value,
    style,
    title: title || label || "",
    placeholder: type === "date" ? "MM-DD-YYYY" : placeholder || "",
    onChange,
    type: type || "text",
    disabled: disabled || false,
    onBlur: onBlur || (() => { })
  }

  if (min) {
    props.minDate = dateFormatted(min)
  }
  if (max) {
    props.maxDate = dateFormatted(max)
  }

  return (
    <div
      className={className || `${label ? "column" : "w-100"}`}
      style={style || { minWidth: 100 }}
    >
      {label && (
        <p className="multiExpLblBlk">
          <b>{label}</b>
          {required ? <small> <i className="fa fa-asterisk text-danger" /></small> : null}
        </p>
      )}
      <DatePicker
        selected={value || ""}
        {...props}
        showYearDropdown
        showMonthDropdown
        onChange={(val) => {
          onChange({target: {name, value: val ? dateFormatted(val) : null}})
        }}
        className="form-control"
      />
      {error && (
        <p
          className="is-small text-error has-text-danger "
          style={{ fontSize: 12 }}
        >
          {error}
        </p>
      )}
    </div>
  )
}
