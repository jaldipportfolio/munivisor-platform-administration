import React, { Component } from "react"
import {Button, Col, FormGroup, Input, InputGroup, InputGroupAddon, Row} from "reactstrap"
import ReactTable from "react-table"
import { Link } from "react-router-dom"
import Accordion from "./Accordion"
import { getTenantUserList } from "../Services/actions/Tenant"
import "react-table/react-table.css"
import Loader from "./Loader"

const highlightSearch = (string, searchQuery) => {
  const reg = new RegExp(
    searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&"),
    "i"
  )
  return string.replace(reg, str => `<mark>${str}</mark>`)
}

class UserList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      usersList: [],
      firmId: "",
      usersSearch: "",
      page: 0,
      total: 0,
      pages: 0,
      loading: true
    }
  }

  async componentWillMount() {
    const { firmId } = this.props
    this.setState({
      firmId,
      loading: false
    })
  }

    decoupleResult = result => ({
      total: result && result.metadata.length > 0 ? result.metadata[0].total : 0,
      pages: result && result.metadata.length > 0 ? result.metadata[0].pages : 0,
      data: result ? result.data : []
    })

    getFilteredData = async (pageDetails) => {
      this.setState({ loading: true, page: {global: pageDetails}  })
      const { firmId, usersSearch } = this.state
      const { page, pageSize, sorted } = pageDetails
      let sortedFields = {}
      if (sorted && sorted[0]) {
        sortedFields[sorted[0].id] = sorted[0].desc ? -1 : 1
        this.setState({ sortedFields })
      } else {
        sortedFields = this.state.sortedFields
        if (sortedFields === undefined) {
          sortedFields = { entityName: 1 }
        }
      }
      const payload = {
        filters: {
          userContactTypes:[],
          entityMarketRoleFlags:[],
          entityIssuerFlags:[],
          userPrimaryAddressStates:[],
          freeTextSearchTerm: usersSearch || "",
          entityId: firmId || "",
          activeFlag:"active",
          entityTypes:[
            "Client",
            "Prospect",
            "Third Party"
          ]
        },
        pagination:{
          serverPerformPagination:true,
          currentPage: page,
          size: pageSize,
          sortFields: sortedFields
        }
      }

      const result = await getTenantUserList(payload)

      const data = await result.data
      if (data) {
        const decoupledValue = this.decoupleResult(data && data.pipeLineQueryResults && data.pipeLineQueryResults[0])
        this.setState({
          loading: false,
          usersList: decoupledValue.data || [],
          total: decoupledValue.total,
          pages: decoupledValue.pages,
        })
      }
    }

    onEntitySearch = (e) => {
      const { name, value } = e.target
      const { page } = this.state
      this.setState({
        [name]: value
      }, () => this.getFilteredData(page.global))
    }

    render() {
      const { usersList, firmId, pages, usersSearch, loading, total } = this.state
      return (
        <div className="animated fadeIn">
          { loading ? <Loader/> : null }
          <Row>
            <Col xs="12" sm="12">
              <Accordion
                activeAccordionHeader="Users List"
                count={total || 0}
                showCount
              >
                <div>

                  <FormGroup row className="my-0">
                    <Col xs="6" md="6">
                      <div className="row justify-content-sm-start" style={{marginLeft: 0}}>
                        <Link to={`/new/user?firmId=${firmId}`}
                        ><Button color="primary"> Add New Contact</Button></Link>
                      </div>
                    </Col>
                    <Col xs="6" md="6">
                      <div className="controls">
                        <InputGroup>
                          <Input type="text" name="usersSearch" onChange={this.onEntitySearch} placeholder="Search..." />
                          <InputGroupAddon addonType="append">
                            <Button color="secondary"><i className="fa fa-search"/></Button>
                          </InputGroupAddon>
                        </InputGroup>
                      </div>
                    </Col>
                  </FormGroup>

                  <br/>

                  <ReactTable
                    columns={[
                      {
                        Header: "Full Name",
                        id: "userFullName",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        minWidth: 100,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="hpTablesTd -striped">
                              <div
                                dangerouslySetInnerHTML={{
                                  __html: highlightSearch((`${item.userFirstName} ${item.userLastName}`) || "-", usersSearch)
                                }}
                              />
                            </div>
                          )
                        },
                        sortMethod: (a, b) => `${a.userFullName}` - `${b.userFullName}`
                      },
                      {
                        Header: "Associated Entity",
                        id: "entityName",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        minWidth: 150,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="hpTablesTd wrap-cell-text">
                              <div
                                dangerouslySetInnerHTML={{
                                  __html: highlightSearch((item && item.entityName && item.entityName.toString()) || "-", usersSearch)
                                }}
                              />
                            </div>
                          )
                        },
                        sortMethod: (a, b) => a.entityName !== undefined && b.entityName !== undefined ? a.entityName.localeCompare(b.entityName) : ""
                      },
                      {
                        Header: "Primary Email",
                        id: "userPrimaryEmail",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        minWidth: 150,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="hpTablesTd wrap-cell-text">
                              <div
                                dangerouslySetInnerHTML={{
                                  __html: highlightSearch((item && item.userPrimaryEmail && item.userPrimaryEmail.toString()) || "-", usersSearch)
                                }}
                              />
                            </div>
                          )
                        },
                        sortMethod: (a, b) => `${a.userPrimaryEmail}` - `${b.userPrimaryEmail}`
                      },
                      {
                        Header: "Primary Phone",
                        id: "userPrimaryPhoneNumber",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        minWidth: 100,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="hpTablesTd wrap-cell-text">
                              <div
                                dangerouslySetInnerHTML={{
                                  __html: highlightSearch((item && item.userPrimaryPhoneNumber && item.userPrimaryPhoneNumber.toString()) || "-", usersSearch)
                                }}
                              />
                            </div>
                          )
                        },

                        sortMethod: (a, b) => `${a.userPrimaryPhoneNumber}` - `${b.userPrimaryPhoneNumber}`
                      },
                      {
                        Header: "Primary Address",
                        id: "entityPrimaryAddressGoogle",
                        className: "entityPrimaryAddressGoogle",
                        accessor: item => item,
                        minWidth: 100,
                        Cell: row => {
                          const item = row.value
                          const url = (item && item.userPrimaryAddressGoogle) ? `https://maps.google.com/?q=${item && item.userPrimaryAddressGoogle}` : ""
                          return (
                            <div className="hpTablesTd wrap-cell-text">
                              {
                                url ?
                                  <a
                                    href={url || ""} target="_blank" rel="noopener noreferrer"
                                    dangerouslySetInnerHTML={{
                                      __html: highlightSearch(`${item && item.userPrimaryAddressGoogle}` || "-", usersSearch)
                                    }}
                                  /> :
                                  <span>-</span>
                              }
                            </div>
                          )
                        },
                        sortMethod: (a, b) => `${a.userPrimaryAddressGoogle}` - `${b.userPrimaryAddressGoogle}`
                      }
                    ]}
                    data={usersList || []}
                    manual
                    showPaginationBottom
                    defaultPageSize={10}
                    pageSizeOptions={[5, 10, 20, 50, 100]}
                    className="-striped -highlight is-bordered"
                    style={{ overflowX: "auto" }}
                    pages={pages}
                    showPageJump
                    minRows={2}
                    onFetchData={state => {
                      this.getFilteredData(state)
                    }}
                  />
                </div>
              </Accordion>
            </Col>
          </Row>
        </div>
      )
    }
}

export default UserList
