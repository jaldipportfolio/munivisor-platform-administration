import React, {Component} from "react"
import {
  CardElement,
  CardNumberElement,
  CardExpiryElement,
  CardCVCElement,
  StripeProvider,
  Elements,
  injectStripe
} from "react-stripe-elements"
import "./CheckoutForm.scss"
import {Col, Row} from "reactstrap"
import axios from "axios"
import {toast} from "react-toastify"
import {getToken, muniApiBaseURL, PLATFORM_STRIPE_CLIENT_PUBLIC_KEY} from "../globalutilities/consts"
import Loader from "../GlobalComponents/Loader"


const createOptions = (fontSize, padding) => {
  return {
    style: {
      base: {
        fontSize,
        color: "#32325D",
        fontWeight: 500,
        fontFamily: "Inter UI, Open Sans, Segoe UI, sans-serif",
        fontSmoothing: "antialiased",
        "::placeholder": {
          color: "#CFD7DF"
        },
        padding
      },
      invalid: {
        color: "#E25950"
      }
    }
  }
}

const inputStyle = (fontSize) => {
  return {
    fontSize,
    color: "#32325D",
    fontWeight: 500,
    fontFamily: "Inter UI, Open Sans, Segoe UI, sans-serif",
    fontSmoothing: "antialiased",
    width: "100%",
    "::placeholder": {
      color: "#CFD7DF",
      opacity: "0.6"
    }
  }
}

class _CardForm extends React.Component {
  constructor() {
    super()
    this.state = {
      error: "",
      loading: false
    }
  }

    handleError = (error) => {
      this.setState({error, loading: false})
    }

    handleSubmit = (ev) => {
      ev.preventDefault()
      const  {name, email, amount} = ev.target
      this.setState({
        loading: true
      }, () => {
        if (this.props.stripe) {
          this.props.stripe.createToken().then(async (payload) => {
            console.log("[token]", payload)
            if(payload && payload.error) {
              this.handleError(payload.error.message || "Something went wrong!")
              return toast.error(payload.error.message || "Something went wrong!", {
                position: toast.POSITION.TOP_RIGHT
              })
            }
            const response = await axios.post(
              `${muniApiBaseURL}payment/charge`,
              {
                metadata: {
                  name: name.value,
                  email: email.value,
                },
                receipt_email: email.value,
                amount: amount.value,
                ...payload
              },
              { headers: { Authorization: getToken() } }
            )

            if (response && response.data && response.data.done){
              this.setState({error: "", loading: false})
              toast.success("Payment successfully.", {
                position: toast.POSITION.TOP_RIGHT
              })
            } else{
              this.handleError((response && response.data && response.data.message) || "Something went wrong!")
              toast.error((response && response.data && response.data.message) || "Something went wrong!", {
                position: toast.POSITION.TOP_RIGHT
              })
            }
          })
        } else {
          console.log("Stripe.js hasn't loaded yet.")
        }
      })
    }

    handleBlur = () => {
      console.log("[blur]")
    }

    handleChange = (change) => {
      this.setState({
        error: ""
      })
    }

    handleClick = () => {
      console.log("[click]")
    }

    handleFocus = () => {
      console.log("[focus]")
    }

    handleReady = () => {
      console.log("[ready]")
    }

    render() {
      const { error, loading } = this.state
      return (
        <form onSubmit={this.handleSubmit}>
          { loading ? <Loader/> : null }
          <Row>
            <Col xs="12" sm="6">
              <label>Name
                <input
                  name="name"
                  type="text"
                  placeholder="Jane Doe"
                  style={inputStyle(this.props.fontSize)}
                  required
                />
              </label>
            </Col>
            <Col xs="12" sm="6">
              <label>Email
                <input
                  name="email"
                  type="email"
                  placeholder="jane.doe@example.com"
                  style={inputStyle(this.props.fontSize)}
                  required
                />
              </label>
            </Col>
            <Col xs="12" sm="12">
              <label>Amount
                <input
                  name="amount"
                  type="number"
                  placeholder="$10"
                  style={inputStyle(this.props.fontSize)}
                  required
                />
              </label>
            </Col>
          </Row>
          <label>Card details
            <CardElement
              onBlur={this.handleBlur}
              onChange={this.handleChange}
              onFocus={this.handleFocus}
              onReady={this.handleReady}
              {...createOptions(this.props.fontSize)}
            />
          </label>
          {error ? <p className="error message">{error}</p> : null}
          <button>Make payment</button>
        </form>
      )
    }
}
const CardForm = injectStripe(_CardForm)

class _SplitForm extends React.Component {
  constructor() {
    super()
    this.state = {
      error: "",
      loading: false
    }
  }

    handleError = (error) => {
      this.setState({error, loading: false})
    }

    handleSubmit = (ev) => {
      ev.preventDefault()
      const  {name, email, amount} = ev.target
      this.setState({
        loading: true
      }, () => {
        if (this.props.stripe) {
          this.props.stripe.createToken().then(async (payload) => {
            console.log("[token]", payload)
            if(payload && payload.error) {
              this.handleError(payload.error.message || "Something went wrong!")
              return toast.error(payload.error.message || "Something went wrong!", {
                position: toast.POSITION.TOP_RIGHT
              })
            }
            const response = await axios.post(
              `${muniApiBaseURL}payment/charge`,
              {
                metadata: {
                  name: name.value,
                  email: email.value,
                },
                receipt_email: email.value,
                amount: amount.value,
                ...payload
              },
              { headers: { Authorization: getToken() } }
            )

            if (response && response.data && response.data.done){
              this.setState({error: "", loading: false})
              toast.success("Payment successfully.", {
                position: toast.POSITION.TOP_RIGHT
              })
            } else{
              this.handleError((response && response.data && response.data.message) || "Something went wrong!")
              toast.error((response && response.data && response.data.message) || "Something went wrong!", {
                position: toast.POSITION.TOP_RIGHT
              })
            }
          })
        } else {
          console.log("Stripe.js hasn't loaded yet.")
        }
      })
    }

    submit = (event) => {
      console.log(event)
    }

    handleBlur = () => {
      console.log("[blur]")
    }

    handleChange = (change) => {
      this.setState({
        error: ""
      })
    }

    handleClick = () => {
      console.log("[click]")
    }

    handleFocus = () => {
      console.log("[focus]")
    }

    handleReady = () => {
      console.log("[ready]")
    }

    render() {
      const { error, loading } = this.state
      return (
        <form onSubmit={this.handleSubmit}>
          { loading ? <Loader/> : null }
          <Row>
            <Col xs="12" sm="6">
              <label>Name
                <input
                  name="name"
                  type="text"
                  placeholder="Jane Doe"
                  style={inputStyle(this.props.fontSize)}
                  required
                />
              </label>
            </Col>
            <Col xs="12" sm="6">
              <label>Email
                <input
                  name="email"
                  type="email"
                  placeholder="jane.doe@example.com"
                  style={inputStyle(this.props.fontSize)}
                  required
                />
              </label>
            </Col>
            <Col xs="12" sm="12">
              <label>Amount
                <input
                  name="amount"
                  type="number"
                  placeholder="$10"
                  style={inputStyle(this.props.fontSize)}
                  required
                />
              </label>
            </Col>
          </Row>
          <Row>
            <Col xs="12" sm="12">
              <label>Card number
                <CardNumberElement
                  onBlur={this.handleBlur}
                  onChange={this.handleChange}
                  onFocus={this.handleFocus}
                  onReady={this.handleReady}
                  {...createOptions(this.props.fontSize)}
                />
              </label>
            </Col>
          </Row>
          <Row>
            <Col xs="12" sm="12">
              <label>Expiration date
                <CardExpiryElement
                  onBlur={this.handleBlur}
                  onChange={this.handleChange}
                  onFocus={this.handleFocus}
                  onReady={this.handleReady}
                  {...createOptions(this.props.fontSize)}
                />
              </label>
            </Col>
          </Row>
          <Row>
            <Col xs="12" sm="12">
              <label>CVC
                <CardCVCElement
                  onBlur={this.handleBlur}
                  onChange={this.handleChange}
                  onFocus={this.handleFocus}
                  onReady={this.handleReady}
                  {...createOptions(this.props.fontSize)}
                />
              </label>
            </Col>
          </Row>
          {error ? <p className="error message">{error}</p> : null}
          <button>Make payment</button>
        </form>
      )
    }
}
// const SplitForm = injectStripe(_SplitForm)

class CheckoutFormContainer extends Component {
  constructor() {
    super()
    this.state = {
      elementFontSize: window.innerWidth < 450 ? "14px" : "16px",
      loading: false
    }
    window.addEventListener("resize", () => {
      if (window.innerWidth < 450 && this.state.elementFontSize !== "14px") {
        this.setState({elementFontSize: "14px"})
      } else if (
        window.innerWidth >= 450 &&
                this.state.elementFontSize !== "16px"
      ) {
        this.setState({elementFontSize: "16px"})
      }
    })
  }

  render() {
    const {elementFontSize} = this.state
    return (
      <StripeProvider apiKey={PLATFORM_STRIPE_CLIENT_PUBLIC_KEY}>
        <div className="example payment">
          <h1 className="text-center">Make Payment</h1>
          <Elements>
            <Row>
              <Col xs="12" sm="12" xl="4" md="12" />
              <Col xs="12" sm="12" xl="4" md="12" style={{backgroundColor: "#f6f9fc", border: "2px solid #9dbbf9", padding: 15, borderRadius: 10}}>
                <CardForm fontSize={elementFontSize} />
              </Col>
              <Col className="text-center" xs="12" sm="12" xl="4" md="12"/>
            </Row>
          </Elements>
          <br/>
          {/* <Elements>
            <Row>
              <Col xs="12" sm="12" xl="4" md="12" />
              <Col xs="12" sm="12" xl="4" md="12" style={{backgroundColor: "#f6f9fc", border: "2px solid #9dbbf9", padding: 15, borderRadius: 10}}>
                <SplitForm fontSize={elementFontSize} />
              </Col>
              <Col xs="12" sm="12" xl="4" md="12" />
            </Row>
          </Elements> */}
        </div>
      </StripeProvider>

    )
  }
}

export default CheckoutFormContainer
