import React, { Component } from "react"

class SectionAccordion extends Component {

  constructor(props) {
    super(props)
    this.state = {
      activeAccordions: props.activeItem || [],
      onAccordion: (index) => {
        let { activeAccordions } = this.state
        const { multiple } = this.props
        if (multiple) {
          if (this.state.activeAccordions.includes(index)) {
            activeAccordions = activeAccordions.filter(x => x !== index)
          } else {
            activeAccordions.push(index)
          }
        } else {
          activeAccordions = [index]
        }
        this.setState({
          activeAccordions,
        })
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    // if(nextProps.activeItem.length !== this.props.activeItem.length) {
    if (nextProps.activeItem.length && nextProps.isRequired) {
      this.state = {
        activeAccordions: nextProps.activeItem || [],
        onAccordion: (index) => {
          let { activeAccordions } = this.state
          const { multiple } = this.props
          if (multiple) {
            if (this.state.activeAccordions.includes(index)) {
              activeAccordions = activeAccordions.filter(x => x !== index)
            } else {
              activeAccordions.push(index)
            }
          } else {
            activeAccordions = [index]
          }
          this.setState({
            activeAccordions,
          })
        }
      }
    }
  }

  render() {
    return (
      <section className={`accordions ${!this.props.boxHidden ? "box" : ""}`}>
        {this.props.render(this.state)}
      </section>
    )
  }
}

export default SectionAccordion
