import React from "react"
import PropTypes from "prop-types"
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap"

export const StrapModal = ({ children, closeModal, size, modalState, title, saveModal, disabled, styleBody, buttonName, className }) => {
  return (
    <Modal isOpen={modalState} toggle={closeModal}
      className={`${size || ""} ${  className}`}>
      <ModalHeader toggle={closeModal}>{title}</ModalHeader>
      <ModalBody style={styleBody}>
        {children}
      </ModalBody>
      <ModalFooter>
        {saveModal ? <Button color="primary" onClick={saveModal} disabled={disabled}>{buttonName || "Save"}</Button> : null}
        {" "}
        <Button color="secondary" onClick={closeModal}>Cancel</Button>
      </ModalFooter>
    </Modal>
  )
}

StrapModal.propTypes = {
  closeModal: PropTypes.func.isRequired,
  modalState: PropTypes.bool.isRequired,
}
