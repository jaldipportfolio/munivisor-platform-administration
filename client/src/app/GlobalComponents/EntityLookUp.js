import React, { Component } from "react"
import { DropdownList } from "react-widgets"
import "react-widgets/dist/css/react-widgets.css"
import { getEntityLookUpData } from "../Services/actions/Dashboard"

class EntityLookUp extends Component {
  constructor(props) {
    super(props)
    this.state = {
      entityName: "",
      waiting: false,
      entities: [],
      searchString: ""
    }
  }

  componentDidMount() {
    const { entityName } = this.props
    this.setState({ entityName })
  }

  componentWillReceiveProps(nextProps) {
    const { entityName, participantType } = nextProps
    if(entityName !== this.props.entityName) {
      this.setState({ entityName })
    }
    if(participantType !== this.props.participantType) {
      this.setState({ entities: [] }, this.performSearch)
    }
  }

    onSearch = (searchString) => {
      window.clearTimeout(this.timeout)
      this.setState({
        entities: [], waiting: true, searchString
      }, this.performSearch)
    }

    onCreate = (value) => {
      const { entities } = this.state
      const { participantType } = this.props
      console.log("create value : ", value)
      const entityName = { _id: "", firmName: value, isExisting: false, participantType }
      const isValid = entities.map(i => i.firmName).includes(value)
      if(!isValid){
        this.setState({ entityName })
        this.props.onChange(entityName)
      }
    }

    onChange = (value) => {
      console.log("change value : ", value)
    }

    getEntityData = async () => {
      const { searchString } = this.state
      if(!searchString) {
        return { waiting: false, entities: [] }
      }
      const { entityType, participantType } = this.props
      const entities = await getEntityLookUpData({entityType,
        participantType, searchString })
      entities.sort((x, y) => {
        if(x.isExisting && !y.isExisting) {
          return -1
        } if(y.isExisting && !x.isExisting) {
          return 1
        }
        return (x-y)
      })
      console.log("entities : ", entities)
      this.setState({ entities, waiting: false })
    }

    performSearch = () => {
      const { searchString } = this.state
      if(searchString) {
        this.setState({ entityName: "" })
        this.timeout = window.setTimeout(this.getEntityData, 500)
      } else {
        window.clearTimeout(this.timeout)
        this.setState({ waiting: false, entities: [] })
      }
    }

    toggleOpen = (value) => {
      console.log("toggle value : ", value)
      if(!value) {
        const { entityName } = this.props
        if(entityName && !this.state.entityName) {
          this.setState( { entityName })
        }
      }
    }

    selectEntity = (value) => {
      console.log("select value : ", value)
      this.setState({ entityName: Object.keys(value || {}).length ? value.firmName : ""})
      this.props.onChange(Object.keys(value || {}).length ? value : "")
    }

    render() {
      const { entityType, isRequired, participantType, viewOnly, error } = this.props
      const { waiting, entityName, entities } = this.state
      let disabled = entities.filter(e => e.isExisting)
      if(viewOnly || (!participantType && (entityType !== "Third Party"))) {
        disabled = true
      }
      return (
        <div className="columns">
          <div className="column is-11">
            <DropdownList
              busy={waiting}
              value={entityName}
              data={entities}
              allowCreate="onFilter"
              textField="firmName"
              placeholder="Search by name"
              disabled={ disabled }
              groupBy={value => (value.isExisting ? "Existing" : "Available for selection")}
              filter="contains"
              onSearch={this.onSearch}
              onCreate={this.onCreate}
              onChange={this.onChange}
              onSelect={this.selectEntity}
              onToggle={this.toggleOpen}
            />
            {error && <span className="has-text-danger">{error}</span>}
          </div>
          {isRequired && <span className="icon has-text-danger column"><i className="fas fa-asterisk extra-small-icon" /></span>}
        </div>
      )
    }
}

export default EntityLookUp