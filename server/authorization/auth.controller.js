// import bcrypt from "bcrypt-nodejs"
import jwt from "jsonwebtoken"
import configkeys from "../config"
import {
  getUserDetails
} from "../commonDbFunctions"

// get the expiration interval in seconds
const expirationInterval =
  process.env.PLATFORM_NODE_ENV === "development"
    ? 30 * 24 * 60 * 60
    : (parseInt(process.env.PLATFORM_JWTSECRET) || 1) * 24 * 60 * 60

const tokenForUser = (user, loginDetails) => {
  try {
    const timestamp = new Date().getTime()
    return jwt.sign(
      {
        sub: user.userLoginCredentials.userEmailId,
        iat: timestamp,
        // entityDetails: loginDetails.relatedFaEntities[0],
        exp: Math.floor(Date.now() / 1000) + expirationInterval
      },
      configkeys.secrets.JWT_SECRET
    )
  } catch (err) {
    throw err
  }
}

export const signin = async (req, res) => {
  const userEmail = req.user.userLoginCredentials.userEmailId
  console.log("user email", userEmail)
  try {
    const [
      userDetails
    ] = await Promise.all([
      getUserDetails(userEmail),
    ])

    if (Object.keys(userDetails).length > 0) {
      res.send({
        authenticated: true,
        token: tokenForUser(req.user),
        loginDetails: userDetails,
        error: ""
      })
    } else {
      res.status(422).send({
        authenticated: false,
        token: "",
        loginDetails: {},
        error: `Incorrect email ID : ${userEmail}`
      })
    }
  } catch (e) {
    console.log("The error while sign in is", e)
    res.status(422).send({
      authenticated: false,
      token: "",
      loginDetails: {},
      error: `Unable to Login using email - ${userEmail}`
    })
  }
}

export const signup = async (req, res) => {
}

export const checkAuth = async (req, res) => {
  const userEmail = req.user.userLoginCredentials.userEmailId
  const token = req.headers.authorization
  const decoded = await jwt.verify(token, configkeys.secrets.JWT_SECRET)
  console.log("DECODED TOKEN", JSON.stringify(decoded, null, 2))
  // console.log("headers : ", JSON.stringify(req.headers, null, 2))
  if (userEmail) {
    try {
      const userDetails = await getUserDetails(userEmail)

      if (Object.keys(userDetails).length > 0) {
        res.send({
          authenticated: true,
          token,
          exp: (decoded && decoded.exp) || 0,
          loginDetails: userDetails,
          error: ""
        })
      } else {
        res.send({
          authenticated: false,
          token: "",
          loginDetails: {},
          exp: (decoded && decoded.exp) || 0,
          error: { message: `Incorrect email ID : ${userEmail}` }
        })
      }
    } catch (e) {
      res.send({
        authenticated: false,
        token: "",
        loginDetails: {},
        exp: (decoded && decoded.exp) || 0,
        error: { message: `Unable to Login using email - ${userEmail}` }
      })
    }
  } else {
    res.send({
      authenticated: false,
      token: "",
      loginDetails: {},
      exp: (decoded && decoded.exp) || 0,
      error: { message: `Email ID doesn't exist - ${userEmail}` }
    })
  }
}

export const signUpConfirm = async (req, res) => {
}

export const forgotPassword = async (req, res) => {
}


