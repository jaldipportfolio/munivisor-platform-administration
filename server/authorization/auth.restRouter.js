import express from "express"
import {
  signin,
  signup,
  // signupConfirm,
  // forgotPassword,
  // passwordResetConfirm,
  // changePassword,
  checkAuth
} from "./auth.controller"
import {requireAuth, requireSignIn} from "./auth.middleware"

export const authRouter = express.Router()

authRouter.get("/", requireAuth, (req, res) => {
  res.send({message: "You are accessing a protected route"})
})

authRouter.post("/signin", requireSignIn, signin)
authRouter.post("/signup", signup)
authRouter.get("/checkauth", requireAuth, checkAuth)
// authRouter.get("/signupconfirm", signupConfirm)
// authRouter.post("/forgotpassword", forgotPassword)
// authRouter.post("/changepassword", changePassword)
// authRouter.get("/resetpassconfirm", passwordResetConfirm)
