require("dotenv").config()

const config = {
  expireTime: process.env.JWTEXPIRE,
  secrets: {
    JWT_SECRET: process.env.PLATFORM_JWTSECRET
  },
  db: {
    url: process.env.PLATFORM_DB_URL
  },
  port: process.env.PLATFORM_PORT,
  keys: {
    s3: {
      accessKeyId: process.env.PLATFORM_S3ACCESSKEYID,
      secretAccessKey: process.env.PLATFORM_S3SECRETKEY,
      bucketName: process.env.PLATFORM_S3BUCKET,
      region: process.env.PLATFORM_SESREGION
    },
    ses: {
      bucketName: process.env.PLATFORM_S3BUCKET,
      sesRegion: process.env.PLATFORM_SESREGION,
      accessKeyId: process.env.PLATFORM_SESACCESSKEYID,
      secretAccessKey: process.env.PLATFORM_SESSECRETKEY
    }
  },
}

export default config
