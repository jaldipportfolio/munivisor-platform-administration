import express from "express"
import {
  createTenant,
  createUser
} from "./create.controller"
import tenantConfigController from "../tenantConfig/tenantconfig.controller";

export const createRouter = express.Router()

createRouter.route("/tenant")
  .post(createTenant)

createRouter.route("/user")
  .post(createUser)

createRouter.route("/createtenantconfig")
  .post(tenantConfigController.createOne)