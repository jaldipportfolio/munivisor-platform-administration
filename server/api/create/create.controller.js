import axios from "axios"
import { services } from "../AllServices"
import { getUserActiveApplication } from "../common/common.controller"

const serviceFrom = "create"

export const createTenant = async (req, res) => {
  try {
    const {user, body} = req
    const app = await getUserActiveApplication(user)
    console.log(app)
    const apiRes = await axios.post(`${app.url}${services[serviceFrom].createTenant}`, body, { headers: {Authorization : app.token} })
    res.send(apiRes.data)
  } catch (err) {
    // console.log("Error", err)
    res.status(422).send({error: "Error in create tenant"})
  }
}

export const createUser = async (req, res) => {
  try {
    const {user, body} = req
    const app = await getUserActiveApplication(user)
    console.log(app)
    console.log({user})
    body.userDetails.loginUserFirstName = user && user.userFirstName || ""
    body.userDetails.loginUserLastName = user && user.userLastName || ""
    body.userDetails.emailofUserCreatingNewUser = user && user.userEmailId || ""
    console.log({body})
    const apiRes = await axios.post(`${app.url}${services[serviceFrom].createUsers}`, body, { headers: {Authorization : app.token} })
    res.send(apiRes.data)
  } catch (err) {
    res.status(422).send({error: "Error in create user"})
  }
}
