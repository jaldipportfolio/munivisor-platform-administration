import mongoose, {Schema} from "mongoose"
import timestamps from "mongoose-timestamp"

const transactionsSchema = Schema({
  invoiceNumber: String,
  appId: Schema.ObjectId,
  customerId: String,
  paymentId: String,
  invoiceId: Schema.ObjectId,
  payment_method: String,
  payment_method_details: Object,
  receipt_url: String,
  balance_transaction: String,
  status: String,
  tenantId: Schema.ObjectId,
  object: String,
  metadata: Object,
}).plugin(timestamps)

export const Transactions =  mongoose.model("transactions", transactionsSchema)
