import mongoose, {Schema} from "mongoose"
import timestamps from "mongoose-timestamp"

const invoiceLogs = Schema({
  invoiceId: Schema.ObjectId,
  logMeta: Object,
  tenantId: Schema.ObjectId,
  type: String,
  appId: Schema.ObjectId,
}).plugin(timestamps)

export const InvoiceLogs =  mongoose.model("invoicelogs", invoiceLogs)