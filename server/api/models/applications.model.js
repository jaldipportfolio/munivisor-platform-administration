import mongoose, {Schema} from "mongoose"
import timestamps from "mongoose-timestamp"

const applications = Schema({
  name: String,
  url: String,
  token: String,
}).plugin(timestamps)

export const Applications =  mongoose.model("applications", applications)