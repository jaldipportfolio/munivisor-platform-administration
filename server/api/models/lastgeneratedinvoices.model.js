import mongoose, {Schema} from "mongoose"
import timestamps from "mongoose-timestamp"

const lastGeneratedInvoices = Schema({
  invoiceId: Schema.ObjectId,
  lastInvoiceCreatedAt: Date,
  tenantId: Schema.ObjectId,
  appId: Schema.ObjectId,
}).plugin(timestamps)

export const LastGeneratedInvoices =  mongoose.model("lastgeneratedinvoices", lastGeneratedInvoices)