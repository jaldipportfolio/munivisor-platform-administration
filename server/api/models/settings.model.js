import mongoose, {Schema} from "mongoose"
import timestamps from "mongoose-timestamp"

const settings = Schema({
  billing: Object,
  notifications: Object,
  auditTrail: Object
}).plugin(timestamps)

export const Settings =  mongoose.model("settings", settings)