import { generateControllers } from "../../modules/query"
import { Docs } from "./docs.model"

const postDocDetails = () => async (req, res) => {

  const { meta,...otherbody} = req.body
  let newVersionDetails =[]
  let newBody = {}
  if ( meta ) {
    const {versions,...othermeta} = meta
    newVersionDetails = (versions || []).map( v => ({
      ...v, 
      uploadedByUserId:req.user._id, 
      uploadedByUserTenantId:req.user.tenantId,
      uploadedByUserEntityId:req.user.entityId
    }))
  
    newBody = {...otherbody, meta:{...(othermeta || {}),versions:newVersionDetails}}
  } else {
    newBody = {...otherbody, meta:{} }
  }

  try {
    const newDocument = new Docs({
      ...newBody
    })
    const newDocumentAfterInsert = await newDocument.save()
    res.json(newDocumentAfterInsert)
  } catch (error) {
    res
      .status(500)
      .send({ done: false, error: "Error saving document details" })
  }
}

export default generateControllers(Docs,{createOne:postDocDetails()})

export const getAllDocumentUrlsForTenant = async (tenantId) => {
  const documentUrls = await Docs.aggregate([
    {
      $match:{
        tenantId
      }
    },
    {
      $project:{
        tenantId:1,
        contextType:1,
        contextId:1,
        versions:"$meta.versions"
      }
    },
    {
      $unwind:"$versions"
    },
    {
      $project:{
        tenantId:1,
        contextType:1,
        contextId:1,
        versionId:"$versions.versionId",
        fileName:"$versions.name",
        fileObjectPath:{ $concat: [ "$tenantId", "/", "$contextType","/","$contextType","_","$contextId","/","$versions.name" ] }
      }
    }
  ])
  return documentUrls
}
