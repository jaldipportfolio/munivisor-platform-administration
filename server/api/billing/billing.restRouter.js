import express from "express"
import {
  sendInvoiceToTenant
} from "./billing.controller"

export const billingRouter = express.Router()

billingRouter.route("/sendinvoice")
  .post(sendInvoiceToTenant)