import mongoose, {Schema} from "mongoose"
import timestamps from "mongoose-timestamp"

const invoiceDetails = Schema({
  appId: Schema.ObjectId,
  tenantId: Schema.ObjectId,
  invoiceDocId: Schema.ObjectId,
  meta: Object,
  snap: Object,
  name: String,
  invoiceId: String,
  tenantCreatedAt: Date,
  allFirmUserEmails: Array,
  series50: Number,
  nonSeries50: Number,
  discountApply: String,
  discountPercentage: String,
  status: String,
  totalCost: Number,
  monthlyCharge: Number,
  payableAmount: Number,
  chargPerseries50: Number,
  chargPerNonSeries50: Number,
  billFrequency: String,
  billNumberFrequency: Number,
  receipt_url: String,
  addresses: Array,
  typeOfBilling: String
}).plugin(timestamps)

export const InvoiceDetails =  mongoose.model("invoices", invoiceDetails)
