import axios from "axios"
import AWS from "aws-sdk"
import moment from "moment"
import { services } from "../AllServices"
import {
  Applications,
  Settings,
  InvoiceDetails,
  TenantConfig,
  LastGeneratedInvoices,
  InvoiceLogs,
  SnapShots
} from "../models"
import config from "../../config"
import {
  sendEmailHelper,
  sendAWSEmailWithAttachment,
} from "./helpers"

const {ObjectID} = require("mongodb")

const serviceFrom = "billing"

const s3BucketName = config.keys.s3.bucketName

export const s3 = new AWS.S3({
  region : config.keys.s3.region,
  bucket: s3BucketName,
  accessKeyId: config.keys.s3.accessKeyId,
  secretAccessKey: config.keys.s3.secretAccessKey
})

AWS.config.update({
  region: process.env.PLATFORM_SESREGION || "us-east-1",
  accessKeyId: process.env.PLATFORM_SESACCESSKEYID || "AKIAIMCK6CR7IJIR7DDA",
  secretAccessKey: process.env.PLATFORM_SESSECRETKEY || "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",
})

const diffMonths = (dt2, dt1) =>{
  console.log(dt2,dt1)
  let diff =(dt2.getTime() - dt1.getTime()) / 1000
  diff /= (60 * 60 * 24 * 7 * 4)
  return Math.abs(Math.round(diff))
}

const applyDiscountAndComputeTotalCost = (tenant, discountType, joinToCurMonthDiff, discountPeriod, totalCost, discount, percentageDiscount, freePeriod) => {
  tenant.totalCost = parseFloat(totalCost).toFixed(2)
  tenant._id =  new ObjectID()
  tenant.status = "Open"
  if(discountType === "monthlyDiscount"){
    totalCost = parseInt(joinToCurMonthDiff || 0, 10) <= parseInt(discountPeriod || 0, 10) ? totalCost - (totalCost*discount)/100 : totalCost
    tenant.discountApply = parseInt(joinToCurMonthDiff || 0, 10) <= parseInt(discountPeriod || 0, 10) ? "Monthly Discount" : ""
    tenant.discountPercentage = `${discount}%`
  }
  if(discountType === "percentageDiscount"){
    totalCost -= (totalCost*parseFloat(percentageDiscount || 0))/100
    tenant.discountApply = "Percentage Discount"
    tenant.discountPercentage = `${percentageDiscount}%`
  }
  if(discountType === "firstDiscountPeriod" && parseInt(joinToCurMonthDiff || 0, 10) <= parseInt(freePeriod || 0, 10)){
    totalCost = 0
    tenant.discountApply = "Free Period"
    tenant.discountPercentage = "100%"
  }
  tenant.payableAmount = parseFloat(totalCost).toFixed(2)
  return tenant
}

export const compunteBillingFromPlatform = async () => {
  try {
    const applications = await Applications.find({})
    const setting = await Settings.findOne({})
    let allInvoices = []
    const lsInvoiceLogs = []
    for (const i in applications) {
      const app = applications[i]
      console.log("====URL=====", `${app.url}${services[serviceFrom].getTenantUsersInfo}`)
      let apiRes = {}
      try{
        apiRes = await axios.get(`${app.url}${services[serviceFrom].getTenantUsersInfo}`, { headers: {Authorization : app.token} })
      }catch (e) {
        console.log("Application:", e.message)
        continue
      }
      const tenants = (apiRes && apiRes.data) || {}
      const tenantsSettings = await TenantConfig.find({tenantId: {$in: Object.keys(tenants || {})}}).select("tenantId billing")
      const lastGeneratedInvoiceList = await LastGeneratedInvoices.find({tenantId: {$in: Object.keys(tenants || {})}}).select("lastInvoiceCreatedAt invoiceId tenantId")

      // const dateOffset = 24*60*60*1000  //1 days
      const myDate = new Date()
      myDate.setDate(myDate.getDate() - 1)
      const tenantsSnap = await SnapShots.find({tenantId: {$in: Object.keys(tenants || {})}, date: { $gte: moment(myDate).startOf('day'), $lt: new Date() }}).sort({ _id: -1 })
      // console.log("Tenant Snap Here ...", JSON.stringify(tenantsSnap))

      Object.keys(tenants).forEach(key => {
        console.log("1. go to tour... find user")
        const tenant = tenants[key]

        const snap = tenantsSnap.find(snap => snap.tenantId.toString() === key.toString())
        const tenanatSetting = tenantsSettings.find(tenSet => tenSet.tenantId.toString() === key.toString())
        const lastInvoice = lastGeneratedInvoiceList.find(tenSet => tenSet.tenantId.toString() === key.toString())
        const totalSeries50User = (tenant.series50 && tenant.series50.length) || 0
        const totalNonSeries50User = (tenant.nonSeries50 && tenant.nonSeries50.length) || 0
        const joiningMonthToCurMonthDiff = diffMonths(new Date(tenant.createdAt),  new Date())
        const lastInvoiceMonthToCurMonthDiff = lastInvoice ? diffMonths(new Date(lastInvoice.lastInvoiceCreatedAt),  new Date()) : 0
        const {billFrequency, typeOfBilling, recurringDay, freePeriod, series50, nonSeries50, enterpriseRate, discountType, discountPeriod, discount, percentageDiscount, paymentEnable} = (tenanatSetting && tenanatSetting.billing)  || (setting && setting.billing) || {}
        // console.log(`Last invoice ${tenant.name} ==>`, lastInvoiceMonthToCurMonthDiff || 0)
        // console.log(`Joining Month To Current Month Diff ${tenant.name} ==>`, joiningMonthToCurMonthDiff || 0)

        if(paymentEnable){
          tenants[key].series50 = totalSeries50User
          tenants[key].nonSeries50 = totalNonSeries50User
          tenants[key].billFrequency = billFrequency
          tenants[key].billNumberFrequency = billFrequency === "monthly" ? 1 : billFrequency === "quarterly" ? 3 : billFrequency === "halfYearly" ? 6 : billFrequency === "yearly" ? 12 : 0
          tenants[key].typeOfBilling = typeOfBilling
          tenants[key].snap = snap || {}
          if(typeOfBilling === "enterprise"){
            if(
              (billFrequency === "monthly" && (parseInt(lastInvoiceMonthToCurMonthDiff || 0, 10) === 1 || (!lastInvoice && joiningMonthToCurMonthDiff === 1) || (!lastInvoice && joiningMonthToCurMonthDiff > 1)) || true) ||
              (billFrequency === "quarterly" && (parseInt(lastInvoiceMonthToCurMonthDiff || 0, 10) === 3 || (!lastInvoice && joiningMonthToCurMonthDiff === 3) || (!lastInvoice && joiningMonthToCurMonthDiff > 3)) || true) ||
              (billFrequency === "halfYearly" && (parseInt(lastInvoiceMonthToCurMonthDiff || 0, 10) === 6  || (!lastInvoice && joiningMonthToCurMonthDiff === 6) || (!lastInvoice && joiningMonthToCurMonthDiff > 6)) || true) ||
              (billFrequency === "yearly" && (parseInt(lastInvoiceMonthToCurMonthDiff || 0, 10) === 12  || (!lastInvoice && joiningMonthToCurMonthDiff === 12) || (!lastInvoice && joiningMonthToCurMonthDiff > 12)) || true)
            ) {
              tenants[key].monthlyCharge = parseFloat(enterpriseRate || 0)
              const totalCost = parseFloat(enterpriseRate || 0) * tenants[key].billNumberFrequency
              tenants[key] = applyDiscountAndComputeTotalCost(tenants[key], discountType, joiningMonthToCurMonthDiff, discountPeriod, totalCost, discount, percentageDiscount, freePeriod)
            }
          }else {
            tenants[key].monthlyCharge = (totalSeries50User * parseFloat(series50 || 0)) + (totalNonSeries50User * parseFloat(nonSeries50 || 0))
            const totalCost = ((totalSeries50User * parseFloat(series50 || 0)) + (totalNonSeries50User * parseFloat(nonSeries50 || 0))) * tenants[key].billNumberFrequency
            if(
              (billFrequency === "monthly" && (parseInt(lastInvoiceMonthToCurMonthDiff || 0, 10) === 1 || (!lastInvoice && joiningMonthToCurMonthDiff === 1) || (!lastInvoice && joiningMonthToCurMonthDiff > 1)) || true) ||
              (billFrequency === "quarterly" && (parseInt(lastInvoiceMonthToCurMonthDiff || 0, 10) === 3 || (!lastInvoice && joiningMonthToCurMonthDiff === 3) || (!lastInvoice && joiningMonthToCurMonthDiff > 3)) || true) ||
              (billFrequency === "halfYearly" && (parseInt(lastInvoiceMonthToCurMonthDiff || 0, 10) === 6  || (!lastInvoice && joiningMonthToCurMonthDiff === 6) || (!lastInvoice && joiningMonthToCurMonthDiff > 6)) || true) ||
              (billFrequency === "yearly" && (parseInt(lastInvoiceMonthToCurMonthDiff || 0, 10) === 12  || (!lastInvoice && joiningMonthToCurMonthDiff === 12) || (!lastInvoice && joiningMonthToCurMonthDiff > 12)) || true)
            ) {
              tenants[key].chargPerseries50 = parseFloat(series50 || 0)
              tenants[key].chargPerNonSeries50 = parseFloat(nonSeries50 || 0)
              tenants[key] = applyDiscountAndComputeTotalCost(tenants[key], discountType, joiningMonthToCurMonthDiff, discountPeriod, totalCost, discount, percentageDiscount, freePeriod)
            }
          }
        }
      })

      const tenantDetails = []

      Object.keys(tenants || {}).forEach(tenKey => {
        if(tenants && tenants[tenKey] && tenants[tenKey]._id){
          tenantDetails.push({
            ...tenants[tenKey],
            appId: app._id,
            tenantId: tenKey,
            tenantCreatedAt: tenants[tenKey].createdAt,
            updatedAt: new Date(),
            createdAt: new Date()
          })
          lsInvoiceLogs.push({
            _id: tenants[tenKey].invoiceLogId,
            invoiceId: tenants[tenKey]._id,
            appId: app._id,
            lastInvoiceCreatedAt: new Date(),
            tenantId: tenKey
          })
        }
      })
      allInvoices = [...allInvoices, ...tenantDetails]
    }
    if(allInvoices && allInvoices.length){
      console.log("Invoices : ---- ", JSON.stringify(allInvoices))
      allInvoices = await InvoiceDetails.insertMany(allInvoices)
      for (const i in lsInvoiceLogs) {
        const log = lsInvoiceLogs[i]
        delete log._id
        await LastGeneratedInvoices.findOneAndUpdate(
          { tenantId: ObjectID(log.tenantId) },
          {
            $set: {
              ...log
            }
          },
          {
            new:true,
            upsert: true,
            runValidators: true
          })
      }
    }

    const logs = []
    allInvoices.forEach(inv => {
      logs.push({
        invoiceId: inv.invoiceId,
        tenantId: inv.tenantId,
        logMeta: {
          log: "Invoice generated from system"
        },
        appId: inv.appId,
        updatedAt: new Date(),
        createdAt: new Date()
      })
    })
    await InvoiceLogs.insertMany(logs)

    // console.log("2. generate invoices...", JSON.stringify(allInvoices))
  } catch (err) {
    console.log("Error", err)
  }
}

export const sendInvoiceToTenant = async (req, res) => {
  try {
    const {user} = req
    const {invoiceDocId, invoiceId, invoiceNumber, sendEmail, emailTitle, emailMessage} = req.body
    const meta = {sendEmailOn: sendEmail, emailTitle, emailMessage, approvedAt: new Date()}

    await InvoiceDetails.update({ _id: invoiceId}, {$set: {invoiceId: invoiceNumber, invoiceDocId, status: "Approved", meta}})
    const invoice = await InvoiceDetails.findOne({ _id: invoiceId})

    const attachments = []
    const { contextType, name, originalName, _id } = invoiceDocId
    const objectName = `${contextType}/${name}`
    console.log("objectName in send service : ", objectName)

    // /// Invoice Download URL
    const opType = "getObject"
    const params = {
      Bucket: config.keys.s3.bucketName,
      Key: objectName,
      Expires: ""
    }
    const signedUrls = []
    try {
      const url = s3.getSignedUrl(opType, params)
      attachments.push({ filename: originalName, attachmentUrl: url})
      signedUrls.push(url)
    } catch (err) {
      console.log("err in getting s3 url for doc id: ", _id, err)
    }
    const signedUrlMessage = signedUrls.reduce ( (acc, su) => `${acc}<br/>${su}`, "")
    // const message = `${emailMessage} <hr/> ${signedUrlMessage}`
    const message = `${emailMessage}`

    // //////////////////////////////

    const emailconfig = await sendEmailHelper({
      to:[sendEmail],
      bodytemplate: message,
      subject: emailTitle || "Invoice From Munivisor",
      replyto:[user.userEmailId]
    })
    await sendAWSEmailWithAttachment({ ...emailconfig, attachments })

    await InvoiceLogs.create({
      invoiceId,
      tenantId: invoice.tenantId,
      logMeta: {
        log: `Review and send to client ${invoiceNumber}`,
        approvedAt: new Date(),
        userId: user._id,
        userName: `${user.userFirstName} ${user.userLastName}`
      }
    })

    res.status(200).send({done: true, message: "Invoice send successfully.", invoice})
  } catch (err) {
    console.log(err)
    res.status(422).send({done: false, error: "Error in sending invoice"})
  }
}

