import axios from "axios"
import AWS from "aws-sdk"
import config from "../../config"

const nodemailer = require("nodemailer")

const s3BucketName = config.keys.s3.bucketName

const muniApiBaseURL = `http://localhost:${config.port || 8000}`

export const s3 = new AWS.S3({
  region : config.keys.s3.region,
  bucket: s3BucketName,
  accessKeyId: config.keys.s3.accessKeyId,
  secretAccessKey: config.keys.s3.secretAccessKey
})

AWS.config.update({
  region: process.env.PLATFORM_SESREGION || "us-east-1",
  accessKeyId: process.env.PLATFORM_SESACCESSKEYID || "AKIAIMCK6CR7IJIR7DDA",
  secretAccessKey: process.env.PLATFORM_SESSECRETKEY || "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",
})

export const sendEmailHelper = async ( {to,cc,replyto,bodytemplate, subject}) => {

  console.log("*******SEND EMAIL CONFIGURATION", process.env.PLATFORM_SENDEMAILS)
  const toRecipientEmailList = (process.env.PLATFORM_SENDEMAILS === "Yes") ?  [...to] : [process.env.PLATFORM_FROMEMAIL]
  const ccRecipientEmailList = (process.env.PLATFORM_SENDEMAILS === "Yes")?  [...cc] : [process.env.PLATFORM_REPLYTOEMAIL]
  const replyToRecipientEmailList = (process.env.PLATFORM_SENDEMAILS === "Yes") ?  [...replyto] : [process.env.PLATFORM_REPLYTOEMAIL]
  console.log("********EMAILS BEING SENT TO",{toRecipientEmailList,ccRecipientEmailList,replyToRecipientEmailList})

  const revisedConfig = {
    to:[...toRecipientEmailList],
    cc:[...ccRecipientEmailList],
    bodytemplate,
    subject,
    replyto:[...replyToRecipientEmailList]
  }

  return revisedConfig
}

export const sendAWSEmailWithAttachment = async (emailConfig) => {
  const {to,cc,bodytemplate,subject,replyto, attachments} = emailConfig
  const transporter = nodemailer.createTransport({
    SES: new AWS.SES({
      apiVersion: "2010-12-01"
    })
  })
  let returnStatus
  console.log("attachments : ", JSON.stringify(attachments))
  try {
    const data = await transporter.sendMail({
      from: replyto,
      subject,
      to,
      cc,
      html: bodytemplate,
      attachments: attachments.map(a => ({
        filename: a.filename,
        path: a.attachmentUrl
        // content: a.content.data
      }))
    })
    returnStatus = {status:"success",error:"", data}
    console.log("RETURN STATUS AFTER SENDING EMAILS:", returnStatus)
  } catch (err) {
    console.log("err in sendAWSEmailWithAttachment : ", err)
    returnStatus = {status:"fail",error:"Error Sending email"}
  }
}

// Create sendEmail params
export const sendAWSEmail = async (emailConfig) => {

  console.log(JSON.stringify(emailConfig, null,2))

  const {to,cc,bodytemplate,subject,replyto} = emailConfig
  const params = {
    Destination: { /* required */
      CcAddresses: [...cc],
      ToAddresses: [...to]
    },
    Message: { /* required */
      Body: { /* required */
        Html: {
          Charset: "UTF-8",
          Data: bodytemplate
        },
        Text: {
          Charset: "UTF-8",
          Data: "test email"
        }
      },
      Subject: {
        Charset: "UTF-8",
        Data: subject
      }
    },
    Source: process.env.PLATFORM_REPLYTOEMAIL, /* required */
    ReplyToAddresses: [...replyto],
  }

  // Create the promise and SES service object
  const sendPromise = new AWS.SES({apiVersion: "2010-12-01"}).sendEmail(params).promise()

  let returnStatus
  try {
    const data = await sendPromise
    returnStatus = {status:"success",error:"", data}
    console.log("RETURN STATUS AFTER SENDING EMAILS:", returnStatus)
  } catch (e) {
    console.log(e)
    returnStatus = {status:"fail",error:"Error Sending email"}
  }

  return returnStatus
}

export const getS3DownloadURLs = async (docIds, token, expires) => {
  let urls = []
  try {
    const res = await axios.post(`${muniApiBaseURL}/api/s3/get-s3-download-urls`, { docIds, expires }, {headers:{ "Authorization": token}})
    if(res && res.data) {
      urls = res.data
    }
  } catch(err) {
    console.log("err in getting s3 urls : ", err)
  }
  return urls
}

export const getFilesFromS3 = async (docIds) => {
  const len = docIds.length
  const files = []
  for(let i = 0; i < len; i++) {
    try {
      const res = await axios.get(`${muniApiBaseURL}/api/docs/${docIds[i]}`)
      if(res && res.data) {
        const doc = res.data
        const { contextId, contextType, tenantId, name, originalName } = doc
        const objectName = `${tenantId}/${contextType}/${contextType}_${contextId}/${name}`
        console.log("objectName : ", objectName)
        const fileData = await axios.post(`${muniApiBaseURL}/api/s3/get-s3-object`, { objectName })
        files.push({ filename: originalName, content: fileData.data.result.Body })
      }
    } catch(err) {
      console.log("err in getting file : ", err)
    }
  }
  return files
}
