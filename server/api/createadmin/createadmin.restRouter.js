import express from "express"
import createAdminController from "./createadmin.controller"

export const createAdminRouter = express.Router()

createAdminRouter.route("/")
  .post(createAdminController.generateUser)