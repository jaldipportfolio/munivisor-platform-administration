import { generateControllers } from "../../modules/query"
import { PlatformUser } from "../platformUser/users.model"
import { Applications } from "../models"
import { generateData } from "../seeddata/generateAndLoadUsers"

const generateUser = async (req, res) => {
  console.log("=========Generated User==========>")
  const payload  = await generateData()
  try {
    await PlatformUser.insertMany(payload.userData)
    console.log("users inserted ok.")
  } catch (err) {
    console.log("err in inserting users : ", err)
  }
  try {
    await Applications.insertMany(payload.applicationData)
    console.log("applications inserted ok.")
  } catch (err) {
    console.log("err in inserting applications : ", err)
  }
  res.status(200).send(payload)
}

export default generateControllers(PlatformUser, {
  generateUser
})
