import { generateControllers } from "../../modules/query"
import { PlatformUser } from "./users.model"
import { Applications } from "../models"
import {generateData} from "../seeddata/generateAndLoadUsers"

const {ObjectID} = require("mongodb")

const updateUser = async (req, res) => {
  try{
    const _id = req.params.id
    await PlatformUser.findByIdAndUpdate({_id}, req.body)
    const user = await PlatformUser.findById({_id})
    const userd = {}
    if(user){
      Object.keys(user._doc).forEach(key => {
        if(key !== "userLoginCredentials"){
          userd[key] = user[key]
        }
      })
    }
    res.status(200).send(userd)
  } catch (err) {
    console.log("Error", err)
    res.status(422).send({error: "Error in getting user details"})
  }
}

const getUser = async (req, res) => {
  try{
    const _id = req.params.id
    const user = await PlatformUser.aggregate([{
      $match: { _id: ObjectID(_id) }
    },
    {
      $lookup: {
        from: "applications",
        let: {"appid": "$applications"},
        pipeline: [
          { $match: { $expr: { $in: ["$_id", "$$appid"]}}}
        ],
        as: "applications"
      }
    },
    {
      $project: {
        userId:"$_id",
        userFirstName:1,
        userLastName:1,
        userMiddleName:1,
        userEntitlement:1,
        loginEmailId:{$toLower:"$userLoginCredentials.userEmailId"},
        applications:1,
        byDefaultProject:1,
        notifications: 1,
        settings: 1,
      }
    }
    ])
    res.status(200).send((user && user.length && user[0]) || {})
  } catch (err) {
    console.log("Error", err)
    res.status(422).send({error: "Error in getting user details"})
  }
}

const generateUser = async (req, res) => {
  console.log("=========Generated User==========>")
  const payload  = await generateData()
  // const {allUser} = req.body
  try {
    await PlatformUser.insertMany(payload.userData)
    console.log("users inserted ok.")
  } catch (err) {
    console.log("err in inserting users : ", err)
  }
  try {
    await Applications.insertMany(payload.applicationData)
    console.log("applications inserted ok.")
  } catch (err) {
    console.log("err in inserting applications : ", err)
  }
  res.status(200).send(payload)
}

export default generateControllers(PlatformUser, {
  updateUser,
  getUser,
  generateUser
})
