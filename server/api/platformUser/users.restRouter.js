import express from "express"
import usersController from "./users.controller"

export const platformUserRouter = express.Router()

platformUserRouter.param("id", usersController.findByParam)

platformUserRouter.route("/")
  .get(usersController.getAll)
  .post(usersController.createOne)

platformUserRouter.route("/:id")
  .get(usersController.getUser)
  .put(usersController.updateUser)
  .delete(usersController.createOne)

platformUserRouter.route("/createUser")
  .post(usersController.generateUser)