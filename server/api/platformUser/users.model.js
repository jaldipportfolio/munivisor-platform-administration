import mongoose, {Schema} from "mongoose"
import timestamps from "mongoose-timestamp"

// eslint-disable-next-line no-unused-vars
const userCredentialsSchema = Schema({
  _id:String,
  userId:String, // Should come from the user Collection
  userEmailId:String, // We can get rid of this if we are able to just use the email ID.
  password:String,  // This is the salted password.
  onboardingStatus:String,
  userEmailConfirmString:String,
  userEmailConfirmExpiry:Date,
  passwordConfirmString:String,
  passwordConfirmExpiry:Date,
  isUserSTPEligible:Boolean,
  authSTPToken:String,
  authSTPPassword:String,
  passwordResetIteration:Number,
  passwordResetStatus:String,
  passwordResetDate:Date,
  loginAttempts: { type: Number, default: 0},
  lockUntil: { type: Number},
  lastLoginAttempt: { type: Date}
})

const applicationInfo = Schema({
  name: String,
  url: String,
  token: String,
})

export const notificationsConfigSchema = new Schema({
  self: Boolean,
  client: Boolean,
  thirdParty: Boolean,
}, { _id: false })

export const auditSettingSchema = new Schema({
  auditFlag: String
}, { _id: false })

const platformUserSchema = Schema({
  userEntitlement:String,
  userFirstName:String,
  userMiddleName:String,
  userLastName:String,
  userEmailId: { type: String, required: true},
  userLoginCredentials: userCredentialsSchema,
  byDefaultProject: String,
  billing: Object,
  applications: [Schema.Types.ObjectId],
  notifications: notificationsConfigSchema,
  settings: auditSettingSchema
}).plugin(timestamps)

export const PlatformUser =  mongoose.model("users", platformUserSchema)
