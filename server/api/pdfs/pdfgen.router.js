import express from "express"
import {
  generatePDFDoc,
  generateInvoicePDF,
} from "./pdfgen.controller"

export const pdfGeneratorRouter = express.Router()

pdfGeneratorRouter.route("/testpdf")
  .get(generatePDFDoc)

pdfGeneratorRouter.route("/generateinvoicepdf")
  .post(generateInvoicePDF)

