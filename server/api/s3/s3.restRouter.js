import express from "express"
import AWS from "aws-sdk"

import { Docs } from "../docs/docs.model"
import config from "../../config"

const querystring = require("querystring")

const s3BucketName = config.keys.s3.bucketName

export const s3 = new AWS.S3({
  region : config.keys.s3.region,
  bucket: s3BucketName,
  accessKeyId: config.keys.s3.accessKeyId,
  secretAccessKey: config.keys.s3.secretAccessKey
})

export const s3Router = express.Router()

s3Router.post("/get-signed-url", (req, res) => {
  const { opType, fileName, options } = req.body
  // console.log("tags : ", options.tags )
  const bucketName = s3BucketName
  if(!bucketName || !fileName) {
    return res.status(422).send({ error: "No bucketName or fileName provided" })
  }

  const allowedOps = ["download", "upload"]
  if(!allowedOps.includes(opType)) {
    return res.status(422).send({ error: "invalid opType" })
  }
  const opTypes = { download: "getObject", upload: "putObject" }
  const params = {Bucket: bucketName, Key: fileName, Expires: 90}
  if(opType === "upload") {
    params.ContentType = options.contentType
    // params.Tagging = options.tags ? querystring.stringify(options.tags) : ""
    console.log("tags : ", params.Tagging )
  } else if(opType === "download" && options && options.versionId ) {
    params.VersionId = options.versionId
  }
  let url
  try {
    url = s3.getSignedUrl(opTypes[opType], params)
  } catch (err) {
    console.log("err in getting s3 url : ", err)
    return res.status(422).send({ error: "err in getting s3 url" })
  }
  return res.send({ url })
})

s3Router.post("/get-s3-object", async (req, res) => {
  let { bucketName, objectName } = req.body
  bucketName = s3BucketName
  if(!bucketName || !objectName) {
    return res.status(422).send({ error: "No bucketName or objectName provided" })
  }

  const params = {
    Bucket: bucketName,
    Key: objectName
  }
  console.log("objectName 123 : ", objectName)
  let result
  try {
    result = await s3.getObject(params).promise()
  } catch (err) {
    console.log("error in getting object : ", err)
    return res.status(422).send({ error: "error in getting object" })
  }

  return res.send({ result })
})

s3Router.post("/put-s3-object", async (req, res) => {
  let { bucketName, fileName, contentType, fileData, tags } = req.body
  bucketName = s3BucketName
  if(!bucketName || !fileName || !fileData || !contentType) {
    return res.status(422).send({ error: "No bucketName or fileName or fileData provided" })
  }

  let Tagging = ""

  if(tags) {
    Tagging = querystring.stringify(tags)
  }

  const params = {
    Bucket: bucketName,
    Key: fileName,
    Body: fileData,
    ContentType: contentType,
    Tagging
  }
  let result
  try {
    result = await s3.putObject(params).promise()
  } catch (err) {
    console.log("error in uploading object : ", err)
    return res.status(422).send({ error: "error in uploading object" })
  }

  return res.send({ result })
})

s3Router.post("/get-s3-object-versions", async (req, res) => {
  let { bucketName, fileName } = req.body
  bucketName = s3BucketName
  if(!bucketName || !fileName) {
    return res.status(422).send({ error: "No bucketName or objectName provided" })
  }

  const params = {
    Bucket: bucketName,
    Prefix: fileName
  }
  let result
  try {
    result = await s3.listObjectVersions(params).promise()
  } catch (err) {
    console.log("error in getting versions : ", err)
    return res.status(422).send({ error: "error in getting versions" })
  }

  return res.json({ result })
})

s3Router.post("/delete-s3-object", async (req, res) => {
  let { bucketName, files } = req.body
  bucketName = s3BucketName
  if(!bucketName || !files) {
    return res.status(422).send({ error: "No bucketName or files provided" })
  }

  const params = {
    Bucket: bucketName,
    Delete: {
      Objects: files,
      Quiet: false
    }
  }
  let result
  try {
    result = await s3.deleteObjects(params).promise()
  } catch (err) {
    console.log("error in deleting object : ", err)
    return res.status(422).send({ error: "error in deleting object" })
  }

  return res.send({ result })
})

s3Router.post("/put-s3-object-tagging", async (req, res) => {
  let { bucketName, fileName, tags } = req.body
  bucketName = s3BucketName
  if(!bucketName || !fileName || !tags) {
    return res.status(422).send({ error: "No bucketName or fileName or tags provided" })
  }

  const TagSet = Object.keys(tags).map(k => ({ Key: k, Value: tags[k] }))

  const params = {
    Bucket: bucketName,
    Key: fileName,
    Tagging: {
      TagSet
    }
  }
  let result
  try {
    result = await s3.putObjectTagging(params).promise()
  } catch (err) {
    console.log("error in updating tags for object : ", err)
    return res.status(422).send({ error: "error in updating tags for object" })
  }

  return res.send({ result })
})

s3Router.get("/get-s3-object-as-stream", (req, res) => {
  let { bucketName, objectName, versionId } = req.query
  bucketName = s3BucketName
  objectName = decodeURIComponent(objectName)
  if(!bucketName || !objectName) {
    return res.status(422).send({ error: "No bucketName or objectName provided" })
  }

  const params = {
    Bucket: bucketName,
    Key: objectName
  }

  if(versionId) {
    params.VersionId = versionId
  }
  s3.getObject(params)
    .on("httpHeaders", function (statusCode, headers){
      console.log("statusCode : ", statusCode)
      console.log("content : ", headers["content-type"])
      res.set("Content-Length", headers["content-length"])
      res.set("Content-Type", headers["content-type"])
      this.response.httpResponse.createUnbufferedStream().pipe(res)
    })
    .on("error", (err) => {
      console.log("err : ", err)
      res.status(422).send({ error: err })
    })
    .send()
})

s3Router.post("/get-s3-download-urls", async (req, res) => {
  let { bucketName, docIds, expires } = req.body
  // console.log("tags : ", options.tags )
  bucketName = s3BucketName
  expires = expires || (30*24*60*60)
  if(!bucketName || !docIds || !docIds.length) {
    return res.status(422).send({ error: "No bucketName or docIds provided" })
  }

  let docs = []

  try {
    docs = await Docs.find({ _id: { $in: docIds } })
    if(!docs || !docs.length) {
      return res.status(422).send({ error: "No documents find" })
    }
  } catch(err) {
    console.log("err in getting documents : ", err)
    return res.status(422).send({ error: "Error in getting documents", err })
  }

  const urls = []
  const opType = "getObject"

  docs.forEach(d => {
    let url
    const { _id, tenantId, contextType, contextId, name } = d
    // const fileName = `${tenantId}/${contextType}/${contextType}_${contextId}/${name}`
    let fileName = ""
    if(tenantId && contextId && contextType && name) {
      fileName = `${tenantId}/${contextType}/${contextType}_${contextId}/${name}`
    } else if(tenantId && contextType && name && contextType === "INVOICES"){
      fileName = `${contextType}/${name}`
    }
    const params = {Bucket: bucketName, Key: fileName, Expires: expires}
    try {
      url = s3.getSignedUrl(opType, params)
      urls.push(url)
    } catch (err) {
      console.log("err in getting s3 url for doc id: ", _id, err)
      // return res.status(422).send({ error: "err in getting s3 url", err })
    }
  })

  res.json(urls)
})
