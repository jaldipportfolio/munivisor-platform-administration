import axios from "axios"
import { services } from "../AllServices"
import { getUserActiveApplication } from "../common/common.controller"

const serviceFrom = "dashboard"

export const getTenantsAsPerRequirement = async (req, res) => {
  try {
    const {user, body} = req
    const app = await getUserActiveApplication(user)
    console.log(app)
    const apiRes = await axios.post(`${app.url}${services[serviceFrom].getTenants}`, body, { headers: {Authorization : app.token} })
    res.send(apiRes.data)
  } catch (err) {
    // console.log("Error", err)
    res.status(422).send({error: "Error in getting tenant list"})
  }
}

export const getTenantUsersListAsPerRequirement = async (req, res) => {
  try {
    const {user, body} = req
    const app = await getUserActiveApplication(user)
    console.log(app)
    const apiRes = await axios.post(`${app.url}${services[serviceFrom].getTenantUsers}`, body, { headers: {Authorization : app.token} })
    res.send(apiRes.data)
  } catch (err) {
    // console.log("Error", err)
    res.status(422).send({error: "Error in getting tenant list"})
  }
}

export const getPlatformadminFirmDetails = async (req, res) => {
  try {
    const {user} = req
    const {entityId} = req.params
    const app = await getUserActiveApplication(user)
    const apiRes = await axios.get(`${app.url}${services[serviceFrom].getFirmDetails}/${entityId}`, { headers: {Authorization : app.token} })
    res.send(apiRes.data)
  } catch (err) {
    res.status(422).send({error: "Error in getting Firm"})
  }
}
