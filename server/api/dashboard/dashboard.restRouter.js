import express from "express"
import {
  getTenantsAsPerRequirement,
  getTenantUsersListAsPerRequirement,
  getPlatformadminFirmDetails
} from "./dashboard.controller"

export const dashboardRouter = express.Router()

dashboardRouter.route("/tenants")
  .post(getTenantsAsPerRequirement)

dashboardRouter.route("/tenantusers")
  .post(getTenantUsersListAsPerRequirement)

dashboardRouter.route("/:entityId")
  .get(getPlatformadminFirmDetails)
