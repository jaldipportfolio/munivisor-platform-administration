import bcrypt from "bcrypt-nodejs"
import {PlatformUser} from "../platformUser"
import {Applications} from "./../models"

const {ObjectID} = require("mongodb")
// import _ from "lodash"

// const entitlement = ["global-edit","global-readonly","global-readonly-tran-edit"]
const userFirstName = ["Admin","Admin1","Admin2","Admin3"]
// const userMiddleName = ["M","S","A","P","N","O"]
const userLastName = ["Reed","Ford","Butler","Anderson","Andrew","Otaras"]
const emailId = ["admin@otaras.com","admin1@otaras.com","admin2@otaras.com","admin3@otaras.com"]

export const generateData = () => {
  // const allUser = ["jerrya@hotmail.com","herminiaa@hotmail.com","admin@otaras.com"]
  const allUser = []
  const userData = []
  const applicationData = []
  const salt = bcrypt.genSaltSync(10)
  const hashedPassword = bcrypt.hashSync("HumHongeKamyab", salt)
  const userRefId = new ObjectID()

  const application = {
    "_id" : userRefId,
    "name" : "MuniVisor Otaras",
    "url" : "http://localhost:8081",
    "token" : "abcdefghijklmnopqrstuvwxyz"
  }
  applicationData.push(application)

  for (let i = 0; i < emailId.length; i++) {
    if (!allUser.includes(emailId[i])) {
      const user = {
        // _id : new ObjectID(),
        userEntitlement : "global-edit",
        userFirstName : userFirstName[i],
        // userMiddleName : userMiddleName[i],
        userLastName : userLastName[i],
        userEmailId : emailId[i],
        userLoginCredentials : {
          userId : emailId[i],
          userEmailId : emailId[i],
          password : hashedPassword,
          onboardingStatus : "Done",
          userEmailConfirmString : "",
          userEmailConfirmExpiry : null,
          passwordConfirmString : "",
          passwordConfirmExpiry : null,
          passwordResetIteration : 0,
          passwordResetStatus : "",
          passwordResetDate : null,
          isUserSTPEligible : false,
          authSTPToken : null,
          authSTPPassword : null
        },
        applications : [userRefId],
        byDefaultProject : "MuniVisor Otaras",
        updatedAt : new Date()
      }
      userData.push(user)
    }
  }
  return {userData, applicationData}
}

/* export const insertUsersAndApplications = async () => {
  let userDataCreate = []
  try {
    userDataCreate = await generateData()
  } catch (err) {
    console.log("err in getting user data : ", err)
  }
  console.log("=============users=============>", userDataCreate)
  try {
    await PlatformUser.insertMany(userDataCreate.userData)
    console.log("users inserted ok.")
  } catch (err) {
    console.log("err in inserting users : ", err)
  }

  try {
    await Applications.insertMany(userDataCreate.applicationData)
    console.log("applications inserted ok.")
  } catch (err) {
    console.log("err in inserting applications : ", err)
  }

  process.exit()
} */

// insertUsersAndApplications()

/* insertUsersAndApplications()
    .then(() => {
        console.log("success")
        process.exit(0)
    })
    .catch(e => console.log(e)) */
