import { MongoClient } from "mongodb"
require("dotenv").config()

const collectionNames = [
    "invoices",
    "users",
    "applications",
    "docs",
    "invoicelogs",
    "lastgeneratedinvoices",
    "settings",
    "tenantconfig",
    "transactions"
]
console.log(process.env.MONGODB_NAME)

MongoClient.connect(process.env.PLATFORM_DB_URL,{ useNewUrlParser: true }, (err, db) => {
    if (err) throw err
    // db pointing to newdb
    const dbase = db.db(process.env.MONGODB_NAME)
    console.log(`Switched to ${dbase} database`)
    collectionNames.forEach ( (c, i) => {
        dbase.collection(c, (errCol, coll) => {
            console.log(`Removing Collectiion ${c}`)
            coll.deleteMany({}, (e, r) => {
                if(e) {
                    console.log(`The collection ${c} doesn't exist moving to next`)
                } else {
                    console.log(`deleted collection ${c}`)
                }
                if(i === (collectionNames.length - 1)) {
                    console.log("Done with Removing all documents from collection")
                    db.close()
                    process.exit(0)
                }
            })
        })
    })
})