
export const services = {
  common: {
    getPicklists: "/api/platformadmincommon/picklists",
    getAllDisablePicklists: "/api/platformadmincommon/disablepicklists",
    checklists: "/api/platformadmincommon/checklists",
    getSnapShots: "/api/platformadmincommon/snapshots",
    updateEntityDetails: "/api/platformadmincommon/entity",
    updatePicklistsAtAppLevel: "/api/platformadmincommon/configs/update-specific",
  },
  create: {
    createTenant: "/api/platformadmin/create/tenant",
    createUsers: "/api/platformadmin/create/users",
  },
  dashboard: {
    getTenants: "/api/platformadmin/tenants",
    getTenantUsers: "/api/platformadmin/tenantusers",
    getFirmDetails: "/api/platformadmin/entity/entities",
  },
  users: {

  },
  billing: {
    getTenantUsersInfo: "/api/platformadmin/tenantusercount"
  }
}
