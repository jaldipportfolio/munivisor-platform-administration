import express from "express"
import tenantConfigController from "./tenantconfig.controller"

export const tenantConfigRouter = express.Router()

tenantConfigRouter.route("/:id")
  .get(tenantConfigController.getTenantConfig)
  .put(tenantConfigController.updateTenantConfig)