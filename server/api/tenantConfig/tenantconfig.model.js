import mongoose, {Schema} from "mongoose"
import timestamps from "mongoose-timestamp"

const tenantConfig = Schema({
  tenantId: String,
  userId: String,
  billing: Object
}).plugin(timestamps)

export const TenantConfig =  mongoose.model("tenantconfig", tenantConfig)