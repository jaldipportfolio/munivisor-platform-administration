import {generateControllers} from "../../modules/query"
import { TenantConfig } from "./tenantconfig.model"


const getTenantConfig = async (req, res) => {
  try{
    const _id = req.params.id
    const tenant = await TenantConfig.find({tenantId: _id})
    res.status(200).send(tenant)
  } catch (err) {
    console.log("Error", err)
    res.status(422).send({error: "Error in getting tenant details"})
  }
}

const updateTenantConfig = async (req, res) => {
  try{
    const {id} = req.params
    req.body.updatedAt = new Date()
    const result = await TenantConfig.update({ _id: id}, {$set: req.body})
    if(result){
      const tenant = await TenantConfig.find({_id: id})
      res.status(200).send(tenant)
    }
  } catch (err) {
    console.log("Error", err)
    res.status(422).send({error: "Error in getting tenant details"})
  }
}

export default generateControllers(TenantConfig, {
  getTenantConfig,
  updateTenantConfig
})
