import axios from "axios"
import moment from "moment"
import {services} from "../AllServices"
import {Applications, InvoiceDetails, Settings, InvoiceLogs, SnapShots} from "../models"

const serviceFrom = "common"
const {ObjectID} = require("mongodb")

export const getUserActiveApplication = (user) => ((user && user.applications) || []).find(app => app.name === user.byDefaultProject)

export const getPlatformadminPicklists = async (req, res) => {
  try {
    const {user} = req
    const app = await getUserActiveApplication(user)
    const queryURL = req.originalUrl.split("/") || []
    let queryString = ""
    if(queryURL && queryURL.length){
      const queryURLLength = queryURL.length - 1
      queryString = queryURL[queryURLLength].split("?").pop() || ""
    }
    const query = queryString ? `?${queryString}` : ""
    const apiRes = await axios.get(`${app.url}${services[serviceFrom].getPicklists}${query}`, { headers: {Authorization : app.token} })
    res.send(apiRes.data)
  } catch (err) {
    res.send({done: false, error: "Error in getting picklits"})
  }
}

export const getPlatformSetting = async (req, res) => {
  try {
    const setting = await Settings.findOne({})
    res.send(setting)
  } catch (err) {
    res.status(422).send({error: "Error in getting platform Setting"})
  }
}

export const fetchInvoiceData = async (req, res) => {
  const {page, pageSize, sorted, searchField, tenantId, startDate, endDate } = req.body
  try {
    const invalid = /[°"§%()\[\]{}=\\?´`'#<>|,;.:+_-]+/g  // eslint-disable-line
    const newSearchString = searchField.replace(invalid, "")
    const searchterm = new RegExp(newSearchString,"i")
    const matchQuery = []
    const app = await getUserActiveApplication(req.user)
    if(tenantId){
      matchQuery.push({
        $match: {
          tenantId: ObjectID(tenantId),
          appId: ObjectID(app._id)
        }
      })
    }else {
      matchQuery.push({
        $match: {
          appId: ObjectID(app._id)
        }
      })
    }

    if(startDate || endDate){
      const createdAt = {}
      if(startDate){
        createdAt.$gte = new Date(moment(startDate).utc().toDate())
      }
      if(endDate){
        createdAt.$lte = new Date(moment(endDate).utc().toDate())
      }
      matchQuery.push({ $match:{
        $and:[ {"createdAt": createdAt}]
      }})
    }

    const invoice = await InvoiceDetails.aggregate([
      ...matchQuery,
      {
        $addFields: {
          keySearchDetails: {
            $concat: [ "$name", " ", "$typeOfBilling", " ", "$status", "$billFrequency" ]
          }
        }
      },
      {
        $match: {
          $or: [{ keySearchDetails: { $regex: searchterm, $options: "i" } }]
        }
      },
      {
        $facet: {
          metadata: [{ $count: "total" }, { $addFields: { pages: { $ceil: { $divide: ["$total", pageSize] } } } }],
          data: [{ $sort: {...sorted} }, { $skip: page > 0 ? ( pageSize*page) : 0  }, { $limit: pageSize }]
        }
      }
    ])
    res.send(invoice)
  } catch (err) {
    console.log("fetchInvoiceData error => ",err)
    res.status(422).send({error: "Error in getting invoices data"})
  }
}

export const updatePlatformSetting = async (req, res) => {
  const {settingId} = req.query
  if(settingId){
    try {
      await Settings.updateOne({_id: settingId}, req.body)
      const setting = await Settings.findOne({_id: settingId})
      res.send(setting)
    } catch (err) {
      res.status(422).send({error: "Error in update platform Setting"})
    }
  }else {
    try {
      const insert = await Settings.create(req.body)
      res.send(insert)
    } catch (err) {
      res.status(422).send({error: "Error in update platform Setting"})
    }
  }
}

export const getInvoicePreviewById = async (req, res) =>{
  const {invoiceId } = req.params
  try {
    if(!invoiceId){
      res.status(422).send({done: false, error: "please pass the invoice id"})
    }
    const invoice = await InvoiceDetails.findOne({_id: invoiceId})
    if(!invoice){
      res.status(422).send({done: false, error: "invoice not found."})
    }
    res.status(200).send({done: true, invoice})
  } catch (err) {
    res.status(422).send({error: "Error in getting invoice preview"})
  }
}

export const updateInvoicePreviewById = async (req, res) =>{
  const {invoiceId } = req.params
  try {
    if(!invoiceId){
      res.status(422).send({done: false, error: "please pass the invoice id"})
    }
    await InvoiceDetails.updateOne({_id: invoiceId}, req.body)
    const invoice = await InvoiceDetails.findOne({_id: invoiceId})
    if(!invoice){
      res.status(422).send({done: false, error: "invoice not found."})
    }
    res.status(200).send({done: true, invoice})
  } catch (err) {
    res.status(422).send({done: false, error: "Error in updating invoice preview"})
  }
}

export const getSnapShots = async (req, res) => {

  const {firmId} = req.params
  const {startDate, endDate, currentPage, size, sortFields} = req.body

  try {

    if(!firmId){
      res.status(422).send({done: false, error: "Please pass firm id"})
    }

    let snapShotsList = []
    let total = 0
    if(startDate && endDate) {

      const date = `${endDate}T23:59:59`
      total = await SnapShots.find({tenantId: ObjectID(firmId), date: { $gte: new Date(startDate), $lte: new Date(date) }}).count()
      snapShotsList = await SnapShots.find({tenantId: ObjectID(firmId), date: { $gte: new Date(startDate), $lte: new Date(date) }}).limit(size).skip(size * currentPage).sort(sortFields)

    } else {

      total = await SnapShots.find({ tenantId: ObjectID(firmId)}).count()
      snapShotsList = await SnapShots.find({tenantId: ObjectID(firmId)}).limit(size).skip(size * currentPage).sort(sortFields)

    }
    const pages = Math.ceil(total/size)
    res.send({done: true, snapShots: { data: snapShotsList, metadata: { total, pages } } })

  } catch (err) {
    res.status(422).send({done: false, error: "Error in Getting Snap Shots List"})
  }
}

export const createInvoiceLogs = async (req, res) => { // eslint-disable-line
  const { user } = req
  if(!user) {
    return res.status(422).send({ error: "No user found" })
  }
  try {
    req.body = req.body.map(log => ({
      ...log,
      logMeta: {
        ...log.logMeta,
        userId: user._id,
        userName: `${user.userFirstName} ${user.userLastName}`
      },
      updatedAt: new Date(),
      createdAt: new Date()
    }))
    const logs = await InvoiceLogs.insertMany(req.body)
    res.json({ message: "OK", logs })
  } catch (error) {
    res.status(422).send({ error })
  }
}

export const createApplications = async (req, res) => { // eslint-disable-line

  const { name, url, token } = req.body

  try {

    const regexValue = new RegExp(name.trim() || "", "i")

    const appList = await Applications.aggregate([
      {
        $match: {
          name: { $regex: regexValue, $options: "i" }
        }
      }
    ])

    const matchAppName = appList.find(app => app.name.toLowerCase() === name.toLowerCase().trim())
    if(matchAppName){
      return res.status(203).send({done:false, message:"Project Name Is Already Existing Enter Another Name!"})
    }
    const app = await Applications.create({name, url, token})
    res.json({done: true, app, appList })

  } catch (error) {
    console.log("Error:", error)
    return res.status(422).send({ error })
  }
}


export const getApplications = async (req, res) => {
  try {
    const applications = await Applications.find({}).sort({date: -1})
    res.send({done: true, applications})
  } catch (err) {
    res.status(422).send({done: false, error: "Error in Getting Snap Shots List"})
  }
}

export const getInvoiceLogs = async (req, res) => {  // eslint-disable-line
  const {page, pageSize, sorted, searchField, invoiceId, startDate, endDate } = req.body
  try {
    const invalid = /[°"§%()\[\]{}=\\?´`'#<>|,;.:+_-]+/g // eslint-disable-line
    const newSearchString = searchField.replace(invalid, "")
    const searchterm = new RegExp(newSearchString,"i")
    const matchQuery = []

    if(!invoiceId) return res.status(422).send({done: false, error: "missed query params invoice id" })

    if(startDate || endDate){
      const createdAt = {}
      if(startDate){
        createdAt.$gte = new Date(startDate)
      }
      if(endDate){
        createdAt.$lte = new Date(endDate)
      }
      matchQuery.push({createdAt})
    }
    const query =  [{
      $match:{
        $and:[
          ...matchQuery,
          { invoiceId: ObjectID(invoiceId) }
        ]
      },
    },
    {
      $addFields: {
        keySearchDetails: {
          $concat: [ "$logMeta.log", " ", "$logMeta.userName" ]
        }
      }
    },
    {
      $match: {
        $or: [{ keySearchDetails: { $regex: searchterm, $options: "i" } }]
      }
    },
    {
      $facet: {
        metadata: [{ $count: "total" }, { $addFields: { pages: { $ceil: { $divide: ["$total", pageSize] } } } }],
        data: [{ $sort: {...sorted} }, { $skip: page > 0 ? ( pageSize*page) : 0  }, { $limit: pageSize }]
      }
    }]
    const logs = await InvoiceLogs.aggregate(query)
    res.send(logs)
  } catch (err) {
    res.status(422).send({done: false, error: "Error in Getting Snap Shots List"})
  }
}


export const getAllDisablePicklists = async (req, res) => {
  try {
    const {user} = req
    const app = await getUserActiveApplication(user)
    const queryURL = req.originalUrl.split("/") || []
    let queryString = ""
    if(queryURL && queryURL.length){
      const queryURLLength = queryURL.length - 1
      queryString = queryURL[queryURLLength].split("?").pop() || ""
    }
    const query = queryString ? `?${queryString}` : ""
    console.log("API_URL :---- ", `${app.url}${services[serviceFrom].getAllDisablePicklists}${query}`)
    const apiRes = await axios.get(`${app.url}${services[serviceFrom].getAllDisablePicklists}${query}`, { headers: {Authorization : app.token} })
    res.send(apiRes.data)
  } catch (err) {
    res.status(422).send({error: "Error in getting picklits"})
  }
}

export const putPicklist = async (req, res) => {
  try {
    const {user} = req
    const app = await getUserActiveApplication(user)
    const apiRes = await axios.put(`${app.url}${services[serviceFrom].getAllDisablePicklists}`, req.body,{ headers: {Authorization : app.token} })
    res.send(apiRes.data)
  } catch (err) {
    res.status(422).send({done: false, error: "Error in updating picklist"})
  }
}

export const deletePicklist = async (req, res) => {
  try {
    const {user} = req
    const app = await getUserActiveApplication(user)
    const queryURL = req.originalUrl.split("/") || []
    let queryString = ""
    if(queryURL && queryURL.length){
      const queryURLLength = queryURL.length - 1
      queryString = queryURL[queryURLLength].split("?").pop() || ""
    }
    const query = queryString ? `?${queryString}` : ""
    console.log("API_URL :---- ", `${app.url}${services[serviceFrom].getAllDisablePicklists}${query}`)
    const apiRes = await axios.delete(`${app.url}${services[serviceFrom].getAllDisablePicklists}${query}`, { headers: {Authorization : app.token} })
    res.send(apiRes.data)
  } catch (err) {
    res.status(422).send({error: "Error in delete picklist"})
  }
}

export const getAllChecklists = async (req, res) => {
  try {
    const {user} = req
    const app = await getUserActiveApplication(user)
    const queryURL = req.originalUrl.split("/") || []
    let queryString = ""
    if(queryURL && queryURL.length){
      const queryURLLength = queryURL.length - 1
      queryString = queryURL[queryURLLength].split("?").pop() || ""
    }
    const query = queryString ? `?${queryString}` : ""
    const apiRes = await axios.get(`${app.url}${services[serviceFrom].checklists}${query}`, { headers: {Authorization : app.token} })
    res.send(apiRes.data)
  } catch (err) {
    res.status(422).send({error: "Error in getting picklits"})
  }
}

export const updateChecklist = async (req, res) => {
  try {
    const {user} = req
    const app = await getUserActiveApplication(user)
    const queryURL = req.originalUrl.split("/") || []
    let queryString = ""
    if(queryURL && queryURL.length){
      const queryURLLength = queryURL.length - 1
      queryString = queryURL[queryURLLength].split("?").pop() || ""
    }
    const query = queryString ? `?${queryString}` : ""
    const apiRes = await axios.post(`${app.url}${services[serviceFrom].checklists}${query}`, req.body, { headers: {Authorization : app.token} })
    res.send(apiRes.data)
  } catch (err) {
    res.status(422).send({error: "Error in getting picklits"})
  }
}

export const updateFirmInfo = async (req, res) => {
  const {user} = req
  const app = await getUserActiveApplication(user)
  try {
    const {entityId} = req.params
    const apiRes = await axios.post(`${app.url}${services[serviceFrom].updateEntityDetails}/${entityId}`,req.body, { headers: {Authorization : app.token} })
    res.send(apiRes.data)
  } catch (err) {
    res.status(422).send({error: "Error in getting Firm"})
  }
}

export const savePicklistsData = async (req, res) =>{
  const {user} = req
  const app = await getUserActiveApplication(user)
  try {
    const apiRes = await axios.post(`${app.url}${services[serviceFrom].updatePicklistsAtAppLevel}`, req.body, { headers: {Authorization : app.token}})
    res.send(apiRes.data)
  } catch (err) {
    res.status(422).send({error: "Error in getting Firm"})
  }
}