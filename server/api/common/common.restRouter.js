import express from "express"
import {
  fetchInvoiceData,
  getPlatformadminPicklists,
  getPlatformSetting,
  updatePlatformSetting,
  getInvoicePreviewById,
  updateInvoicePreviewById,
  getSnapShots,
  createInvoiceLogs,
  createApplications,
  getApplications,
  getInvoiceLogs,
  getAllDisablePicklists,
  putPicklist,
  deletePicklist,
  getAllChecklists,
  updateChecklist,
  updateFirmInfo,
  savePicklistsData
} from "./common.controller"

export const commonRouter = express.Router()

commonRouter.route("/picklists")
  .get(getPlatformadminPicklists)

commonRouter.route("/disablepicklists")
  .get(getAllDisablePicklists)
  .put(putPicklist)
  .delete(deletePicklist)

commonRouter.route("/checklists")
  .get(getAllChecklists)
  .post(updateChecklist)

commonRouter.route("/platformsetting")
  .get(getPlatformSetting)
  .put(updatePlatformSetting)

commonRouter.route("/invoice")
  .post(fetchInvoiceData)

commonRouter.route("/invoicespreview/:invoiceId")
  .get(getInvoicePreviewById)
  .put(updateInvoicePreviewById)

commonRouter.route("/snapshot/:firmId")
  .post(getSnapShots)

commonRouter.route("/invoicelogs")
  .put(getInvoiceLogs)
  .post(createInvoiceLogs)

commonRouter.route("/applications")
  .post(createApplications)
  .get(getApplications)

commonRouter.route("/updateFirm/:entityId")
  .post(updateFirmInfo)

commonRouter.route("/savepicklists")
  .post(savePicklistsData)