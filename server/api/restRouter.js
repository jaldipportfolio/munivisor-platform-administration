import express from "express"

import {platformUserRouter} from "./platformUser"
import {commonRouter} from "./common"
import {dashboardRouter} from "./dashboard"
import { createRouter } from "./create"
import {paymentRouter} from "./payment"
import {billingRouter} from "./billing"
import {tenantConfigRouter} from "./tenantConfig"
import {pdfGeneratorRouter} from "./pdfs"
import {s3Router} from "./s3"
import {docsRouter} from "./docs"
import {transactionsRouter} from "./transactions"

export const restRouter = express.Router()


// The authorization routes go here Api Routs
restRouter.use("/platformuser", platformUserRouter)
restRouter.use("/common", commonRouter)
restRouter.use("/dashboard", dashboardRouter)
restRouter.use("/platformadmin/create", createRouter)
restRouter.use("/payment", paymentRouter)
restRouter.use("/billing", billingRouter)
restRouter.use("/pdf", pdfGeneratorRouter)
restRouter.use("/s3", s3Router)
restRouter.use("/docs", docsRouter)
restRouter.use("/transactions", transactionsRouter)

restRouter.use("/tenantconfig", tenantConfigRouter)
