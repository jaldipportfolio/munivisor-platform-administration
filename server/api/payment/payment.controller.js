import moment from "moment"
import {InvoiceDetails, Transactions} from "../models"
import {sendAWSEmail, sendEmailHelper} from "../billing/helpers"

const stripe = require("stripe")(process.env.PLATFORM_STRIPE_SECRET_KEY)

export const createCustomer = async (req, res) => {
  try {
    const payload = {
      account_balance: 1500, // optional
      address: {
        line1: "60, Spring wood", // required
        line2: "", // optional
        city: "", // optional
        state: "", // optional,
        country: "", // optional
        postal_code: "", // optional
      }, // optional
      email: "vinit@dev.com", // optional
      invoice_prefix: "invoice", // optional
      invoice_settings: {
        custom_fields: null,
        default_payment_method: null,
        footer: null
      }, // optional object
      metadata: {}, // optional object
      name: "Vinit Dev", // optional
      phone: "+91 45852 12354", // optional
      description: "Customer for jenny.rosen@example.com", // optional
      source: "tok_visa" // optional field obtained with Stripe.js
    }


    stripe.customers.create(payload,(err, customer)  => {
      if(err){
        return res.status(err.statusCode || 500).send(err)
      }
      res.send({done: true, customer})
    })
  } catch (err) {
    // console.log("Error", err)
    res.status(422).send({error: "Error in getting tenant list"})
  }
}

export const takeCharge1 = async (req, res) => {
  try {
    const {token, metadata, receipt_email, amount} = req.body
    if(token && token.id){
      stripe.charges.create({
        amount: 4000,
        currency: "usd",
        description: "An example charge",
        source: token.id,
        receipt_email,
        metadata
      },(err, status)  => {
        if(err){
          return res.status(err.statusCode || 500).send({done: false, message: "An unknown error occurred", ...err })
        }
        return res.send({done: true, status})
      })
    }else {
      res.status(422).send({done: false, message: "Please pass token id"})
    }
  } catch (err) {
    res.status(500).send({done: false, message: "Error in take a charge"})
  }
}

export const takeCharge = async (req, res) => {
  try {
    const {token, metadata, receipt_email, amount} = req.body
    if(token && token.id){
      stripe.customers.create({
        email: receipt_email,
        source: token.id,
        description: "Otaras Client",
      }, (err, customer) => {
        if(err){
          return res.status(err.statusCode || 500).send({done: false, message: "An unknown error occurred while creating customer", ...err })
        }
        stripe.charges.create({
          receipt_email,
          metadata,
          amount,
          description: "Web Development MuniVisor",
          currency: "usd",
          customer: customer.id,
          // source: token.id,
        }, (err, status)  => {
          if(err){
            return res.status(err.statusCode || 500).send({done: false, message: "An unknown error occurred while taking a charge", ...err })
          }
          return res.send({done: true, status})
        })
      })
    }else {
      res.status(422).send({done: false, message: "Please pass token id"})
    }
  } catch (err) {
    res.status(500).send({done: false, message: "Error in take a charge"})
  }
}

export const getAllCharges  = async (req, res) => {
  try {
    stripe.charges.list(
      { limit: 10 }, (err, charges) => {
        if(err){
          return res.status(err.statusCode || 500).send(err)
        }
        return res.send({done: true, charges})
      }
    )
  } catch (err) {
    res.status(500).send({done: false, message: "Error in getting charges list"})
  }
}

export const paymentRefund  = async (req, res) => {
  try {
    const {user} = req
    const {_id, invoiceId, receipt_url, payableAmount, refundAmount, meta, appId, tenantId, reason, reasonDescription} = req.body
    const transaction = await Transactions.findOne({invoiceId: _id, object: "charge"})

    if(!transaction){
      return res.status(500).send({done: false, message: "Invoice transaction not found"})
    }
    const payload = {
      charge: transaction.paymentId,
      amount: Number(refundAmount)*100,
      metadata: {
        ...transaction.meta,
        invoiceNumber: invoiceId,
        payableAmount,
        appId,
        tenantId,
        invoiceId: _id,
        customerId: transaction.customerId,
        receipt_url: transaction.receipt_url,
        reasonDescription
      }
    }
    if(reason){
      payload.reason = reason // duplicate, fraudulent, and requested_by_customer
    }

    console.log("===================================")
    console.log(receipt_url, meta.sendEmailOn)
    console.log("===================================")

    stripe.refunds.create(payload, async (err, refund) => {
      if(err){
        console.log("Errrrrr: ", err.message)
        return res.status(err.statusCode || 500).send({done: false, message: err.message || "Error from payment refund"})
      }
      await InvoiceDetails.updateOne({_id}, { $set: { status: "Refunded" } })

      const message = `The requested amount has now been refunded. <br/> Here are the details of this transaction for your reference <br/> <p>Invoice Number: ${invoiceId}</p> <p>Amount Refunded: $${Number(refundAmount) || 0}</p> <a href=${receipt_url}>Get receipt</a>`

      const emailconfig = await sendEmailHelper({
        to:[meta.sendEmailOn],
        bodytemplate: message,
        subject: "Refund Successful",
        replyto:[user.userEmailId]
      })
      // await sendAWSEmailWithAttachment({ ...emailconfig, attachments: [{ filename: "Refund Receipt", attachmentUrl: receipt_url } ] })
      await sendAWSEmail(emailconfig)
      return res.send({done: true, refund})
    })
  } catch (err) {
    res.status(500).send({done: false, message: err.message || "Getting error on payment refund"})
  }
}

export const getAllEvents  = async (req, res) => {
  const {types, pageSize, starting_after, ending_before, created} = req.body
  try {
    stripe.events.list(
      {limit: pageSize, types, starting_after, ending_before, "include[]": "total_count", created }, (err, events) => {
        if(err){
          console.log("getAllEvents error =>>",err)
          return res.status(err.statusCode || 500).send({done: false, message: "Error in getting payment event list"})
        }
        const data = (events.data || []).map(item => ({
          "id": item.id,
          "date": moment.unix(item && item.created).format("MM/DD/YYYY hh:mm A") || "-",
          "email": ((item.data && item.data.object) && (item.data.object.email || item.data.object.receipt_email)) || "-",
          "name": (item.data && item.data.object && ((item.data.object.metadata && item.data.object.metadata.name) || item.data.object.name)) || "-",
          "description": (item.data && item.data.object && item.data.object.description) || "-",
          "amount": (item.data && item.data.object && item.data.object.amount >= 0 ? (item.data && item.data.object && item.data.object.amount) : "-"),
          "amountRefunded": (item.data && item.data.object && item.data.object.amount_refunded >= 0  ? (item.data && item.data.object && item.data.object.amount_refunded) : "-"),
          "receiptUrl": (item.data && item.data.object && item.data.object.receipt_url) || "",
        }))
        return res.send({done: true, total: events.total_count, data})
      }
    )
  } catch (err) {
    res.status(500).send({done: false, message: "Error in getting payment events list"})
  }
}

export const fetchPaymentRefundList = async (req, res) => {
  const {charge} = req.query
  try {
    stripe.refunds.list(
      { charge },
      (err, refunds) => {
        if(err){
          console.log("Stripe error: ", err)
          return res.status(err.statusCode || 500).send({done: false, message: "Error in getting payment event list"})
        }
        res.send({done: true, data: (refunds && refunds.data) || []})
      })
  } catch (err) {
    console.log(err)
    res.status(500).send({done: false, message: "Error in getting payment refund list"})
  }
}