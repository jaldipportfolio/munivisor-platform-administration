import express from "express"
import {
  createCustomer,
  takeCharge,
  getAllCharges,
  paymentRefund,
  getAllEvents,
  fetchPaymentRefundList
} from "./payment.controller"

export const paymentRouter = express.Router()

paymentRouter.route("/createcustomer")
  .post(createCustomer)


paymentRouter.route("/charge")
  .post(takeCharge)

paymentRouter.route("/charges")
  .get(getAllCharges)

paymentRouter.route("/refunds")
  .get(fetchPaymentRefundList)
  .post(paymentRefund)

paymentRouter.route("/events")
  .post(getAllEvents)