import express from "express"
import fs from "fs"
import path from "path"
import cors from "cors"
import setupMiddware from "./middlewares"
import { authRouter } from "./authorization"
import { serviceRouter } from "./service"
import { restRouter, apiErrorHandler } from "./api"
import { connect } from "./db"
import logger from "./modules/logger"
import {requireAuth} from "./authorization/auth.middleware"
import {createAdminRouter} from "./api/createadmin"
import createAdminAuth from "./helpers/createAdminAuth"


console.log("THIS IS THE ENVIRONMENT", process.env.PLATFORM_NODE_ENV)
// Declare an app from express
const app = express()

setupMiddware(app)

require("dotenv").config()

logger.debug("Debugging info")
logger.verbose("Verbose info")
logger.info("Hello world")
logger.warn("Warning message")
logger.error("Error info")

connect()
const env = process.env.PLATFORM_NODE_ENV || "development"

app.use(cors())

app.use("/auth", (req, res, next) => {
  // res.header("Access-Control-Allow-Origin", process.env.PLATFORM_FRONTEND_URL ) // Access-Control-Allow-Origin
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS")
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Authorization, Accept, Access-Control-Al" +
      "low-Methods"
  )
  res.header("X-Frame-Options", "deny")
  res.header("X-Content-Type-Options", "nosniff")

  next()
})
app.use("/auth", authRouter)
app.use("/service", serviceRouter)
app.use("/createadmin", [createAdminAuth] , createAdminRouter)
app.use("/api", (req, res, next) => {
  // res.header("Access-Control-Allow-Origin", process.env.PLATFORM_FRONTEND_URL)
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS")
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Authorization, Accept, Access-Control-Al" +
      "low-Methods"
  )
  res.header("X-Frame-Options", "deny")
  res.header("X-Content-Type-Options", "nosniff")
  next()
})
app.use("/api", requireAuth, restRouter)
app.use(apiErrorHandler)

const appRoot = fs.realpathSync(process.cwd())

if (env === "production") {
  console.log("The root of the application is", appRoot)
  const clientPath = path.resolve(appRoot, "build/client")
  const indexPath = path.join(clientPath, "index.html")
  console.log(clientPath)
  console.log(indexPath)

  app.use(express.static(clientPath))
  app.get("/*", (req, res) => {
    res.sendFile(indexPath)
  })
}

export default app
