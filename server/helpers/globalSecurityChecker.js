import isEmpty from "lodash/isEmpty"

const isObject = (a)=> (!!a) && (a.constructor === Object)
const isArray = (a)=> (!!a) && (a.constructor === Array)

const validRegexPatterns = {
  alpha:/^[a-z\d\-_\$,//&\.\s]+$/i,
  email:/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  web:/[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi,
  url:/https?:\/\/(www\.)?[-a-zA-Z0-9@:%.-_\+~#=]{1,256}(\.[a-z]{1,4})?\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gi,
  num:/^\d+\.\d{0,7}$/,
  num1:/(\d{1,3})(,\d{1,3})*(\.\d{1,})?/,
  bool:/^true|false$/,
  dateform1:/(\d\d\d\d)(-|\\|\/)(\d\d)(-|\\|\/)(\d\d)(T|\s)(\d\d):(\d\d):(\d){1,10}?(\.\d{0,7}Z)/,
  dateform2:(/(\d\d\d\d)-(\d\d)-(\d\d)T(\d\d):(\d\d):(\d\d)\+(\d\d):(\d\d)/),
  dateform3:(/(\d\d\d\d)-(\d\d)-(\d\d)T(\d\d):(\d\d):(\d\d)\+(\d\d):(\d\d)/),
  dateform4:(/(Sun|Mon|Tue|Wed|Thu|Fri|Sat),\s(\d\d)\s(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s(\d\d\d\d)/gi),
  ipaddress:/^(([1-9]?\d|1\d\d|2[0-5][0-5]|2[0-4]\d)\.){3}([1-9]?\d|1\d\d|2[0-5][0-5]|2[0-4]\d)$/g,
  phoneFormat:/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im
}

const invalidRegexPatterns = {
  doextensions:/\.(js|exe|vbs|bat|jar|war)/gi
  // doextensions:/\.(?!(js|exe|vbs|bat|jar|war))(?:[\?#]|$)/i
}

const validRegexPatternsForKeys = {
  alpha:/^[a-z\d\-_]+$/i,
}

const validKeyValues = (v) => {
  
  const validOptions = Object.values(validRegexPatternsForKeys).reduce( (acc,regEx) => {
    const regex = new RegExp(regEx)
    const res = regex.test(v)
    return acc || res
  }, false )

  return validOptions

}


const validObjectValues = (v) => {

  const validOptions = Object.values(validRegexPatterns).reduce( (acc,regEx,ind) => {
    const regex = new RegExp(regEx)
    const res = regex.test(v)
    // console.log(`VALID : ${v} - ${Object.keys(validRegexPatterns)[ind]} : ${res}`)
    return acc || res
  }, false )

  const invalidOptions = Object.values(invalidRegexPatterns).reduce( (acc,regEx,ind) => {
    const regex = new RegExp(regEx)
    const res = ! regex.test(v)
    // console.log(`INVALID : ${v} - ${Object.keys(invalidRegexPatterns)[ind]} : ${res}`)
    return acc && res
  }, true )
  if(! (validOptions && invalidOptions) ) {
    console.log(`${v} - Final Return value => ${validOptions && invalidOptions}`)
  }
  return validOptions && invalidOptions
}

const requestSecurityParserObjectValues = ( reqObject ) => {
  const keys = Object.keys(reqObject)
  console.log("THE OBJECT BEING EVALUATED IS",reqObject )
  const flagForAllValues =  keys.reduce( (finalAcc,k) => {
    if(!isEmpty(reqObject[k]) && isObject(reqObject[k])) {
      const objVal = reqObject[k]
      console.log("ENTERED OBJECT ROUTINE", objVal)
      return finalAcc && requestSecurityParserObjectValues(objVal)
    }
    else if(!isEmpty(reqObject[k]) && isArray(reqObject[k])) {
      const arrayVal = reqObject[k]
      console.log("ENTERED OBJECT ROUTINE", arrayVal)
      return finalAcc && arrayVal.reduce ( (accArr, arrElem) => {
        if( !isEmpty(arrElem) && isObject(arrElem) ) {
          return accArr && requestSecurityParserObjectValues(arrElem)
        }
        else if (isEmpty(arrElem)) {
          return accArr && true
        }
        return accArr && validObjectValues(arrElem)
      },true)
    }
    else if (isEmpty(reqObject[k])) {
      return finalAcc && true
    }
    console.log({previous:finalAcc,new:finalAcc && validObjectValues(reqObject[k])})
    return finalAcc && validObjectValues(reqObject[k])
  },true)

  return flagForAllValues

}

const requestParserObjectKeys = (reqObject) => {
  const keys = Object.keys(reqObject)
  const flagForAllKeys = keys.reduce( (finalAcc,k) => {
    if(isObject(reqObject[k])) {
      return finalAcc && requestParserObjectKeys(reqObject[k])
    }
    else if(!isEmpty(reqObject[k]) && isArray(reqObject[k])) {
      const arrayVal = reqObject[k]
      console.log("ENTERED OBJECT ROUTINE", arrayVal)

      return finalAcc && arrayVal.reduce ( (accArr, arrElem) => {
        if( !isEmpty(arrElem) && isObject(arrElem) ) {
          return accArr && requestParserObjectKeys(arrElem)
        }
        else if (isEmpty(arrElem)) {
          return accArr && true
        }
        return accArr && validKeyValues(arrElem)
      },true)
    }
    return finalAcc && validKeyValues(k)
  },true)

  return flagForAllKeys
}

export const requestSecurityParser = (reqObject) => {
  const v1 = requestSecurityParserObjectValues(reqObject)
  console.log("The return value from the Value Parser is - ", v1)
  const v2 = requestParserObjectKeys(reqObject)
  console.log("The return value from the key Parser is - ", v2)
  return (v1 || false) && (v2 || false)
}


