const fs = require("fs")

export const readJson = (path, cb) => {
  fs.readFile(require.resolve(path), (err, data) => {
    if (err) {
      console.log("There was an error processing the JSON file. The file might not exist or the path is incorrect")
      cb(err)
    }
    else {
      console.log("There are no errors the data has been parsed. sending the output back in the call back")
      cb(null, JSON.parse(data))

    }

  })
}
