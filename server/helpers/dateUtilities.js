import moment from "moment"

export const getDateString = (field, range) => {
 
  const dateRanges = {
    MTD: {
      $gte:new Date(moment.utc().startOf("month").toDate()),
      $lte:new Date(moment.utc().toDate())
    },
    YTD: {
      $gte:new Date(moment.utc().startOf("year").toDate()),
      $lte:new Date(moment.utc().toDate())
    },
    QTD: {
      $gte:new Date(moment.utc().startOf("quarter").toDate()),
      $lte:new Date(moment.utc().toDate())
    },
    PREVYEAR: {
      $gte: new Date(moment.utc().startOf("year").subtract(1, "year")),
      $lte:new Date(moment.utc().toDate())
    },
    PREV5YEARS: {
      $gte: new Date(moment.utc().startOf("year").subtract(5, "year")),
      $lte:new Date(moment.utc().toDate())
    },
    PREV10YEARS: {
      $gte: new Date(moment.utc().startOf("year").subtract(10, "year")),
      $lte:new Date(moment.utc().toDate())
    },
    CURRENTMONTH: {
      $gte: new Date(moment.utc().startOf("month").toDate()),
      $lte:new Date(moment.utc().endOf("month").toDate())
    },
    CURRENTWEEK: {
      $gte: new Date(moment.utc().startOf("week").toDate()),
      $lte:new Date(moment.utc().endOf("week").toDate())
    },
    TODAY: {
      $gte: new Date(moment.utc().startOf("day").toDate()),
      $lt:new Date(moment.utc().endOf("day").toDate())
    }
  }
  
  if ( Object.keys(dateRanges).includes(range)) {
    return { [field]:{...dateRanges[range]}}
  }
  return { [field]: {...dateRanges.YTD}}
}
