const createAdminAuth = (req, res, next) => {
  const { authorization } = req.headers
  let valid = false

  if (authorization) {
    const decryptedCode = Buffer.from(authorization || "", "base64").toString()
    if (decryptedCode === "generateUsersAndApplicationForPlatformAdmin") {
      valid = true
    }
  }

  if (!valid) {
    return res.status(403).json({ error: "No credentials sent!" })
  }
  next()
}

export default createAdminAuth
