import {
  PlatformUser
} from "../api/platformUser/users.model"


export const getMappedLoggedInUser = async (email) => {
  let returnObject
  const regexemail = new RegExp(email,"i")

  try{
    returnObject = await PlatformUser.aggregate([{
      $match: {
        "userLoginCredentials.userEmailId": {$regex:regexemail,$options:"i"}
      }
    },
    {
      $lookup: {
        from: "applications",
        let: {"appid": "$applications"},
        pipeline: [
          { $match: { $expr: { $in: ["$_id", "$$appid"]}}}
        ],
        as: "applications"
      }
    },
    {
      $project: {
        userId:"$_id",
        userFirstName:1,
        userLastName:1,
        userEntitlement:1,
        loginEmailId:{$toLower:"$userLoginCredentials.userEmailId"},
        applications:1,
        byDefaultProject:1,
        notifications: 1,
        settings: 1,
      }
    }
    ])
  }
  catch(err) {
    console.log("There is an error")
  }
  return returnObject[0]
}

export const getUserDetails = async (email) => {
  return await getMappedLoggedInUser(email)
}
