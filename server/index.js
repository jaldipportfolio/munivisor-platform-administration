import http from "http"

import app from "./server"
import {
  compunteBillingFromPlatform
} from "./api/billing/billing.controller"
import { PlatFormAdminSnapShots } from "./service/service.controller"

const {CronJob} = require("cron")

require("dotenv").config()

let server = null
const currentApp = app

if (process.env.PLATFORM_NODE_ENV === "development") {
  server = http.createServer(app)
} else {
  console.log("This is the production environment")
  server = app
}

new CronJob("0 0 */2 * * *", () => {
  // new CronJob("0 59 9 28 * *", () => {
  compunteBillingFromPlatform()
}, null, true, process.env.PLATFORM_TIMEZONE)

new CronJob("0 0 */1 * * *", () => {
// new CronJob("00 45 10 * * *", () => {
  PlatFormAdminSnapShots()
}, null, true, process.env.PLATFORM_TIMEZONE)

const PORT = process.env.PLATFORM_PORT || 8000
server.listen(PORT, async () => {
  try {
    console.log(`Server listening on port ${PORT}`)
  } catch (err) {
    console.log("Server init error", err)
  }
})


