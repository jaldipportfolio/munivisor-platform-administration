import bodyParser from "body-parser"
import morgan from "morgan"
import helmet       from "helmet"
import logger from "./../modules/logger"
import {requireAuth} from "./../authorization/auth.middleware"

require("dotenv").config()

const requestIp = require("request-ip")

const requireLogin = (req,res, next) => {
  if( !req.user) {
    return res.status(401).send({ error: "Login is necesssary"})
  }
  next()
}

let securityConfigurationEnabled 

if ( process.env.CSP_REPORTONLY && process.env.CSP_REPORTONLY === "Yes" ) {
  securityConfigurationEnabled = true
}
else if ( process.env.CSP_REPORTONLY && process.env.CSP_REPORTONLY === "No" ) {
  securityConfigurationEnabled = false
} else {
  securityConfigurationEnabled = true
}

console.log("REPORT ONLY PROCESS ENV", securityConfigurationEnabled )


const setGlobalMiddleware = (app) => {
  app.use(bodyParser.urlencoded({limit: "50mb", extended: true}))
  app.use(bodyParser.json({limit: "50mb"}))
  app.use(bodyParser.text({ type: "application/x-ndjson" }))
  app.use(morgan("combined", { stream: logger.stream }))
  app.use(requestIp.mw())
  app.use(helmet())
  app.use(helmet.xssFilter())
  // app.use(helmet.noCache())
  app.use(helmet.contentSecurityPolicy({
    directives: {
      defaultSrc: ["'self'", "https://*.otaras.com","https://*.munivisor.com", "http://localhost" ],
      objectSrc: ["'none'"],
      scriptSrc: ["'self'", "http://code.jquery.com", "http://*", "'unsafe-inline'", "'unsafe-eval'"],
      styleSrc: ["'self'", "https://use.fontawesome.com", "https://cdnjs.cloudflare.com","https://cdn.ckeditor.com", "'unsafe-inline'", "'unsafe-eval'"],
      imgSrc: ["'self'", "https://lipis.github.io/","https://cdn.ckeditor.com", "https://bulma.io","https://maps.googleapis.com","https://maps.gstatic.com/", "data:"],
      fontSrc: ["'self'", "https://fonts.gstatic.com", "https://use.fontawesome.com","https://cdnjs.cloudflare.com","data:"],
      connectSrc: ["'self'", `${process.env.PLATFORM_S3BUCKET}.s3.amazonaws.com`, "https://ipv4.icanhazip.com/"],
      reportUri: "/api/security/cspviolationreport",
      upgradeInsecureRequests: true
    },
    reportOnly: securityConfigurationEnabled
  }))
  app.disable("x-powered-by")

  const sixtyDaysInSeconds = 5184000
  app.use(helmet.hsts({
    maxAge: sixtyDaysInSeconds
  }))

  const MAX_CONTENT_LENGTH_ACCEPTED = 9999
  // app.use(contentLength.validateMax({max: MAX_CONTENT_LENGTH_ACCEPTED, status: 400, message: `The content length exceeded - ${MAX_CONTENT_LENGTH_ACCEPTED}`})) // max size accepted for the content-length
  // if ( ! securityConfigurationEnabled ) {
  //   app.use(checkInputDataSecurity())
  // }

  app.all(["/search"], requireAuth)

}

export default setGlobalMiddleware
export * from "./requestRateLimiter"
