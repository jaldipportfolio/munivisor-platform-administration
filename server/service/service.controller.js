import axios from "axios"
import ObjectID from "bson-objectid"
import {SnapShots} from "./snapshots.model"
import {InvoiceDetails} from "../api/billing/billing.model"
import {Applications} from "../api/models/applications.model"
import {services} from "../api/AllServices"
import { Docs } from "../api/docs/docs.model"
import {InvoiceLogs, Settings, TenantConfig} from "../api/models"
import { Transactions } from "../api/transactions/transactions.model"
import {sendAWSEmail, sendEmailHelper} from "../api/billing/helpers"
import AWS from "aws-sdk"
import config from "../config"
import fileSize from "file-size"


const serviceFrom = "common"
const stripe = require("stripe")(process.env.PLATFORM_STRIPE_SECRET_KEY)
const s3BucketName = config.keys.s3.bucketName


export const authMiddleware = async (req, res, next) => {
  try {
    const token = req.headers.authorization
    if (token !== process.env.PLATFORM_CALL_FROM_MUNI_CLIENT_TOKEN) {
      return res.status(500).send({
        error: "Unauthorized"
      })
    }
    next()
  } catch (err) {
    return res.status(403).send({ error: "Unauthorized" })
  }
}

export const s3 = new AWS.S3({
  region : config.keys.s3.region,
  bucket: s3BucketName,
  accessKeyId: config.keys.s3.accessKeyId,
  secretAccessKey: config.keys.s3.secretAccessKey
})

export const PlatFormAdminSnapShots = async () => {

  try {

    console.log("Snap Shots in Progress..")
    const applications = await Applications.find({})
    for (const i in applications) {
      const app = applications[i]
      const apiRes = await axios.get(`${app.url}${services[serviceFrom].getSnapShots}/${app && app._id}`, {headers: {Authorization: app.token}})

      await SnapShots.insertMany(apiRes.data && apiRes.data.snapShots)
      console.log("Snap Shots Added Successfully")
    }

  } catch (err) {
    console.log("Error")
  }
}

export const fetchBillingInvoices = async (req, res) => {
  const { entityId } = req.body
  try {

    if(entityId){
      const invoiceList = await InvoiceDetails.find({tenantId: ObjectID(entityId), status: { $nin: ["Open", "Rejected"] }})
      res.status(200).send({done:true, invoiceList})
    } else {
      return res.send({done: false, message: "Please pass Entity Id"})
    }

  } catch (err) {
    console.log("Error", err)
    res.status(422).send({error: "Error in compunte billing"})
  }
}

export const makePayment = async (req, res) => {

  const { token, metadata, receipt_email, amount, address, tenantId, invoiceId, invoiceNumber, appId } = req.body
  const { addressLine1, addressLine2, city, state, country, zipCode, officePhone } = address
  const phone = officePhone && officePhone.length && officePhone[0] && officePhone[0].phoneNumber || ""

  try {

    let existingCustomerId = ""
    let createdCustomer = {}

    if(token && token.id){

      const existingCustomer = await stripe.customers.list({email: receipt_email})

      if(existingCustomer && existingCustomer.data && existingCustomer.data.length){

        existingCustomerId = existingCustomer.data[0] && existingCustomer.data[0].id || ""

      } else {

        const payload = {
          address: {
            line1: addressLine1 || "", // required
            line2: addressLine2 || "", // optional
            city: city || "", // optional
            state: state || "", // optional,
            country: country || "", // optional
            postal_code: zipCode && zipCode.zip1 || "", // optional
          },
          // account_balance: 1500,
          phone,
          name: metadata.name,
          email: receipt_email,
          source: token.id,
          description: "Otaras Client"
        }
        createdCustomer = await stripe.customers.create(payload)

      }

      const customer = existingCustomerId || createdCustomer && createdCustomer.id

      await stripe.charges.create({
        receipt_email,
        metadata,
        amount,
        description: "Web Development MuniVisor",
        currency: "usd",
        customer,
        // source: token.id,
      }, async (err, response)  => {

        if(err){

          await InvoiceLogs.create({
            invoiceId,
            tenantId,
            logMeta: {
              log: `Payment Is Failed ${err.message || ""}`,
              userName: metadata && metadata.name || ""},
            appId,
          })

          return res.send({done: false, message: err.message, error: err })
        }

        const { customer, id, payment_method, payment_method_details, receipt_url, balance_transaction, status, object, source, amount } = response

        await Transactions.create({
          customerId: customer,
          paymentId: id,
          invoiceNumber,
          appId,
          invoiceId,
          tenantId,
          payment_method,
          payment_method_details,
          receipt_url,
          balance_transaction,
          status,
          object,
          metadata
        })

        await InvoiceDetails.updateOne({_id: invoiceId}, { $set: { status: "Paid", receipt_url } })

        const emailconfig = await sendEmailHelper({
          to:[receipt_email],
          bodytemplate: `Your payment has been received. Here are the details of this transaction for your reference <br/> <p>Invoice Number: ${invoiceId}</p> <p>Amount Paid: $${(Number(amount) || 0) / 100}</p><a href=${receipt_url}>Get receipt</a>`,
          subject: "Payment successful",
          replyto:[process.env.PLATFORM_REPLYTOEMAIL]
        })
        await sendAWSEmail( emailconfig )

        const { brand, last4 } = source

        await InvoiceLogs.create({
          invoiceId,
          tenantId,
          logMeta: {
            log: `Amount $${amount / 100} Payment By ${brand || ""} ${source && source.object || ""} and ${brand || ""} ${source && source.object || ""} last 4 Digit Is ${last4 || ""}`,
            userName: metadata && metadata.name || ""},
          appId,
        })

        return res.send({done: true, response})

      })

    } else {

      await InvoiceLogs.create({
        invoiceId,
        tenantId,
        logMeta: {
          log: "Payment Is Failed Because Of Invalid Token Id",
          userName: metadata && metadata.name || ""},
        appId,
      })

      return res.send({done: false, message: "Please pass token id"})
    }

  } catch (err) {
    console.log("Error", err)
    return res.send({done: false, message: "something went wrong in make payment", error: "something went wrong in make payment"})
  }
}

export const fetchPlatFormDocs = async (req, res) => {

  const { docId } = req.params
  try {

    if(docId){
      const doc = await Docs.findOne({_id: ObjectID(docId)})
      return res.status(200).send({done:true, doc})
    }
    return res.send({done: false, message: "Please pass Doc Id"})


  } catch (err) {
    console.log("Error", err)
    res.status(422).send({error: "Error in fetch docs"})
  }
}

export const getTenantConfigForMuni = async (req, res) => {
  
  try{
    const _id = req.params.id
   
    let tenant = await TenantConfig.findOne({tenantId: _id})
    if(!tenant ){
      tenant = await Settings.findOne({}).select("billing")
    }
    res.status(200).send(tenant || {})
  } catch (err) {
    console.log("Error", err)
    res.status(422).send({error: "Error in getting tenant details"})
  }
}
