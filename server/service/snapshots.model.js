import mongoose, {Schema} from "mongoose"
import timestamps from "mongoose-timestamp"


const SnapShotsSchema = Schema({
  appId: { type: Schema.Types.ObjectId },
  tenantId: { type: Schema.Types.ObjectId },
  firmName: String,
  dbStorage: String,
  transactions: Number,
  client: Number,
  prospect: Number,
  thirdParty: Number,
  users: Object,
  transactionWise: Object,
  elasticStorage: Object,
  storageSize: String,
  date: Date
}).plugin(timestamps)

export const SnapShots =  mongoose.model("snapshots", SnapShotsSchema)


