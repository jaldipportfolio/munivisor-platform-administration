import express from "express"
import {
  authMiddleware,
  fetchBillingInvoices,
  makePayment,
  fetchPlatFormDocs,
  getTenantConfigForMuni
} from "./service.controller"

export const serviceRouter = express.Router()

serviceRouter.route("/invoice")
  .post(authMiddleware, fetchBillingInvoices)

serviceRouter.route("/payment")
  .post(authMiddleware, makePayment)

serviceRouter.route("/platformdocs/:docId")
  .get(authMiddleware, fetchPlatFormDocs)

serviceRouter.route("/tenantconfig/:id")
  .get(authMiddleware, getTenantConfigForMuni)