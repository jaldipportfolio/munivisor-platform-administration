#!/bin/bash

cd /opt/codedeploy-agent/deployment-root/$DEPLOYMENT_GROUP_ID/$DEPLOYMENT_ID/deployment-archive
find . -type f -not \( -name 'final.tar' -or -name 'appspec.yml' -or -name 'start_application.sh' -or -name 'stop_application.sh' \) -delete
find . -type l -not \( -name 'final.tar' -or -name 'appspec.yml' -or -name 'start_application.sh' -or -name 'stop_application.sh' \) -delete
find . -type d -not \( -name 'final.tar' -or -name 'appspec.yml' -or -name 'start_application.sh' -or -name 'stop_application.sh' \) -delete
