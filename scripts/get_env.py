#!/usr/bin/python

import os
import subprocess
import shlex
import json
import shutil

AWS_REGION = "us-east-1"

ENV_MAP = {"development":"DEV",
           "production":"PROD",
           "beta":"BETA",
           "STAG":"STAG"}

get_param_command_str = "aws ssm describe-parameters --filters \"Key=Name,Values="
command_str = "aws ssm get-parameters --with-decryption --names"
add_region = " --region {0}".format(AWS_REGION)
SCRIPT_DIR = os.path.abspath(os.path.dirname(__file__) + './..')

def run_command(cmd):
    """given shell command, returns communication tuple of stdout and stderr"""
    args = shlex.split(cmd)
    return subprocess.Popen(args,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE,
                            stdin=subprocess.PIPE).communicate()

def get_env():
    env_name = os.environ.get('ENV_NAME','development')
    return env_name

def get_parameter_list(environment):
    get_parameters_command = get_param_command_str + environment + "\"" + add_region
    output = run_command(get_parameters_command)
    parameter_list = []
    if output[1] != '':
        print("Error encountered while getting list of parameters")
        print(output[1])
        exit(1)
    else:
        json_string = output[0].replace('\n','')
        json_dict = json.loads(json_string)
        print("List of Parameters received from Parameter Store, preparing parameter list.")
        for parameter in json_dict["Parameters"]:
            parameter_list.append(parameter["Name"])
    return parameter_list

def process_parameters(parameter_list, environment, command_str, add_region):
    env_len = len(environment) + 1
    param_str=''
    for param in parameter_list:
        param_str += " " + param

    command_str = command_str + param_str + add_region
    output = run_command(command_str)
    if output[1] != '':
        print("Error encountered while getting values from Parameter Store")
        print(output[1])
        exit(1)
    else:
        json_string = output[0].replace('\n','')
        json_dict = json.loads(json_string)
        if len(json_dict["InvalidParameters"]) != 0:
            print("Invalid parameters encoutered while getting values from Parameter Store")
            print(json_dict["InvalidParameters"])
            exit(1)
        else:
            print("Parameters received from Parameter Store, populating .env file.")
            fileH = open('{0}/.env'.format(SCRIPT_DIR), 'r+')
            file_content = fileH.read()
            file_lines = file_content.splitlines()
            new_lines = []
            for parameter in json_dict["Parameters"]:
                if parameter["Name"][0] == '/':
                    parameter_name = parameter["Name"][1:]
                else:
                    parameter_name = parameter["Name"]
                found = False
                for idx in range(0, len(file_lines)):
                    if parameter_name[env_len:] == file_lines[idx].split("=")[0]:
                        print('Replacing value of {0}'.format(parameter_name))
                        key_value = file_lines[idx].split('=')
                        key_value[1] = parameter["Value"]
                        file_lines[idx] = '='.join(key_value)
                        found = True
                if not found:
                    new_lines.append('{0}={1}'.format(parameter_name[env_len:],parameter["Value"]))

            file_lines += new_lines
            out = "\n".join(file_lines)

            out = out + '\n'
            fileH.seek(0)
            fileH.write(out)
            fileH.truncate()
            fileH.close()


try:
    environment = get_env()
    if environment == '':
        print("Unable to find environment in the .env file")
        exit(1)

    if environment in ENV_MAP.keys():
        environment = ENV_MAP[environment]

    #Create file, if it does not exist.
    fileH = open('{0}/.env'.format(SCRIPT_DIR), 'a+')
    fileH.write("")
    fileH.close()

    PARAMETER_LIST = get_parameter_list(environment)
    #print(PARAMETER_LIST)

    if len(PARAMETER_LIST) <= 10:
        process_parameters(PARAMETER_LIST, environment, command_str, add_region)
    else:
        start_idx = 0
        end_idx = 10
        max_idx = len(PARAMETER_LIST)
        while True:
            process_parameters(PARAMETER_LIST[start_idx:end_idx], environment, command_str, add_region)
            if end_idx == max_idx:
                break
            else:
                start_idx += 10
                end_idx += 10
                if end_idx > max_idx:
                    end_idx = max_idx

except Exception as e:
    print("Failed to set .env file parameter.")
    print(e)

