#!/bin/bash

cd /opt/codedeploy-agent/deployment-root/$DEPLOYMENT_GROUP_ID/$DEPLOYMENT_ID/deployment-archive
mkdir -p /var/www
rm -rf /var/www/*
tar -xzf ./final.tar -C /var/www/
chown -R ubuntu:ubuntu /var/www
