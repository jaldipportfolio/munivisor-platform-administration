#!/bin/bash

cd /var/www
pm2 jlist | wc -w
if [ $? -eq 1 ];then
        echo "Nothing to stop"
        exit 0
    else
        pm2 stop 'yarn start'
fi